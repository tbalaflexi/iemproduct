﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DBHelper;
using DataAccessHandler.Models.EOW;
using Shared.Data;
namespace EOW.Data
{

    public class Claim_Data
    {
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        EOWCommon cmObjEOW = new EOWCommon();
        CommonMethods cmnObj = new CommonMethods();
        #region CRUD Methods for Claim Grid.
        public DataTable Get_BatchDetails(Int64 BatchGid, Int64 ECFGid)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Batch_Gid, BatchGid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Gid, ECFGid, DbType.Int64));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.sp_BatchDetails, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion

        #region Common methods fro Dropdowns binndings.
        public DataTable Get_DropDown_List(string MasterName, Int64 Master_Gid, string EmpName)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Claim_MasterName, MasterName, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Claim_MasterGid, Master_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Claim_Emp_Name, EmpName, DbType.String));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.sp_Claim_Getdropdownlist, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion

        #region CRUD Methods for Claim Grid.
        public DataTable Read_Claim(string GirdName, Int64 BatchGid, Int64 EcfGid)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_GirdName, GirdName, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Batch_Gid, BatchGid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Gid, EcfGid, DbType.Int64));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.sp_Claim_GridList, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable Save_Claim(Claim_Model ObjModel)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Action, cmObjEOW.Create, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Type_Gid, ObjModel.HDR_Type_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_SubType_Gid, ObjModel.HDR_SubType_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Raiser_Gid, ObjModel.HDR_Raiser_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Batch_Gid, ObjModel.HDR_Batch_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Batch_No, ObjModel.HDR_Batch_No, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Batch_Amount, ObjModel.HDR_Batch_Amount, DbType.Double));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Gid, ObjModel.CLM_Ecf_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_No, ObjModel.CLM_Ecf_No, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Createmode, ObjModel.CLM_Create_Mode, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_EcfRaiser_Gid, ObjModel.CLM_Emp_Gid, DbType.Int64));
                if (ObjModel.CLM_Claim_Date != null)
                {
                    if (ObjModel.CLM_Claim_Date.Length > 19)
                    {
                        cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Date, DateTime.Parse(ObjModel.CLM_Claim_Date.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Date, DateTime.ParseExact(ObjModel.CLM_Claim_Date.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Date, ObjModel.CLM_Claim_Date, DbType.String));
                }
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Amount, ObjModel.CLM_Claim_Amount, DbType.Double));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Description, ObjModel.CLM_Claim_Description, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_PersonCount, ObjModel.CLM_Person_Count, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.LoggedUser_Gid, ObjModel.Logged_UserGid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_EmployeeIds, ObjModel.HDR_EmployeeIds, DbType.String));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.sp_Claim_DML, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable Update_Claim(Claim_Model ObjModel)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Action, cmObjEOW.Update, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Type_Gid, ObjModel.HDR_Type_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_SubType_Gid, ObjModel.HDR_SubType_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Raiser_Gid, ObjModel.HDR_Raiser_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Batch_Gid, ObjModel.HDR_Batch_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Batch_No, ObjModel.HDR_Batch_No, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Batch_Amount, ObjModel.HDR_Batch_Amount, DbType.Double));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Gid, ObjModel.CLM_Ecf_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_No, ObjModel.CLM_Ecf_No, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Createmode, ObjModel.CLM_Create_Mode, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_EcfRaiser_Gid, ObjModel.CLM_Emp_Gid, DbType.Int64));
                if (ObjModel.CLM_Claim_Date != null)
                {
                    if (ObjModel.CLM_Claim_Date.Length > 19)
                    {
                        cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Date, DateTime.Parse(ObjModel.CLM_Claim_Date.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Date, DateTime.ParseExact(ObjModel.CLM_Claim_Date.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Date, ObjModel.CLM_Claim_Date, DbType.String));
                }
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Amount, ObjModel.CLM_Claim_Amount, DbType.Double));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Description, ObjModel.CLM_Claim_Description, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_PersonCount, ObjModel.CLM_Person_Count, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.LoggedUser_Gid, ObjModel.Logged_UserGid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_EmployeeIds, ObjModel.HDR_EmployeeIds, DbType.String));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.sp_Claim_DML, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable Delete_ClaimDetails(Claim_Model ObjModel)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_GirdName, ObjModel.Common_Gridname, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Batch_Gid, ObjModel.HDR_Batch_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_GridGid, ObjModel.Common_Gridgid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.LoggedUser_Gid, ObjModel.Logged_UserGid, DbType.Int64));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.sp_Deleteclaimdetails, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion

        #region CRUD Methods for Expense Grid.
        public DataTable Save_Expense(Claim_Model objModel)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Action, cmObjEOW.Create, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_invicegid, objModel.Exp_gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Gid, objModel.Exp_ecfgid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_ivoiceno, objModel.Exp_invoiceno, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_taxtype, objModel.Exp_taxtype, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_naturegid, objModel.Exp_natureexpgid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_catggid, objModel.Exp_catggid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_subcatggid, objModel.Exp_subcatggid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_glno, objModel.Exp_glcode, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_debitamount, objModel.Exp_debitamount, DbType.Double));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_hsngid, objModel.Exp_hsngid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.LoggedUser_Gid, objModel.Logged_UserGid, DbType.Int64));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.sp_Expense_DML, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        

        public DataTable Update_Expense(Claim_Model objModel)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Action, cmObjEOW.Update, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_invicegid, objModel.Exp_gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Gid, objModel.Exp_ecfgid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_ivoiceno, objModel.Exp_invoiceno, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_taxtype, objModel.Exp_taxtype, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_naturegid, objModel.Exp_natureexpgid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_catggid, objModel.Exp_catggid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_subcatggid, objModel.Exp_subcatggid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_glno, objModel.Exp_glcode, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_debitamount, objModel.Exp_debitamount, DbType.Double));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_hsngid, objModel.Exp_hsngid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.LoggedUser_Gid, objModel.Logged_UserGid, DbType.Int64));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.sp_Expense_DML, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion

        #region Update method for payment grid.
        public DataTable Update_Payment(Claim_Model objModel)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_pay_crdgid, objModel.Pay_gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_pay_crdecfgid, objModel.Pay_ecfgid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_pay_crdpaymode, objModel.Pay_paymodemode, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_pay_crdrefno, objModel.Pay_referenceno, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_pay_crdbeneficiary, objModel.Pay_beneficiary, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_pay_crdglcode, objModel.Pay_glcode, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_pay_crdamount, objModel.Pay_creditamount, DbType.Double));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.LoggedUser_Gid, objModel.Logged_UserGid, DbType.Int64));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.sp_Updatepaymentdetails, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion

        #region Update Method for Remarks grid.
        public DataTable Update_Remarks(Claim_Model objModel)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Gid, objModel.Rem_gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_rem_remarks, objModel.Rem_remarks, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.LoggedUser_Gid, objModel.Logged_UserGid, DbType.String));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.sp_Upateremarks, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion

        #region Save/Update Method for Travel/Boarding/Conveyance.
        public DataTable Update_TravelBoardingConveyance_Details(Claim_Model objModel)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_ecftravelgid, objModel.Det_gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_ecfgid, objModel.Det_ecfgid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_debitelinegid, objModel.Det_debilnegid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_type, objModel.Det_type, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_transgid, objModel.Det_typegid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_transclassgid, objModel.Det_travelclassgid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_cityfrom, objModel.Det_placefrom, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_cityto, objModel.Det_placeto, DbType.String));
                if (objModel.Det_datefrom != null)
                {
                    if (objModel.Det_datefrom.Length > 19)
                    {
                        cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_datefrom, DateTime.Parse(objModel.Det_datefrom.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_datefrom, DateTime.ParseExact(objModel.Det_datefrom.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_datefrom, objModel.Det_datefrom, DbType.String));
                }

                if (objModel.Det_dateto != null)
                {
                    if (objModel.Det_dateto.Length > 19)
                    {
                        cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_dateto, DateTime.Parse(objModel.Det_dateto.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_dateto, DateTime.ParseExact(objModel.Det_dateto.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_dateto, objModel.Det_dateto, DbType.String));
                }
                // cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_datefrom, objModel.Det_datefrom, DbType.DateTime));
                // cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_dateto, objModel.Det_dateto, DbType.DateTime));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_distance, objModel.Det_km, DbType.Decimal));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_rate, objModel.Det_rate, DbType.Decimal));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_amount, objModel.Det_amount, DbType.Double));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_hotel, objModel.Det_hotel, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_detailtypeid, objModel.Det_gridtypegid, DbType.Int16));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.LoggedUser_Gid, objModel.Logged_UserGid, DbType.Int64));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.sp_UpdateTravelBoardingConveyance, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion

        #region method for updating tax details.
        public DataTable Update_TaxDetails(Claim_Model objModel)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_tax_type, objModel.Hidden_taxtype, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_tax_ecfgid, objModel.Hidden_ecfgid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_tax_debitlinegid, objModel.Hidden_debitlinegids, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_tax_suppliergid, objModel.Tax_suppliergid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_tax_providergstin, objModel.Tax_providergstin, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_tax_providergid, objModel.Tax_providerlocationgid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_tax_receivergid, objModel.Tax_receiverlocationgid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_tax_receivergstin, objModel.Tax_receivergstin, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_tax_invoiceno, objModel.Tax_invoiceno, DbType.String));
                if (objModel.Tax_invoicedate != null)
                {
                    if (objModel.Tax_invoicedate.Length > 19)
                    {
                        cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_tax_invoicedate, DateTime.Parse(objModel.Tax_invoicedate.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_tax_invoicedate, DateTime.ParseExact(objModel.Tax_invoicedate.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_tax_invoicedate, objModel.Tax_invoicedate, DbType.String));
                }
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_tax_invoiceamount, objModel.Tax_invoiceamount, DbType.Double));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.LoggedUser_Gid, objModel.Logged_UserGid, DbType.Int64));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.sp_UpdateTexDetails, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable Get_TaxDetails(Claim_Model objModel)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_tax_type, objModel.Hidden_taxtype, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_tax_ecfgid, objModel.Hidden_ecfgid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_tax_debitlinegid, objModel.Hidden_debitlinegids, DbType.String));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.sp_GetTexDetails, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion

        #region Claim action Draft/Submit.
        public DataTable Claim_DraftSubmit(Claim_Model objModel)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Action, objModel.Button_Action, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Batch_Gid, objModel.HDR_Batch_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.UserGid, objModel.Logged_UserGid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Createmode, objModel.CLM_Create_Mode, DbType.String));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.sp_DraftSubmit, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion

        #region Claim ECF Bulk upload
        public DataTable Save_Claim_BulkECF(Claim_Model ObjModel)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Action, cmObjEOW.Create, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Type_Gid, ObjModel.HDR_Type_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_SubType_Gid, ObjModel.HDR_SubType_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Raiser_Code, ObjModel.HDR_Raiser_Code, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Batch_Gid, ObjModel.HDR_Batch_Gid, DbType.Int64)); 
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Createmode, ObjModel.CLM_Create_Mode, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_EcfRaiser_Gid, ObjModel.Logged_UserGid, DbType.Int64));
                if (ObjModel.CLM_Claim_Date != null)
                {
                    if (ObjModel.CLM_Claim_Date.Length > 20)
                    {
                        cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Date, DateTime.Parse(ObjModel.CLM_Claim_Date.Substring(0, 10)), DbType.DateTime));
                    }
                    else
                    {
                        cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Date, DateTime.ParseExact(ObjModel.CLM_Claim_Date.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Date, ObjModel.CLM_Claim_Date, DbType.String));
                }
                //cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Date, DateTime.Parse(ObjModel.CLM_Claim_Date.Substring(0, 10)), DbType.DateTime));
                //cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Date, cmnObj.ConvertDate(ObjModel.CLM_Claim_Date.Substring(0, 10)), DbType.String)); 
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Amount, ObjModel.CLM_Claim_Amount, DbType.Double));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Description, ObjModel.CLM_Claim_Description, DbType.String)); 
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.LoggedUser_Gid, ObjModel.Logged_UserGid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_EmployeeCodes, ObjModel.HDR_Employee_Codes, DbType.String));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.sp_Claim_DML_Bulk_ECF, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable Save_Claim_BulkLocalConveyance(Claim_Model objModel)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Batch_Gid, objModel.HDR_Batch_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Type_Gid, objModel.HDR_Type_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_SubType_Gid, objModel.HDR_SubType_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_EmployeeCode, objModel.HDR_Raiser_Code, DbType.String));                
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Createmode, objModel.CLM_Create_Mode, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_EcfRaiser_Gid, objModel.Logged_UserGid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_ReferenceNo, objModel.CLM_Ecf_No, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_type, objModel.Det_type, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.P_Expenseid, objModel.Exp_gid, DbType.Int64)); 
                if (objModel.Det_datefrom != null)
                {
                    if (objModel.Det_datefrom.Length > 20)
                    {
                        cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.P_InvDate, DateTime.Parse(objModel.Det_datefrom.Substring(0, 10)), DbType.DateTime));
                    }
                    else
                    {
                        cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.P_InvDate, DateTime.ParseExact(objModel.Det_datefrom.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.P_InvDate, objModel.Det_datefrom, DbType.String));
                }
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_cityfrom, objModel.Det_placefrom, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_cityto, objModel.Det_placeto, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_distance, objModel.Det_km, DbType.Double));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_rate, objModel.Det_rate, DbType.Double));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_det_amount, objModel.Det_amount, DbType.Double));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.LoggedUser_Gid, objModel.Logged_UserGid, DbType.Int64));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.SPEOWSet_Bulk_LocalConveyance, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable Save_Bulk_Expense(Claim_Model objModel)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Action, cmObjEOW.Create, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_No, objModel.CLM_Ecf_No, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_taxtype, objModel.Exp_taxtype.Substring(0,1), DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_naturename, objModel.Exp_natureexpname, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_catgname, objModel.Exp_catgname, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_subcatgname, objModel.Exp_subcatgname, DbType.String)); 
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_debitamount, objModel.Exp_debitamount, DbType.Double));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_exp_hsnname, objModel.Exp_hsnname, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.LoggedUser_Gid, objModel.Logged_UserGid, DbType.Int64));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.sp_Bulk_Expense_DML, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion

        #region CRUD Methods for Claim Grid.
        public DataTable Update_Checklist(APCheckerEntities _objapmodel)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_ap_checklist_gid, _objapmodel.CheckList_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_ap_checklist_ecfgid, _objapmodel.CheckList_ecf_gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_ap_checklist_reason, _objapmodel.CheckList_Reason, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_ap_checklist_status, _objapmodel.CheckList_status, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.UserGid, _objapmodel.logged_user_gid, DbType.Int64));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.sp_APChecklist_Updation, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable APChecklist_Submit(APCheckerEntities objAPModel)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Action, objAPModel.AP_Action, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Gid, objAPModel.CheckList_ecf_gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Status, objAPModel.CheckList_status, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_rem_remarks, objAPModel.AP_Remarks, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_ap_ecfgids, objAPModel.ecfgids, DbType.String));
                if (objAPModel.gldate != null)
                {
                    if (objAPModel.gldate.Length > 19)
                    {
                        cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_ap_gldate, DateTime.Parse(objAPModel.gldate.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_ap_gldate, DateTime.ParseExact(objAPModel.gldate.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_ap_gldate, objAPModel.gldate, DbType.String));
                }
                //cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_ap_gldate, objAPModel.gldate, DbType.DateTime));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.UserGid, objAPModel.logged_user_gid, DbType.Int64));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.sp_Checker_Submit, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataSet Get_Selected_Checklists(Claim_Model objModel)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Ecf_Gid, objModel.HDR_EcfGId, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_SubType_Gid, objModel.HDR_SubType_Gid, DbType.Int16));
                ds = cmObjEOW.dbManager.GetDataSet(cmObjEOW.sp_Selected_Checklist, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        #endregion

    }
}
