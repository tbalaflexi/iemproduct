﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccessHandler.Models.EOW;

namespace EOW.Data
{
    public class AdvanceRequisition_Data
    {
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        EOWCommon cmObjEOW = new EOWCommon();

        #region Common method for bindings dropdown lists.
        public DataTable Get_dropdownlist_records(string MasterName, Int64 Master_Gid)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Claim_MasterName, MasterName, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Claim_MasterGid, Master_Gid, DbType.Int64));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.sp_get_advance_dropdownrecords, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion

        #region methods for create/update advance requisition.
        public DataTable Save_AdvanceRequisition(AdvanceRequisition_Model _ObjModel)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Action, cmObjEOW.Create, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_batch_gid, _ObjModel.Batch_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_ecf_gid, _ObjModel.Ref_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_type_gid, _ObjModel.Type_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_subtype_gid, _ObjModel.SubType_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_raiser_gid, _ObjModel.Employee_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_supplier_gid, _ObjModel.Supplier_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_advtype_gid, _ObjModel.AdvanceType_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_gl_code, _ObjModel.GL_CodeDesc, DbType.String));
                if (_ObjModel.Liquidation_Date != null)
                {
                    if (_ObjModel.Liquidation_Date.Length > 19)
                    {
                        cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_liquidation_date, DateTime.Parse(_ObjModel.Liquidation_Date.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_liquidation_date, DateTime.ParseExact(_ObjModel.Liquidation_Date.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_liquidation_date, _ObjModel.Liquidation_Date, DbType.String));
                }
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_description, _ObjModel.Description, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_advance_amount, _ObjModel.Advance_Amount, DbType.Decimal));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_supemp_type, _ObjModel.Sup_Emp_Type, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.UserGid, _ObjModel.Logged_UserGid, DbType.Int64));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.sp_set_advancerequisition, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable Update_AdvanceRequisition(AdvanceRequisition_Model _ObjModel)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Action, cmObjEOW.Update, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_batch_gid, _ObjModel.Batch_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_ecf_gid, _ObjModel.Ref_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_type_gid, _ObjModel.Type_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_subtype_gid, _ObjModel.SubType_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_raiser_gid, _ObjModel.Employee_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_supplier_gid, _ObjModel.Supplier_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_advtype_gid, _ObjModel.AdvanceType_Gid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_gl_code, _ObjModel.GL_CodeDesc, DbType.String));
                if (_ObjModel.Liquidation_Date != null)
                {
                    if (_ObjModel.Liquidation_Date.Length > 19)
                    {
                        cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_liquidation_date, DateTime.Parse(_ObjModel.Liquidation_Date.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_liquidation_date, DateTime.ParseExact(_ObjModel.Liquidation_Date.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_liquidation_date, _ObjModel.Liquidation_Date, DbType.String));
                }
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_description, _ObjModel.Description, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_advance_amount, _ObjModel.Advance_Amount, DbType.Decimal));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_supemp_type, _ObjModel.Sup_Emp_Type, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.UserGid, _ObjModel.Logged_UserGid, DbType.Int64));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.sp_set_advancerequisition, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion

        #region #region common method for fetching grid records.
        public DataTable Get_ARF_records(string GridName, Int64 BatchGid, Int64 EcfGid)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_grid_name, GridName, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_batch_gid, BatchGid, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_adv_ecf_gid, EcfGid, DbType.Int64));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.sp_get_advancerequisitiondetails, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion
    }
}
