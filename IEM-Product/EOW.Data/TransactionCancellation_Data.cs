﻿using DataAccessHandler.Models.EOW;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EOW.Data
{
   public class TransactionCancellation_Data
    {

        TransactionCancellation_Model ModelObject = new TransactionCancellation_Model();                                   // TransactionCancellation model object
        List<TransactionCancellation_Model> ModelList = new List<TransactionCancellation_Model>();                         //TransactionCancellation list model object
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();

        EOWCommon cmObjEOW = new EOWCommon();  //list model object
      
       //Transaction Cancellation Read
        public List<TransactionCancellation_Model> ReadRecord(Int64 User_GId)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.LoggedUser_Gid, User_GId, DbType.Int64));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.Spgettransactioncancellation, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());

               
                foreach (DataRow dr in dt.Rows)
                {
                    ModelList.Add
                        (
                        new TransactionCancellation_Model
                        {
                            id = Convert.ToInt64(dr["ecf_gid"].ToString()),
                            ecf_no = dr["ecf_no"].ToString(),
                            ecf_amount = (dr["ecf_amount"].ToString()),
                            // ecf_date = dr["ecf_date"].ToString(),
                            raiser = dr["Raiser"].ToString(),
                            suppliername = dr["ECF Supplier/Employee"].ToString(),
                            ClaimType = dr["ClaimType"].ToString(),
                            ecf_status = dr["ecfstatus_name"].ToString(),
                            // Action = dr["Action"].ToString(),
                            //  SupplierEmployee_Type = dr["ecf_supplier_employee"].ToString(),
                            Batch_Gid = Convert.ToInt64(dr["ecf_batch_gid"].ToString()),
                            Ecf_Gid = Convert.ToInt64(dr["ecf_gid"].ToString()),
                            Raiser_Gid = Convert.ToInt64(dr["Raiser_Gid"].ToString()),
                            Approver_Status = dr["Approval"].ToString(),
                        });
                }
                return ModelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        //Delete Transaction Cancellation
        public DataTable DeleteRecord(string claim)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.p_Action, cmObjEOW.Delete, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.ecfid, claim, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.LoggedUser_Gid,1, DbType.Int32));
                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.SptransactioncancellationDelete, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
    }
}
