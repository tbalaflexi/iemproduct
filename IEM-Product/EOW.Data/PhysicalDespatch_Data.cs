﻿using DataAccessHandler.Models.EOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace EOW.Data
{
   public class PhysicalDespatch_Data
    {
       PhysicalDespatch_Model ModelObject = new PhysicalDespatch_Model();
       List<PhysicalDespatch_Model> ModelList = new List<PhysicalDespatch_Model>();       
        EOWCommon cmObjEOW = new EOWCommon();
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        public DataSet ReadPhysicalDespatch(Int64 userId)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.userID, 50, userId, DbType.Int32));
                ds = cmObjEOW.dbManager.GetDataSet(cmObjEOW.spGetPhysicalDespatchDetails, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());

                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Save_EcfDespatchedList(PhysicalDespatch_Model modelObj)
        {
            try
            {
                cmObjEOW.parameters = new List<IDbDataParameter>();
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.userID, 50, modelObj.loginUserID, DbType.Int32));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.actionType, modelObj.actionType, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.ecfSelectedList, modelObj.ecfSelectedList, DbType.String));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.despatchModeID, modelObj.ecfDespatchModeID, DbType.Int64));
                cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.ewbNumber, modelObj.despatchEWBNumber, DbType.String));
                if (modelObj.ecfDespatchDate != null)
                {
                    if (modelObj.ecfDespatchDate.Length > 19)
                    {
                        cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.despatchDate, DateTime.Parse(modelObj.ecfDespatchDate.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.despatchDate, DateTime.ParseExact(modelObj.ecfDespatchDate.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    cmObjEOW.parameters.Add(cmObjEOW.dbManager.CreateParameter(cmObjEOW.despatchDate, modelObj.ecfDespatchDate, DbType.String));
                }


                dt = cmObjEOW.dbManager.GetDataTable(cmObjEOW.spSetPhysicalDespatchDetails, CommandType.StoredProcedure, cmObjEOW.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
           }
}
