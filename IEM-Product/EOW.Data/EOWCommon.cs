﻿using DataAccessHandler;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EOW.Data
{
    public class EOWCommon
    {

        #region Connection String and Parameter Values
        public DBManager dbManager = new DBManager("ConnectionString");
        public List<IDbDataParameter> parameters;
        #endregion

        #region Strored Procedure CURD
        public string strSP = "";
        public readonly string SPValidateLogin = "SP_Validate_Supplier";
        public readonly string SPGetsupDetails = "SP_Get_SupplierDetails";
        public readonly string SPGetInvDetails = "SP_Get_SupplierInvDetails";
        public readonly string SPGetPoDetails = "SP_Get_PoDetails";

        #endregion

        #region Common Params
        public readonly string TypeCode = "QCD_Type";
        public readonly string SubTypeCode = "QCD_SubType";
        public readonly string ExpNatCode = "QCD_Natureofexpense";
        public readonly string ExpCatCode = "QCD_ExpenseCategory";
        public readonly string AssetCatCode = "QCD_AssetCategory";
        public readonly string HSNCode = "QCD_HSN";
        public readonly string GLCode = "QCD_GLNO";
        public readonly string TaxCode = "QCD_Tax";
        public readonly string SubTaxCode = "QCD_SubTax";
        public readonly string Tds = "TDS";
        #endregion

        #region Operation Type(CURD)
        public readonly string Create = "C";
        public readonly string Read = "R";
        public readonly string Update = "U";
        public readonly string Delete = "D";
        public readonly string Insert = "I";
        #endregion

        #region Loged User Details
        public string LoggedUser = "Flexi";
        public string LoggedUser_Gid = "In_logingid";
        public readonly string UserGid = "In_UserGid";
        #endregion

        #region Claim Screen - Stored procedure names(CRUD).
        public readonly string sp_BatchDetails = "SP_EOW_Get_ClaimBatchDetails";
        public readonly string sp_Claim_Getdropdownlist = "SP_EOW_Claim_GetDropDownList";
        public readonly string sp_Claim_GridList = "SP_EOW_Get_ClaimDetails";
        public readonly string sp_Claim_DML = "SP_EOW_DML_Ecf"; //claim grid.
        public readonly string sp_Expense_DML = "SP_EOW_DML_Expense";//expense grid.
        public readonly string sp_Deleteclaimdetails = "SP_EOW_DeleteClaimDetails"; //delete records common for all grid.
        public readonly string sp_Upateremarks = "SP_EOW_UpdateClaimRemarks";
        public readonly string sp_Updatepaymentdetails = "SP_EOW_Claim_SetCreditline";
        public readonly string sp_UpdateTravelBoardingConveyance = "SP_EOW_DML_TravalBoardingConveyance";
        public readonly string sp_GetTexDetails = "SP_Get_Claim_TaxDetails";
        public readonly string sp_UpdateTexDetails = "SP_EOW_Claim_UpdateTaxDetails";
        public readonly string sp_DraftSubmit = "SP_EOW_Set_ClaimSubmit";
        //public readonly string sp_GetChecklist = "SP_FS_Get_APChecklist";
        #endregion

        #region Claim Screen - Stored procedure parameters.
        public readonly string p_Claim_MasterName = "In_Master";
        public readonly string p_Claim_MasterGid = "In_MasterGid";
        public readonly string p_Claim_Emp_Name = "In_Employee";
        public readonly string p_Batch_Gid = "In_batchgid";
        public readonly string p_GirdName = "In_gridname";
        public readonly string p_GridGid = "In_gridgid";
        //claim grid params.
        public readonly string p_Action = "In_action";
        public readonly string p_Type_Gid = "In_typegid";
        public readonly string p_SubType_Gid = "In_subtypegid";
        public readonly string p_Raiser_Gid = "In_raisergid";
        public readonly string p_Batch_No= "In_batchno";
        public readonly string p_Batch_Amount = "In_batchamount";
        public readonly string p_Ecf_Gid = "In_ecfgid";
        public readonly string p_Ecf_No = "In_ecfno";
        public readonly string p_Ecf_Createmode = "In_ecfcreatemode";
        public readonly string p_EcfRaiser_Gid = "In_ecf_raisergid";
        public readonly string p_Ecf_Date = "In_ecf_date";
        public readonly string p_Ecf_Month = "In_ecf_month";
        public readonly string p_Ecf_Amount = "In_ecfamount";
        public readonly string p_Ecf_Description = "In_ecf_desc";
        public readonly string p_Ecf_PersonCount = "In_ecf_personcount";
        public readonly string p_Ecf_EmployeeIds = "In_ecf_employeeids";

        //expense grid params.
        public readonly string p_exp_invicegid = "In_invoicegid";
        public readonly string p_exp_ivoiceno = "In_invoiceno";
        public readonly string p_exp_taxtype = "In_taxtype";
        public readonly string p_exp_naturegid = "In_expnaturegid";
        public readonly string p_exp_catggid = "In_expcatggid";
        public readonly string p_exp_subcatggid = "In_expsubcatggid";
        public readonly string p_exp_glno = "In_expglno";
        public readonly string p_exp_debitamount = "In_debitamount";
        public readonly string p_exp_hsngid = "In_exphsngid";
        public readonly string P_HsnType = "In_HsnType";

        //payment grid params.
        public readonly string p_pay_crdgid = "In_crlinegid";
        public readonly string p_pay_crdecfgid = "In_crecfgid";
        public readonly string p_pay_crdpaymode = "In_crpaymode";
        public readonly string p_pay_crdrefno = "In_crrefno";
        public readonly string p_pay_crdbeneficiary = "In_crbeneficiary";
        public readonly string p_pay_crdglcode = "In_crglcode";
        public readonly string p_pay_crdamount = "In_cramount";

        //remarks grid params.
        public readonly string p_rem_remarks = "In_remarks";

        //details grid params.
        public readonly string p_det_ecftravelgid = "In_ecftravel_gid";
        public readonly string p_det_ecfgid = "In_ecftravel_ecfgid";
        public readonly string p_det_debitelinegid = "In_ecftravel_debitelinegid";
        public readonly string p_det_type = "In_ecftravel_type";
        public readonly string p_det_transgid = "In_ecftravel_transportgid";
        public readonly string p_det_transclassgid = "In_ecftravel_transportclassgid";
        public readonly string p_det_cityfrom = "In_ecftravel_cityfrom";
        public readonly string p_det_cityto = "In_ecftravel_cityto";
        public readonly string p_det_datefrom = "In_ecftravel_datefrom";
        public readonly string p_det_dateto = "In_ecftravel_dateto";
        public readonly string p_det_distance = "In_ecftravel_distance";
        public readonly string p_det_rate = "In_eecftravel_rate";
        public readonly string p_det_amount = "In_ecftravel_amount";
        public readonly string p_det_hotel = "In_ecftravel_hotel";
        public readonly string p_det_detailtypeid = "In_debitline_typeid"; 
        //tax grid params.
        public readonly string p_tax_type = "In_taxtype";
        public readonly string p_tax_ecfgid = "In_ecfgid";
        public readonly string p_tax_debitlinegid = "In_debitlinegid";
        public readonly string p_tax_suppliergid = "In_suppliergid";
        public readonly string p_tax_providergstin = "In_providergstin";
        public readonly string p_tax_providergid = "In_providergid";
        public readonly string p_tax_receivergid = "In_receivergid";
        public readonly string p_tax_receivergstin = "In_receivergstin";
        public readonly string p_tax_invoiceno = "In_invoiceno";
        public readonly string p_tax_invoicedate = "In_invoicedate";
        public readonly string p_tax_invoiceamount = "In_invoiceamount";
        #endregion

        #region Advance Requisition Screen - Stored procedure names(CRUD).
        public readonly string sp_get_advance_dropdownrecords = "SP_EOW_Get_AdvanceReq_DropDownValues";
        public readonly string sp_set_advancerequisition = "SP_EOW_Set_AdvanceDebitLine";
        public readonly string sp_get_advancerequisitiondetails = "SP_EOW_Get_AdvanceRequisitionDetails";
        #endregion

        #region Advance Requisition Screen - Stored procedure parameters.
        public readonly string p_adv_type_gid = "In_type_gid";
        public readonly string p_adv_subtype_gid = "In_subtype_gid";
        public readonly string p_adv_raiser_gid = "In_raiser_gid";
        public readonly string p_adv_supplier_gid = "In_supplier_gid";
        public readonly string p_adv_batch_gid = "In_batch_gid";
        public readonly string p_adv_ecf_gid = "In_ecf_gid";
        public readonly string p_adv_advtype_gid = "In_advtype_gid";
        public readonly string p_adv_gl_code = "In_gl_code";
        public readonly string p_adv_liquidation_date = "In_liquidation_date";
        public readonly string p_adv_description = "In_description";
        public readonly string p_adv_advance_amount = "In_advance_amount";
        public readonly string p_adv_grid_name = "In_grid_name";
        public readonly string p_adv_supemp_type = "In_supplieremployee_type";
        #endregion

        #region Supplier Invoice - Stored procedure Names.
        public readonly string SpSupplierInvoice = "SP_EOW_Set_SupplierInvoice";
        public readonly string SpgetSupplierInv = "SP_EOW_Get_SupplierInvoice";
        public readonly string SPgetEowgstDropdown = "SP_EOW_Get_InvDropDowns/Autocomplete";
        public readonly string SPgetExpDropDowns = "SP_EOW_Get_ExpDropDowns";
        public readonly string SpSupplierInvExpense = "SP_EOW_Set_SupplierExpense";
        public readonly string SpSupplierInvCredit = "SP_EOW_Set_SupplierCredit";
        public readonly string SPgetCreditDropDowns = "SP_EOW_Get_CrDropDowns";
        public readonly string SPSubmitSupplierInv = "SP_EOW_Set_SupplierInvoiceSubmit";
        public readonly string SPApproveSupplierInv = "SP_EOW_Set_DelmatApproveAction";
        public readonly string SPSupplierInvRemark = "SP_EOW_Set_SupplierInvoiceRemarks";
        public readonly string SPGetSupplierInvTax = "SP_EOW_Get_SupplierInvoiceTax";
        public readonly string SPSetSupplierInvTax = "SP_EOW_Set_SupplierInvoiceTax";
        public readonly string SpSupplierInvPomapping = "SP_EOW_Set_SupplierPOMapping";
        //Ramya COA
        public readonly string SPGetCOADropDowns = "SP_EOW_Get_COADropDowns";
        public readonly string SpGetCOAGrid = "SP_EOW_Get_DebitlineCOA";
        public readonly string SpEOWSetCOAGrid = "SP_EOW_SupplierClaim_DML_COA";
        public readonly string SPGetReferenceNoDropDown = "SP_EOW_Get_ReferenceNo";
        #endregion

        #region Supplier Invoice - Stored Procedure parameters.
        /*Invoice Parameters*/
        public readonly string P_Invno = "In_Invoiceno";
        public readonly string P_InvDate = "In_InvoiceDate";
        public readonly string P_ServiceMonth = "In_ServiceMonth";
        public readonly string P_InvAmt = "In_InvoiceAmt";
        public readonly string P_ProviderLoc = "In_ProviderLoc";
        public readonly string P_Providergstin = "In_ProviderGstn";
        public readonly string P_ReceiverLoc = "In_ReceiverLoc";
        public readonly string P_Receivergstin = "In_ReceiverGstn";
        public readonly string P_Taxstatus = "In_Taxstatus";
        public readonly string P_Currencyid = "In_Currencyid";
        public readonly string P_Suppliergid = "In_SupplierGid";
        public readonly string P_InvDesc = "In_InvoiceDesc";
        public readonly string P_InvDoctype = "In_DocType";
        public readonly string P_InvDocsubtype = "In_DocSubtype";
        public readonly string P_Raiserid = "In_Raiserid";
        public readonly string P_Amortflag = "In_Amortflag";
        public readonly string P_Amrtfrom= "In_AmortFrom";
        public readonly string P_Amortto = "In_AmortTo";
        public readonly string P_AmortDesc = "In_AmortDesc";
        public readonly string P_Batchid= "In_BatchId";
        public readonly string P_TotalAmt = "In_TotalAmt";
        public readonly string P_Urgentflag = "In_UrgentFlag";
        public readonly string P_Action = "In_action";
        public readonly string P_ProvisionFlag = "In_Provisionflag";
        public readonly string P_Retentionflag = "In_Retentionflag";
        public readonly string P_EcfRefid = "In_EcfRefId";
        public readonly string P_Stateid = "In_Stateid";
        public readonly string P_Supplierid = "In_Supplierid";
        public readonly string P_RetentionRate = "In_RetentionRate";
        public readonly string P_RetentionAmt = "In_RetenionAmt";
        public readonly string P_RetentionException = "In_Retentionexception";
        public readonly string P_Ecfids = "In_Ecfids";
        public readonly string P_QueueRemark = "In_QueueRemark";
        public readonly string P_Taxid = "In_Taxid";
        public readonly string P_Subtaxid = "In_Subtaxid";
        public readonly string P_TaxableAmt = "In_TaxaleAmt";
        public readonly string P_TaxRate = "In_TaxRate";
        public readonly string P_InvTaxid = "In_InvTaxid";
        public readonly string P_TypeFlag = "In_TypeFlag";
        public readonly string P_Invid = "In_Invid";
        public readonly string P_Grndetails = "In_Grndetails";
        public readonly string P_InvPoitemid = "In_InvPoitemid";

        //For Approve Action
        public readonly string P_Batchids = "In_Batchids";
        public readonly string p_Ecfdesignation = "In_Ecfdesignation";
        public readonly string p_EMPdelmattype = "In_EMPdelmattype";
        public readonly string p_SUPdelmattype = "In_SUPdelmattype";
        public readonly string p_EcfInprocess = "In_EcfInprocess";
        public readonly string p_EcfApproved = "In_EcfApproved";
        public readonly string p_EcfHold = "In_ecfHold";
        public readonly string p_EcfConcurrentApproval = "In_EcfConcurrentApproval";
        public readonly string p_EcfRejected = "In_EcfRejected";
        public readonly string p_centralckeckerreject = "In_centralckeckerreject";
        public readonly string p_centralmaker = "In_centralmaker";
        public readonly string p_Status = "In_Status";
        public readonly string p_Delegates = "In_Delegates";
        public readonly string p_Remarks = "In_Remarks"; 


        /*Expense Parametes*/
        public readonly string P_ExpSubcatid = "In_Expsubcatid";
        public readonly string P_ExpCatid = "In_ExpCatid";
        public readonly string P_MastersCode = "In_QCMastCode";
        public readonly string P_Invoiceid = "In_Invoiceid";
        public readonly string P_Expenseid = "In_Expenseid";
        public readonly string P_ExpNatureid = "In_ExpNatureid";
        public readonly string P_Glid = "In_Glid";
        public readonly string P_ExpAmt = "In_ExpAmt";
        public readonly string P_HsnId = "In_HsnId";
        public readonly string P_ExpDesc = "In_ExpDesc";
        public readonly string p_GLName = "In_GLName";

        /*Credit Parameters*/
        public readonly string P_CrAmt = "In_CrAmt";
        public readonly string P_CrRefno = "In_CrRefno";
        public readonly string P_CrBankid = "In_CrBankid";
        public readonly string P_CrBeneficiary = "In_CrBeneficiary";
        public readonly string P_CrIfscid = "In_CrIfscid";
        public readonly string P_CrPaymodeid = "In_CrPaymodeid";
        public readonly string P_CrPaymode = "In_CrPaymode";
        public readonly string P_CrDescription = "In_CrDesc";
        public readonly string P_Crditid = "In_Creditid";

        /*ARF Adjustment*/

        /*PO Mapping*/
        public readonly string P_Poid = "In_Poid";

        #endregion

        //Ramya added for COA
        public readonly string P_COA_gid = "In_COA_gid";
        public readonly string P_COA_Expense_Gid = "In_COA_Expense_Gid";
        public readonly string P_COA_GL_No = "In_COA_GL_No";
        public readonly string P_COA_FC_Code = "In_COA_FC_Code";
        public readonly string P_COA_CC_Code = "In_COA_CC_Code";
        public readonly string P_COA_Product_Code = "In_COA_Product_Code";
        public readonly string P_COA_OU_Code = "In_COA_OU_Code";
        public readonly string P_COA_Amount = "In_COA_Amount";
        public readonly string P_Batch_Gid = "In_Batch_Gid";
        public readonly string P_ECF_Gid = "In_ECF_Gid";

        //Bulk Debitline Upload 
        public readonly string P_ExpNatureName = "In_ExpNatureName";
        public readonly string P_ExpCatName = "In_ExpCatName";
        public readonly string P_ExpSubcatName = "In_ExpSubcatName";
        public readonly string P_FCCode = "In_FCCode";
        public readonly string P_CCCode = "In_CCCode";
        public readonly string P_ProductCode = "In_ProductCode";
        public readonly string P_OUCode = "In_OUCode";
        public readonly string P_HsnCode = "In_Hsncode";
        public readonly string P_TaxType = "In_TaxType";
        public readonly string SpSupplierInvBulkExpense = "SP_EOW_Set_SupplierBulkExpense";

        //Bulk Invoice Upload
        public readonly string P_Suppliercode = "In_SupplierCode";
        public readonly string p_Raiser_Code = "In_raisercode";
        public readonly string SpSupplierInvBulkInvoice = "SP_EOW_Set_SupplierBulkInvoice";
        public readonly string p_Ecf_EmployeeCodes = "In_ecf_employeecodes";
        public readonly string sp_Claim_DML_Bulk_ECF = "SP_EOW_DML_Bulk_Ecf"; //claim ECF grid.

        //Bulk COA
        public readonly string p_ReferenceNo = "In_ReferenceNo";
        public readonly string SpEOWSetCOAUpload = "SP_EOW_SupplierClaim_DML_UploadCOA";

        //Bulk Conveyance
        public readonly string SPEOWSet_Bulk_LocalConveyance = "SP_EOW_DML_BulkLocalConveyance";
        public readonly string p_EmployeeCode = "In_employeecode";

        //Bulk Claim Expense
        public readonly string p_exp_naturename = "In_expnaturename";
        public readonly string p_exp_catgname = "In_expcatgname";
        public readonly string p_exp_subcatgname = "In_expsubcatgname";
        public readonly string p_exp_hsnname = "In_exphsnname";

        public readonly string sp_Bulk_Expense_DML = "SP_EOW_DML_Bulk_Expense";//expense grid.


        #region stored procedure for Transaction Cancellation
        public readonly string Spgettransactioncancellation = "Sp_Get_transactioncancellation";
        public readonly string SptransactioncancellationDelete = "Sp_Delete_TransactionCancellation";

        public readonly string ecfid = "In_ecf_id";

         
        #endregion

        #region AP Checker Stored procedure names
        public readonly string sp_APChecklist_Updation = "SP_FS_Set_APChecklistReason";
        public readonly string sp_Checker_Submit = "SP_FS_Set_AuditMakerSubmit";
        public readonly string sp_Selected_Checklist = "SP_FS_Get_Selected_Checklist";
        #endregion

        #region AP Checker Stored procedure params
        public readonly string p_ap_checklist_gid = "In_checklist_gid";
        public readonly string p_ap_checklist_ecfgid = "In_checklist_ecfgid";
        public readonly string p_ap_checklist_reason = "In_checklist_reason";
        public readonly string p_ap_checklist_status = "In_checklist_status";
        public readonly string p_ap_ecfgids = "In_EcfGids";
        public readonly string p_ap_gldate = "In_GlDate";
        #endregion

        #region physicaldespatch
        public readonly string userID = "In_UserID";
        public readonly string ecfSelectedList = "In_EcfSelectedList";
        public readonly string despatchModeID = "In_EcfDespatchModeID";
        public readonly string despatchDate = "In_EcfDespatchDate";
        public readonly string ewbNumber = "In_EcfDespatchAWBNumber";
        public readonly string actionType = "In_ActionType";
        public readonly string spGetPhysicalDespatchDetails = "SP_Get_PhysicalDespatchDetails";
        public readonly string spSetPhysicalDespatchDetails = "Sp_Set_PhysicalDespatchDetails";
        #endregion
    }

}
