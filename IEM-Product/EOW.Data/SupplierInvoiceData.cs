﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessHandler.Models.EOW;
using SharedData;
using Shared.Data;

namespace EOW.Data
{
    public class SupplierInvoiceData
    {
        #region Declarations
        //SupplierInvoiceData dataObj = new SupplierInvoiceData();
        EOWCommon cmnParams = new EOWCommon();
        CommonMethods cmnObj = new CommonMethods();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        #endregion

        #region Invoices
        public DataTable SaveInvoice(SupplierInvoice_Model data)
        {
            try
            {
                data.InvServiceMonth = Convert.ToString(DateTime.Today);
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Invno, string.IsNullOrEmpty(data.InvoiceNo) ? "0" : data.InvoiceNo, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_InvDate, cmnObj.Dateconversion(Convert.ToString(data.InvoiceDate)), DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ServiceMonth, cmnObj.Dateconversion(Convert.ToString(data.InvoiceDate)), DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_InvAmt, data.InvoiceAmt, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ProviderLoc, data.ProvLocid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Providergstin, string.IsNullOrEmpty(data.ProviderGstn) ? "0" : data.ProviderGstn, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ReceiverLoc, data.RecvLocid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Receivergstin, string.IsNullOrEmpty(data.ReceiverGstn) ? "0" : data.ReceiverGstn, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Taxstatus, string.IsNullOrEmpty(data.TaxStatus) ? "0" : data.TaxStatus, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Currencyid, data.Currencyid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Supplierid, data.Supplierid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_InvDesc, string.IsNullOrEmpty(data.InvDescription) ? "0" : data.InvDescription, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_InvDoctype, data.Type_Gid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_InvDocsubtype, data.SubType_Gid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Raiserid, data.Raiserid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Amortflag, string.IsNullOrEmpty(data.AmortFlag) ? "0" : data.AmortFlag, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Amrtfrom, cmnObj.Dateconversion(data.AmortFrom), DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Amortto, cmnObj.Dateconversion(data.AmortTo), DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_AmortDesc, string.IsNullOrEmpty(data.AmortDesc) ? "0" : data.AmortDesc, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Batchid, data.BatchGid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_TotalAmt, data.InvoiceAmt, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Urgentflag, string.IsNullOrEmpty(data.UrgentFlag) ? "0" : data.UrgentFlag, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, data._Action, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ProvisionFlag, string.IsNullOrEmpty(data.ProvisionFlag) ? "0" : data.ProvisionFlag, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Retentionflag, string.IsNullOrEmpty(data.RetentionFlag) ? "0" : data.RetentionFlag, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EcfRefid, data.Refid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_RetentionRate, data.RetentionRate, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_RetentionAmt, data.RetentionAmt, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_RetentionException, data.RetentionException, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_TypeFlag, string.IsNullOrEmpty(data.TypeFlag) ? "0" : data.TypeFlag, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.UserGid, data.Loginid, DbType.String));
                dt = cmnParams.dbManager.GetDataTable(cmnParams.SpSupplierInvoice, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetInvoices(SupplierInvoice_Model data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Batchid, data.BatchGid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EcfRefid, data.Refid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Expenseid, data.Expenseid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Poid, data.Poid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, data._Action, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.UserGid, data.Loginid, DbType.String));
                ds = cmnParams.dbManager.GetDataSet(cmnParams.SpgetSupplierInv, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Expenses
        public DataTable SaveExpense(SupplierInvoice_Model data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EcfRefid, data.Refid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Invoiceid, data.Invoiceid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Expenseid, data.Expenseid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ExpNatureid, data.ExpNatureid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ExpCatid, data.ExpCatid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ExpSubcatid, data.ExpSubcatid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Glid, data.Glid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ExpAmt, data.ExpenseAmt, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_HsnId, data.HsnId, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ExpDesc, string.IsNullOrEmpty(data.ExpDesc) ? "0" : data.ExpDesc, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, data._Action, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.UserGid, data.Loginid, DbType.String));
                dt = cmnParams.dbManager.GetDataTable(cmnParams.SpSupplierInvExpense, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Creditline
        public DataTable SaveCredit(SupplierInvoice_Model data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EcfRefid, data.Refid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Invoiceid, data.Invoiceid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Crditid, data.Creditid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_CrAmt, data.CreditAmt, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_CrRefno, string.IsNullOrEmpty(data.AccountNo) ? "0" : data.AccountNo, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_CrBankid, data.Bankid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_CrBeneficiary, string.IsNullOrEmpty(data.Beneficiary) ? "0" : data.Beneficiary, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Glid, data.Glid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_CrPaymodeid, data.Paymodeid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_CrPaymode, string.IsNullOrEmpty(data.Paymode) ? "0" : data.Paymode, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_CrDescription, string.IsNullOrEmpty(data.CreditDesc) ? "0" : data.CreditDesc, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, data._Action, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.UserGid, data.Loginid, DbType.String));
                dt = cmnParams.dbManager.GetDataTable(cmnParams.SpSupplierInvCredit, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region Remarks
        public DataTable SaveRemarks(SupplierInvoice_Model data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EcfRefid, data.Refid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Ecfids, string.IsNullOrEmpty(data.Ecfids) ? "0" : data.Ecfids, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_Remarks, string.IsNullOrEmpty(data.EcfRemark) ? "0" : data.EcfRemark, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, data._Action, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.UserGid, data.Loginid, DbType.String));
                dt = cmnParams.dbManager.GetDataTable(cmnParams.SPSupplierInvRemark, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region Get Supplier
        public DataSet GetSupplier(SupplierInvoice_Model data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, data._Action, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Stateid, 0, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Supplierid, 0, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Currencyid, data.Currencyid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.UserGid, data.Loginid, DbType.String));
                ds = cmnParams.dbManager.GetDataSet(cmnParams.SPgetEowgstDropdown, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Get Raiser
        public DataSet GetRaiser(SupplierInvoice_Model data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, data._Action, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Stateid, 0, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Supplierid, 0, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Currencyid, data.Currencyid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.UserGid, data.Loginid, DbType.String));
                ds = cmnParams.dbManager.GetDataSet(cmnParams.SPgetEowgstDropdown, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Ramya Added
        public DataTable Get_Raiser_DropDown_List(string MasterName, Int64 Master_Gid, string EmpName)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_Claim_MasterName, MasterName, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_Claim_MasterGid, Master_Gid, DbType.Int64));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_Claim_Emp_Name, EmpName, DbType.String));
                dt = cmnParams.dbManager.GetDataTable(cmnParams.sp_Claim_Getdropdownlist, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion

        #region GetGstnDropdown
        public DataSet GetGstnDropdown(SupplierInvoice_Model data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, data._Action, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Stateid, data.Stateid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Supplierid, data.Supplierid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Currencyid, data.Currencyid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.UserGid, data.Loginid, DbType.String));
                ds = cmnParams.dbManager.GetDataSet(cmnParams.SPgetEowgstDropdown, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DropDown For Expense
        public DataSet GetExpDropDown(SupplierInvoice_Model data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, data._Action, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ExpCatid, data.ExpCatid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ExpSubcatid, data.ExpSubcatid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Batchid, data.BatchGid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_MastersCode, string.IsNullOrEmpty(data.MasterCode) ? "0" : data.MasterCode, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_HsnType, string.IsNullOrEmpty(data.HsnType) ? "0" : data.HsnType, DbType.String));
                //cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Glid, string.IsNullOrEmpty(data.Glid.ToString()) ? "0" : data.Glid.ToString(), DbType.String));
                //cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_GLName, string.IsNullOrEmpty(data.GlName) ? "0" : data.GlName, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.UserGid, data.Loginid, DbType.String));
                ds = cmnParams.dbManager.GetDataSet(cmnParams.SPgetExpDropDowns, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DropDown for Payment
        public DataSet GetCrDropDown(SupplierInvoice_Model data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, data._Action, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_CrPaymodeid, data.Paymodeid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Glid, data.Glid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Batchid, data.BatchGid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Supplierid, data.Supplierid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EcfRefid, data.Refid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.UserGid, data.Loginid, DbType.String));
                //Ramya added
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_GLName, string.IsNullOrEmpty(data.GlName) ? "" : data.GlName, DbType.String));
                ds = cmnParams.dbManager.GetDataSet(cmnParams.SPgetCreditDropDowns, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Submit Invoice Details
        public DataTable SubmitInvoice(SupplierInvoice_Model data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, cmnParams.Insert, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Batchid, data.BatchGid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Ecfids, string.IsNullOrEmpty(data.Ecfids) ? "0" : data.Ecfids, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_QueueRemark, string.IsNullOrEmpty(data.QueueRemark) ? "0" : data.QueueRemark, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.UserGid, data.Loginid, DbType.String));
                dt = cmnParams.dbManager.GetDataTable(cmnParams.SPSubmitSupplierInv, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable ApproveInvoice(SupplierInvoice_Model data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Ecfids, data.Refid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Batchids, string.IsNullOrEmpty(data.Batchids) ? "0" : data.Batchids, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_Ecfdesignation, string.IsNullOrEmpty(data.Ecfdesignation) ? "0" : data.Ecfdesignation, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_EMPdelmattype, string.IsNullOrEmpty(data.EMPdelmattype) ? "0" : data.EMPdelmattype, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_SUPdelmattype, string.IsNullOrEmpty(data.SUPdelmattype) ? "0" : data.SUPdelmattype, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_EcfInprocess, string.IsNullOrEmpty(data.EcfInprocess) ? "0" : data.EcfInprocess, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_EcfApproved, string.IsNullOrEmpty(data.EcfApproved) ? "0" : data.EcfApproved, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_EcfHold, string.IsNullOrEmpty(data.EcfHold) ? "0" : data.EcfHold, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_EcfConcurrentApproval, string.IsNullOrEmpty(data.EcfConcurrentApproval) ? "0" : data.EcfConcurrentApproval, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_EcfRejected, string.IsNullOrEmpty(data.EcfRejected) ? "0" : data.EcfRejected, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_centralckeckerreject, string.IsNullOrEmpty(data.centralckeckerreject) ? "0" : data.centralckeckerreject, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_centralmaker, string.IsNullOrEmpty(data.centralmaker) ? "0" : data.centralmaker, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_Status, string.IsNullOrEmpty(data.Status) ? "0" : data.Status, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_Delegates, string.IsNullOrEmpty(data.Delegates) ? "0" : data.Delegates, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_Remarks, string.IsNullOrEmpty(data.Remarks) ? "0" : data.Remarks, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.UserGid, data.Loginid, DbType.String));
                dt = cmnParams.dbManager.GetDataTable(cmnParams.SPApproveSupplierInv, CommandType.StoredProcedure, cmnParams.parameters.ToArray());

              /*cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_QUEUEGID, cmnParams.Insert, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_QUEUEACTIONBY, data.BatchGid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_QUEUEACTIONSTATUS, data.Ecfids, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_ECFGID, data.Loginid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_INVOICEID, cmnParams.Insert, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_ECFAMOUNT, data.BatchGid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_DELEGATE, data.Ecfids, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_ECFRAISER, data.Loginid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_ECFREMARK, cmnParams.Insert, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_titlevalues, data.BatchGid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_RESULT, data.Ecfids, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_Additionalflagnewq, data.Loginid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_ECFDESC, data.BatchGid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_ACTION, data.Ecfids, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_loginuserid, data.Loginid, DbType.String));*/
                //dt = cmnParams.dbManager.GetDataTable(cmnParams.SPApproveSupplierInv, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion
		
		//Ramya COA
        #region DropDown For COA
        public DataSet GetCOADropDown(SupplierInvoice_Model data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, data._Action, DbType.String));
                ds = cmnParams.dbManager.GetDataSet(cmnParams.SPGetCOADropDowns, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetReferenceNoDropDown(SupplierInvoice_Model data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, data._Action, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Batch_Gid, data.BatchGid, DbType.Int64));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ECF_Gid, data.Refid, DbType.Int64));
                ds = cmnParams.dbManager.GetDataSet(cmnParams.SPGetReferenceNoDropDown, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetCOA(SupplierInvoice_Model data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Expenseid, data.Expenseid, DbType.String));
                ds = cmnParams.dbManager.GetDataSet(cmnParams.SpGetCOAGrid, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public DataTable SaveCOA(SupplierInvoice_Model data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, "C", DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_COA_gid, data.COAid, DbType.Int64));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_COA_Expense_Gid, data.Expenseid, DbType.Int64));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_COA_GL_No, string.IsNullOrEmpty(data.GlName) ? "0" : data.GlName, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_COA_FC_Code, string.IsNullOrEmpty(data.FC_Code) ? "0" : data.FC_Code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_COA_CC_Code, string.IsNullOrEmpty(data.CC_Code) ? "0" : data.CC_Code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_COA_Product_Code, string.IsNullOrEmpty(data.Product_Code) ? "0" : data.Product_Code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_COA_OU_Code, string.IsNullOrEmpty(data.OU_Code) ? "0" : data.OU_Code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_COA_Amount, data.COAAmount, DbType.Decimal));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.LoggedUser_Gid, data.Loginid, DbType.Int64));
                dt = cmnParams.dbManager.GetDataTable(cmnParams.SpEOWSetCOAGrid, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable Remove_COA(Int64 COA_Gid)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, cmnParams.Delete, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_COA_gid, COA_Gid, DbType.Int64));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_COA_Expense_Gid, 0, DbType.Int64));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_COA_GL_No, "", DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_COA_FC_Code, "", DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_COA_CC_Code, "", DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_COA_Product_Code, "", DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_COA_OU_Code, "", DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_COA_Amount, 0, DbType.Decimal));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.LoggedUser_Gid, 1, DbType.Int64));
                dt = cmnParams.dbManager.GetDataTable(cmnParams.SpEOWSetCOAGrid, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetPaymodeDetails(SupplierInvoice_Model _data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, _data._Action, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_CrPaymodeid, _data.Paymodeid, DbType.Int64));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Glid, _data.Glid, DbType.Int64));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Batchid, _data.BatchGid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Supplierid, _data.Supplierid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EcfRefid, _data.Refid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_GLName, _data.GlName, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.UserGid, _data.Loginid, DbType.Int64));
                dt = cmnParams.dbManager.GetDataTable(cmnParams.SPgetCreditDropDowns, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region TDS
        public DataSet GetTaxDetails(SupplierInvoice_Model _data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EcfRefid, _data.Refid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Invoiceid, _data.Invoiceid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Taxid, _data.Taxid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Subtaxid, _data.SubTaxid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, _data._Action, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.UserGid, _data.Loginid, DbType.Int64));
                ds = cmnParams.dbManager.GetDataSet(cmnParams.SPGetSupplierInvTax, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable SaveTax(SupplierInvoice_Model _data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EcfRefid, _data.Refid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Invoiceid, _data.Invoiceid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Taxid, _data.Taxid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Subtaxid, _data.SubTaxid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_TaxableAmt, _data.TaxableAmt, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_TaxRate, _data.TaxRate, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_InvTaxid, _data.InvTaxid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, _data._Action, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.UserGid, _data.Loginid, DbType.Int64));
                dt = cmnParams.dbManager.GetDataTable(cmnParams.SPSetSupplierInvTax, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Bulk Debitline Upload
        public DataTable Save_UploadDebitline(SupplierInvoice_Model data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_Batch_Gid, data.BatchGid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_Ecf_No, string.IsNullOrEmpty(data.RefrenceNo) ? "0" : data.RefrenceNo, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ExpNatureName, string.IsNullOrEmpty(data.ExpNaturename) ? "0" : data.ExpNaturename, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ExpCatName, string.IsNullOrEmpty(data.ExpCatname) ? "0" : data.ExpCatname, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ExpSubcatName, string.IsNullOrEmpty(data.ExpSubcatname) ? "0" : data.ExpSubcatname, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ExpDesc, string.IsNullOrEmpty(data.ExpDesc) ? "0" : data.ExpDesc, DbType.String));
               /* cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_FCCode, data.FC_Code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_CCCode, data.CC_Code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ProductCode, data.Product_Code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_OUCode, data.OU_Code, DbType.String));*/
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ExpAmt, data.ExpenseAmt, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_HsnCode, string.IsNullOrEmpty(data.HsnName) ? "0" : data.HsnName, DbType.String)); 
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, "I", DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.UserGid, data.Loginid, DbType.String));
                dt = cmnParams.dbManager.GetDataTable(cmnParams.SpSupplierInvBulkExpense, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable Save_UploadInvoice(SupplierInvoice_Model data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Invno, string.IsNullOrEmpty(data.InvoiceNo) ? "0" : data.InvoiceNo, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_InvDate, cmnObj.ConvertDate(Convert.ToString(data.InvoiceDate).Substring(0,10)), DbType.String)); 
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_InvAmt, data.InvoiceAmt, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ProviderLoc, string.IsNullOrEmpty(data.ProviderLoc) ? "0" : data.ProviderLoc, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ReceiverLoc, string.IsNullOrEmpty(data.ReceiverLoc) ? "0" : data.ReceiverLoc, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Taxstatus, string.IsNullOrEmpty(data.TaxStatus) ? "0" : data.TaxStatus, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Currencyid, data.Currencyid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Suppliercode, string.IsNullOrEmpty(data.SupplierCode) ? "0" : data.SupplierCode, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_InvDesc, string.IsNullOrEmpty(data.InvDescription) ? "0" : data.InvDescription, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_InvDoctype, data.Type_Gid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_InvDocsubtype, data.SubType_Gid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Raiserid, data.Raiserid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Amortflag, string.IsNullOrEmpty(data.AmortFlag) ? "0" : data.AmortFlag, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Amrtfrom, data.AmortFrom, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Amortto, data.AmortTo, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_AmortDesc, string.IsNullOrEmpty(data.AmortDesc) ? "0" : data.AmortDesc, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Batchid, data.BatchGid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_TotalAmt, data.InvoiceAmt, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Urgentflag, string.IsNullOrEmpty(data.UrgentFlag) ? "0" : data.UrgentFlag, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, "I", DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ProvisionFlag, string.IsNullOrEmpty(data.ProvisionFlag) ? "0" : data.ProvisionFlag, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Retentionflag, string.IsNullOrEmpty(data.RetentionFlag) ? "0" : data.RetentionFlag, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EcfRefid, data.Refid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_RetentionRate, data.RetentionRate, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_RetentionAmt, data.RetentionAmt, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_RetentionException, data.RetentionException, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.UserGid, data.Loginid, DbType.String));
                dt = cmnParams.dbManager.GetDataTable(cmnParams.SpSupplierInvBulkInvoice, CommandType.StoredProcedure, cmnParams.parameters.ToArray());               
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable Save_UploadCOA(SupplierInvoice_Model data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, "C", DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_Batch_Gid, data.BatchGid, DbType.Int64));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_Type_Gid, data.Type_Gid, DbType.Int64));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_SubType_Gid, data.SubType_Gid, DbType.Int64));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.p_ReferenceNo, string.IsNullOrEmpty(data.RefrenceNo) ? "0" : data.RefrenceNo, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_COA_GL_No, string.IsNullOrEmpty(data.GL_No) ? "0" : data.GL_No, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_COA_FC_Code, string.IsNullOrEmpty(data.FC_Code) ? "0" : data.FC_Code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_COA_CC_Code, string.IsNullOrEmpty(data.CC_Code) ? "0" : data.CC_Code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_COA_Product_Code, string.IsNullOrEmpty(data.Product_Code) ? "0" : data.Product_Code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_COA_OU_Code, string.IsNullOrEmpty(data.OU_Code) ? "0" : data.OU_Code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_COA_Amount, data.COAAmount, DbType.Decimal));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.LoggedUser_Gid, data.Loginid, DbType.Int64));
                dt = cmnParams.dbManager.GetDataTable(cmnParams.SpEOWSetCOAUpload, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
#endregion
        #region PO Mapping
        public DataTable SetPODetails(SupplierInvoice_Model data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Grndetails, data.Grndetids, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EcfRefid, data.Refid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.LoggedUser_Gid, data.Loginid, DbType.Int64));
                dt = cmnParams.dbManager.GetDataTable(cmnParams.SpSupplierInvPomapping, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public DataTable PoMappingDeletion(SupplierInvoice_Model data)
        //{
        //    try
        //    {
        //        cmnParams.parameters = new List<IDbDataParameter>();
        //        cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_InvPoitemid, data.Invoicepoitemid, DbType.String));
        //        cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.LoggedUser_Gid, data.Loginid, DbType.Int64));
        //        dt = cmnParams.dbManager.GetDataTable(cmnParams.SpSupplierInvExpense, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
        //        return dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        #endregion
    }
}