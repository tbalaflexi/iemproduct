﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHandler.Models.FlexiBuy
{
    public class POUpload_Model
    {
        public Int64 LoginId { get; set; }
        public string Action { get; set; }

        public Int64 SlNo { get; set; }
        public Int64 poupload_gid {get;set;}
        public Int64 File_Gid { get; set; }
        public string Poheader_pono { get; set; }
        public string poheader_desc { get; set; }
        public string  poheader_period_from { get; set; }
        public string poheader_period_to { get; set; }
        public string poheader_vendor_code { get; set; }
        public string poheader_vendor_contact_code { get; set; }
        public string poheader_type { get; set; }
        public string poheader_request { get; set; }
        public string poheader_raiser_code { get; set; }
        public string podts_item_desc { get; set; }
        public string podts_item_Qty { get; set; }
        public string podts_unit_price { get; set; }
        public string podts_item_total { get; set; }
        public string podts_service_code { get; set; }
        public string podts_uom_code { get; set; }
        public string podts_shipment_type { get; set; }
        public string podts_shipment_branch_code { get; set; }
        public string podts_shipment_emp_code { get; set; }
        public string grninwrdheader_dcno { get; set; }
        public string grninwrdheader_invoiceno { get; set; }
        public string grn_inward_qty { get; set; }
        public string asset_sl_no { get; set; }
        public string grn_putuseddate { get; set; }
        public string grn_mft_name { get; set; }
        public string podts_gst_applicable { get; set; }
        public string HSN_Code { get; set; }
        public string podts_tax_amount { get; set; }
        public string provider_loc { get; set; }
        public string receiver_loc { get; set; }
        public string cwiprelease_branch_code { get; set; }
        public string cwiprelease_emp_code { get; set; }     
        public string poupload_status { get; set; }
        public string poupload_remarks { get; set; }
        public string poupload_move_flag { get; set; }
        public string poupload_valid_flag { get; set; }

        public string POUpload_FileName { get; set; }
        public string POUpload_NewFileName { get; set; }
        public string POUpload_FileType { get; set; }
        public Int64  FileCount { get; set; }
        public string ServerPath { get; set; }
      
      

    }
}
