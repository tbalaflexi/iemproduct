﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHandler.Models.EOW
{
  public class PhysicalDespatch_Model
    {
        public Int64    id{ get; set; }
        public Int64    ecfID{ get; set; }
        public string   ecfNumber{ get; set; }
        public string   ecfSubType{ get; set; }
        public Int64    raiserID{ get; set; }
        public string   raiserName{ get; set; }
        public Int64    ecfEmployeeGid { get; set; }
        public  string  ecfEmployeeName{get;set;}
        public Int64    supplierID{ get; set; }
        public string   supplierCode{ get; set; }
        public string   supplierName{ get; set; }
        public Int64    ecfStatusID{ get; set; }
        public string   ecfStatus{ get; set; }
        public string   ecfDate { get; set; }
        public string   ecfAmount{ get; set; }
        public Boolean  ecfSelectedId{ get; set; }
        public string   ecfSelectedList { get; set; }
        public string   ecfDespatchDate{ get; set; }
        public Int64    ecfDespatchModeID{ get; set; }
        public string   ecfDespatchModeName{ get; set; }
        public string   despatchEWBNumber{ get; set; }
        public Int64    loginUserID { get; set; }
        public string   actionType { get; set; }
        public Int64 batchID { get; set; }
    }
}
