﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DataAccessHandler.Models.EOW
{
    public class Claim_Model
    {

        #region Header Parameters
        public string Button_Action { get; set; }
        public Int64 Common_Gridgid { get; set; }
        public string Common_Gridname { get; set; }
        public Int64 HDR_Type_Gid { get; set; }
        public string HDR_Type_Name { get; set; }
        public Int64 HDR_SubType_Gid { get; set; }
        public string HDR_SubType_Name { get; set; }
        public Int64 HDR_Raiser_Gid { get; set; }
        public string HDR_Raiser { get; set; }
        public Int64 HDR_Batch_Gid { get; set; }
        public string HDR_Batch_No { get; set; }
        public decimal HDR_Batch_Amount { get; set; }
        public Int64 Logged_UserGid { get; set; }
        public Int64 HDR_EcfGId { get; set; }
        public string HDR_Status { get; set; }
        public string HDR_EmployeeIds { get; set; }
        public string Approver_Remarks { get; set; }
        public Int16 TaxCount { get; set; }
        public Int16 AttachmentCount { get; set; }
        public Int16 DetailCount { get; set; }
        public string Display_Batch_Status { get; set; }
        #endregion

        #region Claim Parameters
        public Int64 CLM_Ecf_Gid { get; set; }
        public string CLM_Ecf_No { get; set; }
        public Int64 CLM_Emp_Gid { get; set; }
        public string CLM_Emp_Code { get; set; }

        [Required(ErrorMessage = "Claim Date Cannot be blank.!")]
        public string CLM_Claim_Date { get; set; }

        [Required(ErrorMessage = "Claim Month Cannot be blank.!")]
        public string CLM_Claim_Month { get; set; }

        [Required(ErrorMessage = "Claim Amount Cannot be blank.!")]
        [Range(1, 99999999, ErrorMessage = "Claim Amount should be minimum 1 and not more than 99999999.!")]
        public double CLM_Claim_Amount { get; set; }
        public string CLM_Claim_Description { get; set; }
        public string CLM_Add_EmpGid { get; set; }
        public string CLM_Add_EmpName { get; set; }
        public string CLM_Create_Mode { get; set; }
        public Int64 CLM_Person_Count { get; set; }
        #endregion

        #region Expense Parameters
        public Int64 Exp_gid { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please select the Ref No.!")]
        public Int64 Exp_ecfgid { get; set; }
        public string Exp_ecfno { get; set; }

        [Required(ErrorMessage = "Invoice No Cannot be blank.!")]
        public string Exp_invoiceno { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please select the Nature of Expense.!")]
        public Int64 Exp_natureexpgid { get; set; }
        public string Exp_natureexpname { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please select the Expense Category.!")]
        public Int64 Exp_catggid { get; set; }
        public string Exp_catgname { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please select the Expense Sub-Category.!")]
        public Int64 Exp_subcatggid { get; set; }
        public string Exp_subcatgname { get; set; }

        [Required(ErrorMessage = "Invoice No Cannot be GL Code.!")]
        public string Exp_glcode { get; set; }

        [Required(ErrorMessage = "Debit Amount Cannot be blank.!")]
        [Range(1, 99999999, ErrorMessage = "Debit Amount should be minimum 1 and not more than 99999999.!")]
        public double Exp_debitamount { get; set; }

        [Required(ErrorMessage = "Please select Tax Type.!")]
        public string Exp_taxtype { get; set; }
        public string Exp_taxtypetext { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please select the HSN.!")]
        public Int64 Exp_hsngid { get; set; }
        public string Exp_hsnname { get; set; }
        #endregion

        #region Payment Parameters
        public Int64 Pay_gid { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please select the Ref No.!")]
        public Int64 Pay_ecfgid { get; set; }
        public string Pay_ecfno { get; set; }
        public string Pay_paymodemode { get; set; }
        public string Pay_paymodename { get; set; }
        public string Pay_beneficiary { get; set; }
        [Required(ErrorMessage = "Credit Amount field could not blank.!")]
        [Range(1, 99999999, ErrorMessage = "Amount should be minimum 1 and not more than 99999999.!")]
        public double Pay_creditamount { get; set; }
        [Required(ErrorMessage = "Payment Ref No field could not blank.!")]
        public string Pay_referenceno { get; set; }
        public string Pay_paymentdate { get; set; }
        public string Pay_paymentstatus { get; set; }
        public string Pay_glcode { get; set; }
        public string Pay_glname { get; set; }
        #endregion

        #region Remarks Parameters.
        public Int64 Rem_gid { get; set; }
        public string Rem_ecfno { get; set; }
        public string Rem_status { get; set; }
        [Required(ErrorMessage = "Remarks field can not be blank.!")]
        public string Rem_remarks { get; set; }
        #endregion

        #region Details Parametrs.
        public Int64 Det_gid { get; set; }

        public Int64 Det_debilnegid { get; set; }
        public Int64 Det_ecfgid { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please select the Type.!")]
        public Int64 Det_typegid { get; set; }
        public string Det_typename { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Please select the Travel Class.!")]
        public Int64 Det_travelclassgid { get; set; }
        public string Det_travelclassname { get; set; }
        [Required(ErrorMessage = "Please select the Date From.!")]
        public string Det_datefrom { get; set; }
        [Required(ErrorMessage = "Please select the Date To.!")]
        public string Det_dateto { get; set; }
        [Required(ErrorMessage = "Place From field can not be blank.!")]
        public string Det_placefrom { get; set; }
        [Required(ErrorMessage = "Place to field can not be blank.!")]
        public string Det_placeto { get; set; }
        [Required(ErrorMessage = "Hotel field can not be blank.!")]
        public string Det_hotel { get; set; }
        [Required(ErrorMessage = "KM field can not be blank.!")]
        public double Det_km { get; set; }
        [Required(ErrorMessage = "Rate field can not be blank.!")]
        public double Det_rate { get; set; }
        [Required(ErrorMessage = "Amount field could not blank.!")]
        [Range(1, 99999999, ErrorMessage = "Amount should be minimum 1 and not more than 99999999.!")]
        public double Det_amount { get; set; }
        public string Det_type { get; set; }
        public Int16 Det_gridtypegid { get; set; }
        public string Det_gridtypename { get; set; }
        #endregion

        #region Tax Parameters.
        public Int64 Tax_suppliergid { get; set; }
        public string Tax_suppliername { get; set; }
        public string Tax_providergstin { get; set; }
        public Int64 Tax_providerlocationgid { get; set; }
        public string Tax_providerlocationname { get; set; }
        public string Tax_receivergstin { get; set; }
        public Int64 Tax_receiverlocationgid { get; set; }
        public string Tax_receiverlocationname { get; set; }
        public string Tax_invoiceno { get; set; }
        public string Tax_invoicedate { get; set; }
        public double Tax_invoiceamount { get; set; }
        public string Hidden_debitlinegids { get; set; }
        public Int64 Hidden_ecfgid { get; set; }
        public string Hidden_taxtype { get; set; }

        public Int64 Taxable_debitlinegid { get; set; }
        public string Taxable_glcodedesc { get; set; }
        public string Taxable_hsncode { get; set; }
        public string Taxable_hsndesc { get; set; }
        public string Taxable_subtype { get; set; }
        public double Taxable_amount { get; set; }
        public double Taxable_rate { get; set; }
        public double Taxable_gsttaxamount { get; set; }
        #endregion

        #region PPX Entities.
        public Int64 ppx_ecfgid { get; set; }
        public string ppx_ecfno { get; set; }
        public string ppx_beneficiary { get; set; }
        public string ppx_beneficiaryaccount { get; set; }
        public Int64 ppx_ecfarfgid { get; set; }
        public decimal ppx_arfamount { get; set; }
        public decimal ppx_arfexception { get; set; }
        public string ppx_arfglno { get; set; }
        #endregion

        #region Bulk upload
        public string HDR_Raiser_Code { get; set; }
        public string HDR_Employee_Codes { get; set; }
        #endregion
    }

    public class APCheckerEntities
    {
        public Int64 CheckList_Gid { get; set; }
        public string CheckList_Name { get; set; }
        public string CheckList_Reason { get; set; }
        public string CheckList_Flag { get; set; }
        public bool CheckList_IsChecked { get; set; }

        public Int64 CheckList_ecf_gid { get; set; }
        public string CheckList_status { get; set; }
        public Int64 logged_user_gid { get; set; }

        public string CheckList_notok_reason { get; set; }

        public string AP_Action { get; set; }
        public string AP_Remarks { get; set; }
        public string ecfgids { get; set; }
        public string gldate { get; set; }
    }   
}
