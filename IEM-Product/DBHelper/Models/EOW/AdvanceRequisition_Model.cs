﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHandler.Models.EOW
{
    public class AdvanceRequisition_Model
    {
        #region Header Entities.
        public string PageStatus { get; set; }
        public Int64 Type_Gid { get; set; }
        public string Type_Name { get; set; }
        public Int64 SubType_Gid { get; set; }
        public string SubType_Name { get; set; }
        public Int64 Employee_Gid { get; set; }
        public string Employee_Name { get; set; }
        public Int64 Supplier_Gid { get; set; }
        public string Supplier_Name { get; set; }
        public string Batch_No { get; set; }
        public double Batch_Amount { get; set; }
        public Int64 Batch_Gid { get; set; }
        public Int64 Ecf_Gid { get; set; }
        public Int64 Logged_UserGid { get; set; }
        public string Sup_Emp_Type { get; set; }
        public string Display_Batch_Status { get; set; }
        #endregion

        #region Debitline Entities.
        public Int64 Arf_Gid { get; set; }
        public Int64 Ref_Gid { get; set; }
        public string Ref_No { get; set; }
        [Required(ErrorMessage = "Emp/Supplier Code field could not blank.!")]
        public string Emp_Supl_Code { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Please select the Advance Type.!")]
        public Int64 AdvanceType_Gid { get; set; }
        public string AdvanceType_Name { get; set; }
        [Required(ErrorMessage = "GL Code field could not blank.!")]
        public string GL_CodeDesc { get; set; }
        [Required(ErrorMessage = "Description field could not blank.!")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Advance Amount Cannot be blank.!")]
        [Range(1, 99999999, ErrorMessage = "Advance Amount should be minimum 1 and not more than 99999999.!")]
        public double Advance_Amount { get; set; }
        [Required(ErrorMessage = "Liquidation Date field could not blank.!")]
        public string Liquidation_Date { get; set; }
        public Int64 Debitline_Gid { get; set; }
        public Int64 Invoice_Gid { get; set; }
        public string Approver_Remarks { get; set; }
        #endregion
    }
}
