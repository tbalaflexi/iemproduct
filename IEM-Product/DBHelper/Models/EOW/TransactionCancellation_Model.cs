﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHandler.Models.EOW
{
   public class TransactionCancellation_Model
    {
        public Int64 id { get; set; }
        public string ecf_no { get; set; }
        public string ClaimType { get; set; }
        public string raiser { get; set; }
        public string suppliername { get; set; }
        public string ecf_amount { get; set; }
        public string ecf_status { get; set; }
        public string Action { get; set; }
        public Int64 Login_UserID { get; set; }
        public Int64 Raiser_Gid { get; set; }
        public Int64 Ecf_Gid { get; set; }
        public Int64 Batch_Gid { get; set; }
        public string Approver_Status { get; set; }

        public Int64 invoice_gid { get; set; }
        public Int64 creditline_gid { get; set; }
        public Int64 debitline_gid { get; set; }
        public Int64 invoicetax_gid { get; set; }
        public Int64 COA_gid { get; set; }

        public string result { get; set; }
        public Int64 Logged_UserGid { get; set; }
        public string ecfid { get; set; }

        public string claim { get; set; }
       
    }
}
