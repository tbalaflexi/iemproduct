﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHandler.Models.EOW
{
    public class SupplierInvoice_Model
    {
        public string Type_Name { get; set; }
        public string Type_Code { get; set; }
        public Int64 Type_Gid { get; set; }

        public string SubType_Name { get; set; }
        public string SubType_Code { get; set; }
        public Int64 SubType_Gid { get; set; }

        public string Code { get; set; }
        public string CodeName { get; set; }
        public string Codeid { get; set; }
        public string MasterCode { get; set; }

        //[UIHint("TDSType")]
        public string TaxName { get; set; }
        public string TaxCode { get; set; }
        public Int64 Taxid { get; set; }

        //[UIHint("TDSSubType")]
        public string SubTaxName { get; set; }
        public string SubTaxCode { get; set; }
        public Int64 SubTaxid { get; set; }

        public Int64 Loginid { get; set; }
        public string Display_Batch_Status { get; set; }
        public string Values { get; set; }

        public string TranId { get; set; }
        // public Int64 TranId { get; set; }
        [UIHint("GetTranTypeDropDown")]
        public string Tran_Type { get; set; }
        public string TransubId { get; set; }
        [UIHint("GetTranSubTypeDropdown")]
        public string Tran_SubType { get; set; }


        public string RefrenceNo { get; set; }
        public string BatchNo { get; set; }
        public Int64 BatchGid { get; set; }
        public double BatchAmt { get; set; }
        public Int64 Refid { get; set; }
        public string Ecfids { get; set; }
        public Int64 SupplierGid { get; set; }
        public Int64 Supplierid { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        [Required(ErrorMessage = "Invoice No should not be blank.!")]
        public string InvoiceNo { get; set; }
        public Int64 Invoiceid { get; set; }
        public string InvoiceDate { get; set; }
        public string InvServiceMonth { get; set; }
        public decimal InvoiceAmt { get; set; }
        public string InvDescription { get; set; }
        public string TaxStatus { get; set; }
        public string GstStatus { get; set; }
        public string RcmStatus { get; set; }
        public string ProviderLoc { get; set; }
        public Int64 ProvLocid { get; set; }
        public string ReceiverLoc { get; set; }
        public Int64 RecvLocid { get; set; }
        public string ProviderGstn { get; set; }
        public string ReceiverGstn { get; set; }
        public Int64 Currencyid { get; set; }
        public string CurrencyName { get; set; }
        public decimal CurrencyRate {get;set;}
        public Int64 Raiserid { get; set; }
        public string RaiserName { get; set; }
        public string UrgentFlag { get; set; }
        public string _Action { get; set; }
        public string ProvisionFlag { get; set; }
        public string Clear { get; set; }
        public string Message { get; set; }
        public string GSTNType { get; set; }
        public Int64 Stateid { get; set; }
        public string GridType { get; set; }
        public string EcfRemarks { get; set; }
        public Int64 InvTaxid { get; set; }
        public string TypeFlag { get; set; }
        public Int16 TdsCount { get; set; }
        public Int16 AttachmentCount { get; set; }
        public Int16 TaxCount { get; set; }


        /*Amort*/
        public string AmortFlag { get; set; }
        public string AmortFrom { get; set; }
        public string AmortTo { get; set; }
        public string AmortDesc { get; set; }

        /*Retention*/
        public string RetentionFlag { get; set; }
        public decimal RetentionRate { get; set; }
        public decimal RetentionAmt { get; set; }
        public decimal RetentionException { get; set; }
        

        /*Expense Grid*/
        public Int64 ExpNatureid { get; set; }
        public string ExpNaturename { get; set; }
        public Int64 ExpCatid { get; set; }
        public string ExpCatname { get; set; }
        public Int64 ExpSubcatid { get; set; }
        public string ExpSubcatname { get; set; }
        public Int64 Glid { get; set; }
        public string GlName { get; set; }
        public decimal ExpenseAmt { get; set; }
        public Int64 AssetCatid { get; set; }
        public string AssetCatName { get; set; }
        public Int64 AssetSubcatid { get; set; }
        public string AssetSubcatname { get; set; }
        public Int64 AssetGlid { get; set; }
        public string AssetGlName { get; set; }
        public Int64 HsnId { get; set; }
        public string HsnName { get; set; }
        public string DropDowntype { get; set; }
        public string ExpDesc { get; set; }
        public Int64 Expenseid { get; set; }
        public decimal ExpAmt { get; set; }
        public string Tax { get; set; }
        public string HsnType { get; set; }
        public Int64 Invoicepoitemid { get; set; }

        /*Creditline Grid*/
        public Int64 Creditid { get; set; }
        public Int64 Bankid { get; set; }
        public string BankName { get; set; }
        public string Beneficiary { get; set; }
        public string IfscCode { get; set; }
        public decimal CreditAmt { get; set; }
        public string AccountNo { get; set; }
        public string Paymode { get; set; }
        public Int64 Paymodeid { get; set; }
        public string CreditDesc { get; set; }
        public string Ecfstatus { get; set; }
        public string EcfRemark { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public Int64 BranchId { get; set; }
        //[Required(ErrorMessage = "Remark is required")]
        public string QueueRemark { get; set; }

        /*Tax Grid*/
        public decimal TaxableAmt { get; set; }
        public decimal TaxAmt { get; set; }
        public string TaxType { get; set; }
        public decimal TaxRate { get; set; }
        public string TaxGl { get; set; }
        public string Data { get; set; }

        /*PPX Adjustment*/
        public decimal EcfarfAmt { get; set; }
        public decimal ExceptionAmt { get; set; }
        public string ArfDesc { get; set; }
        public Int64 ArfId { get; set; }
        public DateTime LiqudatonDate { get; set; }
		/*COA Grid*/
        public Int64 COAid { get; set; }
        // public string GlName { get; set; } 
        [Required(ErrorMessage = "FC is required")]
        [UIHint("FC")]
        public string FC_Code { get; set; }
        [UIHint("CC")]
        public string CC_Code { get; set; }
        [UIHint("Product")]
        public string Product_Code { get; set; }
        [UIHint("OU")]
        public string OU_Code { get; set; }

        [Required(ErrorMessage = "Amount is required")]
        [Range(0.01, 999999999, ErrorMessage = "Amount must be greater than 0.00")]
        [RegularExpression(@"^\d+(\.\d{1,2})?$", ErrorMessage = "Invalid Amount")]
        public decimal COAAmount { get; set; }

        public Int64 FC_gid { get; set; }
        public Int64 CC_gid { get; set; }
        public Int64 Product_gid { get; set; }
        public Int64 OU_gid { get; set; }
        public Int64 ecf_gid { get; set; }
        public string ecf_code { get; set; }
        public Int64 GL_Gid { get; set; }
        public string GL_No { get; set; }

        //Ramya For Approve Claim
        public Int64 approver_gid { get; set; }
        public Int64 queue_gid { get; set; }

        public string PageFor { get; set; }

        public string Batchids{get;set;}
        public string Ecfdesignation{get;set;}
        public string EMPdelmattype{get;set;}
        public string SUPdelmattype{get;set;}
        public string EcfInprocess{get;set;}
        public string EcfApproved{get;set;}
        public string EcfHold{get;set;}
        public string EcfConcurrentApproval{get;set;}
        public string EcfRejected{get;set;}
        public string centralckeckerreject{get;set;}
        public string centralmaker{get;set;}
        public string Status{get;set;}
        public string Delegates{get;set;}
        public string Remarks{get;set;}

        public Int64 HDR_EcfGId { get; set; }

        //Bulk Upload
        public string UploadMethod { get; set; }
        public string EmployeeCode { get; set; }

        public string CLM_Create_Mode { get; set; }
        public string Claimtype { get; set; }

        //PO Mapping
        public Int64 Poid { get; set; }
        public Int64 Podetid { get; set; }
        public string PoNo { get; set; }
        public string PoDesc { get; set; }
        public decimal Pototalamt { get; set; }
        public decimal PodetTotamt { get; set; }
        public string ProdServName { get; set; }
        public decimal PodetOrdQty { get; set; }
        public decimal PodetUnitprice { get; set; }
        public decimal GrndetRecQty { get; set; }
        public Int64 GrnHeadid { get; set; }
        public Int64 Grndetid { get; set; }
        public decimal PodetUnit { get; set; }
        public string GrnRefno { get; set; }
        public decimal BalanceQty { get; set; }
        public decimal CurrentQty { get; set; }
        public string Grndetids { get; set; }
        public decimal MappedAmt { get; set; }
        public decimal MappedQty { get; set; }
        public Int64 InvPOCount { get; set; }

        public Int64 HDR_Raiser_Gid { get; set; }
        public string HD_Sus_Selected_GL { get; set; }
    }
}
