﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBHelper
{
    public class Transaction_Model
    {
        public Int64 TranGId { get; set; }
      // public string TranId { get; set; }
        // [Range(1, int.MaxValue, ErrorMessage = "Select  Transaction Type.!")]
          [Range(1, int.MaxValue, ErrorMessage = "Select  Transaction Type.!")]
        public Int64 TranId { get; set; }

        [UIHint("GetTranTypeDropDown")]
        public string Tran_Type { get; set; }
       // public string TransubId { get; set; }
       // [Range(1, int.MaxValue, ErrorMessage = "Select Transaction Sub Type.!")]

        [Range(1, int.MaxValue, ErrorMessage = "Select Transaction Sub Type.!")]
        public Int64 TransubId { get; set; }
       // [Range(1, int.MaxValue, ErrorMessage = "Select Transaction Sub Type.!")]
        [UIHint("GetTranSubTypeDropdown")]
        public string Tran_SubType { get; set; }
        [UIHint("GetAttachementDropdown")]
        public string Tran_Attachement { get; set; }
        [UIHint("physicalReceiptingDropdown")]
        public string Tran_PhysicalReceipting { get; set; }
        public string Tran_InsertedBy { get; set; }
        public string Tran_InsertedDate { get; set; }
        public string Tran_ModifiedBy { get; set; }
        public string Trab_ModifiedDate { get; set; }
        public string DeletedBy { get; set; }
        public string Master_Desc { get; set; }
        public string result { get; set; }
    }
}
