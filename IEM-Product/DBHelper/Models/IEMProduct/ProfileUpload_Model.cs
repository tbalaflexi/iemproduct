﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHandler.Models.IEMProduct
{
   public class ProfileUpload_Model
    {
        public Int64 LoginId { get; set; }
        public string Action { get; set; }
        public string Emp_Password { get; set; }

        public Int64 EmpUpload_Gid { get; set; }
        public Int64 File_Gid { get; set; }
        public Int64 SlNo { get; set; }
        public string Emp_Code { get; set; }
        public string Emp_Name { get; set; }
        public string Emp_Gender { get; set; }
        public string Emp_Dob { get; set; }
        public string Emp_Doj { get; set; }
        public string Designation_code { get; set; }
        public string Grade_code { get; set; }
        public string Dept_Code { get; set; }
        public string Emp_Branch_Code { get; set; }
        public string FC_Code { get; set; }
        public string CC_Code { get; set; }
        public string Product_Code { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string City_Code { get; set; }
        public string Pincode { get; set; }
        public string Personal_email { get; set; }
        public string Office_Email { get; set; }
        public string Mobil_No { get; set; }
        public string Bank_Code { get; set; }
        public string Bank_Branch_Code { get; set; }
        public string Acc_No { get; set; }
        public string Acc_Type { get; set; }
        public string Emp_Card_No { get; set; }
        public string Emp_Parmanent_Addr { get; set; }
        public string Supervisor_Code { get; set; }
        public string Employee_dor { get; set; }
        public string Last_Working_date { get; set; }
        public string Effictive_date { get; set; }
        public string Emp_Upload_Status { get; set; }
        public string Emp_Upload_Remarks { get; set; }

        public string POUpload_FileName { get; set; }
        public string POUpload_NewFileName { get; set; }
        public string POUpload_FileType { get; set; }
        public Int64 FileCount { get; set; }
        public string ServerPath { get; set; }
    }
}
