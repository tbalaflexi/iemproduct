﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBHelper 
{
    public class PayBank_Model
    {
        public Int64 paybank_gid { get; set; }
        public string bank_name { get; set; }

       
        public string BankCode { get; set; }
        
        public string paybank_QCbank { get; set; }
        public string paybank_Depend_Code { get; set; }

        
       public string Paybankcode_Depend { get; set; }
    
        public string paybank_bank_code { get; set; }

        
       [Range(1, int.MaxValue, ErrorMessage = "Select Bank Name.!")]
        public Int64 Depend_Code { get; set; }
        public string paybank_bank_name { get; set; }
        public string TranId { get; set; }

        [Required(ErrorMessage = "Please Enter The Account No")]
        public string paybank_acc_no { get; set; }
       
        public string paybankifscid { get; set; }
        public string paybank_ifsc_code { get; set; }
 

        public string paybankbranchid { get; set; }

       
        public string paybank_branch_name { get; set; }
        
        [Range(1, int.MaxValue, ErrorMessage = "Select GL No.!")]
        public Int64 paybankglid { get; set; }
       
        

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Please enter GL No"), MaxLength(30)]  
        public string paybank_gl_no { get; set; }

                [Required(ErrorMessage = "Please Enter The Period From")]
        public string paybank_period_from { get; set; }
                [Required(ErrorMessage = "Please Enter The Period To")]
        public string paybank_period_to { get; set; }
        public string paybank_memo_filename { get; set; }
        public string paybank_online_filename { get; set; }
        public string paybank_bank_address { get; set; }
        public string paybank_default { get; set; }
        public string paybank_CHQ_default { get; set; }
        public string paybank_EFT_default { get; set; }
        public string paybank_ERA_default { get; set; }
        public string paybank_DD_default { get; set; }
        public string paybank_insert_by { get; set; }
        public string paybank_insert_date { get; set; }
        public string paybank_update_by { get; set; }
        public string paybank_update_date { get; set; }
        public string Bank_deleted_by { get; set; }
        public string result { get; set; }
     
        public string bankname { get; set; }
        

    }
}
