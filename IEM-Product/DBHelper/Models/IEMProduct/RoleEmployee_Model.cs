﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHandler.Models.IEMProduct
{
    public class RoleEmployee_Model
    {
        public Int64 Role_Gid { get; set; }
        public string Role { get; set; }
        public string RoleGroup { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public Int64 Employee_Gid { get; set; }
        public Int64 roleemployee_gid { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }
        public Int64 HD_Employee_Gid { get; set; }
        public String RoleIds { get; set; }
    }
}
