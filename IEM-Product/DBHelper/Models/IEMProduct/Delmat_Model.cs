﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
//Ramya Added this File
namespace DataAccessHandler.Models.IEMProduct
{
    public class Delmat_Model
    {
        public string Action { get; set; }
        public Int64 Delmat_Gid { get; set; }
        public Int64 ExistingDelmat_Gid { get; set; }
        public string ExistingDelmat_Name { get; set; }
        public Int64 Slabrange_Gid { get; set; }
        [Required(ErrorMessage = "Please Enter The Slap Range Name")]
       
        public string Slabrange_Name { get; set; }
        [Required(ErrorMessage = "Please Enter The Slap Range From")]
        [Range(1, int.MaxValue, ErrorMessage = "Please Enter The Slap Range From!")]
        public decimal Slabrange_From { get; set; }
        [Required(ErrorMessage = "Please Enter The Slap Range To")]
        [Range(1, int.MaxValue, ErrorMessage = "Please Enter The Slap Range To!")]
        public decimal Slabrange_To { get; set; }

        public Int64 delmatexception_gid { get; set; }
        public Int64 delmatexception_delmat_gid { get; set; }

        public Int64 delmatexception_to { get; set; }
           [Required(ErrorMessage = "Please Enter The  Title")]
        public string delmatexception_title { get; set; }
           [Range(1, int.MaxValue, ErrorMessage = "Please Enter The  Limit.!")]
        public decimal delmatexception_Limit { get; set; }

        [UIHint("Employee")]
      //  [Range(1, int.MaxValue, ErrorMessage = "Select Employee Name.!")]
        public string EmployeeCode { get; set; }
        
         [Range(1, int.MaxValue, ErrorMessage = "Select Employee Name.!")]
        public Int64 Employee_Gid { get; set; }


         [Required(ErrorMessage = "Please Enter The Department Name")]
         public string Dept_Gid { get; set; }
        public string Dept_Name { get; set; }
        public string Dept_Code { get; set; }

        [UIHint("Grade")]
        public string Grade_Code { get; set; }
        public Int64 Grade_Gid { get; set; }

        [UIHint("Designation")]
        public string Designation_Code { get; set; }
        public Int64 Designation_Gid { get; set; }

        [UIHint("Title")]

        [Range(1, int.MaxValue, ErrorMessage = "Select Title.!.!")]
        public string Title_Text { get; set; }
            [Range(1, int.MaxValue, ErrorMessage = "Select Title.!")]
        public string Title_Code { get; set; }
                  // [Range(1, int.MaxValue, ErrorMessage = "Select Check Box .!.!")]
        public Boolean IsIncluded { get; set; }
          [Required(ErrorMessage = "Please Enter The  Flow Order")]
        public Int64 DelmatFlow_Gid { get; set; }
               [Range(1, int.MaxValue, ErrorMessage = "Select Flow Order.!.!")]
        public Int32 DelmatFlow_Order { get; set; }
          [Range(1, int.MaxValue, ErrorMessage = "Select Title.!.!")]
        public string DelmatFlow_Title { get; set; }
          [Range(1, int.MaxValue, ErrorMessage = "Select Title Ref .!.!")]
        public Int64 DelmatFlow_Title_Ref_Gid { get; set; }
            [Range(1, int.MaxValue, ErrorMessage = "Select Slab Range .!.!")]
        public Int64 DelmatMatrix_SlabRange_Gid { get; set; }
        public decimal DelmatFlow_Limit { get; set; }

        [UIHint("Title_Ref")]
  
        public string Title_Ref_Code { get; set; }
            [Range(1, int.MaxValue, ErrorMessage = "Select Title Ref Value.!")]
        public Int64 Title_Ref_Gid { get; set; }

        [UIHint("Limit")]

        [Required(ErrorMessage = "Please Enter The  Limit")]
        [Range(1, int.MaxValue, ErrorMessage = "Select Slab Range.!")]
        public string Slabrange_LimitName { get; set; }

        public string Master_Code { get; set; }

        public Int64 Dependent_Gid { get; set; }
        public Int64 Parent_Gid { get; set; }


        #region Delmat Header
        public Int64 DelmatHeader_gid { get; set; }

        [Required(ErrorMessage = "Please Enter The Delmat Name")]
        public string Delmat_Name { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Select Type Name.!")]
        public Int64 Type_gid { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Select Sub Type Name.!")]
        public string SubType_gids { get; set; }
        public string AttributeChilds { get; set; }
        public string Dept_Gids { get; set; }
        public Int64 ExistingDelmatGid { get; set; }

        [Required(ErrorMessage = "Version is required")]
        [Range(0.01, 999999999, ErrorMessage = "Version must be greater than 1")]
        [RegularExpression(@"^\d+(\.\d{1,2})?$", ErrorMessage = "Invalid Version")]
        public Decimal Version { get; set; }

        [Required(ErrorMessage = "Please Select The Clone")]
        public string IsCloned { get; set; }
        public string IsActive { get; set; }
        #endregion

    }
}
