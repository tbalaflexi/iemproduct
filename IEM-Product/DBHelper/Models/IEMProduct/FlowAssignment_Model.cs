﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DataAccessHandler.Models.IEMProduct
{
   public class FlowAssignment_Model
    {

       public   int       assignTypeID      { get; set; }
       public   string    assignTypeName    { get; set; }
       public   Int64     assignRefID       { get; set; }
       public   string    assignRefNumber   { get; set; }
       public   Int64     assignQueueFromID { get; set; }
       public   string    assignQueueFrom   { get; set; }
        public Int64 assignToID { get; set; }
        public string assignTo { get; set; }
       public   string    assignRefType     { get; set; }
       public   string    assignRefSubType  { get; set; }
       public   int       assignRefStatusID { get; set; }
       public   string    assignRefStatus   { get; set; }
       public   int       assignRaiserID    { get; set; }
       public   string    assignRaiserName  { get; set; }
       public   string    assignRemarks     { get; set; }
       public   string    assignSelectedID  { get; set; }
       public   Int64     assignRefUserID   { get; set; }
       public   string    assignRefUserName { get; set; }
       public   string    actionType { get; set; }
       public   Int64     loginUserID { get; set; }
       public Int64 flowassignToID { get; set; }
        
       [UIHint("FlowAssignTo")]      
       public string flowassignTo { get; set; }
    }
}
