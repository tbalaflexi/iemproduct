﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBHelper
{
    public class WorkFlow_Model
    {
        public Int64 WorkFlow_Gid { get; set; }

        public string Type_Name { get; set; }
        public string Type_Code { get; set; }
        //Ramya Added for Attribute 
        public Int64 Type_Gid { get; set; }

        public string SubType_Name { get; set; }
        public string SubType_Code { get; set; }

        public Int64 Attribute_Gid { get; set; }
        public string Attribute_Parent { get; set; }
        public string Attribute_Child { get; set; }

        [UIHint("RoleFrom")]
        public string RoleFrom_Name { get; set; }

        public Int64 RoleFrom_Code { get; set; }

        [UIHint("Action")]
        public string Action_Name { get; set; }
        public Int64 Action_Code { get; set; }

        [UIHint("Status")]
        public string Status_Name { get; set; }
        public Int64 Status_Code { get; set; }

        [UIHint("RoleTo")]
        public string RoleTo_Name { get; set; }
        public Int64 RoleTo_Code { get; set; }

        [UIHint("TimeOutRoleTo")]
        public string TimeOut_RoleTo_Name { get; set; }
        public Int64 TimeOut_RoleTo_Code { get; set; }

        public string WF_Parent { get; set; }
        public string WF_Child { get; set; }

        public Int64 TimeOut { get; set; }
        public string Additional { get; set; }
        public string HDN_Type { get; set; }
        public string HDN_SubType { get; set; }

        public Int64 Dept_Gid { get; set; }
        public string Dept_Name { get; set; }
        public string Dept_Code { get; set; }

        public Int64 Logged_UserGID { get; set; }

        public Int64 workflow_id { get; set; }
        public string workflow_typecode { get; set; }
        public string workflow_typename { get; set; }
        public string workflow_subtypecode { get; set; }
        public string workflow_subtypename { get; set; }
        public string workflow_attrib_parentid { get; set; }
        public string workflow_attrib_parentname { get; set; }
        public string workflow_attrib_childid { get; set; }
        public string workflow_attrib_childname { get; set; }
        public string workflow_action { get; set; }
    }

    public class WorkFlowAttribute_Model
    {
        public Int64 Attr_Parent_Gid { get; set; }
        public string Attr_Parent_Name { get; set; }
        public List<WorkFlowAttributeChild_Model> lst_child { get; set; }
        
    }
    public class WorkFlowAttributeChild_Model
    {
        public Int64 Attr_Child_Gid { get; set; }
        public Int64 Attr_Child_ParentGid { get; set; }
        public string Attr_Child_Name { get; set; }
    }
}
