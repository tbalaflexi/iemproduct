﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHandler.Models.IEMProduct
{
   public class PeriodClose_Model
    {
       public Int64 PeriodCloseGid { get; set; }
        [UIHint("PeriodCloseDateFrom")]
       [DataType(DataType.Date)]
       [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}"
           , ApplyFormatInEditMode = true)]
       [Required(ErrorMessage =
           "Join Date can not be greater than current date")] 
       public string PeriodCloseFromDate { get; set; }
         [UIHint("PeriodCloseDateTo")]

       public string PeriodCloseToDate { get; set; }
         [UIHint("PeriodCloseStatus")]
       public string PeriodCloseStatus { get; set; }
         public Int64 id { get; set; }
       public string result { get; set; }
    }
}
