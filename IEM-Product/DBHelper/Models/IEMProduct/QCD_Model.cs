﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DataAccessHandler.Models.IEMProduct
{
    public class QCD_Model
    {
        public string Type_Name { get; set; }
        public string Type_Code { get; set; }
        public Int64 Type_Gid { get; set; }

        public string SubType_Name { get; set; }
        public string SubType_Code { get; set; }
        public Int64 SubType_Gid { get; set; }

        public string Dept_Name { get; set; }
        public string Dept_Code { get; set; }
        public Int64 Dept_Gid { get; set; }

        public Int64 Dependent_Gid { get; set; }
        public Int64 Parent_Gid { get; set; }
        public string Master_Code { get; set; }

        public string Action_Type { get; set; }

        [UIHint("Title_Ref")]
        public string Title_Ref_Code { get; set; }
        public Int64 Title_Ref_Gid { get; set; }

        public string Values { get; set; }
         
    }
}
