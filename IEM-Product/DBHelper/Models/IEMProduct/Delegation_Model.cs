﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHandler.Models.IEMProduct
{
   public class Delegation_Model
    {
       public Int64 delegate_gid { get; set; }
       public Int64 moduleid {get;set;}
       public string delegate_by { get; set; }
       public string delegate_to { get; set; }

        [Required(ErrorMessage = "Please Enter The Delmat Flag")]
       public string delegate_delmat_flag { get; set; }
       public string delegate_remark { get; set; }

        [Required(ErrorMessage = "Please Select Date From")]
        //[Remote("ValidateDateEqualOrGreater", HttpMethod = "Post",
     //ErrorMessage = "Date isn't equal or greater than current date.")]
     //  [Display(Name = "Start Date")]
     //  [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
       public string delegate_period_from { get; set; }

        [Required(ErrorMessage = "Please Select Date To")]
      //  [Display(Name = "End Date")]
       // [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
       // [MyDate(ErrorMessage = "Back date entry not allowed")]
      //  [otherPropertyName = "StartDate", ErrorMessage = "End date must be greater than start date"]
   
       public string delegate_period_to { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Select  Module Name.!")]
       public Int64 delegate_delmattypeid { get; set; }
       public string delegate_delmattype { get; set; }
       public string status { get; set; }
       public string insertedby { get; set; }
       public string inserteddate { get; set; }
       public string modifiedby { get; set; }
       public string modifieddate { get; set; }
       public string deletedby { get; set; }
       public string result { get; set; }
       // [Range(1, int.MaxValue, ErrorMessage = "Select  Status.!")]
       //public Int64 id { get; set; }
      // [Required(ErrorMessage = "Please select  Status")]
     //  [Display(Name = "User type")]

       [Required(ErrorMessage = "Please select The status")]
       public string delegate_active { get; set; }
       public Int64 loginid { get; set; }

       public string delegate_workflow_Role { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Select  Role.!")]
       public Int64 Roleid { get; set; }
       public string Rolename { get; set; }
         [Range(1, int.MaxValue, ErrorMessage = "Select  Assign To Name/ ID.!")]
       public Int64 empid { get; set; }
       public string empname { get; set; }
       public Int64 raiserid { get; set; }
       public Int64 Selected_supervisor_gid { get; set; }
    }
}
