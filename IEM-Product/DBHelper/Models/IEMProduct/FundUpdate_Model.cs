﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHandler.Models.IEMProduct
{
  public  class FundUpdate_Model
    {
      public Int64 fundupdate_gid { get; set; }
     // public string fundupdate_bankname { get; set; }
       [UIHint("FundUpdateDate")]
      public string fundupdate_date { get; set; }
       [Range(1, int.MaxValue, ErrorMessage = "Please Enter The Opening Balance.!")]
      public Int64 fundupdate_openingbal { get; set; }
       [Range(1, int.MaxValue, ErrorMessage = "Please Enter The Fund Update In Flow.!")]
      public Int64 fundupdate_inflow { get; set; }
      public Int64 fundupdate_fundavailable { get; set; }
      public string result { get; set; }
              
      public Int64 fundupdate_paybankname_id { get; set; }
 //[UIHint("FundUpdateBankNameDropdown")]
      public string bankname { get; set; }
  //   [UIHint("FundUpdateAccountNo")]
      public string accountNo { get; set; }
     public Int64 id { get; set; }
    }
}
