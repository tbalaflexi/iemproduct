﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHandler.Models.IEMProduct
{
    public class MailTemplate_Model
    {
        public Int64 mailtemplate_id { get; set; }
        public Int64 mailtemplate_type_id { get; set; }
        public string mailtemplate_type_name { get; set; }
        public Int64 mailtemplate_subtype_id { get; set; }
        public string mailtemplate_subtype_name { get; set; }
        public Int64 mailtemplate_status_id { get; set; }
        public string mailtemplate_status_name { get; set; }
        public Int64 mailtemplate_torole_id { get; set; }
        public string mailtemplate_torole_name { get; set; }
        public Int64 mailtemplate_ccrole_id { get; set; }
        public string mailtemplate_ccrole_name { get; set; }
        public string mailtemplate_subject { get; set; }
        public string mailtemplate_template { get; set; }
        public string mailtemplate_attachment { get; set; }
        public string mailtemplate_content { get; set; }
        public string mailtemplate_signature { get; set; }
        public Int64 logged_user_id { get; set; }
        public bool supplier_IsChecked { get; set; }
        public string mailtemplate_additionalcc { get; set; }

        public Int64 mailfield_id { get; set; }
        public string mailfield_name { get; set; }
        public string supplier_flag { get; set; }

        public string subtype_ids { get; set; }
        public string status_ids { get; set; }
        public string torole_ids { get; set; }
        public string ccrole_ids { get; set; }
        public string mailfields_ids { get; set; }
    }
}
