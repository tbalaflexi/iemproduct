﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBHelper
{
    public class Workbench_Model
    {
        public Int64 id { get; set; }
        public string ecf_no { get; set; }
        public string ecf_date { get; set; }
        public string ecf_amount { get; set; }
        public string ClaimType { get; set; }
        public string employee_name { get; set; }
        public string supplierheader_name { get; set; }
        public string ecf_status { get; set; }
        public string Action { get; set; }
        public Int64 Batch_Gid { get; set; }
        public string SupplierEmployee_Type { get; set; }
        public Int64 Login_UserID { get; set; }

        public Int64 Ecf_Gid { get; set; }
        public Int64 Raiser_Gid { get; set; }
        public string Approver_Status { get; set; }

        public string ecfDescription { get; set; }
        public string displaying_status { get; set; }
    }
}
