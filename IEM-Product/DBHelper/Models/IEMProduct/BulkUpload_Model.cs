﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DBHelper
{
    public class BulkUpload_Model
    {
        public string Master_Code { get; set; }
        public string Parent_code { get; set; }
        public string Dependant_Code { get; set; }
        public string Dependant_ParentCode { get; set; }
        public string Action { get; set; }
        [Required(ErrorMessage = "Please Upload File")]
        [Display(Name = "Upload File")]
        public string file { get; set; }
        public string Branch { get; set; }
        public string UploadMethod { get; set; }
    }

    public class SheetData
    {
        public int SheetId { get; set; }
        public string SheetName { get; set; }
    }
}
