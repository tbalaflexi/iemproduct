﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHandler.Models.IEMProduct
{
    public class MenuModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string url { get; set; }
        public int ParentId { get; set; }
        public int SortOrder { get; set; }
    }
}
