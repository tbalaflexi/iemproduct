﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBHelper
{
   public  class Profile_Model
    {
       public string Action { get; set; }
       public Int64 Emp_Gid { get; set; }
       [Required(ErrorMessage = "Please Enter The Employee Name")]
       public string Emp_Name { get; set; }
       [Required(ErrorMessage = "Please Enter The Employee Code")]
       public string Emp_Code { get; set; }
       public string Gender { get; set; }
       //[Required(ErrorMessage = "Please Enter The Account No")]
       public String DOB { get; set; }
      // [Required(ErrorMessage = "Please Enter The Account No")]
       public String DOJ { get; set; }
       public string Designation { get; set; }
       public string Department { get; set; }
       public string Branch { get; set; }
       public string Grade { get; set; }
       public string FC_Name { get; set; }
       public string CC_Name { get; set; }
       public string Product_Code { get; set; }
       [Required(ErrorMessage = "Please Enter The Address")]
       public string Address { get; set; }
       public string Emp_Addr1 { get; set; }
       public string Emp_Addr2 { get; set; }
       public string City { get; set; }
       public string Pincode { get; set; }
       
       public string Personal_Email { get; set; }
       [Required(ErrorMessage = "Please Enter Email Address")]
       [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
       public string Office_Email { get; set; }
       public string Bank_Name { get; set; }
       public string ERA { get; set; }
       public string IFSC_Code { get; set; }
       public string Card_No { get; set; }
       public string Function_Name { get; set; }
       public Int64 Employee_Sup_Gid { get; set;}
       public string Sup_Name { get; set; }
       public string emp_status { get; set; }
       public string branch_flag { get; set; }
       public string Iem_Designation { get; set; }

       public string EMP_DES_Name { get; set; }
       public Int64 EMP_DES_Gid { get; set; }

       //Designation
       [Range(1, int.MaxValue, ErrorMessage = "Select Designation Name.!")]
       public Int64 Designationid { get; set; }
       public string DesignationName { get; set; }
       //GRdae
       [Range(1, int.MaxValue, ErrorMessage = "Select Grade Name.!")]
       public Int64 Gradeid { get; set; }
       public string GradeName { get; set; }

       //Department
       [Range(1, int.MaxValue, ErrorMessage = "Select Department Name.!")]
       public Int64 Departmentid { get; set; }
       public string Departmentname { get; set; }

        //FC
       [Range(1, int.MaxValue, ErrorMessage = "Select FC Name.!")]
       public Int64  Fcid {get; set;}
       public string FcName { get; set; }

       //CC
       [Range(1, int.MaxValue, ErrorMessage = "Select CC Name.!")]
       public Int64 CCid { get; set; }
       public string CCName { get; set; }

       //product
       [Range(1, int.MaxValue, ErrorMessage = "Select Product Name.!")]
       public Int64 Productid { get; set; }
       public string ProductName { get; set; }
       //bank
       [Range(1, int.MaxValue, ErrorMessage = "Select Bank Name.!")]
       public Int64 Bankid { get; set; }
       public string BankName { get; set; }
       //branch
       [Range(1, int.MaxValue, ErrorMessage = "Select Branch Name.!")]
       public Int64 branchid { get; set; }
       public string branchname { get; set; }
       //City
       [Range(1, int.MaxValue, ErrorMessage = "Select City Name.!")]
       public Int64 cityid { get; set; }
       public string cityname { get; set; }

       //Employee
       [Range(1, int.MaxValue, ErrorMessage = "Select Employee Name.!")]
       public Int64 empid { get; set; }
       public string empname { get; set; }

       [Required(ErrorMessage = "Please Enter The Mobile No")]
       public string mobileno { get; set; }
     //  [DataType(DataType.Text)]

       [Required(ErrorMessage = "Please Enter The Password")]
       [StringLength(12, MinimumLength = 6)]
      // [StringLength(6, ErrorMessage = "Must be 6 characters", MinimumLength = 6)]
       public string password { get; set; }
      // public string pincode { get; set; }
       public string employee_bankaccounttype { get; set; }
       public string employee_dor { get; set; }
       public string HRIS_LASTMODIFIEDON { get; set; }
       public string employee_lastworkingdate { get; set; }
       public string employee_effectivedate { get; set; }
       public string result { get; set; }
       public string employee_employeebranch_gid { get; set; }
       public string AccountNo { get; set; }

       //Employee branch Master
       public Int64 empbranch_id { get; set; }
       public string empbranch_name { get; set; }

       public Int64 employee_supervisor_gid { get; set; }
       public string Supervisor_name { get; set; }

       public Int64 Selected_supervisor_gid { get; set; }
    }
}
