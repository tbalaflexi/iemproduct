﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DataAccessHandler.Models.IEMProduct
{
    public class Dashboard_Model
    {

        public Int64    dashTypeID                  { get; set;}
        public string   dashType                    { get; set;}
        public int      dashRaiserDraftCount        { get; set;}
        public int      dashRaiserInProcessCount    { get; set;}
        public int      dashRaiserRejectedCount     { get; set;}
        public int      dashRaiserMyApprovalCount   { get; set;}
        public int      dashAPCheckerCount          { get; set;}
        public int      dashAPAuthoriserCount       { get; set;}
        public int      dashAPPaymentCount          { get; set;}
        public string   dashDateFrom                { get; set;}
        public string   dashDateTo                  { get; set;}
        public string   dashLoginRole               { get; set;}
        public string   dashDateFrom1               { get; set; }
        public string   dashDateTo1                 { get; set; }
        public string   IsApCheckFlag               { get; set;}
    }
}
