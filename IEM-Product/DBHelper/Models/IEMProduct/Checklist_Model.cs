﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHandler.Models.IEMProduct
{
  public  class Checklist_Model
    {
      public Int64 checklist_gid { get; set; }
       
      public Int64 TranId { get; set; }
      public string Tran_Type { get; set; }
      
      public Int64 TransubId { get; set; }
      public string Tran_SubType { get; set; }
      [Required(ErrorMessage = "Check List Item is Required")]
   
      public string checklist_item { get; set; }
   
      [Required(ErrorMessage = "Check List Reason  is Required")]
      public string checklist_reason { get; set; }
      public Int64 ststusid { get; set; }
      public string statusname { get; set; }
      public string result { get; set; }
      public string status { get; set; }
       [Range(1, int.MaxValue, ErrorMessage = "Please Enter The Work Order.!")]
      public Int64 workorder { get; set; }
       public Int64 value { get; set; }
    }
}
