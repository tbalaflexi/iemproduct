﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBHelper
{
    public class SubCategory_Model
    {
        public Int64 expsubcat_gid { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Select Nature Of Expense.!")]
        public Int64 NatureOfExpenseid { get; set; }
        [UIHint("NatureOfExpenseDropdown")]
        public string NatureOfExpense { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Select Expense Category.!")]
        public Int64 expsubcat_expcat_masters_gid { get; set; }
        [UIHint("ExpenseCategoryDropdown")]
        public string expcategory { get; set; }
        [Required(ErrorMessage = "Please Enter The  SubCategory Code")]
        public string expsubcat_code { get; set; }
        [Required(ErrorMessage = "Please Enter The SubCategory Name")]
        public string expsubcat_name { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Select GL NO.!")]
        public Int64 paybankglid { get; set; }
        [UIHint("GLNODropdown")]
        public string paybank_gl_no { get; set; }
        public string expsubcat_insert_by { get; set; }
        public string expsubcat_insert_date { get; set; }
        public string expsubcat_update_by { get; set; }
        public string expsubcat_update_date { get; set; }
        public string DeletedBy { get; set; }
        public string result { get; set; }
    }
}
