﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHandler.Models.IEMProduct
{
    public class AccessPrivileges_Model
    {
        public Int64 Role_SLNo { get; set; }
        public Int64 Role_Gid { get; set; }
        public string Role_Code { get; set; }
        public string Role_Name { get; set; }
        public string Role_GroupName { get; set; }
        public Int64 LoginUser_Id { get; set; }

        public class UserGroups
        {
            public List<menu> menu { get; set; }
            public string Txtrole_code { get; set; }
            public string Txtrole_name { get; set; }
            public string Txtrole_groupname { get; set; }
        }

        public partial class menu
        {
            public int menu_gid { get; set; }
            public string menu_name { get; set; }
            public string menu_url { get; set; }
            public int parent_menu_gid { get; set; }
            public int menu_order { get; set; }
            public bool rights_flag { get; set; }
        }
    }
}
