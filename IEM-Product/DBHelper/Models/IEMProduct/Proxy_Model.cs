﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHandler.Models.IEMProduct
{
    public class Proxy_Model
    {
        public Int64 proxyGid { get; set; }
        public string proxyFrom { get; set; }
       
       
        public string proxyRemark { get; set; }
        public string Action { get; set; }
        public string ProxyGrade { get; set; }
        public string MobileNo { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeCode { get; set; }
        public Int64 EmployeeId { get; set; }
        public string RaiserName { get; set; }
        public string RaiserCode { get; set; }
        public string EmployeeGrade { get; set; }
        public string EmployeeMobile { get; set; }




         
    
        [UIHint("ProxyTo")]
        public string proxyTo { get; set; }
        // [Range(1, int.MaxValue, ErrorMessage = "Select Proxy Name.!")]
        public Int64 proxyToId { get; set; }
       // [Required(ErrorMessage = "Please Select The Proxy Period From Date")]
     
        [UIHint("proxyPeriodFrom")]
         public string proxyPeriodFrom { get; set; }
         //[Range(1, int.MaxValue, ErrorMessage = "Select Role Name.!")]
        public Int64 proxyFromId { get; set; }
        // [Required(ErrorMessage = "Please Enter The  Proxy Period To Date")]
      
     [UIHint("proxyPeriodTo")]
     public string proxyPeriodTo { get; set; }

        [UIHint("proxyActive")]

       public string proxyActive { get; set; }
        // [Range(1, int.MaxValue, ErrorMessage = "Select  Status.!")]
        public string proxyActiveId { get; set; }

         public Int64 Selected_supervisor_gid { get; set; }
         
    }
}
