﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBHelper
{
    public class Attachment_Model
    {
        public Int64 attachment_gid { get; set; }
        public int attachment_ref_flag { get; set; }
        public int attachment_ref_gid { get; set; }
        public string attachment_module_flag { get; set; }

        [Required(ErrorMessage = "Please Upload File")]
        [Display(Name = "Upload File")]
        public string file { get; set; }

        public string attachment_filename { get; set; }
        [Required(ErrorMessage = "Document Group is Required")]
        [Range(1, int.MaxValue, ErrorMessage = "Document Group is Required.!")]
        public Int64 attachment_attachmenttype_gid { get; set; }
        public string docgrpname { get; set; }

        [Required(ErrorMessage = "Description is Required")]
        public string attachment_desc { get; set; }
        [Required(ErrorMessage = "Document Name Is Required")]
        public string attachment_docname { get; set; }
        public int attachment_invoice_gid { get; set; }
        public string attachment_inserted_by { get; set; }
        public string attachment_inserted_date { get; set; }
        public string result { get; set; }
        public Int64 id { get; set; }

        public Int64 Logger_UserId { get; set; }

        #region AuditTrail
        public Int64 QiD { get; set; }
        public string EmpCode { get; set; }
        public string EmpName { get; set; }
        public string ActionDate { get; set; }
        public string ActionType { get; set; }
        public string Remarks { get; set; }
        public string Designation { get; set; }
        public Int64 Ref_Gid { get; set; }
        #endregion

        #region Another attachment params
        public Int64 attach_batch_gid { get; set; }
        public Int64 attach_ecf_gid { get; set; }
        public string attach_module { get; set; }
        public Int64 invoice_gid { get; set; }
        public Int64 debitline_gid { get; set; }
        public string batch_no { get; set; }
        public string ecf_no { get; set; }
        public string attach_debitinvoice_flag { get; set; }
        #endregion
    }
}
