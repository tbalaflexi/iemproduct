﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHandler.Models.FlexiSpend
{
    public class EFTFileGeneration_Model
    {
        public Int64 PayBank_Gid { get; set; }
        public string PayBank { get; set; }
        public Int64 PayMode_Gid { get; set; }
        public string Pay_paymodemode { get; set; }
        public string Supplier_Name { get; set; }
        public string Supplier_Code { get; set; }
        public string Employee_Name { get; set; }
        public string Employee_Code { get; set; }
        public string PV_From { get; set; }
        public string ClaimType_Gid { get; set; }
        public string ClaimType { get; set; }
        public double PV_To_Amount { get; set; }
        public double PV_From_Amount { get; set; }
        public string PV_To { get; set; }
        public string PV_No { get; set; }
        public string BatchNo { get; set; }
        public Int64 PV_Gid { get; set; }
        public string Benificary_Bank_Name { get; set; }
        public string PV_Date { get; set; }
        public string SNo { get; set; }
        public string PV_Gids { get; set; }
        public Int64 PV_Cancel_GID { get; set; }
        public string PV_Cancel_Reason { get; set; }
        public string PV_Status { get; set; }

        #region Generate Memo and Online File
        public string Mode { get; set; }
        public string AccountType { get; set; }
        public string BranchDetails { get; set; }
        public string MemoNo { get; set; }
        public string MemoTime { get; set; }
        public string MemoDate { get; set; }

        public string BenName { get; set; }
        public string BenAccNo { get; set; }
        public string BenAddress { get; set; }
        public string BenBankname { get; set; }
        public string BenBankIfscCode { get; set; }
        public string AmountInWords { get; set; }
        public string AmountInFigures { get; set; }

        public string RemitterName { get; set; }
        public string RemitterAccNo { get; set; }
        public string RemitterCashDeposited { get; set; }
        public string RemitterMobilePhoneNo { get; set; }
        public string RemitterEmailId { get; set; }
        public string RemitterAddress { get; set; }
        public string Remarks { get; set; }

        public string NameOfVendor { get; set; }
        public string BankAccNo { get; set; }
        public string BankName { get; set; }
        public string IFSCCode { get; set; }
        public string Amount { get; set; }
        public string RemitterDetails { get; set; }
        public decimal TotalAmount { get; set; }
        public List<EFTFileGeneration_Model> DetailArray { get; set; }
        public Int32 ViewType { get; set; } 

       
        #endregion
    }

    public class DDTemplate
    {
        public string Date { get; set; }
        public string LetterNo { get; set; }
        public string BankAddress { get; set; }
        public string CompanyAccountNo { get; set; }
        public string DDFavoring { get; set; }
        public string PayableAt { get; set; }
        public string Amount { get; set; }
        public string AmountInWords { get; set; }
        public decimal Totalamount { get; set; }
        public List<DDInnerDetails> DetailArray { get; set; }
    }
    public class DDInnerDetails
    {
        public string DDFavoring { get; set; }
        public string PayableAt { get; set; }
        public string Amount { get; set; }
    }
    public class ERATemplate
    {
        public string Date { get; set; }
        public string LetterNo { get; set; }
        public string BankAddress { get; set; }
        public string CompanyAccountNo { get; set; }
        public string Amount { get; set; }
        public string AmountInWords { get; set; }
        public string KindAttan { get; set; }
        public List<ERAInnerDetails> DetailArray { get; set; }
    }

    public class ERAInnerDetails
    {
        public string VendorCode { get; set; }
        public string NameOfVendor { get; set; }
        public string BankAccountNo { get; set; }
        public string Amount { get; set; }
        public string RemittanceDetails { get; set; }
        public string IFSCCode { get; set; }
    }
}
