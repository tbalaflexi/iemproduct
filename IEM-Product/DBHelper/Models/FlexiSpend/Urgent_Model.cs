﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHandler.Models.FlexiSpend
{
    public class Urgent_Model
    {
        public Int64 LoginId { get; set; }
        public string EmpName_Sr { get; set; }
        public Int64 Refid { get; set; }
        public Int64 EmpGid { get; set; }
        public string EmpName { get; set; }
        public decimal Amount { get; set; }
        public string Reason { get; set; }
        public string Action { get; set; }
        
        public Int64 EcfGid { get; set; }
        public DateTime EcfDate { get; set; }
        public string RefNo { get; set; }
        public string Type_SubType { get; set; }
        public string EcfStatus { get; set; }
        public decimal EcfAmount { get; set; }
        public string RaiserId_Name { get; set; }
        public string SupplierId_Name { get; set; }
        public string Attributes { get; set; }
        public string Physical { get; set; }
        public DateTime RecivedDate { get; set; }
        public string BatchNo { get; set; }

        public string Maker_Reason { get; set; }
        public string UrgentECF_Gid { get; set; }
        public Int64 UrgentId { get; set; }
        public Int64 UrgentStatus { get; set; }
        public Int64 MakerId { get; set; }
        public Int64 CheckerId { get; set; }
        public string Checker_Reason { get; set; }
        
    }
}
