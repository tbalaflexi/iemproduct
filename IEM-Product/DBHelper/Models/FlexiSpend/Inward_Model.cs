﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHandler.Models.FlexiSpend
{
    public class Inward_Model
    {
        public DateTime RefDate { get; set; }
        public Int64 Refid { get; set; }
        public string Refids { get; set; }
        public string Refno { get; set; }
        public Int64 Raiserid { get; set; }
        public string RaiserName { get; set; }
        public string RaiserCode { get; set; }
        public Int64 StatusId { get; set; }
        public string Status { get; set; }
        public decimal EcfAmt { get; set; }
        public Int64 SupplierId { get; set; }
        public string SupplierName { get; set; }
        public string SupplierCode { get; set; }
        public string AttributeDet { get; set; }
        public DateTime ReciverDate { get; set; }
        public string RecvDate { get; set; }
        public Int64 CourierId { get; set; }
        public string CourierName { get; set; }
        public string AwbNo { get; set; }
        public Int64 BatchId { get; set; }
        public string BatchNo { get; set; }
        public Int64 InwardCount { get; set; }
        public Int64 BatchCount { get; set; }
        public string SupEmpName { get; set; }
        public Int64 SupEmpId { get; set; }
        public string Pouchids { get; set; }
        public string PhysicalReceipt { get; set; }
        public string Receipt { get; set; }

        public Int64 LoginId { get; set; }
        public string _Action { get; set; }
        public string Gridtype { get; set; }

        public string Type_Name { get; set; }
        public string Type_Code { get; set; }
        public Int64 Type_Gid { get; set; }

        public string SubType_Name { get; set; }
        public string SubType_Code { get; set; }
        public Int64 SubType_Gid { get; set; }
    }
}
