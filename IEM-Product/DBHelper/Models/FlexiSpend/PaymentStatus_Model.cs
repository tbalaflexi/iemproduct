﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DataAccessHandler.Models.FlexiSpend
{
   public class PaymentStatus_Model
    {

      public Int64  paymentBankID { get; set; }
      public string paymentBank { get; set; }
      public string paymentGLValDate { get; set; }
      public string paymentPVNumber { get; set; }
      public string paymentAmount { get; set; }
      public string paymentBeneficiary { get; set; }
      public string paymentMode { get; set; }
      public Int64  paymentPayrunVoucherID { get; set; }
      public Int64 LoginId { get; set; }
      public string SelectedValues { get; set; }
      public Int64 chqid { get; set; }
      public string Action { get; set; }


        
      public Int64 paymentStatusID { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Select  Payment Status.!")]
      public Int64 chqstatus_code { get; set; }
       // public string chqstatus_code { get; set; }
       [UIHint("GetPaymentStatus")]
      public string chqstatus_name { get; set; }
        //[Required(ErrorMessage = "Select Payment Date")]
        [Required(ErrorMessage = "Select Payment Date")]
       //validate:Must be greater than current date
       [DataType(DataType.DateTime)]
        [UIHint("paymentDate")]
       public  DateTime paymentDate { get; set; }

     //  [UIHint("paymentRefNo")]
        [Range(1, int.MaxValue, ErrorMessage = "Enter The Payment Ref No.!")]
        public Int64 paymentRefNo { get; set; }

       //[UIHint("paymentReaseon")]
        [Required(ErrorMessage = "Please Enter The Payment Reason")]
       
        public string paymentReaseon { get; set; }

        public string pay_mode { get; set; }

        public Int64 payrunvoucher_memostatus { get; set; }
        public string Paymentstatus { get; set; }
        public string payrunvoucher_memoreason { get; set; }

    }
}
