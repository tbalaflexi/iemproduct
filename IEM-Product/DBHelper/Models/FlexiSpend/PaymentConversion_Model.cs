﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHandler.Models.FlexiSpend
{
    public class PaymentConversion_Model
    {
        public Int64 LoginId { get; set; }
         public string Action { get; set; }

        public Int64 PV_Gid { get; set; }
        public string Payment_Bank { get; set; }
        public Int64 Ecf_Gid { get; set; }
        public Int64 PVGL_Gid { get; set; }
        public DateTime GLValue_Date { get; set; }
        public string PV_No { get; set; }
        public decimal PV_Amount { get; set; }
        public string PV_Beneficiary { get; set; }
        public string PV_Paymode { get; set; }
        public string PV_Status { get; set; }
        public string PV_Reason { get; set; }
         [UIHint("GetPaymodeDetails")]
        public string PV_Change_Paymode { get; set; }        
        public string PV_Change_Beneficiary { get; set; }
         [UIHint("GLValueDate")]
        public DateTime Change_GLValue_Date { get; set; }
        public string ECF_Supplier_Employee_Type { get; set; }
        public Int64 ECF_Supplier_Employee_Gid { get; set; }

        public string EMP_SUP_Type { get; set; }
        public Int64  ECFGid { get; set; }
        public string SupplierGid { get; set; }
        public Int64 PaymentGid { get; set; }

        public string SelectedValues { get; set; }
        public Int64 PaymentReissue_Id { get; set; }

        public string PaymentReissue_Gid { get; set; }
        public string CheckList_status { get; set; }

        
        
    }
}
