﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHandler.Models.FlexiSpend
{
    public class APCheker_Model
    {
        public Int64 Batch_Gid { get; set; }
        public Int64 Ref_Gid { get; set; }
        public string Ref_No { get; set; }
        public DateTime Ref_Date { get; set; }
        public string Type_SubType { get; set; }
        public string Status { get; set; }
        public decimal Amount { get; set; }
        public Int64 Raiser { get; set; }
        public string Raiser_Id_Name { get; set; }
        public string Supplier_Code_Name { get; set; }
        public string Gl_Value_Date { get; set; }
        public Int64 Logged_UserID { get; set; }

        public string Type { get; set; }
        public string SubType { get; set; }
        public Int64 Type_Gid { get; set; }
        public Int64 SubType_Gid { get; set; }

        public string GLValue_Date { get; set; }
        public string Auth_Remarks { get; set; }
        public int Auth_Status { get; set; }

        public string ecfgids { get; set; }
    }
}
