﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHandler.Models.FlexiSpend
{
    public class PaymentRun_Model
    {
        #region Fund Position
        public string Fund_PayBank_Name { get; set; }
        public string Fund_PayBank_Date { get; set; }
        public double Fund_Available { get; set; }
        public double Fund_Utilised { get; set; }
        public double Fund_Balance { get; set; }
        public double Fund_YettoUtilise { get; set; }
        public double Fund_AvailableBalance { get; set; }
        #endregion

        #region PaymentRun Before GL
        public Int64  PaymentRunGL_ECF_Gid { get; set; }
        public string PaymentRunGL_ECF_No { get; set; }
        public string PaymentRunGL_ECF_Date { get; set; }
        public string PaymentRunGL_ECF_DocType_SubType { get; set; }
        public string PaymentRunGL_ECF_Status { get; set; }
        public double PaymentRunGL_ECF_Amount { get; set; }
        public string PaymentRunGL_ECF_Raiser { get; set; }
        public string PaymentRunGL_ECF_Supplier { get; set; }
        public string PaymentRunGL_ECF_Attributes { get; set; }
        public string PaymentRunGL_ECF_Netting { get; set; }
        public Int64 PaymentRunGL_ECF_Raiser_GID { get; set; }
        public string PaymentRunGL_ECF_ClaimType { get; set; }
        public Int64 PaymentRunGL_Batch_Gid { get; set; }
        public string PaymentRunGL_ECF_Gids { get; set; }
        public double PaymentRunGL_ECF_Creditline_Amount { get; set; }
        #endregion

        #region PaymentRun After GL
        public string PaymentRunGL_No { get; set; }
        public string PaymentRunGL_Date { get; set; }
        public string PaymentRunGL_DocType_SubType { get; set; }
        public string PaymentRunGL_Status { get; set; }
        public double PaymentRunGL_Amount { get; set; }
        public string PaymentRunGL_Raiser { get; set; }
        public string PaymentRunGL_Supplier { get; set; }
        public string PaymentRunGL_Attributes { get; set; }
        //public string PaymentRunGL_Netting { get; set; }
        public string PaymentRunGL_GLDate { get; set; }
        public string PaymentRunGL_PayBank_Name { get; set; }
        public Int64 PaymentRunGL_PayBank_Gid { get; set; }
        #endregion
    }
}
