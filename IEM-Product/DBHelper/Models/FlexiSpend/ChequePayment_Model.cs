﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DataAccessHandler.Models.FlexiSpend
{
   public class ChequePayment_Model
    {
       public string result { get; set; }
       public Int64 chq_gid { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Please Enter The  Cheque Number.!")]
       public Int64 chq_no { get; set; }
       public string chq_status { get; set; }
       public string chq_remark { get; set; }
       public string chq_available_flag { get; set; }
       public Int64 chq_payrunvocher_gid { get; set; }
      
         [UIHint("ChequePaymentDate")]
         [Required(ErrorMessage = "Please Enter The Cheque Date")]
        public string chq_date { get; set; }
       public string chq_cleared_date { get; set; }
       public string chq_isprinted { get; set; }
       public string chq_printed_date { get; set; }
      public string bankname { get; set; }
       public  string glvaluedate { get; set; }
       public Int64 pvno { get; set; }
       public decimal pvamount { get; set; }
       public string beneficiary { get; set; }
       public string paymode { get; set; }
       public Int64 logingid { get; set; }
       public Int64 payrunvouchergid { get; set; }
       public string Action { get; set; }

       //Despatch
        [Range(1, int.MaxValue, ErrorMessage = "Select Despatch Mode.!")]
       public Int64 despatchid { get; set; }
      // [Required(ErrorMessage = "Please Select The Despatch Mode")]
         [UIHint("ChequeDespatchMode")]
       public string chq_depatch_mode { get; set; }
         [Range(1, int.MaxValue, ErrorMessage = "Please Enter The Courier Name")]
         public Int64 courierid { get; set; }
       [UIHint("CheckDespatchCourrierName")]
       public string chq_courier_name { get; set; }
       
       [Range(1, int.MaxValue, ErrorMessage = "Please Enter The AWB Number.!")]
       public Int64 chq_awb_no { get; set; }
     
      [UIHint("ChequeDespatchDate")]
      [Required(ErrorMessage = "Please Enter The Despatch Date")]
       public string chq_despatch_date { get; set; }
    }
}
