﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHandler.Models.FlexiSpend
{
    public class RejectDespatch_Model
    {
        public string ecfgids { get; set; }
        public Int64 ecfid { get; set; }
        public Int64 Refid { get; set; }
        public Int64 batchgid { get; set; }
        public Int64 raiserid { get; set; }
        public string Approver_Status { get; set; }
        public string refno { get; set; }
        public string date { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Select  Transaction Type.!")]
        public Int64 TranId { get; set; }
        public string Tran_Type { get; set; }
       // public string type { get; set; }
        public string subtype { get; set; }
        public string status { get; set; }
        public decimal amount { get; set; }
        public string raiseridname { get; set; }
        public string supliercodename { get; set; }
        public string attribute { get; set; }
        public string rejectdespatchname { get; set; }
        //public Int64 ecfstatus_status { get; set; }
        public string select { get; set; }
        public string Action { get; set; }

        // Despatch
        public Int64 despatchgid { get; set; }
        public string despatch_gids { get; set; }
        public Int64 despatchid { get; set; } 
        public string chq_depatch_mode { get; set; }
        public Int64 courierid { get; set; }
        public string courriername { get; set; }
        public Int64  awbno { get; set; }
        public string despatchdate { get; set; }

        public Int64 logingid { get; set; }
        public string result { get; set; }

        // inex
        public Int64 inex_gid { get; set; }
        public string inexid { get; set; }
        public string inexdate { get; set; }
        public string inexby { get; set; }
        public string inexremark { get; set; }
        public string inexreason { get; set; }
        public string remark { get; set; }
        public Int64 id { get; set; }
        public string reviewstatus { get; set; }

        public string ecfstatus_status { get; set; }
        public string Attributes { get; set; }


        //Re Audit Review
        public string queue_action_remark { get; set; }
        public Int64 queue_action_by { get; set; }
        public Int64 queue_from { get; set; }

    }
}
