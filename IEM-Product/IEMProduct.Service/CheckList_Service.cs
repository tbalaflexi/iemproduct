﻿using DataAccessHandler.Models.IEMProduct;
using IEMProduct.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace IEMProduct.Service
{
  public  class CheckList_Service
    {

        Checklist_Model ModelObject = new Checklist_Model();                                   // checklist model object
        List<Checklist_Model> ModelList = new List<Checklist_Model>();                       // checklist list model object
        CheckList_Data DataObject = new CheckList_Data();                                   // checklist data object





        public List<Checklist_Model> ReadRecord()
        {
            try
            {
                return DataObject.ReadRecord();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // Transaction Save
        public DataTable SaveRecord(Checklist_Model ModelObject)
        {
            try
            {
                return DataObject.SaveRecord(ModelObject);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Transaction Update
        public DataTable UpdateRecord(Checklist_Model ModelObject)
        {
            try
            {
                return DataObject.UpdateRecord(ModelObject);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Transaction delete
        public DataTable DeleteRecord(Checklist_Model ModelObject)
        {
            try
            {
                return DataObject.DeleteRecord(ModelObject);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // GetTranType

        public List<Checklist_Model> Getdropdown(string parentcode)
        {
            try
            {
                List<Checklist_Model> dropdown = new List<Checklist_Model>();
                DataTable dt1 = new DataTable();
                dt1 = DataObject.Getdropdown(parentcode);
                dropdown = ConvertDataTable<Checklist_Model>(dt1);
                return dropdown;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //GetTranSubType

        public List<Checklist_Model> GetTranSubType(string parentcode)
        {
            try
            {
                List<Checklist_Model> dropdown = new List<Checklist_Model>();
                DataTable dt1 = new DataTable();
                dt1 = DataObject.Getdropdown(parentcode);
                dropdown = ConvertDataTable<Checklist_Model>(dt1);
                return dropdown;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // non qc master dropdown values for atachement and  physical receipting

        public List<Checklist_Model> dropdownvalues(string flag)
        {
            try
            {
                List<Checklist_Model> dropdown = new List<Checklist_Model>();
                DataTable dt1 = new DataTable();

                dt1 = DataObject.dropdownvalues(flag);
                dropdown = ConvertDataTable<Checklist_Model>(dt1);
                return dropdown;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Convert Datatable to List
        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
        #endregion


        //get tran sub type 

        public DataTable GetValues(string TranId, string parentcode)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = DataObject.GetValues(TranId, parentcode);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
