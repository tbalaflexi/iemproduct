﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessHandler.Models.IEMProduct;
using IEMProduct.Data;
using System.Data;


 

namespace IEMProduct.Service
{

 public class FlowAssignment_Service
 {
     FlowAssignment_Model lstMdl = new FlowAssignment_Model();
     FlowAssignment_Data lstData = new FlowAssignment_Data();
     DataSet ds = new DataSet();
     DataTable dt = new DataTable();


     public List<FlowAssignment_Model> ReadFlowAssignment(Int64 User_GID,Int64 assignType, string assignRefType)
     {
         List<FlowAssignment_Model> lst = new List<FlowAssignment_Model>();
         try
         {
             dt = lstData.ReadFlowAssignment(User_GID,assignType, assignRefType);
             
             if (dt.Rows.Count > 0)
             {
                 foreach (DataRow dr in dt.Rows)
                 {
                     FlowAssignment_Model objModel = new FlowAssignment_Model();

                     objModel.assignRefID = Convert.ToInt64(dr["ecf_gid"].ToString());
                     objModel.assignRefNumber = dr["ecf_no"].ToString();
                     objModel.assignRefType = dr["TypeSubType"].ToString();
                     objModel.assignRaiserName = dr["Raiser"].ToString();
                     objModel.assignRefStatus = dr["Status"].ToString();
                     objModel.assignQueueFrom = dr["QueueFrom"].ToString();
                     objModel.assignTo = dr["AssignTo"].ToString();
                     objModel.flowassignTo = dr["AssignTo"].ToString();
                     objModel.assignRemarks = dr["Remark"].ToString();
                    
                     lst.Add(objModel);
                  }
             }

         }
         catch (Exception ex)
         {

             throw ex;
         }
         return lst;
     }

     public DataTable Save_FlowAssignmentList(FlowAssignment_Model modelObj)
     {
         try
         {
             return lstData.Save_FlowAssignmentList(modelObj);
         }
         catch (Exception ex)
         {
             throw ex;
         }
     }


     #region FlowAssignmentDrpList Bindings

     public List<FlowAssignment_Model> GetAssignmentTypeList()
     {
         List<FlowAssignment_Model> lst = new List<FlowAssignment_Model>();
         try
         {
             
             ds = lstData.GetAssignmentTypeList("");
             if (ds.Tables[0].Rows.Count > 0)
             {
                 foreach (DataRow row in ds.Tables[0].Rows )
                 {
                     FlowAssignment_Model objModel = new FlowAssignment_Model();
                     objModel.assignTypeID = Convert.ToInt32(row["TypeGid"].ToString());
                     objModel.assignTypeName = row["TypeName"].ToString();
                     lst.Add(objModel);
                 }

                
             }
         }
         catch (Exception ex)
         {
             throw ex;
         }
         return lst;

     }


     public List<FlowAssignment_Model> GetAssignToList(string EmployeeName)
     {
         List<FlowAssignment_Model> lst = new List<FlowAssignment_Model>();
         //RoleEmployee_Data ObjData = new RoleEmployee_Data();
         try
         {
             //dt = ObjData.Get_Employee_List(EmployeeName);
             ds = lstData.GetAssignmentTypeList(EmployeeName); 

             if (ds.Tables[1].Rows.Count > 0)
             {
                 for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                 {
                     FlowAssignment_Model objModel = new FlowAssignment_Model();
                     objModel.assignToID = Convert.ToInt64(ds.Tables[1].Rows[i][0].ToString());
                     objModel.assignTo = ds.Tables[1].Rows[i][1].ToString();
                     objModel.flowassignToID = Convert.ToInt64(ds.Tables[1].Rows[i][0].ToString());
                     objModel.flowassignTo = ds.Tables[1].Rows[i][1].ToString();

                     lst.Add(objModel);
                 }
             }
         }
         catch (Exception ex)
         {
             throw ex;
         }
         return lst;

     }






     public List<FlowAssignment_Model> GetAssignmentRefNoUserList(int assignTypeID, string FindSearch)
     {
         List<FlowAssignment_Model> lst = new List<FlowAssignment_Model>();
         try
         {
             string assignType = string.Empty;
             dt = lstData.GetAssignmentRefNoUserList(assignTypeID, FindSearch);
             if (dt.Rows.Count > 0)
             {
                 assignType = dt.Rows[0]["AssignType"].ToString();

                 if (assignType == "UserType")
                 {
                     foreach (DataRow row in dt.Rows)
                     {
                         FlowAssignment_Model objModel = new FlowAssignment_Model();
                         objModel.assignRefUserID = Convert.ToInt64(row["EmployeeID"].ToString());
                         objModel.assignRefUserName = row["Employee"].ToString();
                         lst.Add(objModel);
                     }
                 }

                 else if (assignType == "RefType")
                 {
                     foreach (DataRow row in dt.Rows)
                     {
                         FlowAssignment_Model objModel = new FlowAssignment_Model();
                         objModel.assignRefUserID = Convert.ToInt64(row["ECFID"].ToString());
                         objModel.assignRefUserName = row["EcfNo"].ToString();
                         lst.Add(objModel);
                     }
                 }
                 else 
                 {
                     foreach (DataRow row in dt.Rows)
                     {
                         FlowAssignment_Model objModel = new FlowAssignment_Model();
                         objModel.assignRefUserID = Convert.ToInt64(row["RefTypeID"].ToString());
                         objModel.assignRefUserName = row["RefType"].ToString();
                         lst.Add(objModel);
                     }
                 }
  

             }
         }
         catch (Exception ex)
         {
             throw ex;
         }
         return lst;

     } 

     #endregion


 }
}
