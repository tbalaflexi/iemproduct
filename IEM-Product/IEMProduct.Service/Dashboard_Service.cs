﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessHandler.Models.IEMProduct;
using IEMProduct.Data;
using System.Data;

namespace IEMProduct.Service
{
    public class Dashboard_Service
    {

        List<Dashboard_Model> ModelObject = new List<Dashboard_Model>();
        Dashboard_Data DataObject = new Dashboard_Data();
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();

        public List<Dashboard_Model> GetRaiserCountList(Int64 userId)
        {
            List<Dashboard_Model> lst = new List<Dashboard_Model>();
            try
            {
                ds = DataObject.ReadDashboardList(userId);
                dt = ds.Tables[1];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Dashboard_Model objModel = new Dashboard_Model();
                        objModel.dashTypeID = Convert.ToInt64(dr["TypeID"].ToString());
                        objModel.dashType =     dr["ClaimType"].ToString() ;
                        objModel.dashRaiserDraftCount =Convert.ToInt32(dr["Draft"].ToString());
                        objModel.dashRaiserInProcessCount = Convert.ToInt32(dr["InProcess"].ToString());
                        objModel.dashRaiserRejectedCount =Convert.ToInt32( dr["Rejected"].ToString());
                        objModel.dashRaiserMyApprovalCount =Convert.ToInt32(dr["MyApproval"].ToString());                      
                        lst.Add(objModel);
                    }
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return lst;

        }

        public List<Dashboard_Model> GetAPCountList(Int64 userId)
        {
            List<Dashboard_Model> lst = new List<Dashboard_Model>();
            try
            {
                ds = DataObject.ReadDashboardList(userId);
                dt = ds.Tables[2];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Dashboard_Model objModel = new Dashboard_Model();
                        objModel.dashTypeID = Convert.ToInt64(dr["TypeID"].ToString());
                        objModel.dashType = dr["ClaimType"].ToString();
                        objModel.dashRaiserDraftCount = Convert.ToInt32(dr["Checker"].ToString());
                        objModel.dashRaiserInProcessCount = Convert.ToInt32(dr["Authoriser"].ToString());
                        objModel.dashRaiserRejectedCount = Convert.ToInt32(dr["Payment"].ToString());                       
                        lst.Add(objModel);
                    }
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return lst;
        }

        public DataTable ReadRaiserChartList(Int64 User_GID,string dashDateFrom,string dashDateTo)
        {
            try
            {
                ds = DataObject.ReadChartList(User_GID, dashDateFrom, dashDateTo);
                dt = ds.Tables[0];
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public DataTable ReadAPChartList(Int64 User_GID, string dashDateFrom, string dashDateTo)
                 {
            try
            {
                ds = DataObject.ReadChartList(User_GID,dashDateFrom, dashDateTo);
                dt = ds.Tables[1];
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }


        public List<Dashboard_Model> ApCheckFlag(Int64 userId)
        {
            List<Dashboard_Model> lst = new List<Dashboard_Model>();
            try
            {
                ds = DataObject.ReadDashboardList(userId);
                dt = ds.Tables[3];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Dashboard_Model objModel = new Dashboard_Model();
                        objModel.IsApCheckFlag = dr["IsApFlag"].ToString();
                        
                        lst.Add(objModel);
                    }
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return lst;

        }



         
    }
}
