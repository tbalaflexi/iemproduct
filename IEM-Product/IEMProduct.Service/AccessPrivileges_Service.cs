﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccessHandler.Models.IEMProduct;
using IEMProduct.Data;

namespace IEMProduct.Service
{
    public class AccessPrivileges_Service
    {
        AccessPrivileges_Data dataObj = new AccessPrivileges_Data();
        AccessPrivileges_Model modelObj = new AccessPrivileges_Model();
        DataTable dt = new DataTable();

        public List<AccessPrivileges_Model> AccessPrivileges_GridDatas(Int64 LoginUser_Id)
        {
            List<AccessPrivileges_Model> lst_ = new List<AccessPrivileges_Model>();
            try
            {
                dt = dataObj.AccessPrivileges_GridDatas(LoginUser_Id, "GridSummery", 0);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        AccessPrivileges_Model addObj_ = new AccessPrivileges_Model();
                        addObj_.Role_SLNo = Convert.ToInt64(row["sl_no"].ToString());
                        addObj_.Role_Gid = Convert.ToInt64(row["role_id"].ToString());
                        addObj_.Role_Code = row["role_code"].ToString();
                        addObj_.Role_Name = row["role_name"].ToString();
                        addObj_.Role_GroupName = row["role_groupname"].ToString();
                        lst_.Add(addObj_);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst_;
        }

        public static AccessPrivileges_Model.UserGroups GetTreeview(Int64 Role_GId)
        {
            DataTable dt = new DataTable();
            AccessPrivileges_Data dataObj = new AccessPrivileges_Data();
            List<AccessPrivileges_Model.menu> ls_menu = new List<AccessPrivileges_Model.menu>();
            AccessPrivileges_Model.UserGroups usergroups = new AccessPrivileges_Model.UserGroups();
            try
            {
                dt = dataObj.AccessPrivileges_GridDatas(0, "TreeView", Role_GId);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        int menu_access = 0;
                        AccessPrivileges_Model.menu menu = new AccessPrivileges_Model.menu();
                        menu.menu_gid = Convert.ToInt32(dr["menu_gid"].ToString());
                        menu.parent_menu_gid = Convert.ToInt32(dr["menu_parent_gid"].ToString());
                        menu.menu_name = dr["menu_name"].ToString();
                        menu.menu_order = Convert.ToInt32(dr["menu_displayorder"].ToString());
                        if (!String.IsNullOrEmpty(dr["rolemenu_access"].ToString()))
                        {
                            menu_access = Convert.ToInt32(dr["rolemenu_access"].ToString());
                        }
                        else
                        {
                            menu_access = 0;
                        }
                        if (menu_access == 0)
                        {
                            menu.rights_flag = false;
                        }
                        else
                        {
                            menu.rights_flag = true;
                        }
                        ls_menu.Add(menu);
                    }
                }
                usergroups.menu = ls_menu;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return usergroups;
        }

        public DataTable Update_MenueRights(Int64 Role_Gid, string menuchecked, string menunotchecked, Int64 LoggedUser_ID)
        {
            try
            {
                return dataObj.Update_MenueRights(Role_Gid, menuchecked, menunotchecked, LoggedUser_ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
