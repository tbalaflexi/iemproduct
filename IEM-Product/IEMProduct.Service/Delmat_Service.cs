﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using SharedData;
using IEMProduct.Data;
using DataAccessHandler.Models.IEMProduct;
namespace IEMProduct.Service
{
    public class Delmat_Service
    {
        Delmat_Data dataObj = new Delmat_Data();
        DropDown_Bindings Objdd = new DropDown_Bindings();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();

        //delmat summary
        public List<Delmat_Model> ReadRecord(Delmat_Model objModel)
        {
            List<Delmat_Model> lst = new List<Delmat_Model>();
            try
            {
                //dt = dataObj.Get_DropDownLists(Master, Depend_Code, TypeCode, SubTypeCode, AttributeParent, AttributeChild);

                dt = dataObj.ReadRecord();
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0].ToString() != "NoRecords.!")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            Delmat_Model modelObj = new Delmat_Model();
                             

                           modelObj.Delmat_Gid = Convert.ToInt64(dt.Rows[i][0].ToString());
                            modelObj.Delmat_Name = dt.Rows[i][1].ToString();
                            modelObj.Version = Convert.ToDecimal(dt.Rows[i][2].ToString());
                            modelObj.IsActive = dt.Rows[i][3].ToString();;
                            lst.Add(modelObj);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }









        public List<Delmat_Model> Get_SlabRange(Delmat_Model objModel)
        {
            List<Delmat_Model> lst = new List<Delmat_Model>();
            try
            {
                //dt = dataObj.Get_DropDownLists(Master, Depend_Code, TypeCode, SubTypeCode, AttributeParent, AttributeChild);

                dt = dataObj.LoadSlabRange();
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0].ToString() != "NoRecords.!")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            Delmat_Model modelObj = new Delmat_Model();
                            modelObj.Slabrange_Gid = Convert.ToInt64(dt.Rows[i][0].ToString());
                            modelObj.Slabrange_Name = dt.Rows[i][1].ToString();
                            modelObj.Slabrange_From = Convert.ToDecimal(dt.Rows[i][2].ToString());
                            modelObj.Slabrange_To = Convert.ToDecimal(dt.Rows[i][3].ToString());
                            lst.Add(modelObj);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }
        public List<Delmat_Model> Get_SlabRangeLimit()
        {
            List<Delmat_Model> lst = new List<Delmat_Model>();
            try
            {
                //dt = dataObj.Get_DropDownLists(Master, Depend_Code, TypeCode, SubTypeCode, AttributeParent, AttributeChild);

                dt = dataObj.LoadSlabRange();
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0].ToString() != "NoRecords.!")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            Delmat_Model modelObj = new Delmat_Model();
                            modelObj.Slabrange_Gid = Convert.ToInt64(dt.Rows[i][0].ToString());
                            modelObj.Slabrange_Name = dt.Rows[i][1].ToString();
                            modelObj.Slabrange_LimitName = dt.Rows[i][1].ToString();
                            modelObj.Slabrange_From = Convert.ToDecimal(dt.Rows[i][2].ToString());
                            modelObj.Slabrange_To = Convert.ToDecimal(dt.Rows[i][3].ToString());
                            lst.Add(modelObj);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }
        public List<Delmat_Model> Get_TitleDropdown(string Title_Text)
        {
            List<Delmat_Model> lst = new List<Delmat_Model>();
            Delmat_Model ObjQCD = new Delmat_Model();
            try
            {
                //dt = dataObj.Get_DropDownLists(Master, Depend_Code, TypeCode, SubTypeCode, AttributeParent, AttributeChild);
                if (Title_Text.ToString().Equals("Grade"))
                {
                    ObjQCD.Master_Code = "QCD_Grade";
                    dt = Objdd.DropDownValues(ObjQCD);
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            Delmat_Model modelObj = new Delmat_Model();
                            modelObj.Title_Ref_Gid = Convert.ToInt64(dt.Rows[i][0].ToString());
                            modelObj.Title_Ref_Code = dt.Rows[i][2].ToString();
                            lst.Add(modelObj);
                        }
                    }
                }
                else if (Title_Text.ToString().Equals("Designation"))
                {
                    ObjQCD.Master_Code = "QCD_Designation";
                    dt = Objdd.DropDownValues(ObjQCD);
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            Delmat_Model modelObj = new Delmat_Model();
                            modelObj.Title_Ref_Gid = Convert.ToInt64(dt.Rows[i][0].ToString());
                            modelObj.Title_Ref_Code = dt.Rows[i][2].ToString();
                            lst.Add(modelObj);
                        }
                    }
                }
                else if (Title_Text.ToString().Equals("Role"))
                {
                    ObjQCD.Master_Code = "QCD_Role";
                    dt = Objdd.DropDownValues(ObjQCD);
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            Delmat_Model modelObj = new Delmat_Model();
                            modelObj.Title_Ref_Gid = Convert.ToInt64(dt.Rows[i][0].ToString());
                            modelObj.Title_Ref_Code = dt.Rows[i][2].ToString();
                            lst.Add(modelObj);
                        }
                    }
                }
                if (Title_Text.ToString().Equals("Employee"))
                {
                    dt = dataObj.GetEmployee();
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            Delmat_Model modelObj = new Delmat_Model();
                            modelObj.Title_Ref_Gid = Convert.ToInt64(dt.Rows[i][0].ToString());
                            modelObj.Title_Ref_Code = dt.Rows[i][1].ToString();
                            lst.Add(modelObj);
                        }
                    }
                }

               
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }
        public List<Delmat_Model> Get_Employee()
        {
            List<Delmat_Model> lst = new List<Delmat_Model>();
            try
            {
                //dt = dataObj.Get_DropDownLists(Master, Depend_Code, TypeCode, SubTypeCode, AttributeParent, AttributeChild); 
                dt = dataObj.GetEmployee();
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Delmat_Model modelObj = new Delmat_Model();
                        modelObj.Employee_Gid = Convert.ToInt64(dt.Rows[i][0].ToString());
                        modelObj.EmployeeCode = dt.Rows[i][1].ToString();
                        lst.Add(modelObj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }
        //public List<Delmat_Model> Get_SlabRange()
        //{
        //    List<Delmat_Model> lst = new List<Delmat_Model>();
        //    try
        //    {
        //        //dt = dataObj.Get_DropDownLists(Master, Depend_Code, TypeCode, SubTypeCode, AttributeParent, AttributeChild); 
        //        dt = dataObj.GetEmployee();
        //        if (dt.Rows.Count > 0)
        //        {
        //            for (int i = 0; i < dt.Rows.Count; i++)
        //            {
        //                Delmat_Model modelObj = new Delmat_Model();
        //                modelObj.Employee_Gid = Convert.ToInt64(dt.Rows[i][0].ToString());
        //                modelObj.EmployeeCode = dt.Rows[i][1].ToString();
        //                lst.Add(modelObj);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return lst;
        //}
        public List<Delmat_Model> Get_DelmatName(Delmat_Model ObjModel)
        {
            List<Delmat_Model> lst = new List<Delmat_Model>();
            try
            {
                //dt = dataObj.Get_DropDownLists(Master, Depend_Code, TypeCode, SubTypeCode, AttributeParent, AttributeChild); 
                dt = dataObj.GetDelmatName(ObjModel);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Delmat_Model modelObj = new Delmat_Model();
                        modelObj.ExistingDelmat_Gid = Convert.ToInt64(dt.Rows[i]["delmat_gid"].ToString());
                        modelObj.ExistingDelmat_Name = dt.Rows[i]["delmat_name"].ToString();
                        lst.Add(modelObj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }
        public DataTable Save_Delmat_SlabRange(Delmat_Model modelObj)
        {
            try
            {
                return dataObj.Save_Delmat_SlabRange(modelObj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<Delmat_Model> Get_Exception(Delmat_Model ObjModel)
        {
            List<Delmat_Model> lst = new List<Delmat_Model>();
            try
            {
                //dt = dataObj.Get_DropDownLists(Master, Depend_Code, TypeCode, SubTypeCode, AttributeParent, AttributeChild);

                dt = dataObj.LoadException(ObjModel);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0].ToString() != "NoRecords.!")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            Delmat_Model modelObj = new Delmat_Model();
                            modelObj.delmatexception_gid = Convert.ToInt64(dt.Rows[i][0].ToString());
                            modelObj.Employee_Gid = Convert.ToInt64(dt.Rows[i][1].ToString());
                            modelObj.EmployeeCode = dt.Rows[i][2].ToString();
                            modelObj.delmatexception_title = dt.Rows[i][3].ToString();
                            modelObj.delmatexception_Limit = Convert.ToDecimal(dt.Rows[i][4].ToString());
                            lst.Add(modelObj);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }
        public DataTable Save_Update_Delmat_Exception(Delmat_Model modelObj)
        {
            try
            {
                return dataObj.Save_Update_Delmat_Exception(modelObj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // delete delmat
        public DataTable Remove_Delmat(Int64 Delmat_Gid)
        {
            try
            {
                return dataObj.Remove_Delmat(Delmat_Gid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable Remove_Exception(Int64 Delmat_Exeption_Gid)
        {
            try
            {
                return dataObj.Remove_Exception(Delmat_Exeption_Gid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable Remove_SlabRange(Int64 SlabRange_Gid)
        {
            try
            {
                return dataObj.Remove_SlabRange(SlabRange_Gid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable Save_Update_DelmatFlow(Delmat_Model modelObj)
        {
            try
            {
                return dataObj.Save_Update_DelmatFlow(modelObj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Delmat_Model> Read_DelmatFlow(Int64 Delmat_Gid)
        {
            List<Delmat_Model> lst = new List<Delmat_Model>();
            try
            {
                //dt = dataObj.Get_DropDownLists(Master, Depend_Code, TypeCode, SubTypeCode, AttributeParent, AttributeChild);

                dt = dataObj.Read_DelmatFlow(Delmat_Gid);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0].ToString() != "NoRecords.!")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            Delmat_Model modelObj = new Delmat_Model();
                            modelObj.DelmatFlow_Gid = Convert.ToInt64(dt.Rows[i][0].ToString());
                            modelObj.Title_Text = dt.Rows[i][2].ToString();
                            modelObj.Title_Code = dt.Rows[i][2].ToString();
                            modelObj.Title_Ref_Code = dt.Rows[i][3].ToString();
                            modelObj.Title_Ref_Gid = Convert.ToInt64(dt.Rows[i][7].ToString());
                            modelObj.Slabrange_LimitName = dt.Rows[i][4].ToString();
                            modelObj.Slabrange_Gid =Convert.ToInt64(dt.Rows[i][8].ToString());
                            modelObj.IsIncluded = Convert.ToBoolean(dt.Rows[i][5]); 
                            modelObj.DelmatFlow_Order = Convert.ToInt32(dt.Rows[i][6].ToString());
                            lst.Add(modelObj);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }
        public List<Delmat_Model> Get_Title()
        {
            List<Delmat_Model> lst = new List<Delmat_Model>();
            try
            {   
                Delmat_Model modelObj = new Delmat_Model();
                modelObj.Title_Text = "--Select--";
                modelObj.Title_Code = "";
                lst.Add(modelObj);

                modelObj = new Delmat_Model();
                modelObj.Title_Text = "Grade";
                modelObj.Title_Code = "Grade";
                lst.Add(modelObj);

                modelObj = new Delmat_Model();
                modelObj.Title_Text = "Designation";
                modelObj.Title_Code = "Designation";
                lst.Add(modelObj);

                modelObj = new Delmat_Model();
                modelObj.Title_Text = "Employee";
                modelObj.Title_Code = "Employee";
                lst.Add(modelObj);

                modelObj = new Delmat_Model();
                modelObj.Title_Text = "Role";
                modelObj.Title_Code = "Role";
                lst.Add(modelObj); 

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }
        public DataTable Remove_DelmatFlow(Int64 DelmatFlow_Gid)
        {
            try
            {
                return dataObj.Remove_DelmatFlow(DelmatFlow_Gid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable Save_Update_Delmat(Delmat_Model ObjModel)
        {
            try
            {
                return dataObj.Save_Update_Delmat(ObjModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Save_DelmatMatrix(Int64 Delmat_Gid)
        {
            try
            {
                return dataObj.Save_DelmatMatrix(Delmat_Gid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Read Header

        public List<Delmat_Model> Read_DelmatHeader(Int64 Delmat_Gid)
        {
            List<Delmat_Model> lst = new List<Delmat_Model>();
            try
            {
                //dt = dataObj.Get_DropDownLists(Master, Depend_Code, TypeCode, SubTypeCode, AttributeParent, AttributeChild);

                dt = dataObj.Read_DelmatHeader(Delmat_Gid);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0].ToString() != "NoRecords.!")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            Delmat_Model modelObj = new Delmat_Model();
                            modelObj.Delmat_Gid = Convert.ToInt64(dt.Rows[i]["delmat_gid"].ToString());
                            modelObj.Delmat_Name = (dt.Rows[i]["delmat_name"].ToString());
                            modelObj.Type_gid = Convert.ToInt64(dt.Rows[i]["delmat_type_gid"].ToString());
                            modelObj.SubType_gids = dt.Rows[i]["delmatsubtype_subtype_gids"].ToString();
                            modelObj.AttributeChilds = (dt.Rows[i]["delmatattribute_attributechild_gids"].ToString());
                            modelObj.ExistingDelmatGid = Convert.ToInt64(dt.Rows[i]["delmat_Cloned_delmat_gid"].ToString());
                            modelObj.Version = Convert.ToDecimal(dt.Rows[i]["delmat_Version"].ToString());
                            modelObj.IsCloned = (dt.Rows[i]["Delmat_Cloned"].ToString());
                            modelObj.IsActive = (dt.Rows[i]["delmat_active"].ToString());
                            modelObj.Dept_Gids = Convert.ToString(dt.Rows[i]["delmatdept_dept_gids"]);
                            
                            lst.Add(modelObj);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }
 

    }
}
