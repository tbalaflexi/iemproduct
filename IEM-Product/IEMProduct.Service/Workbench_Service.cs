﻿using DBHelper;
using IEMProduct.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMProduct.Service
{


   public class Workbench_Service
    {

        Workbench_Model ModelObject = new Workbench_Model();                                   // Workbench model object
        List<Workbench_Model> ModelList = new List<Workbench_Model>();                       // Workbench list model object
                                // Attachment data object
        Workbench_Data DataObject = new Workbench_Data();


        // Workbench read 
        public List<Workbench_Model> ReadRecord(Int64 User_Gid, Int64 Login_Gid, string LoginMode)
        {
            try
            {
                return DataObject.ReadRecord(User_Gid, Login_Gid, LoginMode);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<Workbench_Model> ReadRecordApprover(Int64 User_Gid, Int64 Login_Gid, string LoginMode)
        {
            try
            {
                return DataObject.ReadRecordApprover(User_Gid, Login_Gid, LoginMode);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

         public List<Workbench_Model> ReadActionHistory(Workbench_Model obj)
        {
            try
            {
                return DataObject.ReadActionHistory(obj);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        
    }
}
