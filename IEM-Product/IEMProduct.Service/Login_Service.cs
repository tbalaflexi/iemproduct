﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using IEMProduct.Data;
using System.Security.Cryptography;
using System.IO;
using DataAccessHandler.Models.IEMProduct;

namespace IEMProduct.Service
{
    public class Login_Service
    {
        Login_Data dataObj = new Login_Data();
        DataTable dt = new DataTable();
        public DataTable CheckUserCredential(string usercode, string userpassword, string LoginMode, string ProxyFrom)
        {
            string EncryptionKey = "abc123";
            string encryptedpassword = string.Empty;
            try
            {
                byte[] clearBytes = System.Text.Encoding.Unicode.GetBytes(userpassword);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        encryptedpassword = Convert.ToBase64String(ms.ToArray());
                    }
                }
                return dataObj.CheckUserCredential(usercode, encryptedpassword, LoginMode, ProxyFrom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Proxy_Model> Get_ProxyFrom(string UserName)
        {
            List<Proxy_Model> lst = new List<Proxy_Model>();
            try
            {
                dt = dataObj.Get_ProxyFrom(UserName);
                foreach (DataRow row in dt.Rows)
                {
                    Proxy_Model objModel = new Proxy_Model();
                    objModel.proxyFromId = Convert.ToInt16(row["Proxy_From_Gid"].ToString());
                    objModel.proxyFrom = row["Proxy_From"].ToString();
                    lst.Add(objModel);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }
    }
}
