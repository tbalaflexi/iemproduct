﻿using DataAccessHandler.Models.IEMProduct;
using IEMProduct.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DBHelper;
//using SharedData;
using System.Security.Cryptography;
using System.IO;

namespace IEMProduct.Service
{
   public class ProfileUpload_Service
    {
        #region Declarations
        ProfileUpload_Data objData = new ProfileUpload_Data();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        List<ProfileUpload_Model> lst = new List<ProfileUpload_Model>();
        #endregion

        public List<ProfileUpload_Model> Get_ProfileUPloadDetailList(ProfileUpload_Model Obj_Model)
        {

            try
            {
                Int64 j = 0;
                ds = objData.Get_ProfileUploadDetailList(Obj_Model);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                      
                        ProfileUpload_Model _obj = new ProfileUpload_Model();
                         j++;
                        _obj.SlNo = j;
                        _obj.EmpUpload_Gid = Convert.ToInt64(dt.Rows[i]["empupload_gid"].ToString());
                        _obj.File_Gid = Convert.ToInt64(dt.Rows[i]["file_gid"].ToString());
                        _obj.Emp_Code = dt.Rows[i]["employee_code"].ToString();

                        _obj.Emp_Name = dt.Rows[i]["employee_name"].ToString();
                        _obj.Emp_Gender = dt.Rows[i]["employee_gender"].ToString();
                        _obj.Emp_Dob = dt.Rows[i]["employee_dob"].ToString();
                        _obj.Emp_Doj = dt.Rows[i]["employee_doj"].ToString();
                        _obj.Designation_code = dt.Rows[i]["designation_code"].ToString();
                        _obj.Grade_code = dt.Rows[i]["grade_code"].ToString();
                        _obj.Dept_Code = dt.Rows[i]["dept_code"].ToString();
                        _obj.Emp_Branch_Code = dt.Rows[i]["employee_branch_code"].ToString();
                        _obj.FC_Code = dt.Rows[i]["FC_Code"].ToString();
                        _obj.CC_Code = dt.Rows[i]["CC_Code"].ToString();
                        _obj.Product_Code = dt.Rows[i]["product_code"].ToString();
                        _obj.Addr1 = dt.Rows[i]["addr1"].ToString();
                        _obj.Addr2 = dt.Rows[i]["addr2"].ToString();
                        _obj.City_Code = dt.Rows[i]["city_code"].ToString();
                        _obj.Pincode = dt.Rows[i]["pincode"].ToString();
                        _obj.Personal_email = dt.Rows[i]["personal_email"].ToString();
                        _obj.Office_Email = dt.Rows[i]["office_email"].ToString();
                        _obj.Mobil_No = dt.Rows[i]["mobile_no"].ToString();
                        _obj.Bank_Code = dt.Rows[i]["Bank_Code"].ToString();
                        _obj.Bank_Branch_Code = dt.Rows[i]["bank_branch_code"].ToString();
                        _obj.Acc_No = dt.Rows[i]["Acc_No"].ToString();
                        _obj.Acc_Type = dt.Rows[i]["Acc_Type"].ToString();
                        _obj.Emp_Card_No = dt.Rows[i]["employee_card_no"].ToString();
                        _obj.Emp_Parmanent_Addr = dt.Rows[i]["employee_permanent_addr"].ToString();
                        _obj.Supervisor_Code = dt.Rows[i]["supervisor_code"].ToString();
                        _obj.Employee_dor = dt.Rows[i]["employee_dor"].ToString();
                        _obj.Last_Working_date = dt.Rows[i]["last_working_date"].ToString();
                        _obj.Effictive_date = dt.Rows[i]["effictive_date"].ToString();
                        _obj.Emp_Upload_Status = dt.Rows[i]["employe_upload_status"].ToString();
                        _obj.Emp_Upload_Remarks = dt.Rows[i]["Remarks"].ToString();
                        
                        lst.Add(_obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }
        public Int64 Get_FileCount(ProfileUpload_Model Obj_Model)
        {
            Int64 FileCount = 0;
            try
            {

                ds = objData.Get_ProfileUploadFileCount(Obj_Model);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        FileCount = Convert.ToInt64(dt.Rows[i]["FileCount"]);

                    }
                }

                return FileCount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int64 FileInst_Data(ProfileUpload_Model Obj_Model)
        {
            Int64 FileGid = 0;
            try
            {
                ds = objData.FileInst_Data(Obj_Model);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        FileGid = Convert.ToInt64(dt.Rows[i]["File_gid"]);
                    }
                }

                return FileGid;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable SaveTempProfileUploadData(ProfileUpload_Model Obj_Model)
        {
            try
            {
                dt = objData.SetProfileUpload_TempTable(Obj_Model);

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Int64 TruncateTempData(ProfileUpload_Model Obj_Model)
        {
            Int64 Result = 0;
            try
            {
                dt = objData.DeleteTempTable(Obj_Model);
                if (dt.Rows.Count > 0)
                {
                    Result = 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
   
        public DataTable SetFileConfirm_Data(ProfileUpload_Model Obj_Model)
        {
            try
            {
                dt = objData.SetFileConfirm_Data(Obj_Model);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
