﻿using DataAccessHandler.Models.IEMProduct;
using IEMProduct.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace IEMProduct.Service
{
    public class Query_Service
    {
        Query_Model ModelObject = new Query_Model();                                   // Query model object
        List<Query_Model> ModelList = new List<Query_Model>();                       // Query list model object
        Query_Data DataObject = new Query_Data();                                   // Query data object  , Int64 User_GID,, User_GID
        DataTable dt1 = new DataTable();
        //Query Read Int64 User_Gid
        public List<Query_Model> ReadRecord(Query_Model ModelObject)
        {
            try
            {
                return DataObject.ReadRecord(ModelObject);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // Department Dropdown 
        public List<Query_Model> GetDepartment(string parentcode)
        {
            try
            {
                List<Query_Model> dropdown = new List<Query_Model>();

                dt1 = DataObject.GetDepartment(parentcode);
                dropdown = ConvertDataTable<Query_Model>(dt1);
                return dropdown;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //// Status Dropdown
        //public List<Query_Model> Getstatus(string parentcode)
        //{
        //    try
        //    {
        //        List<Query_Model> dropdown = new List<Query_Model>();

        //        dt1 = DataObject.GetDepartment(parentcode);
        //        dropdown = ConvertDataTable<Query_Model>(dt1);
        //        return dropdown;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        // status dropdown binding

        public List<Query_Model> dropdownvalues(string flag)
        {
            try
            {
                List<Query_Model> dropdown = new List<Query_Model>();
                DataTable dt1 = new DataTable();

                dt1 = DataObject.dropdownvalues(flag);
                dropdown = ConvertDataTable<Query_Model>(dt1);
                return dropdown;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Convert Datatable to List
        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
        #endregion
    }
}
