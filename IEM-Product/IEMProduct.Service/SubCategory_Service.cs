﻿using DBHelper;
using IEMProduct.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace IEMProduct.Service
{
    public class SubCategory_Service
    {
        SubCategory_Model ModelObject = new SubCategory_Model();                                   // Sub Category model object
        List<SubCategory_Model> ModelList = new List<SubCategory_Model>();                       //  Sub Category list model object
        SubCategory_Data DataObject = new SubCategory_Data();                                   //  Sub Category data object

        // Sub Category read 
        public List<SubCategory_Model> ReadRecord()
        {
            try
            {
                return DataObject.ReadRecord();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // Sub Category Save
        public DataTable SaveRecord(SubCategory_Model ModelObject)
        {
            try
            {
                return DataObject.SaveRecord(ModelObject);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Sub Category Update
        public DataTable UpdateRecord(SubCategory_Model ModelObject)
        {
            try
            {
                return DataObject.UpdateRecord(ModelObject);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Sub Category delete
        public DataTable DeleteRecord(SubCategory_Model ModelObject)
        {
            try
            {
                return DataObject.DeleteRecord(ModelObject);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //dropdown bindig Nature Of Expense
        public List<SubCategory_Model> Getdropdown(string parentcode)
        {
            try
            {
                List<SubCategory_Model> dropdown = new List<SubCategory_Model>();
                DataTable dt1 = new DataTable();
                dt1 = DataObject.Getdropdown(parentcode);
                dropdown = ConvertDataTable<SubCategory_Model>(dt1);
                return dropdown;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        #region Convert Datatable to List
        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
        #endregion


        //get values
        public DataTable GetValues(string TranId, string parentcode)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = DataObject.GetValues(TranId, parentcode);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
