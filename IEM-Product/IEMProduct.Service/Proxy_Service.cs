﻿using DataAccessHandler.Models.IEMProduct;
using IEMProduct.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace IEMProduct.Service
{
    public  class Proxy_Service
    {


      
        List<Proxy_Model> ModelList = new List<Proxy_Model>();
        Proxy_Data DataObject = new Proxy_Data();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        // Transaction read 
        public List<Proxy_Model> ReadRecord(Int64 userId)
        {
            List<Proxy_Model> lst = new List<Proxy_Model>();
            try
            {               
                ds = DataObject.ReadRecord(userId);
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Proxy_Model objModel = new Proxy_Model();
                        objModel.proxyGid = Convert.ToInt64(dr["proxyGid"].ToString());
                        objModel.proxyFromId = Convert.ToInt64(dr["proxyFromId"].ToString());
                        objModel.proxyFrom = dr["proxyFrom"].ToString();
                        objModel.proxyToId = Convert.ToInt64(dr["proxyToId"].ToString());
                        objModel.proxyTo = dr["proxyTo"].ToString();
                        objModel.proxyPeriodFrom = dr["proxyPeriodFrom"].ToString();
                        objModel.proxyPeriodTo = dr["proxyPeriodTo"].ToString();
                        objModel.proxyActiveId = dr["proxyActiveId"].ToString(); 
                        objModel.proxyActive = dr["proxyActive"].ToString();
                        objModel.Selected_supervisor_gid = Convert.ToInt64(dr["proxyFromId"].ToString());
                        lst.Add(objModel);
                    }
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return lst;
        }


        public List<Proxy_Model> Get_EmployeeList(Int64 userId)
        {
            List<Proxy_Model> lst = new List<Proxy_Model>();
            try
            {
              
                ds = DataObject.ReadRecord(userId);
                dt = ds.Tables[1];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Proxy_Model objModel = new Proxy_Model();
                        objModel.proxyToId = Convert.ToInt64(dr["proxyToId"].ToString());
                        objModel.proxyTo = dr["proxyTo"].ToString();
                     
                        lst.Add(objModel);
                    }
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return lst;
        }
         

        public DataTable Save_Update_ProxyDetails(Proxy_Model modelObj)
        {
            try
            {
                return DataObject.Save_Update_ProxyDetails(modelObj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Remove_ProxyDetails(Proxy_Model modelObj)
        {
            try
            {
                return DataObject.Remove_ProxyDetails(modelObj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //dropdown emp name and code
        public DataTable GetEmployee_ByCodeorName(string EmployeeName, string flag, string Master)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = DataObject.GetEmployee_ByCodeorName(EmployeeName, flag, Master);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
