﻿//using DataAccessHandler.Models.IEMProduct;
using DBHelper;
using IEMProduct.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.IO;

namespace IEMProduct.Service
{
    public class Attachment_Service
    {
        Attachment_Model ModelObject = new Attachment_Model();                                   // Attachment model object
        List<Attachment_Model> ModelList = new List<Attachment_Model>();                       // Attachment list model object
        //Attachment_Data DataObject = new Attachment_Data();                                   // Attachment data object
        Attachment_Data DataObject = new Attachment_Data();
        DataTable dt = new DataTable();

        // Attachment read 
        public List<Attachment_Model> ReadRecord(Attachment_Model ModelObject)
        {
            try
            {
                return DataObject.ReadRecord(ModelObject);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // Attachment Save
        public DataTable SaveRecord(Attachment_Model ModelObject)
        {
            try
            {
                return DataObject.SaveRecord(ModelObject);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Attachment delete
        public DataTable DeleteRecord(Attachment_Model ModelObject)
        {
            try
            {
                return DataObject.DeleteRecord(ModelObject);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //dropdown binding

        public List<Attachment_Model> dropdownvalues(string flag)
        {
            try
            {
                List<Attachment_Model> dropdown = new List<Attachment_Model>();
                DataTable dt1 = new DataTable();

                dt1 = DataObject.dropdownvalues(flag);
                dropdown = ConvertDataTable<Attachment_Model>(dt1);
                return dropdown;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Convert Datatable to List
        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
        #endregion



        #region get document informations.
        public DataTable Get_Document_Info(Int64 attachment_gid)
        {
            try
            {
                return DataObject.Get_Document_Info(attachment_gid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region Audit Trail
        public List<Attachment_Model> Read_AuditTrail(Int64 ECF_Gid)
        {
            List<Attachment_Model> lst = new List<Attachment_Model>();
            try
            {
                dt = DataObject.Read_AuditTrail(ECF_Gid);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Attachment_Model modelObj = new Attachment_Model();
                        modelObj.QiD = Convert.ToInt64(dt.Rows[i]["QiD"].ToString());
                        modelObj.EmpCode = (dt.Rows[i]["EmpCode"].ToString());
                        modelObj.EmpName = dt.Rows[i]["EmpName"].ToString();
                        modelObj.ActionDate = dt.Rows[i]["ActionDate"].ToString();
                        modelObj.ActionType = (dt.Rows[i]["ActionType"].ToString());
                        modelObj.Remarks = dt.Rows[i]["Remarks"].ToString();
                        modelObj.Designation = dt.Rows[i]["Designation"].ToString();

                        lst.Add(modelObj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }
        #endregion

        public List<Attachment_Model> GetAttachments(Attachment_Model _objModel)
        {
            List<Attachment_Model> lst = new List<Attachment_Model>();
            try
            {
                dt = DataObject.GetAttachments(_objModel);
                if (dt.Rows.Count > 0)
                {
                   foreach(DataRow row in dt.Rows)
                    {
                        Attachment_Model modelObj = new Attachment_Model();
                        modelObj.ecf_no = row["EcfNo"].ToString();
                        modelObj.attachment_gid = Convert.ToInt64(row["Gid"].ToString());
                        modelObj.attachment_filename = row["FileName"].ToString();
                        modelObj.docgrpname = row["TypeName"].ToString();
                        modelObj.attachment_desc = row["Description"].ToString();
                        modelObj.attachment_inserted_by = row["AttachmentBy"].ToString();
                        modelObj.attachment_inserted_date = row["AttachmentDate"].ToString();
                        lst.Add(modelObj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }
    }
}
