﻿using DataAccessHandler.Models.IEMProduct;
using IEMProduct.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace IEMProduct.Service
{
   public class PeriodClose_Service
    {
        PeriodClose_Model ModelObject = new PeriodClose_Model();                                   // PeriodClose model object
        List<PeriodClose_Model> ModelList = new List<PeriodClose_Model>();                       // PeriodClose list model object
        PeriodClose_Data DataObject = new PeriodClose_Data();                                   // PeriodClose data object



        // PeriodClose read 
        public List<PeriodClose_Model> ReadRecord(Int64 User_Gid)
        {
            try
            {
                return DataObject.ReadRecord(User_Gid);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // PeriodClose Save
        public DataTable SaveRecord(PeriodClose_Model ModelObject, Int64 User_Gid)
        {
            try
            {
                return DataObject.SaveRecord(ModelObject, User_Gid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //PeriodClose Update
        public DataTable UpdateRecord(PeriodClose_Model ModelObject, Int64 User_Gid)
        {
            try
            {
                return DataObject.UpdateRecord(ModelObject, User_Gid);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
       // non qc dropdown values
        //public DataTable dropdownvalues(string flag)
        //{
        //    try
        //    {
        //        DataTable dt = new DataTable();
        //        dt = DataObject.dropdownvalues(flag);
        //        return dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        public List<PeriodClose_Model> dropdownvalues(string flag)
        {
            try
            {
                List<PeriodClose_Model> dropdown = new List<PeriodClose_Model>();
                DataTable dt1 = new DataTable();

                dt1 = DataObject.dropdownvalues(flag);
                dropdown = ConvertDataTable<PeriodClose_Model>(dt1);
                return dropdown;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Convert Datatable to List
        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
        #endregion

    }
}
