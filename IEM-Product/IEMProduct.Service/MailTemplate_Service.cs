﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccessHandler.Models.IEMProduct;
using IEMProduct.Data;

namespace IEMProduct.Service
{
    public class MailTemplate_Service
    {
        MailTemplate_Data dataObj = new MailTemplate_Data();
        DataTable dt = new DataTable();

        public List<MailTemplate_Model> Get_MailTemplate_Details(string Master, Int64 Depend_Id, Int64 LoginUser_ID)
        {
            List<MailTemplate_Model> lst_ = new List<MailTemplate_Model>();
            try
            {
                dt = dataObj.Get_MailTemplate_Details(Master, Depend_Id, LoginUser_ID);
                if (dt.Rows.Count > 0)
                {
                    if (Master == "Type")
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            MailTemplate_Model listObj = new MailTemplate_Model();
                            listObj.mailtemplate_type_id = Convert.ToInt64(row["type_id"].ToString());
                            listObj.mailtemplate_type_name = row["type_name"].ToString();
                            lst_.Add(listObj);
                        }

                    }
                    else if (Master == "SubType")
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            MailTemplate_Model listObj = new MailTemplate_Model();
                            listObj.mailtemplate_subtype_id = Convert.ToInt64(row["subtype_id"].ToString());
                            listObj.mailtemplate_subtype_name = row["subtype_name"].ToString();
                            lst_.Add(listObj);
                        }
                    }
                    else if (Master == "Status")
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            MailTemplate_Model listObj = new MailTemplate_Model();
                            listObj.mailtemplate_status_id = Convert.ToInt64(row["ecfstatus_gid"].ToString());
                            listObj.mailtemplate_status_name = row["ecfstatus_displayname"].ToString();
                            lst_.Add(listObj);
                        }
                    }
                    else if (Master == "ToRole")
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            MailTemplate_Model listObj = new MailTemplate_Model();
                            listObj.mailtemplate_torole_id = Convert.ToInt64(row["role_id"].ToString());
                            listObj.mailtemplate_torole_name = row["role_name"].ToString();
                            lst_.Add(listObj);
                        }
                    }
                    else if (Master == "CCRole")
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            MailTemplate_Model listObj = new MailTemplate_Model();
                            listObj.mailtemplate_ccrole_id = Convert.ToInt64(row["role_id"].ToString());
                            listObj.mailtemplate_ccrole_name = row["role_name"].ToString();
                            lst_.Add(listObj);
                        }
                    }
                    else if (Master == "MailFields")
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            MailTemplate_Model listObj = new MailTemplate_Model();
                            listObj.mailfield_id = Convert.ToInt64(row["mailfield_gid"].ToString());
                            listObj.mailfield_name = row["mailfiel_field_name"].ToString();
                            lst_.Add(listObj);
                        }
                    }
                    else if (Master == "MailGridSummery")
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            MailTemplate_Model listObj = new MailTemplate_Model();
                            listObj.mailtemplate_id = Convert.ToInt64(row["mailtemplate_gid"].ToString());
                            listObj.mailtemplate_type_id =Convert.ToInt64(row["Type_Id"].ToString());
                            listObj.mailtemplate_type_name = row["Type_Name"].ToString();
                            listObj.subtype_ids = row["SubType_Id"].ToString();
                            listObj.mailtemplate_subtype_name = row["SubType_Name"].ToString();
                            listObj.status_ids = row["Status_Id"].ToString();
                            listObj.mailtemplate_status_name = row["ecfstatus_displayname"].ToString();
                            listObj.torole_ids = row["ToRole_Id"].ToString();
                            listObj.mailtemplate_torole_name = row["ToRole_Name"].ToString();
                            listObj.ccrole_ids = row["CCRole_Id"].ToString();
                            listObj.mailtemplate_ccrole_name = row["CCRole_Name"].ToString();
                            listObj.mailtemplate_subject = row["mailtemplate_subject"].ToString();
                            listObj.mailtemplate_template = row["mailtemplate_template_name"].ToString();
                            listObj.mailtemplate_attachment = row["mailtemplate_attachments"].ToString();
                            listObj.mailfields_ids = row["mailtemplate_mailfield_gid"].ToString();
                            listObj.mailfield_name = row["feilds_name"].ToString();
                            listObj.mailtemplate_content = row["mailtemplate_content"].ToString();
                            listObj.mailtemplate_signature = row["mailtemplate_signature"].ToString();
                            listObj.supplier_flag = row["mailtemplate_supplier_flag"].ToString();
                            listObj.mailtemplate_additionalcc = row["mailtemplate_additional_cc"].ToString();
                            lst_.Add(listObj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst_;
        }

        public DataTable Save_MailTemplate(MailTemplate_Model objMail)
        {
            try
            {
                return dataObj.Save_MailTemplate(objMail);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Delete_MailTemplate(string Master, Int64 Depend_Id, Int64 LoginUser_ID)
        {
            try
            {
                return dataObj.Get_MailTemplate_Details(Master, Depend_Id, LoginUser_ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
