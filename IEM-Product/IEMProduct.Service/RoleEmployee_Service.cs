﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessHandler.Models.IEMProduct;
using IEMProduct.Data;
using System.Data;

namespace IEMProduct.Service
{
    public class RoleEmployee_Service
    {
        DataTable dt = new DataTable();
        RoleEmployee_Model ObjModel = new RoleEmployee_Model();
        RoleEmployee_Data ObjData = new RoleEmployee_Data();

        public List<RoleEmployee_Model> ReadRecord()
        {
            List<RoleEmployee_Model> lst = new List<RoleEmployee_Model>();
            try
            {
                dt = ObjData.ReadRole();
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        RoleEmployee_Model modelObj = new RoleEmployee_Model();

                        modelObj.Role_Gid = Convert.ToInt64(dt.Rows[i][0].ToString());
                        modelObj.Role = dt.Rows[i][2].ToString();
                        modelObj.RoleGroup = dt.Rows[i][4].ToString();
                        lst.Add(modelObj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }
        public List<RoleEmployee_Model> Read_EmployeeRole(RoleEmployee_Model objModel)
        {
            List<RoleEmployee_Model> lst = new List<RoleEmployee_Model>();
            try
            {
                dt = ObjData.ReadEmployeeByRole(objModel);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        RoleEmployee_Model modelObj = new RoleEmployee_Model();

                        modelObj.roleemployee_gid = Convert.ToInt64(dt.Rows[i][0].ToString());
                        modelObj.EmployeeCode = dt.Rows[i][1].ToString();
                        modelObj.EmployeeName = dt.Rows[i][2].ToString();
                        modelObj.Department = dt.Rows[i][3].ToString();
                        modelObj.Designation = dt.Rows[i][4].ToString();
                        lst.Add(modelObj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }

        // delete delmat
        public DataTable Remove_EmployeeRole(Int64 RoleEmployee_Gid, Int64 LoginUserId)
        {
            try
            {
                return ObjData.Remove_EmployeeRole(RoleEmployee_Gid, LoginUserId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable Save_EmployeeRole(RoleEmployee_Model ObjModel, Int64 LoginUserId)
        {
            try
            {
                return ObjData.Save_EmployeeRole(ObjModel, LoginUserId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<RoleEmployee_Model> Get_Employee_List(string EmployeeName)
        {
            List<RoleEmployee_Model> lst = new List<RoleEmployee_Model>();
            try
            {
                dt = ObjData.Get_Employee_List(EmployeeName);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        RoleEmployee_Model modelObj = new RoleEmployee_Model();
                        modelObj.Employee_Gid = Convert.ToInt64(dt.Rows[i][0].ToString());
                        modelObj.EmployeeName = dt.Rows[i][1].ToString(); 
                        lst.Add(modelObj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }
        public List<RoleEmployee_Model> Get_AllEmployee_List(string Master)
        {
            List<RoleEmployee_Model> lst = new List<RoleEmployee_Model>();
            try
            {
                dt = ObjData.Get_ALLEmployee_List(Master);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        RoleEmployee_Model modelObj = new RoleEmployee_Model();
                        modelObj.Employee_Gid = Convert.ToInt64(dt.Rows[i][0].ToString());
                        modelObj.EmployeeCode = dt.Rows[i][1].ToString();
                        modelObj.EmployeeName = dt.Rows[i][2].ToString();
                        lst.Add(modelObj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }

        public List<RoleEmployee_Model> ReadRoleByEmployee(RoleEmployee_Model ObjModel)
        {
            List<RoleEmployee_Model> lst = new List<RoleEmployee_Model>();
            try
            {
                dt = ObjData.ReadRoleByEmployee(ObjModel);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        RoleEmployee_Model modelObj = new RoleEmployee_Model();

                        modelObj.Role_Gid = Convert.ToInt64(dt.Rows[i][0].ToString());
                        modelObj.Role = dt.Rows[i][1].ToString();
                        lst.Add(modelObj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }

        public List<RoleEmployee_Model> Read_Role()
        {
            List<RoleEmployee_Model> lst = new List<RoleEmployee_Model>();
            try
            {
                dt = ObjData.ReadRole();
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        RoleEmployee_Model modelObj = new RoleEmployee_Model();

                        modelObj.Role_Gid = Convert.ToInt64(dt.Rows[i][0].ToString());
                        modelObj.Role = dt.Rows[i][2].ToString();
                        modelObj.RoleGroup = dt.Rows[i][4].ToString();
                        lst.Add(modelObj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }
        public DataTable Save_RoleToEmployee(RoleEmployee_Model ObjModel, Int64 LoginUserId)
        {
            try
            {
                return ObjData.Save_RoleToEmployee(ObjModel, LoginUserId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
