﻿using DataAccessHandler.Models.IEMProduct;
using IEMProduct.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace IEMProduct.Service
{
   public class Delegation_Service
    {
        Delegation_Model ModelObject = new Delegation_Model();                                   // Delegation model object
        List<Delegation_Model> ModelList = new List<Delegation_Model>();                       // Delegation list model object
        Delegation_Data DataObject = new Delegation_Data();                                   // Delegation data object



        // Delegation read 
        public List<Delegation_Model> ReadRecord(Int64 User_Gid)
        {
            try
            {
                return DataObject.ReadRecord(User_Gid);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // Delegation Save
        public DataTable SaveRecord(Delegation_Model ModelObject, Int64 User_Gid)
        {
            try
            {
                return DataObject.SaveRecord(ModelObject,User_Gid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       //Delegation Update
        public DataTable UpdateRecord(Delegation_Model ModelObject, Int64 User_Gid)
        {
            try
            {
                return DataObject.UpdateRecord(ModelObject, User_Gid);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

       //Delegation Delete

        public DataTable DeleteRecord(Delegation_Model ModelObject, Int64 User_Gid)
        {
            try
            {
                return DataObject.DeleteRecord(ModelObject, User_Gid);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
       // dropdown delmat type
        public List<Delegation_Model> Getdropdown(string parentcode)
        {
            try
            {
                List<Delegation_Model> dropdown = new List<Delegation_Model>();
                DataTable dt1 = new DataTable();
                dt1 = DataObject.Getdropdown(parentcode);
                dropdown = ConvertDataTable<Delegation_Model>(dt1);
                return dropdown;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


       // dropdown values for role
        public List<Delegation_Model> GetRole(string parentcode)
        {
            try
            {
                List<Delegation_Model> dropdown = new List<Delegation_Model>();
                DataTable dt1 = new DataTable();
                dt1 = DataObject.Getdropdown(parentcode);
                dropdown = ConvertDataTable<Delegation_Model>(dt1);
                //if (dt1.Rows.Count > 0)
                //{
                //    foreach (DataRow row in dt1.Rows)
                //    {
                //        Delegation_Model _objmod = new Delegation_Model();
                //        _objmod.Roleid = 0;
                //        _objmod.Rolename = "0";
                //        dropdown.Add(_objmod);
                //    }
                //}
                return dropdown;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Non QC Master DropDown Values

        //public List<Delegation_Model> dropdownvalues(string flag, string EmployeeName)
        //{
        //    try
        //    {
        //        List<Delegation_Model> dropdown = new List<Delegation_Model>();
        //        DataTable dt1 = new DataTable();

        //        dt1 = DataObject.dropdownvalues(flag, EmployeeName);
        //        dropdown = ConvertDataTable<Delegation_Model>(dt1);
        //        return dropdown;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public DataTable GetEmployee_ByCodeorName(string EmployeeName, string flag, string Master)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = DataObject.GetEmployee_ByCodeorName(EmployeeName, flag, Master);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Convert Datatable to List
        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
        #endregion
    }
}
