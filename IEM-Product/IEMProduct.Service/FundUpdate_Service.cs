﻿using DataAccessHandler.Models.IEMProduct;
using IEMProduct.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace IEMProduct.Service
{
  public  class FundUpdate_Service
    {

        FundUpdate_Model ModelObject = new FundUpdate_Model();                                   // checklist model object
        List<FundUpdate_Model> ModelList = new List<FundUpdate_Model>();                       // checklist list model object
        FundUpdate_Data DataObject = new FundUpdate_Data();                                   // checklist data object





        public List<FundUpdate_Model> ReadRecord()
        {
            try
            {
                return DataObject.ReadRecord();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // Transaction Save
        public DataTable SaveRecord(FundUpdate_Model ModelObject)
        {
            try
            {
                return DataObject.SaveRecord(ModelObject);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Transaction Update
        public DataTable UpdateRecord(FundUpdate_Model ModelObject)
        {
            try
            {
                return DataObject.UpdateRecord(ModelObject);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Transaction delete
        public DataTable DeleteRecord(FundUpdate_Model ModelObject)
        {
            try
            {
                return DataObject.DeleteRecord(ModelObject);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //Get Pay bank  bank name
        public List<FundUpdate_Model> dropdownvalues(string flag)
        {
            try
            {
                List<FundUpdate_Model> dropdown = new List<FundUpdate_Model>();
                DataTable dt1 = new DataTable();

                dt1 = DataObject.dropdownvalues(flag);
                if (dt1.Rows.Count > 0)
                {
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        FundUpdate_Model Obj = new FundUpdate_Model();
                        Obj.fundupdate_paybankname_id = Convert.ToInt64(dt1.Rows[i]["fundupdate_paybankname_id"]);
                        Obj.bankname = Convert.ToString(dt1.Rows[i]["bankname"]);
                        dropdown.Add(Obj);
                    }
                }
                return dropdown;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<FundUpdate_Model> dropdowns_values(string flag)
        {
            try
            {
                List<FundUpdate_Model> binding = new List<FundUpdate_Model>();
                DataTable dt1 = new DataTable();

                dt1 = DataObject.dropdownvalues(flag);
                if (dt1.Rows.Count > 0)
                {
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        FundUpdate_Model Objvalues = new FundUpdate_Model();
                        Objvalues.id = Convert.ToInt64(dt1.Rows[i]["id"]);
                        Objvalues.accountNo = Convert.ToString(dt1.Rows[i]["accountNo"]);
                        binding.Add(Objvalues);
                    }
                }
                return binding;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #region Convert Datatable to List
        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
        #endregion

        public DataTable GetValues(string TranId, string parentcode)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = DataObject.GetValues(TranId, parentcode);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



    }
}
