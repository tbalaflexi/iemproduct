﻿using DataAccessHandler.Models.IEMProduct;
using IEMProduct.Data;
using System;
using System.Collections.Generic;
using System.Data;
using DBHelper;
//using SharedData;
using System.Security.Cryptography;
using System.IO;
 

namespace IEMProduct.Service
{
   public  class Profile_Service
    {
       List<Profile_Model> ModelList = new List<Profile_Model>();
       Profile_Data DataObject = new Profile_Data();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
       Profile_Model Obj_Model = new Profile_Model() ;
      // DropDown_Bindings ObjShared = new DropDown_Bindings();
       QCD_Model objModelQcd = new QCD_Model();

       public DataTable  GetEmpDetails_List(Profile_Model Obj_Model)
        {
            
            try
            {               
                dt  = DataObject.GetEmpDetails(Obj_Model);              

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dt;
        }

       public List<Profile_Model> Get_EmployeeRead(Profile_Model Obj_Model)
       {

           try
           {
               Obj_Model.Action = "ALL";
               dt = DataObject.GetEmpDetails(Obj_Model);            
                   
                   for (int i = 0; i < dt.Rows.Count; i++)
                   {
                       Profile_Model _obj = new Profile_Model();
                     
                       _obj.Emp_Gid = Convert.ToInt64(dt.Rows[i]["Emp_Gid"].ToString());
                     //  cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Providergstin, string.IsNullOrEmpty(data.ProviderGstn) ? "0" : data.ProviderGstn, DbType.String));
                       _obj.Emp_Code = string.IsNullOrEmpty(dt.Rows[i]["Emp_Code"].ToString()) ? "" : dt.Rows[i]["Emp_Code"].ToString();
                       _obj.Emp_Name = string.IsNullOrEmpty(dt.Rows[i]["Emp_Name"].ToString()) ? "" : dt.Rows[i]["Emp_Name"].ToString();
                       _obj.Gender = string.IsNullOrEmpty(dt.Rows[i]["Gender"].ToString()) ? "" : dt.Rows[i]["Gender"].ToString();
                       _obj.DOB =   string.IsNullOrEmpty(dt.Rows[i]["EMP_DOB"].ToString()) ? "" : dt.Rows[i]["EMP_DOB"].ToString();
                       _obj.DOJ =  string.IsNullOrEmpty(dt.Rows[i]["Emp_Doj"].ToString()) ? "" : dt.Rows[i]["Emp_Doj"].ToString(); 
                       _obj.Designationid = Convert.ToInt64(dt.Rows[i]["Designation_Gid"].ToString());
                       _obj.DesignationName =  string.IsNullOrEmpty(dt.Rows[i]["Designation"].ToString()) ? "" : dt.Rows[i]["Designation"].ToString(); 
                       _obj.Gradeid = Convert.ToInt64(dt.Rows[i]["Grade_Gid"].ToString());
                       _obj.GradeName =  string.IsNullOrEmpty(dt.Rows[i]["Grade"].ToString()) ? "" : dt.Rows[i]["Grade"].ToString(); 
                       _obj.Departmentid = Convert.ToInt64(dt.Rows[i]["Depart_Gid"].ToString());
                       _obj.Departmentname = string.IsNullOrEmpty(dt.Rows[i]["Department"].ToString()) ? "" : dt.Rows[i]["Department"].ToString(); 
                       _obj.branchid = Convert.ToInt64(dt.Rows[i]["branch_gid"].ToString());
                       _obj.branchname =  string.IsNullOrEmpty(dt.Rows[i]["Branch"].ToString()) ? "" : dt.Rows[i]["Branch"].ToString(); 
                       _obj.Fcid = Convert.ToInt64(dt.Rows[i]["FCID"].ToString());
                       _obj.FcName = string.IsNullOrEmpty(dt.Rows[i]["FC_Name"].ToString()) ? "" : dt.Rows[i]["FC_Name"].ToString(); 
                       _obj.CCid = Convert.ToInt64(dt.Rows[i]["CCid"].ToString());
                       _obj.CCName =  string.IsNullOrEmpty(dt.Rows[i]["CC_Name"].ToString()) ? "" : dt.Rows[i]["CC_Name"].ToString(); 
                       _obj.Productid = Convert.ToInt64(dt.Rows[i]["Product_Gid"].ToString());
                       _obj.ProductName =  string.IsNullOrEmpty(dt.Rows[i]["Product Code"].ToString()) ? "" : dt.Rows[i]["Product Code"].ToString();
                       _obj.Emp_Addr1 =  string.IsNullOrEmpty(dt.Rows[i]["employee_addr1"].ToString()) ? "" : dt.Rows[i]["employee_addr1"].ToString();
                       _obj.Emp_Addr2 =  string.IsNullOrEmpty(dt.Rows[i]["employee_addr2"].ToString()) ? "" : dt.Rows[i]["employee_addr2"].ToString();
                       _obj.Address =  string.IsNullOrEmpty(dt.Rows[i]["employee_permanent_addr"].ToString()) ? "" : dt.Rows[i]["employee_permanent_addr"].ToString(); 
                     //  _obj.AccountNo  = dt.Rows[i]["employee_card_no"].ToString();
                       _obj.mobileno =  string.IsNullOrEmpty(dt.Rows[i]["employee_mobile_no"].ToString()) ? "" : dt.Rows[i]["employee_mobile_no"].ToString();
                       _obj.password =  string.IsNullOrEmpty(dt.Rows[i]["employee_password"].ToString()) ? "" : dt.Rows[i]["employee_password"].ToString();
                       _obj.employee_bankaccounttype = string.IsNullOrEmpty(dt.Rows[i]["employee_bankaccounttype"].ToString()) ? "" : dt.Rows[i]["employee_bankaccounttype"].ToString();

                       _obj.employee_dor = string.IsNullOrEmpty(dt.Rows[i]["employee_dor"].ToString()) ? "" : dt.Rows[i]["employee_dor"].ToString();
                       _obj.employee_lastworkingdate =  string.IsNullOrEmpty(dt.Rows[i]["employee_lastworkingdate"].ToString()) ? "" : dt.Rows[i]["employee_lastworkingdate"].ToString();
                       _obj.employee_effectivedate = string.IsNullOrEmpty(dt.Rows[i]["employee_effectivedate"].ToString()) ? "" : dt.Rows[i]["employee_effectivedate"].ToString();
                       _obj.HRIS_LASTMODIFIEDON =  string.IsNullOrEmpty(dt.Rows[i]["HRIS_LASTMODIFIEDON"].ToString()) ? "" : dt.Rows[i]["HRIS_LASTMODIFIEDON"].ToString();
                       _obj.Selected_supervisor_gid = Convert.ToInt64(dt.Rows[i]["employee_supervisor_gid"].ToString());

                       _obj.cityid = Convert.ToInt64(dt.Rows[i]["cityid"].ToString());
                       _obj.cityname =  string.IsNullOrEmpty(dt.Rows[i]["City"].ToString()) ? "" : dt.Rows[i]["City"].ToString();
                       _obj.Pincode =  string.IsNullOrEmpty(dt.Rows[i]["employee_pincode"].ToString()) ? "" : dt.Rows[i]["employee_pincode"].ToString();
                       _obj.Personal_Email =  string.IsNullOrEmpty(dt.Rows[i]["Personal_Email"].ToString()) ? "" : dt.Rows[i]["Personal_Email"].ToString();
                       _obj.Office_Email =  string.IsNullOrEmpty(dt.Rows[i]["Office_Email"].ToString()) ? "" : dt.Rows[i]["Office_Email"].ToString(); 
                       _obj.Bankid = Convert.ToInt64(dt.Rows[i]["bankid"].ToString());
                       _obj.BankName =  string.IsNullOrEmpty(dt.Rows[i]["Bank_Name"].ToString()) ? "" : dt.Rows[i]["Bank_Name"].ToString();
                       _obj.Card_No =  string.IsNullOrEmpty(dt.Rows[i]["Card_No"].ToString()) ? "" : dt.Rows[i]["Card_No"].ToString(); 
                     //  _obj.IFSC_Code = dt.Rows[i]["IFSC_Code"].ToString();
                                            
                       _obj.employee_supervisor_gid = Convert.ToInt64(dt.Rows[i]["employee_supervisor_gid"].ToString());

                       _obj.emp_status =  string.IsNullOrEmpty(dt.Rows[i]["employee_status"].ToString()) ? "" : dt.Rows[i]["employee_status"].ToString();
                       _obj.AccountNo =  string.IsNullOrEmpty(dt.Rows[i]["account_no"].ToString()) ? "" : dt.Rows[i]["account_no"].ToString();
                       _obj.empbranch_name =  string.IsNullOrEmpty(dt.Rows[i]["empbranchname"].ToString()) ? "" : dt.Rows[i]["empbranchname"].ToString(); 
                     _obj.empbranch_id = Convert.ToInt64(dt.Rows[i]["employee_branch_gid"].ToString());
                     _obj.Supervisor_name =  string.IsNullOrEmpty(dt.Rows[i]["Supervisor_name"].ToString()) ? "" : dt.Rows[i]["Supervisor_name"].ToString(); 
                      // _obj.branch_flag = dt.Rows[i]["branch_flag"].ToString();
                       ModelList.Add(_obj);
                   }
               
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return ModelList;
       }


       public DataTable GetEmpDetailsHirackey_List(Profile_Model Obj_Model)
       {

           try
           {

               dt = DataObject.GetEmpDetailsHirackey(Obj_Model);

          


           }
           catch (Exception ex)
           {

               throw ex;
           }
           return dt;
       }

       //dropdown binding
       // Designation  
       public DataTable Getdesignation(string parentcode)
       {
           try
           {
               DataTable dt = new DataTable();
               dt = DataObject.Getdropdownvalues(parentcode);
               return dt;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

       //Grade 
       public DataTable GetGrade(string parentcode)
       {
           try
           {
               DataTable dt = new DataTable();
               dt = DataObject.Getdropdownvalues(parentcode);
               return dt;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

       //department 
       public DataTable GetDepartment(string parentcode)
       {
           try
           {
               DataTable dt = new DataTable();
               dt = DataObject.Getdropdownvalues(parentcode);
               return dt;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

       //branch  
       public DataTable Getbranch(string parentcode)
       {
           try
           {
               DataTable dt = new DataTable();
               dt = DataObject.Getdropdownvalues(parentcode);
               return dt;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

       //FC 
       public DataTable GetFC(string parentcode)
       {
           try
           {
               DataTable dt = new DataTable();
               dt = DataObject.Getdropdownvalues(parentcode);
               return dt;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

       //CC  
       public DataTable GetCC(string parentcode)
       {
           try
           {
               DataTable dt = new DataTable();
               dt = DataObject.Getdropdownvalues(parentcode);
               return dt;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }


       //Product  

       public DataTable GetProduct(string parentcode)
       {
           try
           {
               DataTable dt = new DataTable();
               dt = DataObject.Getdropdownvalues(parentcode);
               return dt;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

       //bankname   
       public DataTable GetBankName(string parentcode)
       {
           try
           {
               DataTable dt = new DataTable();
               dt = DataObject.Getdropdownvalues(parentcode);
               return dt;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
       //City
       public DataTable Getcity(string parentcode)
       {
           try
           {
               DataTable dt = new DataTable();
               dt = DataObject.Getdropdownvalues(parentcode);
               return dt;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
       //Supervisor name code
       //public DataTable dropdownvalues(string flag)
       //{
       //    try
       //    {
       //        DataTable dt = new DataTable();
       //        dt = DataObject.dropdownvalues(flag);
       //        return dt;
       //    }
       //    catch (Exception ex)
       //    {
       //        throw ex;
       //    }
       //}
       public DataTable GetEmployee_ByCodeorName(string EmployeeName, string flag, string Master)
       {
           try
           {
               DataTable dt = new DataTable();
               dt = DataObject.GetEmployee_ByCodeorName(EmployeeName, flag, Master);
               return dt;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
       // Employee Branch 
       public DataTable GetEmployeeBrranch(string flag)
       {
           try
           {
               DataTable dt = new DataTable();
               dt = DataObject.dropdownvalues(flag);
               return dt;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }


       //Employee Master Insert
       public DataTable SaveRecord(Profile_Model Obj_Model, Int64 User_GID)
       {
           string EncryptionKey = "abc123";
           string encryptedpassword = string.Empty;
           try
           {
               byte[] clearBytes = System.Text.Encoding.Unicode.GetBytes(Obj_Model.password);
               using (Aes encryptor = Aes.Create())
               {
                   Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                   encryptor.Key = pdb.GetBytes(32);
                   encryptor.IV = pdb.GetBytes(16);
                   using (MemoryStream ms = new MemoryStream())
                   {
                       using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                       {
                           cs.Write(clearBytes, 0, clearBytes.Length);
                           cs.Close();
                       }
                       encryptedpassword = Convert.ToBase64String(ms.ToArray());
                       Obj_Model.password =encryptedpassword;
                   }
               }
              // return dataObj.CheckUserCredential(usercode, encryptedpassword, LoginMode, ProxyFrom);
               return DataObject.SaveRecord(Obj_Model, User_GID);
           }
           
           
           
           
           
           //try
           //{
           //    return DataObject.SaveRecord(Obj_Model, User_GID);
           //}
           catch (Exception ex)
           {
               throw ex;
           }
       }
       //Employee Master Updates
       public DataTable UpdateRecord(Profile_Model Obj_Model, Int64 User_GID)
       {
           string EncryptionKey = "abc123";
           string encryptedpassword = string.Empty;
           try
           {
               byte[] clearBytes = System.Text.Encoding.Unicode.GetBytes(Obj_Model.password);
               using (Aes encryptor = Aes.Create())
               {
                   Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                   encryptor.Key = pdb.GetBytes(32);
                   encryptor.IV = pdb.GetBytes(16);
                   using (MemoryStream ms = new MemoryStream())
                   {
                       using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                       {
                           cs.Write(clearBytes, 0, clearBytes.Length);
                           cs.Close();
                       }
                       encryptedpassword = Convert.ToBase64String(ms.ToArray());
                       Obj_Model.password = encryptedpassword;
                   }
               }
               // return dataObj.CheckUserCredential(usercode, encryptedpassword, LoginMode, ProxyFrom);
               return DataObject.UpdateRecord(Obj_Model, User_GID);
           }
           //try
           //{
           //    return DataObject.UpdateRecord(Obj_Model, User_GID);
           //}

           catch (Exception ex)
           {
               throw ex;
           }
       }
       //Employee Master Delete
       public DataTable DeleteRecord(Profile_Model Obj_Model, Int64 User_Gid)
       {
           try
           {
               return DataObject.DeleteRecord(Obj_Model, User_Gid);
           }
           catch (Exception ex)
           {

               throw ex;
           }
       }
    }
}
