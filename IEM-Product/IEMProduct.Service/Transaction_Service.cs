﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBHelper;
using IEMProduct.Data;
using System.Data;
using System.Reflection;
using DataAccessHandler;
using SharedData;
using DataAccessHandler.Models.EOW;

namespace IEMProduct.Service
{
    public class Transaction_Service
    {
        Transaction_Model ModelObject = new Transaction_Model();                                   // Transaction model object
        List<Transaction_Model> ModelList = new List<Transaction_Model>();                       // Transaction list model object
        Transaction_Data DataObject = new Transaction_Data();                                   // Transaction data object
        DropDown_Bindings  dropobj = new DropDown_Bindings();
        SupplierInvoice_Model data = new SupplierInvoice_Model();

        // Transaction read 
        public List<Transaction_Model> ReadRecord()
        {
            try
            {
                return DataObject.ReadRecord();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // Transaction Save
        public DataTable SaveRecord(Transaction_Model ModelObject)
        {
            try
            {
                return DataObject.SaveRecord(ModelObject);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Transaction Update
        public DataTable UpdateRecord(Transaction_Model ModelObject)
        {
            try
            {
                return DataObject.UpdateRecord(ModelObject);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Transaction delete
        public DataTable DeleteRecord(Transaction_Model ModelObject)
        {
            try
            {
                return DataObject.DeleteRecord(ModelObject);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // GetTranType

        public List<Transaction_Model> Getdropdown(string parentcode)
        {
            try
            {
                List<Transaction_Model> dropdown = new List<Transaction_Model>();
                DataTable dt1 = new DataTable();
                dt1 = DataObject.Getdropdown(parentcode);
                dropdown = ConvertDataTable<Transaction_Model>(dt1);
                return dropdown;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //GetTranSubType

        public List<Transaction_Model> GetTranSubType(string parentcode)
        {
            try
            {
                List<Transaction_Model> dropdown = new List<Transaction_Model>();
                DataTable dt1 = new DataTable();
                dt1 = DataObject.Getdropdown(parentcode);
                dropdown = ConvertDataTable<Transaction_Model>(dt1);
                return dropdown;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // non qc master dropdown values for atachement and  physical receipting

        public List<Transaction_Model> dropdownvalues(string flag)
        {
            try
            {
                List<Transaction_Model> dropdown = new List<Transaction_Model>();
                DataTable dt1 = new DataTable();

                dt1 = DataObject.dropdownvalues(flag);
                dropdown = ConvertDataTable<Transaction_Model>(dt1);
                return dropdown;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Convert Datatable to List
        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
        #endregion


        //get tran sub type 

        public DataTable GetValues(string TranId, string parentcode)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = DataObject.GetValues(TranId, parentcode);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //public DataTable GetValues(string TranId, string parentcode)
        //{
        //    try
        //    {
        //        DataTable dt = new DataTable();
        //        dt = dropobj.DropDownValues(TranId, parentcode);
        //        return dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
    }
}
