﻿using DBHelper;
using IEMProduct.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace IEMProduct.Service
{
  public  class PayBank_Service
    {
        PayBank_Model ModelObject = new PayBank_Model();                                          // model object
        PayBank_Data DataObject = new PayBank_Data();

      //PayBank Read
      public List<PayBank_Model> ReadRecord()
      {
          try
          {
              return DataObject.ReadRecord();
          }
          catch (Exception ex)
          {

              throw ex;
          }
      }

      //Pay Bank Service

      public DataTable SaveRecord(PayBank_Model ModelObject)
      {
          try
          {
              return DataObject.SaveRecord(ModelObject);
          }
          catch (Exception ex)
          {
              throw ex;
          }
      }


      //Pay Bank Update

      public DataTable UpdateRecord(PayBank_Model ModelObject)
      {
          try
          {
              return DataObject.UpdateRecord(ModelObject);
          }

          catch (Exception ex)
          {
              throw ex;
          }
      }

      //Pay Bank Delete

      public DataTable DeleteRecord(PayBank_Model ModelObject)
      {
          try
          {
              return DataObject.DeleteRecord(ModelObject);
          }
          catch (Exception ex)
          {

              throw ex;
          }
      }

      //bank code drop down
      public List<PayBank_Model> Getdropdown(string parentcode)
      {
          try
          {
              List<PayBank_Model> dropdown = new List<PayBank_Model>();
              DataTable dt1 = new DataTable();
              dt1 = DataObject.Getdropdown(parentcode);
              dropdown = ConvertDataTable<PayBank_Model>(dt1);
              return dropdown;
          }
          catch (Exception ex)
          {
              throw ex;
          }
      }


      #region Convert Datatable to List
      public static List<T> ConvertDataTable<T>(DataTable dt)
      {
          List<T> data = new List<T>();
          foreach (DataRow row in dt.Rows)
          {
              T item = GetItem<T>(row);
              data.Add(item);
          }
          return data;
      }
      public static T GetItem<T>(DataRow dr)
      {
          Type temp = typeof(T);
          T obj = Activator.CreateInstance<T>();

          foreach (DataColumn column in dr.Table.Columns)
          {
              foreach (PropertyInfo pro in temp.GetProperties())
              {
                  if (pro.Name == column.ColumnName)
                      pro.SetValue(obj, dr[column.ColumnName], null);
                  else
                      continue;
              }
          }
          return obj;
      }
      #endregion

      //bank name GetbranchValues
      public DataTable GetbankValues(string paybank_Depend_Code, string parentcode)
      {
          try
          {
              DataTable dt = new DataTable();
              dt = DataObject.GetbankValues(paybank_Depend_Code, parentcode);
              return dt;
          }
          catch (Exception ex)
          {
              throw ex;
          }
      }


      //branch name
      public DataTable GetbranchValues(string paybank_Depend_Code, string parentcode)
      {
          try
          {
              DataTable dt = new DataTable();
              dt = DataObject.GetbankValues(paybank_Depend_Code, parentcode);
              return dt;
          }
          catch (Exception ex)
          {
              throw ex;
          }
      }
    }
}
