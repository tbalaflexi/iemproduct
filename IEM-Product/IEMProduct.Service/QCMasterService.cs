﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using IEMProduct.Data;
using DBHelper;

namespace IEMProduct.Service
{
    public class QCMasterService
    {

        #region
        DataSet ds = new DataSet();
        QCMasterData QcDataObject = new QCMasterData();//data object
        MasterModel QcModelObject = new MasterModel();
        #endregion

        // Qc Master read 
        public List<MasterModel> ReadRecord()
        {
            try
            {
                return QcDataObject.ReadRecord();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //Qc masterdetail read
        public List<MasterModel> ReadRecordDetails(string getMastercode)
        {
            try
            {
                return QcDataObject.ReadRecordDetails(getMastercode);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //QC Master Insert
        public DataTable SaveRecord(MasterModel QcModelObject)
        {
            try
            {
                return QcDataObject.SaveRecord(QcModelObject);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //QC Master Update 
        public DataTable UpdateRecord(MasterModel QcModelObject)
        {
            try
            {
                return QcDataObject.UpdateRecord(QcModelObject);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        // QC Master delete
        public DataTable DeleteRecord(MasterModel QcModelObject)
        {
            try
            {
                return QcDataObject.DeleteRecord(QcModelObject);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // GetParentCode
        public List<MasterModel> GetParentCode()
        {
            try
            {
                List<MasterModel> dropdown = new List<MasterModel>();
                DataTable dt1 = new DataTable();
                dt1 = QcDataObject.GetParentCode();
                dropdown = ConvertDataTable<MasterModel>(dt1);
                return dropdown;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //GetDepentCode
        public List<MasterModel> GetDependCode()
        {
            try
            {
                List<MasterModel> dropdown = new List<MasterModel>();
                DataTable dt1 = new DataTable();
                dt1 = QcDataObject.GetDependCode();
                dropdown = ConvertDataTable<MasterModel>(dt1);
                return dropdown;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Convert Datatable to List
        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
        #endregion

    }
}
