﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using IEMProduct.Data;
using DBHelper;
using SharedData;
using DataAccessHandler.Models.IEMProduct;
namespace IEMProduct.Service
{
    public class WorkFlow_Service
    {
        WorkFlow_Data dataObj = new WorkFlow_Data();
        //Ramya Added below
        DropDown_Bindings Objdd = new DropDown_Bindings();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();

        public List<WorkFlow_Model> Read_WorkFlow(string Master, string Depend_Code, string TypeCode, string SubTypeCode, string AttributeParent, string AttributeChild)
        {
            List<WorkFlow_Model> _lst = new List<WorkFlow_Model>();
            try
            {
                dt = dataObj.Get_DropDownLists(Master, Depend_Code,TypeCode, SubTypeCode, AttributeParent, AttributeChild);
                if (dt.Rows[0][0].ToString() != "NoRecords.!")
                {
                    if (Master == "WorkFlowSummery")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            WorkFlow_Model modelObj = new WorkFlow_Model();
                            modelObj.workflow_id = Convert.ToInt64(dt.Rows[i]["WF_Gid"].ToString());
                            modelObj.workflow_typecode = dt.Rows[i]["WF_Type"].ToString();
                            modelObj.workflow_typename = dt.Rows[i]["Type"].ToString();
                            modelObj.workflow_subtypecode = dt.Rows[i]["WF_SubType"].ToString();
                            modelObj.workflow_subtypename = dt.Rows[i]["SubType"].ToString();
                            modelObj.workflow_attrib_parentid = dt.Rows[i]["WF_AttributeParent"].ToString();
                            modelObj.workflow_attrib_parentname = dt.Rows[i]["AttributeParent"].ToString();
                            modelObj.workflow_attrib_childid = dt.Rows[i]["WF_AttributeChild"].ToString();
                            modelObj.workflow_attrib_childname = dt.Rows[i]["AttributeChild"].ToString();
                            _lst.Add(modelObj);
                        }
                    }
                    if (Master == "WorkFlow")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            WorkFlow_Model modelObj = new WorkFlow_Model();
                            modelObj.WorkFlow_Gid = Convert.ToInt64(dt.Rows[i]["WFAction_Gid"].ToString());
                            modelObj.RoleFrom_Code = Convert.ToInt64(dt.Rows[i]["WFAction_RoleFrom"].ToString());
                            modelObj.RoleFrom_Name = dt.Rows[i]["RoleFrom_Desc"].ToString();
                            modelObj.Action_Code = Convert.ToInt64(dt.Rows[i]["WFAction_Action"].ToString());
                            modelObj.Action_Name = dt.Rows[i]["Action_Desc"].ToString();
                            modelObj.Status_Code = Convert.ToInt64(dt.Rows[i]["WFAction_Status"].ToString());
                            modelObj.Status_Name = dt.Rows[i]["Status_Desc"].ToString();
                            modelObj.RoleTo_Code = Convert.ToInt64(dt.Rows[i]["WFAction_RoleTo"].ToString());
                            modelObj.RoleTo_Name = dt.Rows[i]["RoleTo_Desc"].ToString();
                            modelObj.TimeOut = Convert.ToInt64(dt.Rows[i]["WFAction_TimeOut"].ToString());
                            modelObj.TimeOut_RoleTo_Code = Convert.ToInt64(dt.Rows[i]["WFAction_TimeOut_RoleTo"].ToString());
                            modelObj.TimeOut_RoleTo_Name = dt.Rows[i]["TimeOut_RoleTo_Desc"].ToString();
                            modelObj.Additional = dt.Rows[i]["WFAction_Additional"].ToString();
                            _lst.Add(modelObj);
                        }
                    }
                    else if (Master == "Type")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            WorkFlow_Model modelObj = new WorkFlow_Model();
                            modelObj.Type_Code = dt.Rows[i]["Type_Code"].ToString();
                            modelObj.Type_Name = dt.Rows[i]["Type_Name"].ToString();
                            _lst.Add(modelObj);
                        }
                    }
                    else if (Master == "SubType")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            WorkFlow_Model modelObj = new WorkFlow_Model();
                            modelObj.SubType_Code = dt.Rows[i]["SubType_Code"].ToString();
                            modelObj.SubType_Name = dt.Rows[i]["SubType_Name"].ToString();
                            _lst.Add(modelObj);
                        }
                    }
                    else if (Master == "Role")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            WorkFlow_Model modelObj = new WorkFlow_Model();
                            modelObj.RoleFrom_Code = Convert.ToInt64(dt.Rows[i]["Role_Code"].ToString());
                            modelObj.RoleFrom_Name = dt.Rows[i]["Role_Name"].ToString();
                            _lst.Add(modelObj);
                        }
                    }
                    else if (Master == "RoleTo")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            WorkFlow_Model modelObj = new WorkFlow_Model();
                            modelObj.RoleTo_Code = Convert.ToInt64(dt.Rows[i]["Role_Code"].ToString());
                            modelObj.RoleTo_Name = dt.Rows[i]["Role_Name"].ToString();
                            _lst.Add(modelObj);
                        }
                    }
                    else if (Master == "TimeOutRole")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            WorkFlow_Model modelObj = new WorkFlow_Model();
                            modelObj.TimeOut_RoleTo_Code = Convert.ToInt64(dt.Rows[i]["Role_Code"].ToString());
                            modelObj.TimeOut_RoleTo_Name = dt.Rows[i]["Role_Name"].ToString();
                            _lst.Add(modelObj);
                        }
                    }
                    else if (Master == "Action")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            WorkFlow_Model modelObj = new WorkFlow_Model();
                            modelObj.Action_Code = Convert.ToInt64(dt.Rows[i]["Action_Code"].ToString());
                            modelObj.Action_Name = dt.Rows[i]["Action_Name"].ToString();
                            _lst.Add(modelObj);
                        }
                    }
                    else if (Master == "Status")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            WorkFlow_Model modelObj = new WorkFlow_Model();
                            modelObj.Status_Code = Convert.ToInt64(dt.Rows[i]["Status_Code"].ToString());
                            modelObj.Status_Name = dt.Rows[i]["Status_Name"].ToString();
                            _lst.Add(modelObj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _lst;
        }

        public List<QCD_Model> Get_DropDownList(QCD_Model ObjModel)
        {
            List<QCD_Model> lst = new List<QCD_Model>();
            try
            {
                //dt = dataObj.Get_DropDownLists(Master, Depend_Code, TypeCode, SubTypeCode, AttributeParent, AttributeChild);
                string Master = ObjModel.Action_Type;
                dt = Objdd.DropDownValues(ObjModel);
                if (dt.Rows.Count > 0)
                {
                    if (Master == "Type")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            QCD_Model modelObj = new QCD_Model();
                            modelObj.Type_Code = dt.Rows[i][0].ToString();
                            modelObj.Type_Name = dt.Rows[i][2].ToString();
                            lst.Add(modelObj);
                        }
                    }
                    else if (Master == "SubType")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            QCD_Model modelObj = new QCD_Model();
                            modelObj.SubType_Code = dt.Rows[i][0].ToString();
                            modelObj.SubType_Name = dt.Rows[i][2].ToString();
                            lst.Add(modelObj);
                        }
                    }
                    else if (Master == "Dept")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            QCD_Model modelObj = new QCD_Model();
                            modelObj.Dept_Gid = Convert.ToInt64(dt.Rows[i][0]);
                            modelObj.Dept_Name = dt.Rows[i][2].ToString();
                            lst.Add(modelObj);
                        }
                    }
                    //else if (Master == "Grade")
                    //{
                    //    for (int i = 0; i < dt.Rows.Count; i++)
                    //    {
                    //        QCD_Model modelObj = new QCD_Model();
                    //        modelObj.Grade_Gid =Convert.ToInt64(dt.Rows[i][0].ToString());
                    //        modelObj.Grade_Code = dt.Rows[i][2].ToString();
                    //        lst.Add(modelObj);
                    //    }
                    //}
                    //else if (Master == "Designation")
                    //{
                    //    for (int i = 0; i < dt.Rows.Count; i++)
                    //    {
                    //        QCD_Model modelObj = new QCD_Model();
                    //        modelObj.Designation_Gid = Convert.ToInt64(dt.Rows[i][0].ToString());
                    //        modelObj.Designation_Code = dt.Rows[i][2].ToString();
                    //        lst.Add(modelObj);
                    //    }
                    //}
                    /*if (Master == "Role")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            QCD_Model modelObj = new QCD_Model();
                            modelObj.RoleFrom_Code = dt.Rows[i][0].ToString();
                            modelObj.RoleFrom_Name = dt.Rows[i][1].ToString();
                            lst.Add(modelObj);
                        }
                    }
                    if (Master == "Action")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            QCD_Model modelObj = new QCD_Model();
                            modelObj.Action_Code = dt.Rows[i][0].ToString();
                            modelObj.Action_Name = dt.Rows[i][1].ToString();
                            lst.Add(modelObj);
                        }
                    }
                    if (Master == "Status")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            QCD_Model modelObj = new QCD_Model();
                            modelObj.Status_Code = dt.Rows[i][0].ToString();
                            modelObj.Status_Name = dt.Rows[i][1].ToString();
                            lst.Add(modelObj);
                        }
                    }
                    if (Master == "RoleTo")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            QCD_Model modelObj = new QCD_Model();
                            modelObj.RoleTo_Code = dt.Rows[i][0].ToString();
                            modelObj.RoleTo_Name = dt.Rows[i][1].ToString();
                            lst.Add(modelObj);
                        }
                    }
                    if (Master == "TimeOutRole")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            QCD_Model modelObj = new QCD_Model();
                            modelObj.TimeOut_RoleTo_Code = dt.Rows[i][0].ToString();
                            modelObj.TimeOut_RoleTo_Name = dt.Rows[i][1].ToString();
                            lst.Add(modelObj);
                        }
                    }
                    if (Master == "WorkFlow")
                    {
                        if (dt.Rows[0][0].ToString() != "NoRecords.!")
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                QCD_Model modelObj = new QCD_Model();
                                modelObj.WorkFlow_Gid = Convert.ToInt64(dt.Rows[i][0].ToString());
                                modelObj.RoleFrom_Code = dt.Rows[i][1].ToString();
                                modelObj.RoleFrom_Name = dt.Rows[i][2].ToString();
                                modelObj.Action_Code = dt.Rows[i][3].ToString();
                                modelObj.Action_Name = dt.Rows[i][4].ToString();
                                modelObj.Status_Code = dt.Rows[i][5].ToString();
                                modelObj.Status_Name = dt.Rows[i][6].ToString();
                                modelObj.RoleTo_Code = dt.Rows[i][7].ToString();
                                modelObj.RoleTo_Name = dt.Rows[i][8].ToString();
                                modelObj.TimeOut = Convert.ToInt64(dt.Rows[i][9].ToString());
                                modelObj.TimeOut_RoleTo_Code = dt.Rows[i][10].ToString();
                                modelObj.TimeOut_RoleTo_Name = dt.Rows[i][11].ToString();
                                modelObj.Additional = dt.Rows[i][12].ToString();
                                lst.Add(modelObj);
                            }
                        }
                       
                    }
                    if (Master == "Attribute")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            QCD_Model modelObj = new QCD_Model();
                            modelObj.Attribute_Gid = Convert.ToInt64(dt.Rows[i][0].ToString());
                            modelObj.Attribute_Parent = dt.Rows[i][1].ToString();
                            modelObj.Attribute_Child = dt.Rows[i][2].ToString();
                            lst.Add(modelObj);
                        }
                    }*/

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }

        public List<Delmat_Model> Get_DropDownList_Dept(Delmat_Model ObjModel)
        {
            List<Delmat_Model> lst = new List<Delmat_Model>();
            try
            {
                //dt = dataObj.Get_DropDownLists(Master, Depend_Code, TypeCode, SubTypeCode, AttributeParent, AttributeChild);

                dt = Objdd.DropDownValues(ObjModel);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Delmat_Model modelObj = new Delmat_Model();
                        modelObj.Dept_Gid = dt.Rows[i][0].ToString();
                        modelObj.Dept_Name = dt.Rows[i][2].ToString();
                        lst.Add(modelObj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }

        public List<WorkFlowAttribute_Model> Get_Attributes(Int64 Type_Gid)
        {
            List<WorkFlowAttribute_Model> lst = new List<WorkFlowAttribute_Model>();
            try
            {
                ds = dataObj.Get_Attributes(Type_Gid);
                if (ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        WorkFlowAttribute_Model modelObj = new WorkFlowAttribute_Model();
                        modelObj.Attr_Parent_Gid = Convert.ToInt64(row["Attribute_Gid"].ToString());
                        modelObj.Attr_Parent_Name = row["Attribute_Desc"].ToString();
                        lst.Add(modelObj);
                        modelObj.lst_child = new List<WorkFlowAttributeChild_Model>();
                        foreach (DataRow row0 in ds.Tables[1].Rows)
                        {
                            WorkFlowAttributeChild_Model modObj = new WorkFlowAttributeChild_Model();
                            modObj.Attr_Child_Gid = Convert.ToInt64(row0["AttributeDetails_Gid"].ToString());
                            modObj.Attr_Child_Name = row0["AttributeDetails_Desc"].ToString();
                            modObj.Attr_Child_ParentGid = Convert.ToInt64(row0["Attribute_Gid"].ToString());
                            modelObj.lst_child.Add(modObj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }

        public DataTable UpdateHeaderDetails(WorkFlow_Model modelObj)
        {
            try
            {
                return dataObj.UpdateHeaderDetails(modelObj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Save_WorkFlow(WorkFlow_Model modelObj)
        {
            try
            {
                return dataObj.Save_WorkFlow(modelObj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Update_WorkFlow(WorkFlow_Model modelObj)
        {
            try
            {
                return dataObj.Update_WorkFlow(modelObj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Remove_WorkFlow(Int64 Master_Gid,Int64 Logged_UserGID)
        {
            try
            {
                return dataObj.Remove_WorkFlow(Master_Gid, Logged_UserGID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Get attributes for workflow
        public List<WorkFlowAttribute_Model> Get_Attributes_ForWorkFlow(string Type_Code, Int64 Logged_UserGid)
        {
            List<WorkFlowAttribute_Model> lst = new List<WorkFlowAttribute_Model>();
            try
            {
                ds = dataObj.Get_Attributes_ForWorkFlow(Type_Code, Logged_UserGid);
                if (ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        WorkFlowAttribute_Model modelObj = new WorkFlowAttribute_Model();
                        modelObj.Attr_Parent_Gid = Convert.ToInt64(row["Attribute_Gid"].ToString());
                        modelObj.Attr_Parent_Name = row["Attribute_Desc"].ToString();
                        lst.Add(modelObj);
                        modelObj.lst_child = new List<WorkFlowAttributeChild_Model>();
                        foreach (DataRow row0 in ds.Tables[1].Rows)
                        {
                            WorkFlowAttributeChild_Model modObj = new WorkFlowAttributeChild_Model();
                            modObj.Attr_Child_Gid = Convert.ToInt64(row0["AttributeDetails_Gid"].ToString());
                            modObj.Attr_Child_Name = row0["AttributeDetails_Desc"].ToString();
                            modObj.Attr_Child_ParentGid = Convert.ToInt64(row0["Attribute_Gid"].ToString());
                            modelObj.lst_child.Add(modObj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }
        #endregion
    }
}
