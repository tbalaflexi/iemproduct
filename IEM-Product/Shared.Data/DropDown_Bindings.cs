﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccessHandler;
using DBHelper;
using DataAccessHandler.Models.IEMProduct;


namespace SharedData
{
    public class DropDown_Bindings
    {
        DataTable dt = new DataTable();

        #region Connection String and Parameter Values
        public DBManager dbManager = new DBManager("ConnectionString");
        public List<IDbDataParameter> parameters;
        #endregion

        #region Strored procedure names and parameters.
        public string strSP = "";
        public readonly string SPGetQCDMaster = "SP_Get_QCD_Master";
        public readonly string SPGetQCDChild = "SP_Get_QCD_Child";
        public readonly string SPGetQCData = "SP_Fetch_QCData";
        public readonly string SPGetDropdown = "SP_Get_DropdownValues";
       //public readonly string SPGetMenu = "SP_GetMenu";
       public readonly string SPGetMenu = "SP_GetMenu_New";

        public readonly string ParentCode = "In_ParentCode";
        public readonly string DependCode = "In_DependCode";
        public readonly string MasterCode = "In_MasterCode";
        public readonly string Values = "In_Values";
        #endregion

        #region Loged User Details
        public readonly string LoggedUser = "Flexi";
        public readonly string LoginUserId = "In_LoginUser_GID";
        public readonly string Login_Mode = "In_LoginMode";
        public readonly string UserGid = "In_UserGid";
        #endregion

        #region Common methods for loading dropdown values using QCD Codes.
        /*public DataTable QCD_Parent(string MasterCode)
        {
            try
            {
                parameters = new List<IDbDataParameter>();
                parameters.Add(dbManager.CreateParameter(ParentCode, MasterCode, DbType.String));
                dt = dbManager.GetDataTable(SPGetQCData, CommandType.StoredProcedure, parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable QCD_Child(string MasterCode,string ParentCode)
        {
            try
            {
                parameters = new List<IDbDataParameter>();
                parameters.Add(dbManager.CreateParameter(ParentCode, MasterCode, DbType.String));
                parameters.Add(dbManager.CreateParameter(DependCode, ParentCode, DbType.String));
                dt = dbManager.GetDataTable(SPGetDropdown, CommandType.StoredProcedure, parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }


        public DataTable QCD_Child(string ParentCode)
        {
            try
            {
                parameters = new List<IDbDataParameter>();
                parameters.Add(dbManager.CreateParameter(ParentCode, ParentCode, DbType.String));
                dt = dbManager.GetDataTable(SPGetDropdown, CommandType.StoredProcedure, parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }*/

        public DataTable DropDownValues(QCD_Model data)
        {
            try
            {
                parameters = new List<IDbDataParameter>();
                parameters.Add(dbManager.CreateParameter(DependCode, data.Dependent_Gid, DbType.String));
                parameters.Add(dbManager.CreateParameter(ParentCode, data.Parent_Gid, DbType.String));
                parameters.Add(dbManager.CreateParameter(MasterCode, data.Master_Code, DbType.String));
                parameters.Add(dbManager.CreateParameter(Values, data.Values, DbType.String));
                parameters.Add(dbManager.CreateParameter(UserGid, 1, DbType.String));
                dt = dbManager.GetDataTable(SPGetDropdown, CommandType.StoredProcedure, parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable DropDownValues(Delmat_Model data)
        {
            try
            {
                parameters = new List<IDbDataParameter>();
                parameters.Add(dbManager.CreateParameter(DependCode, data.Dependent_Gid, DbType.String));
                parameters.Add(dbManager.CreateParameter(ParentCode, data.Parent_Gid, DbType.String));
                parameters.Add(dbManager.CreateParameter(MasterCode, data.Master_Code, DbType.String));
                parameters.Add(dbManager.CreateParameter(UserGid, 1, DbType.String));
                dt = dbManager.GetDataTable(SPGetDropdown, CommandType.StoredProcedure, parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion

        #region Menu
        public IEnumerable<MenuModel> GetMenu(string LoginMode, string InsertBy)
        {
            try
            {
                IList<MenuModel> objList = new List<MenuModel>();
                MenuModel objModel;

                parameters = new List<IDbDataParameter>();
                parameters.Add(dbManager.CreateParameter(LoginUserId, InsertBy, DbType.String));
                parameters.Add(dbManager.CreateParameter(Login_Mode, LoginMode, DbType.String));
                dt = dbManager.GetDataTable(SPGetMenu, CommandType.StoredProcedure, parameters.ToArray());
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    objModel = new MenuModel();
                    objModel.Id = Convert.ToInt32(dt.Rows[i]["menu_gid"].ToString());
                    objModel.Name = dt.Rows[i]["menu_name"].ToString();
                    objModel.url = dt.Rows[i]["menu_link"].ToString();
                    objModel.ParentId = Convert.ToInt32(dt.Rows[i]["menu_parent_gid"].ToString());
                    objModel.SortOrder = Convert.ToInt32(dt.Rows[i]["menu_displayorder"].ToString());
                    objList.Add(objModel);
                }

                return objList;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion

    }
}
