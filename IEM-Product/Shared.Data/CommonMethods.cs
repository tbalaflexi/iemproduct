﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Data
{
    public class CommonMethods
    {
        public string ConvertDate(string _date)
        {
            //if (_date.Contains('/'))
            //{
            //    _date = _date.Replace('/', '-');
            //}
            if (_date != null && _date.Trim() != string.Empty)
            {
                try
                {
                    return _date.Split('/')[2] + "/" + _date.Split('/')[0] + "/" + _date.Split('/')[1];
                }
                catch
                {
                    return "";
                }

            }
            else { return ""; }
        }
        public string Dateconversion(string Date)
        {


            string _Date = "";
            try
            {
                if (Date == null || Date == "" || Date == "0" || Date == "undefined")
                {
                    _Date = "1900-01-01";
                }
                else
                {
                    int len = Date.Length;
                    if (len > 0)
                    {
                        if (len > 10)
                        {

                            DateTime dtfrom = DateTime.ParseExact(Date.Substring(0, 24), "ddd MMM dd yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                            _Date = dtfrom.ToString("yyyy-MM-dd");
                        }
                        else
                        {
                            DateTime myDate = DateTime.ParseExact(Date, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            _Date = myDate.ToString("yyyy-MM-dd");
                        }
                    }
                    else
                    {
                        _Date = "1900-01-01";
                    }
                }


            }

            catch (Exception ex)
            {
                throw ex;
            }
            return _Date;

        }

    }
}
