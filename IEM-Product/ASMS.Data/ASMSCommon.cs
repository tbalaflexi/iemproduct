﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace ASMS.Data
{
    public class AsmsCommon
    {

        #region Strored Procedure CURD
        public string strSP = "";
        public readonly string SPValidateLogin = "SP_Validate_Supplier";
        public readonly string SPGetsupDetails = "SP_Get_SupplierDetails";
        public readonly string SPGetInvDetails = "SP_Get_SupplierInvDetails";
        public readonly string SPGetPoDetails = "SP_Get_PoDetails";

        #endregion

        #region Stored Procedure Params
        public string ID = "@id";
        public string SubCode = "@SubCode";
        public string Password = "@Password";
        public string UserID = "@Userid";
        public string Type = "@Type";
        public string Action = "@Action";
        public string FromDate = "@FromDate";
        public string ToDate = "@ToDate";
        public string Status = "@Status";
        public string @PO = "@PO";
        public string Invno = "@Invno";

        #endregion

        #region Operation Type(CURD)
        public readonly string Create = "C";
        public readonly string Read = "R";
        public readonly string Update = "U";
        public readonly string Delete = "D";
        #endregion

        #region Loged User Details
        public string LoggedUser = "Flexi";
        #endregion
    }
}
