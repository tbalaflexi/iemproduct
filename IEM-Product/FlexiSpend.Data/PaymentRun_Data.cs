﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccessHandler.Models.FlexiSpend;
namespace FlexiSpend.Data
{
    public class PaymentRun_Data
    {
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        FSCommon cmObjFS = new FSCommon();

        #region Fetch Methods for PaymentRun.
        public DataTable Read_FundPosition(Int64 User_Gid)
        {
            try
            {
                cmObjFS.parameters = new List<IDbDataParameter>();
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_User_Gid, User_Gid, DbType.Int64));
                dt = cmObjFS.dbManager.GetDataTable(cmObjFS.SP_Get_AP_FundPosition, CommandType.StoredProcedure, cmObjFS.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable Read_PaymentRun_BeforeGL(Int64 User_Gid)
        {
            try
            {
                cmObjFS.parameters = new List<IDbDataParameter>();
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_User_Gid, User_Gid, DbType.Int64));
                dt = cmObjFS.dbManager.GetDataTable(cmObjFS.SP_Get_AP_ForPaymentRun_BeforeGL, CommandType.StoredProcedure, cmObjFS.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable Read_PaymentRun_AfterGL(Int64 User_Gid)
        {
            try
            {
                cmObjFS.parameters = new List<IDbDataParameter>();
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_User_Gid, User_Gid, DbType.Int64));
                dt = cmObjFS.dbManager.GetDataTable(cmObjFS.SP_Get_AP_ForPaymentRun_AfterGL, CommandType.StoredProcedure, cmObjFS.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable Read_PayBank()
        {
            try
            {
                cmObjFS.parameters = new List<IDbDataParameter>();
                dt = cmObjFS.dbManager.GetDataTable(cmObjFS.SP_Get_PayBank, CommandType.StoredProcedure, cmObjFS.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable Reverse_Authorization(String ECF_GID, Int64 Login_User_Gid)
        {
            try
            {
                cmObjFS.parameters = new List<IDbDataParameter>();
                string ECF_Gids = ECF_GID.Substring(0, ECF_GID.Length - 1);
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_ECF_Gid, ECF_Gids, DbType.String));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_User_Gid, Login_User_Gid, DbType.Int64));
                dt = cmObjFS.dbManager.GetDataTable(cmObjFS.SP_Set_Reverse_Authorization, CommandType.StoredProcedure, cmObjFS.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable Update_GLValue_Date(PaymentRun_Model ObjModel, Int64 Login_User_Gid)
        {
            try
            {
                cmObjFS.parameters = new List<IDbDataParameter>();
                string ECF_Gids=ObjModel.PaymentRunGL_ECF_Gids.Substring(0, ObjModel.PaymentRunGL_ECF_Gids.Length - 1);
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_ECF_Gids, ECF_Gids, DbType.String));
                if (ObjModel.PaymentRunGL_GLDate != null)
                {
                    if (ObjModel.PaymentRunGL_GLDate.Length > 19)
                    {
                        cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_GLValue_Date, DateTime.Parse(ObjModel.PaymentRunGL_GLDate.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_GLValue_Date, DateTime.ParseExact(ObjModel.PaymentRunGL_GLDate.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_GLValue_Date, ObjModel.PaymentRunGL_GLDate, DbType.String));
                }
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_PayBank_Gid, ObjModel.PaymentRunGL_PayBank_Gid, DbType.String));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_User_Gid, Login_User_Gid, DbType.Int64));
                dt = cmObjFS.dbManager.GetDataTable(cmObjFS.SP_Set_PaymentRunGL, CommandType.StoredProcedure, cmObjFS.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable Undo_PaymentRunGLDate(Int64 ECF_GID, Int64 Login_User_Gid)
        {
            try
            {
                cmObjFS.parameters = new List<IDbDataParameter>();
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_ECF_Gid, ECF_GID, DbType.Int64));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_User_Gid, Login_User_Gid, DbType.Int64));
                dt = cmObjFS.dbManager.GetDataTable(cmObjFS.SP_Set_Undo_PaymentRunGLDate, CommandType.StoredProcedure, cmObjFS.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataSet Set_PaymentRun(string ECF_GIDs, Int64 Login_User_Gid)
        {
            try
            {
                cmObjFS.parameters = new List<IDbDataParameter>();
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_ECF_Gids, ECF_GIDs.Substring(0, ECF_GIDs.Length - 1), DbType.String));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_User_Gid, Login_User_Gid, DbType.Int64));
                ds = cmObjFS.dbManager.GetDataSet(cmObjFS.SP_FS_Set_Paymentrun, CommandType.StoredProcedure, cmObjFS.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
       
        #endregion
    }

}
