﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DBHelper;
using DataAccessHandler.Models.FlexiSpend;

namespace FlexiSpend.Data
{
    public class Urgent_Data
    {
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        FSCommon cmnParams = new FSCommon();

        #region Methods for Get Urgent Data List.
        public DataSet Get_UrgentDetailList(Urgent_Model Obj_Model)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Refid, Obj_Model.Refid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EmpId, Obj_Model.EmpGid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Amount, Obj_Model.Amount, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, Obj_Model.Action, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EmpName_Sr, Obj_Model.EmpName_Sr, DbType.String));
                ds = cmnParams.dbManager.GetDataSet(cmnParams.SPUrgentDetails, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        #endregion

        #region Methods for DML Urgent Details.
        public DataTable DML_UrgentDetails(Urgent_Model Obj_Model)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, Obj_Model.Action, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_UrgentId, Obj_Model.UrgentId, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_UrgentECFId, Obj_Model.UrgentECF_Gid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_UrgentStatus, Obj_Model.UrgentStatus, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_UrgentMakerGid, Obj_Model.MakerId, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_UrgentMakerReason, Obj_Model.Maker_Reason, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_UrgentCheckerGid, Obj_Model.CheckerId, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_UrgentCheckerReason, Obj_Model.Checker_Reason, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_LoginId, Obj_Model.LoginId, DbType.String));
                dt = cmnParams.dbManager.GetDataTable (cmnParams.SPDMLUrgentDetail, CommandType.StoredProcedure, cmnParams.parameters.ToArray());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion
    }
}
