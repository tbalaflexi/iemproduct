﻿using DataAccessHandler;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlexiSpend.Data
{
    public class FSCommon
    {


        #region Connection String and Parameter Values
        public DBManager dbManager = new DBManager("ConnectionString");
        public List<IDbDataParameter> parameters;
        #endregion

        #region Strored Procedure CURD
        public string strSP = "";
        public readonly string SPValidateLogin = "SP_Validate_Supplier";
        public readonly string SPGetsupDetails = "SP_Get_SupplierDetails";
        public readonly string SPGetInvDetails = "SP_Get_SupplierInvDetails";
        public readonly string SPGetPoDetails = "SP_Get_PoDetails";

        public readonly string SP_Get_AP_FundPosition = "SP_Get_AP_FundPosition";
        public readonly string SP_Get_AP_ForPaymentRun_BeforeGL = "SP_Get_AP_ForPaymentRun_BeforeGL";
        public readonly string SP_Get_AP_ForPaymentRun_AfterGL = "SP_Get_AP_ForPaymentRun_AfterGL";
        public readonly string SP_Get_PayBank = "SP_Get_PayBank";
        public readonly string SP_Set_Reverse_Authorization = "SP_Set_Reverse_Authorization";
        public readonly string SP_Set_PaymentRunGL = "SP_Set_PaymentRunGL";
        public readonly string SP_Set_Undo_PaymentRunGLDate = "SP_Set_Undo_PaymentRunGLDate";
        public readonly string SP_FS_Set_Paymentrun = "SP_FS_Set_Paymentrun";

        #endregion

        #region Stored Procedure Params
        public string p_User_Gid = "In_User_Gid";
        public string p_ECF_Gid = "In_Ecf_Gid";
        public string p_ECF_Gids = "In_ECF_Gids";
        public string p_GLValue_Date = "In_GLValue_Date";
        public string p_PayBank_Gid = "In_PayBank_Gid";
        public string ID = "@id";
        public string SubCode = "@SubCode";
        public string Password = "@Password";
        public string UserID = "@Userid";
        public string Type = "@Type";
        public string Action = "@Action";
        public string FromDate = "@FromDate";
        public string ToDate = "@ToDate";
        public string Status = "@Status";
        public string @PO = "@PO";
        public string Invno = "@Invno";

        #endregion

        #region Operation Type(CURD)
        public readonly string Create = "C";
        public readonly string Read = "R";
        public readonly string Update = "U";
        public readonly string Delete = "D";
        #endregion

        #region Loged User Details
        public string LoggedUser = "Flexi";
        #endregion

        #region AP Checker Procedure Name.
        public readonly string sp_Get_APCheckerList = "SP_FS_Get_APCheckerList";
        #endregion

        #region Inward Details Procedures
        public readonly string SPInwardDetails = "SP_FS_Get_InwardDetails";
        public readonly string SPSetInwardDetails = "SP_FS_Set_InwardDetails";
        #endregion

        #region Inward Parameters
        public readonly string P_Batchid = "In_BatchId";
        public readonly string P_Ecfno = "In_Ecfno";
        public readonly string P_EcfRefIds = "In_EcfRefIds";
        public readonly string P_Action = "In_action";
        public readonly string P_Userid = "In_Userid";
        public readonly string P_RecvDate = "In_RecvDate";
        public readonly string P_PouchIds = "In_Pouchids";
        public readonly string P_Refid = "In_Refid";
        public readonly string P_AwbNo = "In_AwbNo";
        public readonly string P_CourierId = "In_CourierId";
        #endregion

        #region Urgent Details Procedures
        public readonly string SPUrgentDetails = "SP_FS_Get_EcfUrgent";
        public readonly string SPDMLUrgentDetail = "SP_FS_DML_EcfUrgent";
        #endregion

        #region Urgent Parameters
        public readonly string P_EmpId = "In_Empgid";
        public readonly string P_Amount = "In_Amount";

        public readonly string P_UrgentId = "In_ecfurgent_gid";
        public readonly string P_UrgentECFId = "In_ecfurgent_ecf_gid";
        public readonly string P_UrgentStatus = "In_ecfurgent_status";
        public readonly string P_UrgentMakerGid = "In_ecfurgent_maker_gid";
        public readonly string P_UrgentMakerReason = "In_ecfurgent_maker_reason";
        public readonly string P_UrgentCheckerGid = "In_ecfurgent_checker_gid";
        public readonly string P_UrgentCheckerReason = "In_ecfurgent_checker_remark";
        public readonly string P_LoginId = "In_logingid";

        #endregion

        #region Pullout Details Procedures
        public readonly string SPGetPulloutDetails = "SP_FS_Get_PulloutDetails";
        public readonly string SPSetPulloutDetails = "SP_FS_Set_EcfPullout";
        #endregion

        #region Pullout Parameters
        public readonly string P_PulloutECFId = "In_pullout_ecf_gid";
        public readonly string P_DocHandedId = "In_Doc_Handed_Id";
        public readonly string P_DocPulloutDate = "In_Doc_Pullout_Date";
        public readonly string P_DocInterFilingDate = "In_Doc_Interfiling_date";
        public readonly string P_EmpName_Sr = "In_Employee";
        #endregion


        #region Cheque Payment Procedure
        public readonly string SpGetChequePayment = "SP_Get_chequepayment";
        public readonly string SpDmlChequePayment = "Sp_Dml_ChequePayment";
        #endregion

        #region Cheque Payment parameters
        public readonly string P_ChqGid = "In_chq_gid";
        public readonly string P_ChqNo = "In_chq_no";
        public readonly string P_ChqStatus = "In_chq_status";
        public readonly string P_ChqRemark = "In_chq_remark";
        public readonly string P_ChqAvailableeFlag = "In_chq_available_flag";
        public readonly string P_ChqPayrunGid = "In_chq_payrunvocher_gid";
        public readonly string P_ChqDate = "In_chq_date";
        public readonly string P_ChqIsPrited = "In_chq_isprinted";
        public readonly string P_ChqPrintedDate = "In_chq_printed_date";
        #endregion

        #region Cheque Despatch Procedure
        public readonly string SpGetChequeDespatch = "Sp_Get_ChequeDespatch";
        public readonly string SpDmlChequeDespatch = "Sp_Dml_ChequeDespatch";
        public readonly string SpGetqcDropdown = "SP_Fetch_QCData";
        public readonly string SpGetqconchangevalues = "SP_GetDropdownValues";
        #endregion

        #region Cheque Despatch parameters
        public readonly string TranId = "In_TranGid";
        public readonly string ParentCode = "In_masters_parentcode";
        public readonly string P_DespatchMode = "In_chq_depatch_mode";
        public readonly string P_DespatchCourierName = "In_chq_courier_name";
        public readonly string P_DespatchAwnNo = "In_chq_awb_no";
        public readonly string P_DespatchDate = "In_chq_despatch_date"; 
        #endregion

        #region Payment Conversion Procedures
        public readonly string SPGetPaymentConvDetails = "SP_FS_Get_PaymentConversion";
        public readonly string SPSetPaymentConvDetails_Maker = "SP_FS_Set_PaymentConversionMaker";
        public readonly string SPSetPaymentConvDetails_Checker = "SP_FS_Set_PaymentConversionChecker";
        #endregion

        #region Payment Conversion Parameters
        public readonly string P_EMP_SUP_Type = "In_Emp_Sup_Type";
        public readonly string P_ECF_Gid = "In_Ecf_Gid";
        public readonly string P_Supplier_Emp_Gid = "In_Supplier_Employee_Gid";
        public readonly string P_Paymode_Gid = "In_Paymode_Gid";

        public readonly string P_SelectedValues = "In_SelectedList";

        public readonly string P_AuthorizeStatus = "In_CheckListStatus";
        public readonly string P_PaymentReissueId = "In_PaymentReissueId";
        #endregion

        #region Reject Despatch Procedure
        public readonly string SpGetRejectDespatchreview = "Sp_GetRejectDespatchReview";
        public readonly string SpGetNonqcdropdown = "Sp_NonQCDropDownValues";
        public readonly string SpDmlRejectDespatchReview = "Sp_Dml_RejectdespatchReview";
        public readonly string SpDmlRejectDespatch = "Sp_Dml_RejectdespatchDespatch";
        public readonly string SpgetRejectDespatch="Sp_GetRejectDespatch";
        #endregion

        #region Reject Despatch Parameters
        public readonly string P_flag = "In_flag";
        public readonly string P_inexid = "In_inex_gid";
        public readonly string P_inexstatus = "In_inex_status";
        public readonly string P_inexreviewremark = "In_inex_review_remark";
        public readonly string P_inexdespatchecfid="In_inexdespatch_ecf_gid";
        public readonly string P_inexdespatchinexid="In_inexdespatch_inex_gid";
        public readonly string P_inexdespatcgmodeid = "In_inexdespatchmode_gid";
        public readonly string P_inexdespatchdate="In_inexdespatch_date";
        public readonly string P_inexdespatchawbno="In_inexdespatch_awb_no";
        public readonly string P_inexdespatchcourierid="In_inexdespatch_courier_gid";
        public readonly string P_inexdespatchremark = "In_inexdespatch_remark";
        public readonly string P_inexdespatchinex_gid="In_inexdespatch_inex_gid";
        public readonly string P_inexdespatchgid="In_inexdespatch_gid";
         public readonly string P_inexecfid="In_inex_ecf_gid";
        public readonly string P_ecfids ="In_ecf_gid";
        #endregion

         #region Payment Status
         public readonly string loginUserId = "In_UserID";
         public readonly string paymentGLDate = "In_GLDate";
         public readonly string paymentPVNumber = "In_PVNumber";
         public readonly string paymentMode = "In_Paymode";
         public readonly string paymentStatusID = "In_PaymentStatusID";
         public readonly string paymentRefNo = "In_PaymentRefNo";
         public readonly string paymentDate = "In_PaymentDate";
         public readonly string paymentReason = "In_Reason";
         public readonly string PAction = "In_masters_parentcode";

         public readonly string SpGetPaymentStatus = "SP_Get_PaymentStatus";
         public readonly string SpSetPaymentStatus = "Sp_Set_PaymentStatusUpdate";
        public readonly string  SpDmlPaymentstatus="SP_FS_Set_PaymentStatus";
         #endregion

        #region EFT File Generation
        public string SP_FS_Get_EFTFileToGenerate = "SP_FS_Get_EFTFileToGenerate";
        public string PR_FS_Set_OnlineMemoFileGeneration = "PR_FS_Set_OnlineMemoFileGeneration";
        public string PR_FS_Get_OnlineMemoFilePrint="PR_FS_Get_OnlineMemoFilePrint";
        public string p_PVFromDate = "In_PVFromDate";
        public string p_PVToDate = "In_PVToDate";
        public string p_PvNo = "In_PvNo";
        public string p_PvAmountFrom = "In_PvAmountFrom";
        public string p_PvAmountTo = "In_PvAmountTo";
        public string p_EmpCode = "In_EmpCode";
        public string p_EmpName = "In_EmpName";
        public string p_SuppCode = "In_SuppCode";
        public string p_SuppName = "In_SuppName";
        public string p_ClaimType = "In_ClaimType";
        //public string p_PayBank_Gid = "In_PayBank_Gid";
        public string p_BatchNo = "In_BatchNo";
        public string p_PayMode = "In_PayMode";

        public string p_PvIds = "p_PvIds";
        public string p_Status = "p_Status";
        public string p_CancelPvId = "p_CancelPvId";
        public string p_Reason = "p_Reason";

        public string p_Paymode = "p_Paymode";
        public string p_ViewType = "p_ViewType";
        public string p_PayBankGId = "p_PayBankGId";
        public string p_UId = "p_UId";
        #endregion


    }
}
