﻿using DataAccessHandler.Models.FlexiSpend;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

 

namespace FlexiSpend.Data
{
   public  class ChequePayment_Data
    {
        ChequePayment_Model ModelObject = new ChequePayment_Model();                                   //ChequePayment model object
        List<ChequePayment_Model> ModelList = new List<ChequePayment_Model>();                         //ChequePayment  list model object
        DataTable dt = new DataTable();
                                                                    //list model object
        FSCommon fscmnobj = new FSCommon();

        //ChequePayment Master Read

        public List<ChequePayment_Model> ReadRecord()
        {
            try
            {

                fscmnobj.parameters = new List<IDbDataParameter>();
                
                dt = fscmnobj.dbManager.GetDataTable(fscmnobj.SpGetChequePayment, CommandType.StoredProcedure);
                foreach (DataRow dr in dt.Rows)
                {
                    ModelList.Add
                        (
                        new ChequePayment_Model
                        {
                            chq_gid = Convert.ToInt64(dr["chq_gid"].ToString()),
                            chq_payrunvocher_gid = Convert.ToInt64(dr["payrunvoucher_gid"].ToString()),
                            bankname = dr["bankname"].ToString(),
                            glvaluedate = dr["PaymentRunGL_Date"].ToString(),
                           // glvaluedate =  Convert.ToDateTime(dr["PaymentRunGL_Date"].ToString()),
                            pvno = Convert.ToInt64(dr["payrunvoucher_pvno"].ToString()),
                            pvamount = Convert.ToDecimal(dr["payrunvoucher_amount"].ToString()),
                            beneficiary = dr["payrunvoucher_beneficiary"].ToString(),
                            paymode = dr["payrunvoucher_paymode"].ToString(),
                             chq_no = Convert.ToInt32(dr["chq_no"].ToString()),
                              chq_date = dr["chq_date"].ToString(),

 

                        });
                }
                return ModelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //ChequePayment insert

        public DataTable SaveRecord(ChequePayment_Model ModelObject)
        {
            try
            {
                fscmnobj.parameters = new List<IDbDataParameter>();
                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_Action, 50, fscmnobj.Create, DbType.String));
                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_ChqGid, 0, DbType.Int32));
                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_ChqNo, 50, ModelObject.chq_no, DbType.String));
                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_ChqStatus, 50, ModelObject.chq_status, DbType.String));
                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_ChqAvailableeFlag, 50, ModelObject.chq_available_flag, DbType.String));
                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_ChqPayrunGid, 50, ModelObject.chq_payrunvocher_gid, DbType.Int32));
                //fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.Delegate_Active, 50, ModelObject.delegate_active, DbType.String));
                if (ModelObject.chq_date != null)
                {
                    if (ModelObject.chq_date.Length > 19)
                    {
                        fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_ChqDate, DateTime.Parse(ModelObject.chq_date.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_ChqDate, DateTime.ParseExact(ModelObject.chq_date.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_ChqDate, ModelObject.chq_date, DbType.String));
                }




                // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
                //fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.delegate_role, 50, ModelObject.delegate_workflow_Role, DbType.String));

                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_LoginId, ModelObject.logingid, DbType.String));

                dt = fscmnobj.dbManager.GetDataTable(fscmnobj.SpDmlChequePayment, CommandType.StoredProcedure, fscmnobj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }



        //ChequePayment Update
        public DataTable UpdateRecord(ChequePayment_Model ModelObject)
        {
            try
            {
                fscmnobj.parameters = new List<IDbDataParameter>();
                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_Action, 50, fscmnobj.Create, DbType.String));
                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_ChqGid, ModelObject.chq_gid, DbType.Int32));
                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_ChqNo, 50, ModelObject.chq_no, DbType.String));
                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_ChqStatus, 50, ModelObject.chq_status, DbType.String));
                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_ChqAvailableeFlag, 50, ModelObject.chq_available_flag, DbType.String));
                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_ChqPayrunGid, 50, ModelObject.chq_payrunvocher_gid, DbType.Int32));
                //fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.Delegate_delmattype, 50, ModelObject.delegate_delmattypeid, DbType.String));
                //fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.Delegate_Active, 50, ModelObject.delegate_active, DbType.String));
                if (ModelObject.chq_date != null)
                {
                    if (ModelObject.chq_date.Length > 19)
                    {
                        fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_ChqDate, DateTime.Parse(ModelObject.chq_date.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_ChqDate, DateTime.ParseExact(ModelObject.chq_date.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_ChqDate, ModelObject.chq_date, DbType.String));
                }




               // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
                //fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.delegate_role, 50, ModelObject.delegate_workflow_Role, DbType.String));

                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_LoginId, ModelObject.logingid, DbType.String));

                dt = fscmnobj.dbManager.GetDataTable(fscmnobj.SpDmlChequePayment, CommandType.StoredProcedure, fscmnobj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
    }
}
