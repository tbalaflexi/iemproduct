﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DBHelper;

namespace FlexiSpend.Data
{
    public class APChecker_Data
    {
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        FSCommon cmObjFS = new FSCommon();

        #region Methods for Get AP Checker List.
        public DataTable Get_APCheckerList(string GridAction)
        {
            try
            {
                cmObjFS.parameters = new List<IDbDataParameter>();
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.P_Action, GridAction, DbType.String));
                dt = cmObjFS.dbManager.GetDataTable(cmObjFS.sp_Get_APCheckerList, CommandType.StoredProcedure, cmObjFS.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion

    }
}
