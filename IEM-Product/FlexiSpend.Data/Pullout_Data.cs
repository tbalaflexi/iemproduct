﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DBHelper;
using DataAccessHandler.Models.FlexiSpend;

namespace FlexiSpend.Data
{
    public class Pullout_Data
    {
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        FSCommon cmnParams = new FSCommon();

        #region Methods for Get PulloutData List.
        public DataSet Get_PulloutDetailList(Pullout_Model Obj_Model)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Refid, Obj_Model.Refid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EmpId, Obj_Model.EmpGid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Amount, Obj_Model.Amount, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, Obj_Model.Action, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EmpName_Sr, Obj_Model.EmpName_Sr, DbType.String));
                ds = cmnParams.dbManager.GetDataSet(cmnParams.SPGetPulloutDetails, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region Methods for Set Pullout Data
        public DataTable Set_PulloutDetails(Pullout_Model Obj_Model)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, Obj_Model.Action, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_PulloutECFId, Obj_Model.PulloutECF_Gid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_DocHandedId, Obj_Model.Doc_HandedEmpId, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_DocPulloutDate, Obj_Model.Doc_PulloutDate, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_DocInterFilingDate , Obj_Model.Doc_InterfilingDate , DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_LoginId, Obj_Model.LoginId, DbType.String));

                dt = cmnParams.dbManager.GetDataTable(cmnParams.SPSetPulloutDetails, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
