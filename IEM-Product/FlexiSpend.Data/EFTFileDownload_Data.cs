﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccessHandler.Models.FlexiSpend;
namespace FlexiSpend.Data
{
    public class EFTFileDownload_Data
    {
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        FSCommon cmObjFS = new FSCommon();
        public DataTable Read_EFTGrid(EFTFileGeneration_Model ObjModel, Int64 User_Gid)
        {
            try
            {
                cmObjFS.parameters = new List<IDbDataParameter>();
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_PVFromDate, ObjModel.PV_From, DbType.String));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_PVToDate, ObjModel.PV_To, DbType.String));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_PvNo, ObjModel.PV_No, DbType.String));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_PvAmountFrom, ObjModel.PV_From_Amount, DbType.Double));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_PvAmountTo, ObjModel.PV_To_Amount, DbType.Double));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_EmpCode, ObjModel.Employee_Code, DbType.String));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_EmpName, ObjModel.Employee_Name, DbType.String));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_SuppCode, ObjModel.Supplier_Code, DbType.String));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_SuppName, ObjModel.Supplier_Name, DbType.String));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_ClaimType, ObjModel.ClaimType, DbType.String));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_PayBank_Gid, ObjModel.PayBank_Gid, DbType.Int64));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_BatchNo, ObjModel.BatchNo, DbType.String));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_PayMode, ObjModel.Pay_paymodemode, DbType.String));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_User_Gid, User_Gid, DbType.Int64));
                dt = cmObjFS.dbManager.GetDataTable(cmObjFS.SP_FS_Get_EFTFileToGenerate, CommandType.StoredProcedure, cmObjFS.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable SetEFTMemoDetails(EFTFileGeneration_Model ObjModel, Int64 Login_User_Gid)
        {
            try
            {
                cmObjFS.parameters = new List<IDbDataParameter>();
                /*string ECF_Gids = ECF_GID.Substring(0, ECF_GID.Length - 1);
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_ECF_Gid, ECF_Gids, DbType.String));*/
                string p_pvids = ObjModel.PV_Gids;
                if(ObjModel.PV_Gids.ToString().Contains("on"))
                {
                    p_pvids = ObjModel.PV_Gids.Replace("on|", "");
                }

                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_PvIds, p_pvids, DbType.String));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_Status, ObjModel.PV_Status, DbType.String));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_CancelPvId, ObjModel.PV_Cancel_GID, DbType.Int64));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_Reason, ObjModel.PV_Cancel_Reason, DbType.String));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_User_Gid, Login_User_Gid, DbType.Int64));
                dt = cmObjFS.dbManager.GetDataTable(cmObjFS.PR_FS_Set_OnlineMemoFileGeneration, CommandType.StoredProcedure, cmObjFS.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataSet PrintEFTMemoDetails(EFTFileGeneration_Model ObjModel, Int64 Login_User_Gid)
        {
            try
            {
                cmObjFS.parameters = new List<IDbDataParameter>();
                /*string ECF_Gids = ECF_GID.Substring(0, ECF_GID.Length - 1);
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_ECF_Gid, ECF_Gids, DbType.String));*/
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_PvIds, ObjModel.PV_Gids, DbType.String));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_Paymode, ObjModel.Pay_paymodemode, DbType.String));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_ViewType, ObjModel.ViewType, DbType.Int32));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_PayBankGId, ObjModel.PayBank_Gid, DbType.Int64));
                cmObjFS.parameters.Add(cmObjFS.dbManager.CreateParameter(cmObjFS.p_UId, Login_User_Gid, DbType.Int64));
                ds = cmObjFS.dbManager.GetDataSet(cmObjFS.PR_FS_Get_OnlineMemoFilePrint, CommandType.StoredProcedure, cmObjFS.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
    }
}
