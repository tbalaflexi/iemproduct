﻿using DataAccessHandler.Models.FlexiSpend;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlexiSpend.Data
{
   public class RejectDespatch_Data
    {
        RejectDespatch_Model ModelObject = new RejectDespatch_Model();                                   //Reject Despatch model object
        List<RejectDespatch_Model> ModelList = new List<RejectDespatch_Model>();                         //Reject Despatch  list model object
        DataTable dt = new DataTable();
        //list model object
        FSCommon fscmnobj = new FSCommon();

        //Reject Despatch Master Read For Review

        public List<RejectDespatch_Model> ReadRecord()
        {
            try
            {

                fscmnobj.parameters = new List<IDbDataParameter>();

                dt = fscmnobj.dbManager.GetDataTable(fscmnobj.SpGetRejectDespatchreview, CommandType.StoredProcedure);
                foreach (DataRow dr in dt.Rows)
                {
                    ModelList.Add
                        (
                        new RejectDespatch_Model
                        {
                            //inex_gid = Convert.ToInt64(dr["inex_gid"].ToString()),
                           inexid =  (dr["inex_gid"].ToString()),
                            ecfid =Convert .ToInt64(dr["ecf_gid"].ToString()),
                            batchgid = Convert.ToInt64(dr["ecf_batch_gid"].ToString()),
                             refno = dr["ecf_no"].ToString(),
                               date = (dr["ecf_date"].ToString()),
                                amount = Convert.ToDecimal(dr["ecf_amount"].ToString()),
                                raiserid =Convert.ToInt64(dr["Raiser_Gid"].ToString()),
                            raiseridname = dr["Raiser"].ToString(),
                            supliercodename = (dr["ECF Supplier/Employee"].ToString()),
                            Tran_Type = dr["Type"].ToString(),
                            subtype = dr["SubType"].ToString(),
                             inexdate = (dr["inex_date"].ToString()),
                               inexby = dr["inex_by"].ToString(),
                             inexreason = dr["inex_reason"].ToString(),
                            inexremark = dr["inex_review_remark"].ToString(),
                             Approver_Status = dr["Approval"].ToString(),
                         //  ecfstatus_status = Convert.ToInt64(dr["ecfstatus_status"].ToString()),
                           Attributes = dr["Attributes"].ToString(),
                           status = dr["status"].ToString(),
                          

                        });
                }
                return ModelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

 



        //ChequePayment Update For Review
        public DataTable UpdateRecord(RejectDespatch_Model ModelObject)
        {
            try
            {
                //if (ModelObject.reviewstatus == "Inex")
                //{
                    //string[] myarr = ModelObject.inexid.Split(',');
                    //  foreach (string item in myarr)
                    // {
                    fscmnobj.parameters = new List<IDbDataParameter>();
                    fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_Action, 50, ModelObject.reviewstatus, DbType.String));
                    //fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_inexid, item, DbType.String));
                    fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_inexid, ModelObject.inexid, DbType.String));

                    // fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_inexstatus, 50, ModelObject.status, DbType.String));
                    fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_inexreviewremark, 50, ModelObject.remark, DbType.String));
                    //fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_inexreviewremark, 50, ModelObject.queue_action_remark, DbType.String));
                    //fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_LoginId, 50, ModelObject.queue_action_by, DbType.Int64));
                    //fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_LoginId, 50, ModelObject.queue_from, DbType.Int64));

                    //  fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_inexdespatchecfid, 50, ModelObject.ecfid, DbType.String));
                    // fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_inexdespatchinex_gid, 50, ModelObject.inexid, DbType.String));
                    fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_inexecfid, 50, ModelObject.ecfgids, DbType.String));
                    //fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_inexreviewremark, 50, ModelObject.inexremark, DbType.String));

                    fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_LoginId, ModelObject.logingid, DbType.Int64));

                    dt = fscmnobj.dbManager.GetDataTable(fscmnobj.SpDmlRejectDespatchReview, CommandType.StoredProcedure, fscmnobj.parameters.ToArray());
                    // }
                //}
                //else
                //{
                //    fscmnobj.parameters = new List<IDbDataParameter>();
                //    fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_Action, 50, ModelObject.reviewstatus, DbType.String));

                //    fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_inexreviewremark, 50, ModelObject.remark, DbType.String));
                //    fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_LoginId, 50, ModelObject.queue_action_by, DbType.Int64));
                //    fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_LoginId, 50, ModelObject.queue_from, DbType.Int64));

                //     fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_inexecfid, 50, ModelObject.ecfgids, DbType.String));
                    
                //    dt = fscmnobj.dbManager.GetDataTable(fscmnobj.SpDmlRejectDespatchReview, CommandType.StoredProcedure, fscmnobj.parameters.ToArray());
                
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }


       //qc dropdown For Review(type) and Despatch(despatch mode)
        public DataTable Getdropdown(string parentcode)
        {

            fscmnobj.parameters = new List<IDbDataParameter>();
            fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.ParentCode, 50, parentcode, DbType.String));
            dt = fscmnobj.dbManager.GetDataTable(fscmnobj.SpGetqcDropdown, CommandType.StoredProcedure, fscmnobj.parameters.ToArray());
            return dt;
        }

        //non qc dropdown (review despatch )

        public DataTable dropdownvalues(string flag)
        {

            fscmnobj.parameters = new List<IDbDataParameter>();
            fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_flag, 50, flag, DbType.String));
            dt = fscmnobj.dbManager.GetDataTable(fscmnobj.SpGetNonqcdropdown, CommandType.StoredProcedure, fscmnobj.parameters.ToArray());
            return dt;
        }






        //Reject Despatch Master Read For Despatch

        public List<RejectDespatch_Model> Read_Record()
        {
            try
            {

                fscmnobj.parameters = new List<IDbDataParameter>();

               dt = fscmnobj.dbManager.GetDataTable(fscmnobj.SpgetRejectDespatch, CommandType.StoredProcedure);
                 

                foreach (DataRow dr in dt.Rows)
                {
                    ModelList.Add
                        (
                        new RejectDespatch_Model
                        {
                            despatch_gids = dr["Despatchgid"].ToString(),
                           //// despatchgid = Convert.ToInt64(dr["Despatchgid"].ToString()),
                            // inexid = Convert.ToInt64(dr["inexdespatch_inex_gid"].ToString()),
                            ecfid = Convert.ToInt64(dr["inexdespatch_ecf_gid"].ToString()),
                            batchgid = Convert.ToInt64(dr["ecf_batch_gid"].ToString()),
                            refno = dr["ecf_no"].ToString(),
                            date = (dr["ecf_date"].ToString()),
                            amount = Convert.ToDecimal(dr["ecf_amount"].ToString()),
                            raiserid = Convert.ToInt64(dr["Raiser_Gid"].ToString()),
                            raiseridname = dr["Raiser"].ToString(),
                            supliercodename = (dr["ECF Supplier/Employee"].ToString()),
                            Tran_Type = dr["Type"].ToString(),
                            subtype = dr["SubType"].ToString(),
                            Attributes = dr["Attributes"].ToString(),
                            Approver_Status = dr["Approval"].ToString(),
                            despatchid = Convert.ToInt64(dr["inexdespatchmode_gid"].ToString()),
                            chq_depatch_mode = dr["inexdespatchmode"].ToString(),
                            despatchdate = dr["inexdespatch_date"].ToString(),
                            courierid = Convert.ToInt64(dr["inexdespatch_courier_gid"].ToString()),
                            courriername = dr["couriername"].ToString(),
                            awbno = Convert.ToInt64(dr["inexdespatch_awb_no"].ToString()),



                            //inexdate = (dr["inex_date"].ToString()),
                            //inexby = dr["inex_by"].ToString(),
                            //inexreason = dr["inex_reason"].ToString(),
                            //inexremark = dr["inex_review_remark"].ToString(),
                          
                           // ecfstatus_status = dr["ecfstatus_status"].ToString(),
                           

                          

                        });
                }
                return ModelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


       //Reject Despatch Update  For Despatch
        public DataTable Update_Record(RejectDespatch_Model ModelObject)
        {
            try
            {
                fscmnobj.parameters = new List<IDbDataParameter>();
                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_Action, 50, fscmnobj.Update, DbType.String));
                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_inexdespatchgid, ModelObject.despatch_gids, DbType.String));
                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_inexdespatcgmodeid, 50, ModelObject.despatchid, DbType.Int32));
                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_inexdespatchcourierid, 50, ModelObject.courierid, DbType.Int32));
                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_inexdespatchawbno, 50, ModelObject.awbno, DbType.Int32));
                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_inexdespatchecfid, 50, ModelObject.ecfgids, DbType.String));


                if (ModelObject.despatchdate != null)
                {
                    if (ModelObject.despatchdate.Length > 19)
                    {
                        fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_inexdespatchdate, DateTime.Parse(ModelObject.despatchdate.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_inexdespatchdate, DateTime.ParseExact(ModelObject.despatchdate.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_inexdespatchdate, ModelObject.despatchdate, DbType.String));
                }

                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_LoginId, ModelObject.logingid, DbType.String));
                dt = fscmnobj.dbManager.GetDataTable(fscmnobj.SpDmlRejectDespatch, CommandType.StoredProcedure, fscmnobj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

       
        //onchange values
        public DataTable GetValues(string TranId, string parentcode)
        {
            fscmnobj.parameters = new List<IDbDataParameter>();
            fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.TranId, 50, TranId, DbType.String));
            fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.ParentCode, 50, parentcode, DbType.String));
            dt = fscmnobj.dbManager.GetDataTable(fscmnobj.SpGetqconchangevalues, CommandType.StoredProcedure, fscmnobj.parameters.ToArray());
            return dt;
        }
    }
}
