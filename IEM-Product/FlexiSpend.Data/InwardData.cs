﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessHandler.Models.FlexiSpend;
using Shared.Data;
using Shared;

namespace FlexiSpend.Data
{
    public class InwardData
    {
        #region Declarations
        FSCommon cmnParams = new FSCommon();
        CommonMethods cmnObj = new CommonMethods();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        #endregion

        public DataSet GetInwardDetails(Inward_Model _data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Batchid, _data.BatchId, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Ecfno, _data.Refno, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Refid, _data.Refid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EcfRefIds, _data.Refids, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_PouchIds, _data.Pouchids, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, _data._Action, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Userid, _data.LoginId, DbType.String));
                ds = cmnParams.dbManager.GetDataSet(cmnParams.SPInwardDetails, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet SetInwardDetails(Inward_Model _data)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EcfRefIds, _data.Refids, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_RecvDate, _data.RecvDate, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_AwbNo, _data.AwbNo, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_CourierId, _data.CourierId, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, _data._Action, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Userid, _data.LoginId, DbType.String));
                ds = cmnParams.dbManager.GetDataSet(cmnParams.SPSetInwardDetails, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
