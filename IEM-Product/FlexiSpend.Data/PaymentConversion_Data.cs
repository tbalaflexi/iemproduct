﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DBHelper;
using DataAccessHandler.Models.FlexiSpend;

namespace FlexiSpend.Data
{
    public class PaymentConversion_Data
    {
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        FSCommon cmnParams = new FSCommon();

        #region Methods for Get Payment Conversion Data List.
        public DataSet Get_PaymentConDetailList(PaymentConversion_Model Obj_Model)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Action, Obj_Model.Action, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EMP_SUP_Type, Obj_Model.EMP_SUP_Type, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ECF_Gid, Obj_Model.ECFGid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Supplier_Emp_Gid, Obj_Model.SupplierGid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Paymode_Gid, Obj_Model.PaymentGid, DbType.String));
                ds = cmnParams.dbManager.GetDataSet(cmnParams.SPGetPaymentConvDetails, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region Methods for Set Payment Conversion Maker Data.
        public DataTable Set_PaymentConversionMaker(PaymentConversion_Model Obj_Model)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_SelectedValues, Obj_Model.SelectedValues, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_LoginId, Obj_Model.LoginId, DbType.String));

                dt = cmnParams.dbManager.GetDataTable(cmnParams.SPSetPaymentConvDetails_Maker, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region Methods for Set Payment Conversion Checker Data.
        public DataTable Set_PaymentConversionChecker(PaymentConversion_Model Obj_Model)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_AuthorizeStatus, Obj_Model.CheckList_status, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_PaymentReissueId, Obj_Model.PaymentReissue_Gid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_LoginId, Obj_Model.LoginId, DbType.String));

                dt = cmnParams.dbManager.GetDataTable(cmnParams.SPSetPaymentConvDetails_Checker, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
