﻿using DataAccessHandler.Models.FlexiSpend;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlexiSpend.Data
{
   public class ChequeDespatch_Data
    {
        ChequePayment_Model ModelObject = new ChequePayment_Model();                                   //ChequePayment model object
        List<ChequePayment_Model> ModelList = new List<ChequePayment_Model>();                         //ChequePayment  list model object
        DataTable dt = new DataTable();
        //list model object
        FSCommon fscmnobj = new FSCommon();

        //ChequePayment Master Read

        public List<ChequePayment_Model> ReadRecord()
        {
            try
            {

                fscmnobj.parameters = new List<IDbDataParameter>();

                dt = fscmnobj.dbManager.GetDataTable(fscmnobj.SpGetChequeDespatch, CommandType.StoredProcedure);
                foreach (DataRow dr in dt.Rows)
                {
                    ModelList.Add
                        (
                        new ChequePayment_Model
                        {
                            chq_gid = Convert.ToInt64(dr["chq_gid"].ToString()),
                            // payrunvouchergid = Convert.ToInt64(dr["payrunvoucher_gid"].ToString()),
                            bankname = dr["bankname"].ToString(),
                            glvaluedate = dr["PaymentRunGL_Date"].ToString(),
                            // glvaluedate =  Convert.ToDateTime(dr["PaymentRunGL_Date"].ToString()),
                            pvno = Convert.ToInt64(dr["payrunvoucher_pvno"].ToString()),
                            pvamount = Convert.ToDecimal(dr["payrunvoucher_amount"].ToString()),
                            beneficiary = dr["payrunvoucher_beneficiary"].ToString(),
                            paymode = dr["payrunvoucher_paymode"].ToString(),
                            chq_no = Convert.ToInt32(dr["chq_no"].ToString()),
                            chq_date = dr["chq_date"].ToString(),
                            despatchid = Convert.ToInt64(dr["despatchid"].ToString()),
                            chq_depatch_mode = dr["chq_depatch_mode"].ToString(),
                            courierid = Convert.ToInt32(dr["courier_id"].ToString()),
                            chq_courier_name = dr["chq_courier_name"].ToString(),
                            chq_awb_no = Convert.ToInt32(dr["chq_awb_no"].ToString()),
                            chq_despatch_date=dr["chq_despatch_date"].ToString(),


                        });
                }
                return ModelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //ChequePayment Update
        public DataTable UpdateRecord(ChequePayment_Model ModelObject)
        {
            try
            {
                fscmnobj.parameters = new List<IDbDataParameter>();
                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_Action, 50, fscmnobj.Update, DbType.String));
                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_ChqGid, ModelObject.chq_gid, DbType.Int32));
                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_DespatchMode, 50, ModelObject.despatchid, DbType.String));
                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_DespatchAwnNo, 50, ModelObject.chq_awb_no, DbType.String));
                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_DespatchCourierName, 50, ModelObject.courierid, DbType.String));
                //fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.Delegate_delmattype, 50, ModelObject.delegate_delmattypeid, DbType.String));
                //fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.Delegate_Active, 50, ModelObject.delegate_active, DbType.String));
                if (ModelObject.chq_despatch_date != null)
                {
                    if (ModelObject.chq_despatch_date.Length > 19)
                    {
                        fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_DespatchDate, DateTime.Parse(ModelObject.chq_despatch_date.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_DespatchDate, DateTime.ParseExact(ModelObject.chq_despatch_date.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_DespatchDate, ModelObject.chq_despatch_date, DbType.String));
                }




                // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
                //fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.delegate_role, 50, ModelObject.delegate_workflow_Role, DbType.String));

                fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.P_LoginId, ModelObject.logingid, DbType.String));

                dt = fscmnobj.dbManager.GetDataTable(fscmnobj.SpDmlChequeDespatch, CommandType.StoredProcedure, fscmnobj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

       //dropdown binding QC Master

        public DataTable Getdropdown(string parentcode)
        {

            fscmnobj.parameters = new List<IDbDataParameter>();
            fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.ParentCode, 50, parentcode, DbType.String));
            dt = fscmnobj.dbManager.GetDataTable(fscmnobj.SpGetqcDropdown, CommandType.StoredProcedure, fscmnobj.parameters.ToArray());
            return dt;
        }

       //onchange values
        public DataTable GetValues(string TranId, string parentcode)
        {
            fscmnobj.parameters = new List<IDbDataParameter>();
            fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.TranId, 50, TranId, DbType.String));
            fscmnobj.parameters.Add(fscmnobj.dbManager.CreateParameter(fscmnobj.ParentCode, 50, parentcode, DbType.String));
            dt = fscmnobj.dbManager.GetDataTable(fscmnobj.SpGetqconchangevalues, CommandType.StoredProcedure, fscmnobj.parameters.ToArray());
            return dt;
        }
    }
}
