﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccessHandler.Models.FlexiSpend;

namespace FlexiSpend.Data
{
    public class PaymentStatus_Data
    {
        PaymentStatus_Model ModelObject = new PaymentStatus_Model();
        List<PaymentStatus_Model> ModelList = new List<PaymentStatus_Model>();
        DataTable dt = new DataTable();
        FSCommon ObjCmn = new FSCommon();
        DataSet ds = new DataSet();


        public DataSet ReadPaymentStatus()
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();

                ds = ObjCmn.dbManager.GetDataSet(ObjCmn.SpGetPaymentStatus, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());

                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //update payment status data
        public DataTable UpdatePaymentstatus(PaymentStatus_Model Obj_Model)
        {
          
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.P_SelectedValues, Obj_Model.SelectedValues, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.P_LoginId, Obj_Model.LoginId, DbType.Int32));

                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpDmlPaymentstatus, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //

        public DataSet Getdropdown(PaymentStatus_Model Obj_Model)
        {

            ObjCmn.parameters = new List<IDbDataParameter>();
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PAction, Obj_Model.Action, DbType.String));
            ds = ObjCmn.dbManager.GetDataSet(ObjCmn.SpGetqcDropdown, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            return ds;
        }

    }
}
