﻿using DataAccessHandler;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlexiBuy.Data
{
    public class FBCommon
    {

        #region Connection String and Parameter Values
        public DBManager dbManager = new DBManager("ConnectionString");
        public List<IDbDataParameter> parameters;
        #endregion

        #region Strored Procedure CURD
        public string strSP = "";
        public readonly string SPValidateLogin = "SP_Validate_Supplier";
        public readonly string SPGetsupDetails = "SP_Get_SupplierDetails";
        public readonly string SPGetInvDetails = "SP_Get_SupplierInvDetails";
        public readonly string SPGetPoDetails = "SP_Get_PoDetails";

        #endregion

        #region Stored Procedure Params
        public string ID = "@id";
        public string SubCode = "@SubCode";
        public string Password = "@Password";
        public string UserID = "@Userid";
        public string Type = "@Type";
        public string Action = "@Action";
        public string FromDate = "@FromDate";
        public string ToDate = "@ToDate";
        public string Status = "@Status";
        public string @PO = "@PO";
        public string Invno = "@Invno";

        #endregion

        #region Operation Type(CURD)
        public readonly string Create = "C";
        public readonly string Read = "R";
        public readonly string Update = "U";
        public readonly string Delete = "D";
        #endregion

        #region Loged User Details
        public string LoggedUser = "Flexi";
        #endregion

        #region POUpload Details Procedures
        public readonly string SPGetPOUploadDetails = "SP_FB_Get_POUpload";
        public readonly string SPSetPOUploadDetails = "SP_FB_DML_POUpload";
        public readonly string SPGetPOColumnList = "SP_Get_Poupload_ColumnList";
        public readonly string SPSetFileDetails = "SP_DML_File";
        public readonly string SPSetPOBulkDatas = "SP_Set_Tmp_PoUpload";
        public readonly string SPGetFileCount = "SP_Get_FileCount";
        public readonly string TableName = "fb_tmp_trn_tpoupload";
        #endregion

        #region POUpload Parameters
        public readonly string P_FileGid = "In_File_Gid";
        public readonly string P_LoginUserGid = "In_LoginUser_GID";
        public readonly string P_FileName = "In_File_Name";
        public readonly string P_FileType = "In_File_type";
        public readonly string P_ActionType = "In_ActionType";

        public readonly string P_POHeaderPONO = "In_poheader_pono";
        public readonly string P_POHeaderDesc = "In_poheader_desc";
        public readonly string P_POHeaderPeriodFrom = "In_poheader_period_from";
        public readonly string P_POHeaderPeriodTo = "In_poheader_period_to";
        public readonly string P_VendorCode = "In_poheader_vendor_code";
        public readonly string P_VendorContactCode = "In_poheader_vendor_contact_code";
        public readonly string P_POHeaderType = "In_poheader_type";
        public readonly string P_POHeaderRequest = "In_poheader_request";
        public readonly string P_RaiserCode = "In_poheader_raiser_code";
        public readonly string P_ItemDesc = "In_podts_item_desc";
        public readonly string P_ItemQty = "In_podts_item_Qty";
        public readonly string P_UnitPrice = "In_podts_unit_price";
        public readonly string P_ItemTotal = "In_podts_item_total";
        public readonly string P_ServiceCode = "In_podts_service_code";
        public readonly string P_UOMCode = "In_podts_uom_code";
        public readonly string P_ShipmentType = "In_podts_shipment_type";
        public readonly string P_ShipmentBranchCode = "In_podts_shipment_branch_code";
        public readonly string P_ShipmentEmpCode = "In_podts_shipment_emp_code";
        public readonly string P_GRNDOCNo = "In_grninwrdheader_dcno";
        public readonly string P_GRNInvoiceNo = "In_grninwrdheader_invoiceno";
        public readonly string P_GRNInwardQty = "In_grn_inward_qty";
        public readonly string P_AssetSlNo = "In_asset_sl_no";
        public readonly string P_GRNPutUseDate = "In_grn_putuseddate";
        public readonly string P_GRNMftName = "In_grn_mft_name";
        public readonly string P_GSTApplicable = "In_podts_gst_applicable";
        public readonly string P_HSNCode = "In_HSN_Code";
        public readonly string P_TaxAmount = "In_podts_tax_amount";
        public readonly string P_ProviderLoc = "In_provider_loc";
        public readonly string P_ReceiverLoc = "In_receiver_loc";
        public readonly string P_CwipReleaseBranchCode = "In_cwiprelease_branch_code";
        public readonly string P_CwipReleaseEmpCode = "In_cwiprelease_emp_code";
      
      
        #endregion

    }
}
