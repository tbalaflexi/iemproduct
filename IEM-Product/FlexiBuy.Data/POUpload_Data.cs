﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DBHelper;
using DataAccessHandler.Models.FlexiBuy;



namespace FlexiBuy.Data
{
    public class POUpload_Data 
    {
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        FBCommon cmnParams = new FBCommon();
        string Path1, squery;

        #region Methods for Get POUploadData List.
        public DataSet Get_POUploadDetailList(POUpload_Model Obj_Model)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();

                ds = cmnParams.dbManager.GetDataSet(cmnParams.SPGetPOUploadDetails, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region Methods for Get POUploadFile List.
        public DataSet Get_POUploadFileCount(POUpload_Model Obj_Model)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_FileName, Obj_Model.POUpload_FileName, DbType.String));
                ds = cmnParams.dbManager.GetDataSet(cmnParams.SPGetFileCount, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Methods for Set FileGid
        public DataSet FileInst_Data(POUpload_Model Obj_Model)
        {
         
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_FileName, Obj_Model.POUpload_NewFileName, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_FileType, Obj_Model.POUpload_FileType, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_LoginUserGid, Obj_Model.LoginId, DbType.String));
                ds = cmnParams.dbManager.GetDataSet(cmnParams.SPSetFileDetails, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return ds;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Methods for Set Temp Table PO Upload
        public DataTable SetPOUpload_TempTable (POUpload_Model Obj_Model)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_FileGid, Obj_Model.File_Gid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_POHeaderPONO, Obj_Model.Poheader_pono, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_POHeaderDesc, Obj_Model.poheader_desc, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_POHeaderPeriodFrom, Obj_Model.poheader_period_from, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_POHeaderPeriodTo, Obj_Model.poheader_period_to, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_VendorCode, Obj_Model.poheader_vendor_code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_VendorContactCode, Obj_Model.poheader_vendor_contact_code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_POHeaderType, Obj_Model.poheader_type, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_POHeaderRequest, Obj_Model.poheader_request, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_RaiserCode, Obj_Model.poheader_raiser_code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ItemDesc, Obj_Model.podts_item_desc, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ItemQty, Obj_Model.podts_item_Qty, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_UnitPrice, Obj_Model.podts_unit_price, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ItemTotal, Obj_Model.podts_item_total, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ServiceCode, Obj_Model.podts_service_code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_UOMCode, Obj_Model.podts_uom_code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ShipmentType, Obj_Model.podts_shipment_type, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ShipmentBranchCode, Obj_Model.podts_shipment_branch_code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ShipmentEmpCode, Obj_Model.podts_shipment_emp_code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_GRNDOCNo, Obj_Model.grninwrdheader_dcno, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_GRNInvoiceNo, Obj_Model.grninwrdheader_invoiceno, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_GRNInwardQty, Obj_Model.grn_inward_qty, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_AssetSlNo, Obj_Model.asset_sl_no, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_GRNPutUseDate, Obj_Model.grn_putuseddate, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_GRNMftName, Obj_Model.grn_mft_name, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_GSTApplicable, Obj_Model.podts_gst_applicable, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_HSNCode, Obj_Model.HSN_Code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_TaxAmount, Obj_Model.podts_tax_amount, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ProviderLoc, Obj_Model.provider_loc, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ReceiverLoc, Obj_Model.receiver_loc, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_CwipReleaseBranchCode, Obj_Model.cwiprelease_branch_code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_CwipReleaseEmpCode, Obj_Model.cwiprelease_emp_code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_LoginUserGid, Obj_Model.LoginId, DbType.String));
                dt = cmnParams.dbManager.GetDataTable(cmnParams.SPSetPOBulkDatas, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Medhod for Confirm Temp data to Main Table
        public DataTable SetFileConfirm_Data (POUpload_Model Obj_Model)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
           
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_LoginUserGid, Obj_Model.LoginId, DbType.String));
                dt = cmnParams.dbManager.GetDataTable(cmnParams.SPSetPOUploadDetails, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        #endregion

        //#region Methods for Set POUpload File datas
        //public int PoUpload_Temp_Data(POUpload_Model Obj_Model)
        //{
        //    int count = 0;
        //    try
        //    {
              
        //        dt = GetColumnList(Obj_Model);

        //        if (dt.Rows.Count > 0)
        //        {
        //            Path1 = Obj_Model.ServerPath;
        //            //MySqlBulkLoader bl = new MySqlBulkLoader(con);
        //            //bl.Local = true;
        //            //bl.TableName = cmnParams.TableName;
        //            //bl.LineTerminator = "\n";
        //            //bl.FileName = Path1;
        //            //bl.NumberOfLinesToSkip = 1;

        //            string sqlquery = "Load Data LOCAL INFILE " + Path1 + " IGNORE INTO TABLE chq_trn_tbasefile " +
        //                                 "FIELDS TERMINATED BY ',' LINES TERMINATED BY '\r\n' " +
        //                                 "IGNORE 1 LINES (PO Number,PO Header Description,PO Period From,PO Period TO,Vendor Name,Vendor Contact ID,PO Type,PO Request Type,Raiser ID,PO Line Item Description,PO Line item Quantity,PO Line Item Unit Price,PO Line Item Total,PO Product Service Code,UOM Code,PO Shipment Type,PO Shipment Branch ID,PO Shipment Employee,GRN Inward DOC No,GRN Inward Invoice No,GRN Inward Received Qty,Asset Sl No ,Put To Used Date,Manufacture Name,PO Details GST Applicable,HSN Code,PO Details Tax Amount,Provider Location,Reciver Location,CWIP Release Branch Code,CWIP Release Employee Code,) ;";

        //            //for (int i = 0; i < dt.Rows.Count - 1; i++)
        //            //{
        //            //    bl.Columns.Add(dt.Rows[i]["COLUMN_NAME"].ToString());
        //            //}
        //            //count = bl.Load();
        //            //closedcon();
        //        }




        //        return count;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        //#endregion

        #region Methods for Get PO Temp Table Column List
        public DataTable GetColumnList(POUpload_Model Obj_Model)
        {
            Obj_Model.Action = "POUPLOAD";
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_ActionType, Obj_Model.Action, DbType.String));
                dt = cmnParams.dbManager.GetDataTable(cmnParams.SPGetPOColumnList, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion



    }
}
