﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;

namespace IEMProduct.Service
{
    public class MasterModel
    {
        public Int64    MasterGid       { get; set; }
        public Int64    MasterTranGid   { get; set; }
        [Required(ErrorMessage = "Master Code Should Not Be Blank")]
        public string   MasterCode      { get; set; }
        public string   MasterDesc      { get; set; }
        [Required(ErrorMessage = "Parent Code Should Not Be Blank")]
        public string   ParentCode      { get; set; }
        public string   DependCode      { get; set; }
        public string   InsertedBy      { get; set; }
        public string   InsertedDate    { get; set; }
        public string   ModifiedBy      { get; set; }
        public string   ModifiedDate    { get; set; }
        public string   deletedby       { get; set; }
        public string   code            { get; set; }
    }
}
