﻿using System.Web;
using System.Web.Optimization;

namespace IEMProduct
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                // "~/Scripts/kendo/2015.3.1111/jquery.min.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
              "~/Scripts/kendo/2015.1.318/jquery.min.js",
                /*Script for Kendo Grid*/
                "~/Scripts/kendo/2015.1.318/kendo.all.min.js",
                 "~/Scripts/kendo/2015.1.318/jszip.min.js",
                 "~/Scripts/kendo/2015.1.318/kendo.aspnetmvc.min.js",
                 "~/Scripts/kendo/2015.1.318/kendo.modernizr.custom.js",
                 "~/Scripts/kendo/2015.1.318/kendo.datepicker.min.js",
                /*Script for Jquery Alert*/
                 "~/Scripts/jquery-confirm.min.js",
                /*Script for Jquery*/
                 "~/Scripts/js/main.js",
                 "~/Content/vendor/countdowntime/countdowntime.js",
                 "~/Content/vendor/daterangepicker/moment.min.js",
                 "~/Content/vendor/select2/select2.min.js",
                 "~/Content/vendor/bootstrap/js/bootstrap.min.js",
                 "~/Content/vendor/bootstrap/js/popper.js",
                 "~/Content/vendor/animsition/js/animsition.min.js"
                ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                /*Bootstrap*/
                      "~/Content/Bootstrap/bootstrap4.css",
                      "~/Content/vendor/bootstrap/css/bootstrapv4.min.css",
                /*Common CSS*/
                      "~/Content/Site.css",
                      "~/Content/vendor/animate/animate.css",
                      "~/Content/vendor/css-hamburgers/hamburgers.min.css",
                      "~/Content/vendor/animsition/css/animsition.min.css",
                      "~/Content/vendor/select2/select2.min.css",
                      "~/Content/vendor/daterangepicker/daterangepicker.css",
                      "~/Content/css/util.css",
                /*Script for Jquery Alert*/
                      "~/Content/jquery-confirm.min.css",
                /*Script for Kendo Grid*/
                     "~/Content/kendo/2015.1.318/kendo.common.min.css",
                      "~/Content/kendo/2015.1.318/kendo.default.min.css",
                      "~/Content/kendo/2015.1.318/kendo.dataviz.mobile.min.css",
                /*"~/Content/kendo/2015.1.318/kendo.common.min.css",*/
                      "~/Content/kendo/2015.1.318/kendo.mobile.all.min.css",
                /*Fonts*/
                    //"~/Content/fonts/font-awesome-4.7.0/css/font-awesome.min.css",
                    //"~/Content/fonts/iconic/css/material-design-iconic-font.min.css",
                    "~/Content/css/dist/css/AdminLTE.min.css",
                    "~/Content/css/dist/css/skins/_all-skins.min.css",
                     //"~/Content/css/dist/css/skins/_all-skins.css",
                       "~/Content/vendor/GoogleFonts.css",
                       "~/Content/css/bower_components/bootstrap/dist/css/bootstrap.min.css",
                        "~/Content/css/bower_components/font-awesome/css/font-awesome.min.css",
                        "~/Content/css/bower_components/Ionicons/css/ionicons.min.css",
                        "~/Content/css/CommonStyles.css"
                        //"~/Content/main.css"
                      ));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = false;
        }
    }
}
