﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IEMProduct.Startup))]
namespace IEMProduct
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
