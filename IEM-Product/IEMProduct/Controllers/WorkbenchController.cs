﻿using DBHelper;
using IEMProduct.Service;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;

namespace IEMProduct.Controllers
{
    public class WorkbenchController : Controller
    {


        #region

        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(AttachmentController));             //Declaring Log4Net 
        Workbench_Model ModelObject = new Workbench_Model();                                   // WorkBench model object
        List<Workbench_Model> ModelList = new List<Workbench_Model>();                         //WorkBench list model object
        Workbench_Service ServiceObject = new Workbench_Service();                            // WorkBench master service object
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        string result = "";


        #endregion


        // GET: Workbench
        public ActionResult Workbench()
        {
            ModelObject.Login_UserID = Convert.ToInt64(Session["Employee_Gid"].ToString());
            return View(ModelObject);
        } 
        // WorkBench Read
        public ActionResult ReadGrid([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                string LoginMode = Session["Employee_Role"].ToString();
                Int64 Raiser_Gid = Convert.ToInt64(Session["Employee_Gid"]);
                Int64 Login_User_Gid = 0;
                if (LoginMode.ToString().Equals("P"))
                {
                    Login_User_Gid = Convert.ToInt64(Session["Proxy_InsertBy"].ToString());
                }
                else
                {
                    Login_User_Gid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                } 
                //Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                return Json(ServiceObject.ReadRecord(Raiser_Gid, Login_User_Gid, LoginMode).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }
        }


        public ActionResult ReadGridApprover([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                //Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                string LoginMode = Session["Employee_Role"].ToString();
                Int64 Raiser_Gid = Convert.ToInt64(Session["Employee_Gid"]);
                Int64 Login_User_Gid = 0;
                if (LoginMode.ToString().Equals("P"))
                {
                    Login_User_Gid = Convert.ToInt64(Session["Proxy_InsertBy"].ToString());
                }
                else
                {
                    Login_User_Gid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                }
                return Json(ServiceObject.ReadRecordApprover(Raiser_Gid, Login_User_Gid, LoginMode).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }

        }


        public ActionResult RaiserHistory(Int64 employeeGid )
        {
            ModelObject.Login_UserID = employeeGid;
            return View(ModelObject);
        }

        public ActionResult ApproverHistory(Int64 employeeGid)
        {
            ModelObject.Login_UserID = employeeGid;
            return View(ModelObject);
        }


        public ActionResult ReadActionHistory([DataSourceRequest] DataSourceRequest request, Int64 loginId = 0, string actionType = "")
        {
            try
            {
                ModelObject.Login_UserID = Convert.ToInt64(loginId);
                ModelObject.Action = actionType.ToString();
                return Json(ServiceObject.ReadActionHistory(ModelObject).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }
        }

    }
}