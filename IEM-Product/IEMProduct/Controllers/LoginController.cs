﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using IEMProduct.Service;
using DataAccessHandler.Models.IEMProduct;
namespace IEMProduct.Controllers
{
    public class LoginController : Controller
    {
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(LoginController));
        Proxy_Model ObjProxy = new Proxy_Model();
        Login_Service serviceObj = new Login_Service();
        DataTable dt = new DataTable();
        // GET: Login
        public ActionResult Login()
        {
            Session.Remove("Employee_Gid");
            Session.Remove("Employee_Code");
            Session.Remove("Employee_Name");
            Session.Remove("Grade_Gid");
            Session.Remove("Employee_Role");
            Session.Remove("Proxy_InsertBy");
            return View();
        }

        [HttpPost]
        public ActionResult Login(string username, string pass, string ddlLoginMode, string proxyFromId = "0")
        {
            try
            {
                dt = serviceObj.CheckUserCredential(username, pass, ddlLoginMode, proxyFromId);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0].ToString() != "SQLEXCEPTION" && dt.Rows[0][0].ToString() != "Invalid Credential")
                    {
                        Session["Employee_Gid"] = Convert.ToInt64(dt.Rows[0]["employee_gid"].ToString());
                        Session["Employee_Code"] = dt.Rows[0]["employee_code"].ToString();
                        Session["Employee_Name"] = dt.Rows[0]["employee_name"].ToString();
                        Session["Grade_Gid"] = Convert.ToInt64(dt.Rows[0]["employee_grade_gid"].ToString());
                        Session["Proxy_InsertBy"] = Convert.ToInt64(dt.Rows[0]["InsertBy"].ToString());
                        Session["Employee_Role"] = dt.Rows[0]["employee_role"].ToString();
                        //if(Session["Employee_Role"].ToString().Equals("P"))
                        //{
                        //      return RedirectToAction("Claim", "Claim", new {area ="EOW"});
                        //}
                        //else
                        //{
                        //return RedirectToAction("Workbench", "Workbench");
                        //}            
                        return RedirectToAction("Workbench", "Workbench");
                    }
                    else
                    {
                        ViewBag.Message = "Please check.! Invalid User Credential.!";
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return View();
        }
        //Ramya Proxy
        public JsonResult Get_ProxyFrom(string ProxyFrom)
        {
            List<Proxy_Model> lst = new List<Proxy_Model>();
            try
            {
                lst = serviceObj.Get_ProxyFrom(ProxyFrom);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
    }
}