﻿using DataAccessHandler.Models.IEMProduct;
using IEMProduct.Service;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Kendo.Mvc.Extensions;
using System.Web.Mvc;

namespace IEMProduct.Controllers
{
    public class FundUpdateController : Controller
    {

        #region

        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(TransactionController));             //Declaring Log4Net 
        FundUpdate_Model ModelObject = new FundUpdate_Model();                                   // FundUpdate model object
        List<FundUpdate_Model> ModelList = new List<FundUpdate_Model>();                         //FundUpdate list model object
        FundUpdate_Service ServiceObject = new FundUpdate_Service();                            // FundUpdate master service object
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        string result = "";
        

        #endregion
        // GET: FundUpdate
        public ActionResult FundUpdate()
        {
            return View();
        }


        //FundUpdate Read

        public ActionResult ReadGrid([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                return Json(ServiceObject.ReadRecord().ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }

        }

        //FundUpdate Insert

        public ActionResult SaveGrid(FundUpdate_Model ModelObject)
        {
            try
            {

                
                dt = ServiceObject.SaveRecord(ModelObject);
                ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.result);

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return Json(ModelObject.result);
            }
        }

        //  FundUpdate Update

        public ActionResult UpdateGrid(FundUpdate_Model ModelObject, Int64 _balance =0)
        {
            try
            {

                ModelObject.fundupdate_fundavailable = _balance;
                dt = ServiceObject.UpdateRecord(ModelObject);
                ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.result);

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return Json(ModelObject.result);

            }

        }


        // FundUpdate delete

        public ActionResult DeleteGrid(FundUpdate_Model ModelObject)
        {
            try
            {

                dt = ServiceObject.DeleteRecord(ModelObject);
                if (dt.Rows.Count > 0)
                {
                    result = dt.Rows[0][0].ToString();
                }
                ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return Json(ModelObject.result);
            }

        }




        //binding   Bank dropdown non qc
        //public JsonResult dropdowns_values_binding(string flag)
        //{
        //    List<FundUpdate_Model> dropdown = new List<FundUpdate_Model>();
        //    try
        //    {
        //        dropdown = ServiceObject.dropdowns_values(flag);
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex.ToString());
        //    }
        //    return Json(dropdown, JsonRequestBehavior.AllowGet);
        //}

        //account no
        //public JsonResult dropdowns_values(string flag)
        //{
        //    List<FundUpdate_Model> dropdown = new List<FundUpdate_Model>();
        //    try
        //    {
        //        dropdown = ServiceObject.dropdowns_values(flag);
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex.ToString());
        //    }
        //    return Json(dropdown, JsonRequestBehavior.AllowGet);
        //}

        //onchange values accno

        public ActionResult GetSubType(string TranId, string parentcode)
        {
            List<FundUpdate_Model> sub_TypeList = new List<FundUpdate_Model>();

            try
            {
                dt = ServiceObject.GetValues(TranId, parentcode);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    FundUpdate_Model SubType = new FundUpdate_Model();
                    SubType.id = Convert.ToInt64(dt.Rows[i][0].ToString());
                    SubType.accountNo = dt.Rows[i][1].ToString();
                    sub_TypeList.Add(SubType);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(sub_TypeList, JsonRequestBehavior.AllowGet);
        }
    }
}