﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMProduct.Service;
using Kendo.Mvc.Extensions;
using DataAccessHandler.Models.IEMProduct;
using System.Data;
using Newtonsoft.Json;
using DBHelper;
namespace IEMProduct.Controllers
{
    public class ProfileController : Controller
    {
        #region

        Profile_Service serviceObj = new Profile_Service();
        DataTable dt = new DataTable();
        DataTable result = new DataTable();
        DataRow dr;
        DataSet ds = new DataSet();
        Profile_Model Obj_Model = new Profile_Model();
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(ProfileController));
        #endregion
        // GET: Profile
        public ActionResult Profile()
        {
            return View();
        }
        public ActionResult EmployeeProfile()
        {
            return View();
        }
        //Delegation Read

        public ActionResult ReadGrid([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                Obj_Model.Action = "ALL";
                return Json(serviceObj.Get_EmployeeRead(Obj_Model).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }

        }
        [HttpPost]
        public ActionResult GetEmpDetails(Profile_Model Obj_Model)
        {
            try
            {
                Obj_Model.Emp_Gid = Convert.ToInt64(Session["Employee_Gid"]);
                dt = serviceObj.GetEmpDetails_List(Obj_Model);
                return Json(JsonConvert.SerializeObject(dt), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();

            }
        }
        [HttpPost]
        public ActionResult GetEmpDetailsHirackey(Profile_Model Obj_Model)
        {
            try
            {
                List<tree_model> ModelList = new List<tree_model>();
                Obj_Model.Emp_Gid = Convert.ToInt64(Session["Employee_Gid"]);
                dt = serviceObj.GetEmpDetailsHirackey_List(Obj_Model);
                result = new DataTable();
                result.Clear();
                result.Columns.Add("s_no");
                result.Columns.Add("employee");
                result.Columns.Add("emplevel");
                result.Columns.Add("parent");

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    add_menu(Convert.ToInt32(dt.Rows[i]["s_no"]), dt.Rows[i]["employee"].ToString(), Convert.ToInt32(dt.Rows[i]["emplevel"]), Convert.ToInt32(dt.Rows[i]["parent"]));
                }

                foreach (DataRow datarow in result.Rows)
                {
                    if (datarow["parent"].ToString() == "0")
                    {
                        //tree_model nd = add_node(dt, datarow);
                        tree_model node = new tree_model();
                        tree_model _child_node;

                        node.id = datarow["s_no"].ToString();
                        node.text = datarow["employee"].ToString();
                        node.emplevel = datarow["emplevel"].ToString();
                        node.parent = datarow["parent"].ToString();


                        var rows = result.AsEnumerable()
                                       .Where(r => r.Field<string>("parent") == node.emplevel);

                        foreach (DataRow _row in rows)
                        {
                            _child_node = new tree_model();
                            _child_node = add_node(result, _row);
                            node.items.Add(_child_node);
                        }
                        ModelList.Add(node);
                    }
                }

                return Json(ModelList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();

            }
        }
        private class tree_model
        {
            public string id { get; set; }
            public string text { get; set; }
            public string emplevel { get; set; }
            public string parent { get; set; }
            public bool expanded = true;
            public List<tree_model> items = new List<tree_model>();
        }
        private tree_model add_node(DataTable _dt, DataRow _dr)
        {
            tree_model node = new tree_model();
            try
            {

                tree_model _child_node;

                node.id = _dr["s_no"].ToString();
                node.text = _dr["employee"].ToString();
                node.emplevel = _dr["emplevel"].ToString();
                node.parent = _dr["parent"].ToString();

                var rows = _dt.AsEnumerable()
                               .Where(r => r.Field<string>("parent") == node.emplevel);

                foreach (DataRow _row in rows)
                {
                    _child_node = new tree_model();
                    _child_node = add_node(_dt, _row);
                    node.items.Add(_child_node);
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());

            }
            return node;
        }

        private void add_menu(int id, string text, int emplevel, int parent)
        {
            try
            {
                dr = result.NewRow();
                dr["s_no"] = id.ToString();
                dr["employee"] = text;
                dr["emplevel"] = emplevel.ToString();
                dr["parent"] = parent.ToString();

                result.Rows.Add(dr);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
            }
        }

        //dropdown binding

        #region Common methods for dropdown bindings.
        //Designation
        public ActionResult Getdesignation(string parentcode)
        {
            List<Profile_Model> designationlist = new List<Profile_Model>();

            try
            {
                dt = serviceObj.Getdesignation(parentcode);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Profile_Model designation = new Profile_Model();
                    designation.Designationid = Convert.ToInt64(dt.Rows[i][0].ToString());
                    designation.DesignationName = dt.Rows[i][1].ToString();
                    designationlist.Add(designation);
                }
                var jsonResult = Json(designationlist, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
            
        }

        //Grade
        public ActionResult GetGrade(string parentcode)
        {
            List<Profile_Model> gradelist = new List<Profile_Model>();

            try
            {
                dt = serviceObj.GetGrade(parentcode);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Profile_Model grade = new Profile_Model();
                    grade.Gradeid = Convert.ToInt64(dt.Rows[i][0].ToString());
                    grade.GradeName = dt.Rows[i][1].ToString();
                    gradelist.Add(grade);
                }
                var jsonResult = Json(gradelist, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
           
        }



        //Department 
        public ActionResult GetDepartment(string parentcode)
        {
            List<Profile_Model> departmentlist = new List<Profile_Model>();

            try
            {
                dt = serviceObj.GetDepartment(parentcode);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Profile_Model department = new Profile_Model();
                    department.Departmentid = Convert.ToInt64(dt.Rows[i][0].ToString());
                    department.Departmentname = dt.Rows[i][1].ToString();
                    departmentlist.Add(department);
                }
                var jsonResult = Json(departmentlist, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
           
        }
        //branch
        public ActionResult Getbranch(string parentcode)
        {
            List<Profile_Model> branchlist = new List<Profile_Model>();

            try
            {
                dt = serviceObj.Getbranch(parentcode);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Profile_Model branch = new Profile_Model();
                    branch.branchid = Convert.ToInt64(dt.Rows[i][0].ToString());
                    branch.branchname = dt.Rows[i][1].ToString();
                    branchlist.Add(branch);
                }

                var jsonResult = Json(branchlist, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
           
        }
        //FC
        public ActionResult GetFC(string parentcode)
        {
            List<Profile_Model> FClist = new List<Profile_Model>();

            try
            {
                dt = serviceObj.GetFC(parentcode);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Profile_Model FC = new Profile_Model();
                    FC.Fcid = Convert.ToInt64(dt.Rows[i][0].ToString());
                    FC.FcName = dt.Rows[i][1].ToString();
                    FClist.Add(FC);
                }
                var jsonResult = Json(FClist, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
           
        }
        //CC
        public ActionResult GetCC(string parentcode)
        {
            List<Profile_Model> cclist = new List<Profile_Model>();

            try
            {
                dt = serviceObj.GetCC(parentcode);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Profile_Model cc = new Profile_Model();
                    cc.CCid = Convert.ToInt64(dt.Rows[i][0].ToString());
                    cc.CCName = dt.Rows[i][1].ToString();
                    cclist.Add(cc);
                }
                var jsonResult = Json(cclist, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;      

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
           
        }

        //Product Code

        public ActionResult GetProduct(string parentcode)
        {
            List<Profile_Model> Productlist = new List<Profile_Model>();

            try
            {
                dt = serviceObj.GetProduct(parentcode);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Profile_Model product = new Profile_Model();
                    product.Productid = Convert.ToInt64(dt.Rows[i][0].ToString());
                    product.ProductName = dt.Rows[i][1].ToString();
                    Productlist.Add(product);
                }
                var jsonResult = Json(Productlist, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;  

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
           
        }
        //bank name
        public ActionResult GetBankName(string parentcode)
        {
            List<Profile_Model> Banknamelist = new List<Profile_Model>();

            try
            {
                dt = serviceObj.GetBankName(parentcode);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Profile_Model bank = new Profile_Model();
                    bank.Bankid = Convert.ToInt64(dt.Rows[i][0].ToString());
                    bank.BankName = dt.Rows[i][1].ToString();
                    Banknamelist.Add(bank);
                }
                var jsonResult = Json(Banknamelist, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;  

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
            
        }


        //City
        public ActionResult Getcity(string parentcode)
        {
            List<Profile_Model> citylist = new List<Profile_Model>();

            try
            {
                dt = serviceObj.Getcity(parentcode);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Profile_Model city = new Profile_Model();
                    city.cityid = Convert.ToInt64(dt.Rows[i][0].ToString());
                    city.cityname = dt.Rows[i][1].ToString();
                    citylist.Add(city);
                }
                var jsonResult = Json(citylist, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult; 

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
            
        }
        //Employee Name Code Non QC Master
        public ActionResult GetEmpCodeName(string EmployeeName, string Master, string flag = "")
        {
            List<Profile_Model> emplist = new List<Profile_Model>();
            try
            {
                dt = serviceObj.GetEmployee_ByCodeorName(EmployeeName, flag, Master);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Profile_Model emploee = new Profile_Model();
                    emploee.employee_supervisor_gid = Convert.ToInt64(dt.Rows[i][0].ToString());
                    emploee.Supervisor_name = dt.Rows[i][1].ToString();
                    emplist.Add(emploee);
                }
                var jsonResult = Json(emplist, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
            
        }
     
        public ActionResult GetEmpbranch(string flag)
        {
            List<Profile_Model> empbranchlist = new List<Profile_Model>();
            try
            {
                dt = serviceObj.GetEmployeeBrranch(flag);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Profile_Model employeebranch = new Profile_Model();
                    employeebranch.empbranch_id = Convert.ToInt64(dt.Rows[i][0].ToString());
                    employeebranch.empbranch_name = dt.Rows[i][1].ToString();
                    empbranchlist.Add(employeebranch);
                }
                var jsonResult = Json(empbranchlist, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }

        }
        #endregion

        #region Employee Master insert
        public ActionResult SaveGrid([DataSourceRequest] DataSourceRequest request, Profile_Model Obj_Model)
        {
            try
            {
                Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = serviceObj.SaveRecord(Obj_Model, User_GID);
                Obj_Model.result = JsonConvert.SerializeObject(dt);
                return Json(Obj_Model.result);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                Obj_Model.result = ex.Message.ToString();
                return Json(Obj_Model.result);
            }
        }
        #endregion

        #region employee master update
        public ActionResult UpdateGrid([DataSourceRequest] DataSourceRequest request, Profile_Model Obj_Model)
        {
            try
            {
                Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = serviceObj.UpdateRecord(Obj_Model, User_GID);
                Obj_Model.result = JsonConvert.SerializeObject(dt);
                return Json(Obj_Model.result);

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                Obj_Model.result = ex.Message.ToString();
                return Json(Obj_Model.result);

            }

        }
        #endregion

        #region Employee Master Delete
        public ActionResult DeleteGrid(Profile_Model Obj_Model)
        {
            try
            {
                Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = serviceObj.DeleteRecord(Obj_Model, User_GID);
                //if (dt.Rows.Count > 0)
                //{
                //    result = dt.Rows[0][0].ToString();
                //}
                Obj_Model.result = JsonConvert.SerializeObject(dt);
                return Json(Obj_Model.result, JsonRequestBehavior.AllowGet);

                //dt = serviceObj.Remove_Delmat(Delmat_Gid);
                //Result = JsonConvert.SerializeObject(dt);

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                Obj_Model.result = ex.Message.ToString();
                return Json(Obj_Model.result);
            }

        }
        #endregion
    }
}