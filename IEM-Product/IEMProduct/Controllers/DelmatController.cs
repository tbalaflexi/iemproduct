﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using Newtonsoft.Json;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using DBHelper;
using IEMProduct.Service;
using DataAccessHandler.Models.IEMProduct;

namespace IEMProduct.Controllers
{
    public class DelmatController : Controller
    {
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(DelmatController));
        Delmat_Service serviceObj = new Delmat_Service();
        Delmat_Model ObjModel = new Delmat_Model();
        DataTable dt = new DataTable();
        // GET: Delmat
        public ActionResult Delmat([DataSourceRequest]DataSourceRequest request, Int64 Delmat_Gid = 0)
        {
            try
            {
                ObjModel.Delmat_Gid = Delmat_Gid;
                //serviceObj.Read_DelmatHeader(Delmat_Gid).ToDataSourceResult(request)
                return View();
                  
 
            }
           catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }
           

           
        }

        [HttpGet]
        public ActionResult ReadDelmat()
        {
            return View();

            //return PartialView("Types_Attribute");
        }

        public ActionResult Show_Attribute_PartialView()
        {
            WorkFlow_Model objModel = new WorkFlow_Model();
            return PartialView("Types_Attribute", objModel);
        }

        #region Exception

        [HttpGet]
        public ActionResult Exception()
        {
            //List<SupClassificationModel> obj = new List<SupClassificationModel>();
            //if (listfor != null)
            //{
            //    obj = objModel.GridLoad().ToList();
            //}
            //if (name == "0")
            //{ Session["process"] = ""; }
            //Session["save"] = "";
            //return PartialView(obj);
            return PartialView("Exception");
        }

        public ActionResult Read_Exception([DataSourceRequest]DataSourceRequest request, Delmat_Model ObjModel, Int64 Delmat_Gid = 0)
        {
            try
            {
                ObjModel.Delmat_Gid = Delmat_Gid;
                return Json(serviceObj.Get_Exception(ObjModel).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return View();
        }
        public ActionResult Get_Employee()
        {
            List<Delmat_Model> lst = new List<Delmat_Model>();
            try
            {
                lst = serviceObj.Get_Employee();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Get_DelmatName(Delmat_Model ObjModel)
        {
            List<Delmat_Model> lst = new List<Delmat_Model>();
            try
            {
                lst = serviceObj.Get_DelmatName(ObjModel);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Slab Range
        [HttpGet]
        public ActionResult SlabRange()
        {
            //List<SupClassificationModel> obj = new List<SupClassificationModel>();
            //if (listfor != null)
            //{
            //    obj = objModel.GridLoad().ToList();
            //}
            //if (name == "0")
            //{ Session["process"] = ""; }
            //Session["save"] = "";
            //return PartialView(obj);
            return PartialView("SlabRange");
        }

        #region Get summery list.

        // read delmat --Vadivu
        public ActionResult Read_Delmat([DataSourceRequest]DataSourceRequest request, Delmat_Model ObjModel)
         {
            try
            {
                return Json(serviceObj.ReadRecord(ObjModel).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return View();
        }


        public ActionResult Remove_Delmat(Int64 Delmat_Gid=0)
        {
            string Result = string.Empty;
            try
            {
                dt = serviceObj.Remove_Delmat(Delmat_Gid);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }




        public ActionResult Read_SlabRange([DataSourceRequest]DataSourceRequest request, Delmat_Model ObjModel)
        {
            try
            {
                return Json(serviceObj.Get_SlabRange(ObjModel).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return View();
        }
        public ActionResult Save_Update_Delmat_Exception(Delmat_Model ObjModel, string ActionType = "", Int64 DelmatException_DelmatGid = 0, Int64 Employee_gid = 0)
        {
            string Result = string.Empty;
            try
            {
                ObjModel.Action = ActionType;
                ObjModel.delmatexception_to = Employee_gid;
                ObjModel.delmatexception_delmat_gid = DelmatException_DelmatGid;
                dt = serviceObj.Save_Update_Delmat_Exception(ObjModel);
                Result = JsonConvert.SerializeObject(dt);
                return Json(Result);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                Result = ex.Message.ToString();
                return Json(Result);
            }
            //  return Json(Result, JsonRequestBehavior.AllowGet);
        }
         
        public ActionResult Remove_Exception(Int64 Delmat_Exeption_Gid=0)
        {
            string Result = string.Empty;
            try
            {
                dt = serviceObj.Remove_Exception(Delmat_Exeption_Gid);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Create Flow.

        public ActionResult Save_DelmatFlow(Delmat_Model objmodel)
        {
            string Result = string.Empty;
            //try
            //{
            //    dt = serviceObj.Save_Delmat_SlabRange(objmodel);
            //    Result = JsonConvert.SerializeObject(dt);
            //}
            //catch (Exception ex)
            //{
            //    //logger.Error(ex.ToString());
            //}
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveNew_SlabRange( Delmat_Model objmodel)
        {
            string Result = string.Empty;
            try
            {
                dt = serviceObj.Save_Delmat_SlabRange(objmodel);
                Result = JsonConvert.SerializeObject(dt);
                return Json(Result);


            }
            catch (Exception ex)
            {

                logger.Error(ex.ToString());
                Result = ex.Message.ToString();
                return Json(Result);
            }
            //return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Remove_SlabRange(Int64 SlabRange_Gid=0)
        {
            string Result = string.Empty;
            try
            {
                dt = serviceObj.Remove_SlabRange(SlabRange_Gid);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Get_TitleDropdown(string TitleRefcode="")
        {
            List<Delmat_Model> lst = new List<Delmat_Model>();
            try
            {
                lst = serviceObj.Get_TitleDropdown(TitleRefcode);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Get_Title()
        {
            List<Delmat_Model> lst = new List<Delmat_Model>();
            try
            {
                lst = serviceObj.Get_Title();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Get_SlabRangeLimit()
        {
            List<Delmat_Model> lst = new List<Delmat_Model>();
            try
            {                
                lst = serviceObj.Get_SlabRangeLimit();
                //lst.Add(new Delmat_Model { slabrange_name = "--Select--", slabrange_gid = 0 });
                lst.Insert(0, new Delmat_Model { Slabrange_LimitName = "--Select--", Slabrange_Gid = 0 });
                //lst.Insert(0, "--Select--");
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Save_Update_DelmatFlow(Delmat_Model ObjModel, Int64 DelmatHeader_Gid=0)
        {
            string Result = string.Empty;
            try
            {
                ObjModel.Delmat_Gid = DelmatHeader_Gid;
                dt = serviceObj.Save_Update_DelmatFlow(ObjModel);
                Result = JsonConvert.SerializeObject(dt);
                return Json(Result);


            }
            catch (Exception ex)
            {

                logger.Error(ex.ToString());
                Result = ex.Message.ToString();
                return Json(Result);
            }
            
             
            //return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Read_DelmatFlow([DataSourceRequest]DataSourceRequest request, Int64 DelmatHeader_Gid=0)
        {
            try
            {
                return Json(serviceObj.Read_DelmatFlow(DelmatHeader_Gid).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return View();
        }
        public ActionResult Remove_DelmatFlow(Int64 DelmatFlow_Gid=0)
        {
            string Result = string.Empty;
            try
            {
                dt = serviceObj.Remove_DelmatFlow(DelmatFlow_Gid);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Save_Update_Delmat(Delmat_Model ObjModel)
        {
            //string Result = string.Empty;
            string Result = "test";
            try
            {                
                dt = serviceObj.Save_Update_Delmat(ObjModel);
                Result = JsonConvert.SerializeObject(dt);
                return Json(Result);
            }
            catch (Exception ex)
            {
               logger.Error(ex.ToString());
                Result = ex.Message.ToString();
                return Json(Result);
            }
           // return Json(Result, JsonRequestBehavior.AllowGet);
        }
        #endregion
         
        #endregion

        // Delmat header
        public ActionResult Read_DelmatHeader([DataSourceRequest]DataSourceRequest request, Int64 Delmat_Gid=0)
        {
            try
            {
                //ObjModel.Delmat_Gid = Delmat_Gid;
                return Json(serviceObj.Read_DelmatHeader(Delmat_Gid).ToDataSourceResult(request),JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return View();
        }

        public ActionResult Save_DelmatMatrix(Int64 DelmatHeader_gid)
        {
            //string Result = string.Empty;
            string Result = "";
            try
            {
                dt = serviceObj.Save_DelmatMatrix(DelmatHeader_gid);
                Result = JsonConvert.SerializeObject(dt);
                return Json(Result);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                Result = ex.Message.ToString();
                return Json(Result);
            }
            // return Json(Result, JsonRequestBehavior.AllowGet);
        }


    }
}