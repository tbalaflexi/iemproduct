﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using DataAccessHandler.Models.IEMProduct;
using IEMProduct.Service;
using Kendo.Mvc.Extensions;
using System.Data;
namespace IEMProduct.Controllers
{
    public class FlowAssignmentController : Controller
    {
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(FlowAssignmentController));
        FlowAssignment_Model   lstModel = new FlowAssignment_Model();
        FlowAssignment_Service lstService = new FlowAssignment_Service();
        DataTable dt = new DataTable();


        // GET: FlowAssignment
        public ActionResult FlowAssignment()
        {
            return View();
        }

        public ActionResult ReadFlowAssignment([DataSourceRequest] DataSourceRequest request, Int64 AssignType, string AssignRefType)
        {
            try
            {
                Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                return Json(lstService.ReadFlowAssignment(User_GID,AssignType, AssignRefType).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }

        }

        [HttpPost]
        public ActionResult Save_FlowAssignmentList(FlowAssignment_Model objModel)
        {
            string Result = string.Empty;
            try
            {
                              
                objModel.loginUserID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = lstService.Save_FlowAssignmentList(objModel);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }



        #region FlowAssignmentDrpList Bindings

        public JsonResult GetAssignmentTypeList()
        {
            List<FlowAssignment_Model> lst = new List<FlowAssignment_Model>();
            try
            {
                lst = lstService.GetAssignmentTypeList();

            }
            catch(Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(lst, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetAssignToList(string EmployeeName)
        {
            List<FlowAssignment_Model> lst = new List<FlowAssignment_Model>();
            try
            {
                lst = lstService.GetAssignToList(EmployeeName);

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(lst, JsonRequestBehavior.AllowGet);

        }




        public JsonResult GetAssignmentRefNoUserList(int AssignType = 0, string FindSearch="")
        {
            List<FlowAssignment_Model> lst = new List<FlowAssignment_Model>();
            try
            {
                lst = lstService.GetAssignmentRefNoUserList(AssignType, FindSearch);

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(lst, JsonRequestBehavior.AllowGet);

        }


        #endregion

    }
}