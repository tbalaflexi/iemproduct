﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using DataAccessHandler.Models.IEMProduct;
using IEMProduct.Service;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;

namespace IEMProduct.Controllers
{
    public class MailTemplateController : Controller
    {
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(MailTemplateController));
        MailTemplate_Service serviceObj = new MailTemplate_Service();
        // GET: MailTemplate
        public ActionResult MailTemplate()
        {
            return View();
        }

        public ActionResult MailTemplate_Read([DataSourceRequest]DataSourceRequest request, string Master, Int64 Depend_Id)
        {
            try
            {
                Int64 LoggedUser_ID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                return Json(serviceObj.Get_MailTemplate_Details(Master, Depend_Id, LoggedUser_ID).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }
        }

        public ActionResult DropDown_Read(string Master, Int64 Depend_Id)
        {
            List<MailTemplate_Model> maillist = new List<MailTemplate_Model>();
            try
            {
                Int64 LoginUser_ID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                maillist = serviceObj.Get_MailTemplate_Details(Master, Depend_Id, LoginUser_ID);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(maillist, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Show_MailFieds(Int64 Type_Id)
        {
            try
            {
                ViewBag.MailFields = serviceObj.Get_MailTemplate_Details("MailFields", 0, 0);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return PartialView("MailFields_PartialView", ViewBag.MailFields);
        }

        [HttpPost]
        public ActionResult Save_MailTemplate(MailTemplate_Model objmail)
        {
            string Result = string.Empty;
            DataTable dt = new DataTable();
            try
            {
                objmail.logged_user_id = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = serviceObj.Save_MailTemplate(objmail);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete_MailTemplate(string Master, Int64 Depend_Id)
        {
            string Result = string.Empty;
            DataTable dt = new DataTable();
            try
            {
                Int64 LoginUser_ID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = serviceObj.Delete_MailTemplate(Master, Depend_Id, LoginUser_ID);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
    }
}