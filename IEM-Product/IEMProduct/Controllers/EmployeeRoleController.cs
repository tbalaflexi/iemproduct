﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Data;
using IEMProduct.Service;
using EOW.Service;
using DataAccessHandler.Models.IEMProduct;
using DataAccessHandler.Models.EOW;
using Newtonsoft.Json;

namespace IEMProduct.Controllers
{
    public class EmployeeRoleController : Controller
    {
        DataTable dt = new DataTable();
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(RoleEmployeeController));
        RoleEmployee_Model ObjModel = new RoleEmployee_Model();
        RoleEmployee_Service ObjService = new RoleEmployee_Service(); 
        // GET: EmployeeRole
        public ActionResult Index()
        {
            return View();
        } 
         
        public ActionResult Get_AllEmployee_List([DataSourceRequest]DataSourceRequest request, string Master = "")
        {
            try
            { 
                return Json(ObjService.Get_AllEmployee_List(Master).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }
        }
        public ActionResult Read_RoleByEmployee(RoleEmployee_Model ObjModel)
        {
            List<RoleEmployee_Model> lst = new List<RoleEmployee_Model>();
            try
            {
                lst = ObjService.ReadRoleByEmployee(ObjModel);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            var jsonResult = Json(lst, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        [HttpPost]
        public ActionResult Read_Role( string aaa = "0")
        {
            try
            {
                ViewBag.Role = ObjService.Read_Role();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return PartialView("EmployeeVsRole", ViewBag.Role);
        }

        public ActionResult Save_RoleToEmployee(RoleEmployee_Model ObjModel)
        {
            string Result = string.Empty;
            try
            {
                Int64 LoginUserId = Convert.ToInt64(Session["Employee_Gid"]);
                dt = ObjService.Save_RoleToEmployee(ObjModel, LoginUserId);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        
    }
}