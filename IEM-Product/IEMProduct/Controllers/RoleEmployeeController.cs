﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Data;
using IEMProduct.Service;
using EOW.Service;
using DataAccessHandler.Models.IEMProduct;
using DataAccessHandler.Models.EOW;
using Newtonsoft.Json;
namespace IEMProduct.Controllers
{
    public class RoleEmployeeController : Controller
    {
        DataTable dt = new DataTable();
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(RoleEmployeeController));
        RoleEmployee_Model ObjModel = new RoleEmployee_Model();
        RoleEmployee_Service ObjService = new RoleEmployee_Service();
        Claim_Service ObjClaimService = new Claim_Service();
        // GET: RoleEmployee
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read_Role([DataSourceRequest]DataSourceRequest request)
        {
            try
            {
                return Json(ObjService.ReadRecord().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }
           
        }
        public JsonResult Get_DropDown_List(string Master, string Master_Code, Int64 Master_Gid, string EmpName)
        {
            List<Claim_Model> lst = new List<Claim_Model>();
            try
            {
                //lst = objService.Get_DropDown_List(Master, Master_Code, Master_Gid);
                lst = ObjClaimService.Get_DropDown_List(Master, Master_Code, Master_Gid, EmpName);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            // return Json(lst, JsonRequestBehavior.AllowGet);
            var jsonResult = Json(lst, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public ActionResult Read_EmployeeRole([DataSourceRequest]DataSourceRequest request, string Role_Desc="")
        {
            try
            {
                RoleEmployee_Model ObjModel = new RoleEmployee_Model();
                ObjModel.Role = Role_Desc;
                return Json(ObjService.Read_EmployeeRole(ObjModel).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }

        }
        public ActionResult Remove_EmployeeRole(Int64 RoleEmployee_Gid = 0)
        {
            string Result = string.Empty;
            try
            {
                Int64 LoginUserId = Convert.ToInt64(Session["Employee_Gid"]);
                dt = ObjService.Remove_EmployeeRole(RoleEmployee_Gid,LoginUserId);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Save_EmployeeRole(RoleEmployee_Model ObjModel)
        {
            string Result = string.Empty;
            try
            {
                Int64 LoginUserId = Convert.ToInt64(Session["Employee_Gid"]);
                /*RoleEmployee_Model ObjModel = new RoleEmployee_Model();
                ObjModel.Role_Gid = Role_Gid;
                ObjModel.Employee_Gid = Employee_Gid;*/
                dt = ObjService.Save_EmployeeRole(ObjModel, LoginUserId);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Get_Employee_List(string EmployeeName)
        {
            List<RoleEmployee_Model> lst = new List<RoleEmployee_Model>();
            try
            {
                lst = ObjService.Get_Employee_List(EmployeeName);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            } 
            var jsonResult = Json(lst, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
    }
}