﻿using DBHelper;
using IEMProduct.Service;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using System.Globalization;

namespace IEMProduct.Controllers
{
    public class PayBankController : Controller
    {

        #region
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(TransactionController));        //Log For Net
        PayBank_Model ModelObject = new PayBank_Model();                                          // model object
        PayBank_Service ServiceObject = new PayBank_Service();                                    // Service Object
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        string result = "";
        #endregion


        // GET: Bank
        public ActionResult PayBank()
        {
            ViewBag.paybank_QCbank = "QCD_Bank";
            return View();
        }

        //PayBank Read

        public ActionResult ReadGrid([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                return Json(ServiceObject.ReadRecord().ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }

        }

        // PayBank Save
        public ActionResult SaveGrid(PayBank_Model ModelObject)
        {
            try
            {
                int fromdate = ModelObject.paybank_period_from.Length;
                int todate = ModelObject.paybank_period_to.Length;
                if (fromdate > 10)
                {

                    DateTime dtfrom = DateTime.ParseExact(ModelObject.paybank_period_from.Substring(0, 24), "ddd MMM dd yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    ModelObject.paybank_period_from = dtfrom.ToString("yyyy-MM-dd");
                }
                else
                {
                    DateTime myDate = DateTime.ParseExact(ModelObject.paybank_period_from, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ModelObject.paybank_period_from = myDate.ToString("yyyy-MM-dd");
                }

                if (todate > 10)
                {


                    DateTime dtto = DateTime.ParseExact(ModelObject.paybank_period_to.Substring(0, 24), "ddd MMM dd yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    ModelObject.paybank_period_to = dtto.ToString("yyyy-MM-dd");

                }
                else
                {
                    DateTime myDateto = DateTime.ParseExact(ModelObject.paybank_period_to, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ModelObject.paybank_period_to = myDateto.ToString("yyyy-MM-dd");
                }
               
               
                dt = ServiceObject.SaveRecord(ModelObject);
                ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.result);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return Json(ModelObject.result);
            }
        }

        //PayBank Update

        public ActionResult UpdateGrid(PayBank_Model ModelObject)
        {
            try
            {

                //if (ModelObject.paybank_period_from != "")
                //{
                //    DateTime PeriodFrom = Convert.ToDateTime(ModelObject.paybank_period_from);
                //    ModelObject.paybank_period_from =PeriodFrom.ToString("dd-MM-yyyy");
                //}

                //if (ModelObject.paybank_period_to  != "")
                //{
                //    DateTime PeriodTo = Convert.ToDateTime(ModelObject.paybank_period_to);
                //    ModelObject.paybank_period_from = PeriodTo.ToString("dd-MM-yyyy");
                //}
                //DateTime Fromdate = DateTime.Parse(ModelObject.paybank_period_from, CultureInfo.CreateSpecificCulture("fr-FR"));

                //if (ModelObject.paybank_period_from != "")
                //        {
                //            ModelObject.paybank_period_from = Convert.ToDateTime(ModelObject.paybank_period_from).ToString("dd-MM-yyyy");
                //        }

                int  fromdate = ModelObject.paybank_period_from.Length;
                int todate = ModelObject.paybank_period_to.Length;
                if (fromdate > 10)
                {

                    DateTime dtfrom = DateTime.ParseExact(ModelObject.paybank_period_from.Substring(0, 24), "ddd MMM dd yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    ModelObject.paybank_period_from = dtfrom.ToString("yyyy-MM-dd");
                }
                else
                {
                    DateTime myDate = DateTime.ParseExact(ModelObject.paybank_period_from, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ModelObject.paybank_period_from = myDate.ToString("yyyy-MM-dd"); 
                }
               
                if (todate>10)
                {
                    

                    DateTime dtto = DateTime.ParseExact(ModelObject.paybank_period_to.Substring(0, 24), "ddd MMM dd yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    ModelObject.paybank_period_to = dtto.ToString("yyyy-MM-dd");

                }
                else
                {
                    DateTime myDateto = DateTime.ParseExact(ModelObject.paybank_period_to, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ModelObject.paybank_period_to = myDateto.ToString("yyyy-MM-dd");
                }
               
               

                dt = ServiceObject.UpdateRecord(ModelObject);
                ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.result);

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return Json(ModelObject.result);

            }

        }

        //PayBank Delete

        public ActionResult DeleteGrid(PayBank_Model ModelObject)
        {
            try
            {
              dt = ServiceObject.DeleteRecord(ModelObject);
                if (dt.Rows.Count > 0)
                {
                    result = dt.Rows[0][0].ToString();
                }
                ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return Json(ModelObject.result);
            }

        }


        //bank code Dropdown
        public JsonResult GetBankCode(string parentcode)
        {
            List<PayBank_Model> trantype = new List<PayBank_Model>();
            try
            {
                trantype = ServiceObject.Getdropdown(parentcode);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(trantype, JsonRequestBehavior.AllowGet);
        }

        // QC Bank Branch Drop down
        public JsonResult GetBankBranch(string parentcode)
        {
            List<PayBank_Model> BankBranch = new List<PayBank_Model>();
            try
            {
                BankBranch = ServiceObject.Getdropdown(parentcode);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(BankBranch, JsonRequestBehavior.AllowGet);
        }


        //QC Bank IFSC
        public JsonResult GetBankIFSC(string parentcode)
        {
            List<PayBank_Model> BankIFSC = new List<PayBank_Model>();
            try
            {
                BankIFSC = ServiceObject.Getdropdown(parentcode);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(BankIFSC, JsonRequestBehavior.AllowGet);
        }

        //QC Bank GL NO
         
        public JsonResult GetBankGLNo(string parentcode)
        {
            List<PayBank_Model> BankGLNo = new List<PayBank_Model>();
            try
            {
                BankGLNo = ServiceObject.Getdropdown(parentcode);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(BankGLNo, JsonRequestBehavior.AllowGet);
        }


        //bank name
        public ActionResult Getbranch(string paybank_Depend_Code, string parentcode)
        {
            List<PayBank_Model> sub_TypeList = new List<PayBank_Model>();

            try
            {
                dt = ServiceObject.GetbankValues(paybank_Depend_Code, parentcode);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    PayBank_Model SubType = new PayBank_Model();
                    SubType.Depend_Code = Convert.ToInt64(dt.Rows[i][0].ToString());
                    SubType.bankname = dt.Rows[i][1].ToString();                    
                    SubType.paybank_bank_name = dt.Rows[i][2].ToString();
                    sub_TypeList.Add(SubType);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(sub_TypeList, JsonRequestBehavior.AllowGet);
        }

        //branch name

        public ActionResult GetbranchName(string paybank_Depend_Code, string parentcode)
        {
            List<PayBank_Model> branchlist = new List<PayBank_Model>();

            try
            {
                dt = ServiceObject.GetbranchValues(paybank_Depend_Code, parentcode);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    PayBank_Model SubType = new PayBank_Model();
                    SubType.paybankbranchid = dt.Rows[i][0].ToString();
                    SubType.paybank_branch_name = dt.Rows[i][1].ToString();

                    branchlist.Add(SubType);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(branchlist, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetIfscCode(string paybank_Depend_Code, string parentcode)
        {
            List<PayBank_Model> branchlist = new List<PayBank_Model>();

            try
            {
                dt = ServiceObject.GetbranchValues(paybank_Depend_Code, parentcode);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    PayBank_Model SubType = new PayBank_Model();
                    SubType.paybankifscid = dt.Rows[i][0].ToString();
                    SubType.paybank_ifsc_code = dt.Rows[i][1].ToString();

                    branchlist.Add(SubType);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(branchlist, JsonRequestBehavior.AllowGet);
        }

        
         
    }
}