﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using Kendo.Mvc.UI;
using System.Configuration;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using IEMProduct.Service;
using DBHelper;

namespace IEMProduct.Controllers
{
    public class QCMasterController : Controller
    {
        // GET: /Master/

        #region

        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(QCMasterController));  //Declaring Log4Net 
        MasterModel QcModelObject = new MasterModel();                                   //model object
        List<MasterModel> QcMasterList = new List<MasterModel>();                       //list model object
        QCMasterService QcServiceObject = new QCMasterService();                        //qc master service object
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        string result = "";

        #endregion

        // GET: QCMaster
        public ActionResult QCMaster()
        {
            return View();
        }

        //QC Master Read
        public ActionResult QCGridRead([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                return Json(QcServiceObject.ReadRecord().ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();

            }
        }

        //QC MasterDetails Read
        public ActionResult QCGridReadDetails([DataSourceRequest] DataSourceRequest request, string getMastercode)
        {
            try
            {
                MasterModel qcmodelobject = new MasterModel();
                 return Json(QcServiceObject.ReadRecordDetails(getMastercode).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return View();
        }

        //QC Master Insert
        public ActionResult QcMasterGridSave([DataSourceRequest] DataSourceRequest request, MasterModel qcmodelobject)
        {
            try
            {               
                dt = QcServiceObject.SaveRecord(qcmodelobject);
                qcmodelobject.result = JsonConvert.SerializeObject(dt);
                return Json(qcmodelobject.result);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                qcmodelobject.result = ex.Message.ToString();
                return Json(qcmodelobject.result);
            }
        }

        //QC Master Update
        public ActionResult QcMasterGridUpdate([DataSourceRequest] DataSourceRequest request, MasterModel qcmodelobject)
        {
            try
            {              
                dt = QcServiceObject.UpdateRecord(qcmodelobject);
                qcmodelobject.result = JsonConvert.SerializeObject(dt);
                return Json(qcmodelobject.result);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                qcmodelobject.result = ex.Message.ToString();
                return Json(qcmodelobject.result);
            }

        }

        // QC Master delete
        public ActionResult QcMasterGriddelete(MasterModel qcmodelobject)
        {
            try
            {
                dt = QcServiceObject.DeleteRecord(qcmodelobject);
                if (dt.Rows.Count > 0)
                {
                    result = dt.Rows[0][0].ToString();
                }
                qcmodelobject.result = JsonConvert.SerializeObject(dt);
                return Json(qcmodelobject.result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                qcmodelobject.result = ex.Message.ToString();
                return Json(qcmodelobject.result);
            }

        }

        //binding Parentcode dropdown
        public JsonResult GetParentCode()
        {
            List<MasterModel> Parentcode = new List<MasterModel>();
            try
            {
                Parentcode = QcServiceObject.GetParentCode();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Parentcode, JsonRequestBehavior.AllowGet);
        }

        //binding dependcode dropdown
        public JsonResult GetDependCode()
        {
            List<MasterModel> dependcode = new List<MasterModel>();
            try
            {
                dependcode = QcServiceObject.GetDependCode();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(dependcode, JsonRequestBehavior.AllowGet);
        }



    }
}