﻿using IEMProduct.Service;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using DBHelper;
using System.Configuration;
using System.IO;

namespace IEMProduct.Controllers
{
    public class AttachmentController : Controller
    {
        #region
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(AttachmentController));             //Declaring Log4Net 
        Attachment_Model ModelObject = new Attachment_Model();                                   // Attachment model object
        List<Attachment_Model> ModelList = new List<Attachment_Model>();                         //Attachment list model object
        Attachment_Service ServiceObject = new Attachment_Service();                            // Attachment master service object
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        string result = "";
        #endregion

        // GET: Attachment
        public ActionResult Attachment()
        {
            // ViewBag.result = "";
            return View();
        }

        #region Attachment
        // Attachment Read
        public ActionResult ReadGrid([DataSourceRequest] DataSourceRequest request,Attachment_Model ObjModel)
        {
            try
            {
                return Json(ServiceObject.ReadRecord(ObjModel).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }
        }

        //Attachment save
        [HttpPost]
        public JsonResult AttachmentGrid_Save(HttpPostedFileBase file, Attachment_Model ModelObject)//form save
        {

            try
            {
                if (ModelState.IsValid)
                {
                    Int64 Gid = 0;
                    string filenamenew = "";
                    ModelObject.attachment_filename = file.FileName;
                    ModelObject.Logger_UserId = Convert.ToInt64(Session["Employee_Gid"].ToString());
                    FileInfo fi = new FileInfo(file.FileName);
                    string fileextension = fi.Extension;

                    filenamenew = filenamenew + "" + fileextension;
                    dt = ServiceObject.SaveRecord(ModelObject);

                    //dt = ServiceObject.SaveRecord(ModelObject);
                    Gid = Convert.ToInt64(dt.Rows[0][2].ToString());
                    string a = Server.MapPath("~/Content/UploadedFiles");
                    string b = Path.GetFileName(Gid.ToString() + "_" + file.FileName);
                    string filepathextension = Path.Combine(Server.MapPath("~/Content/UploadedFiles"), Path.GetFileName(Gid.ToString() + "_" + file.FileName));
                    file.SaveAs(filepathextension);
                    ModelObject.result = JsonConvert.SerializeObject(dt);
                    ViewBag.result = "File Uploaded Successfully";
                    //return View("Attachment");
                    return Json(new { result = true });
                }
                else
                {
                    //return View("Attachment");
                    return Json(new { dataerror = true, errormsg = "This file format is not supported" });
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                result = "Form Save Process Failed";
            }
            return Json(result);
        }

        // Attachment Delete
        public ActionResult DeleteGrid(Attachment_Model ModelObject)
        {
            try
            {
                ModelObject.Logger_UserId = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = ServiceObject.DeleteRecord(ModelObject);
                if (dt.Rows.Count > 0)
                {
                    result = dt.Rows[0][0].ToString();
                }
                ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return Json(ModelObject.result);
            }

        }

        //Attachment DocGroup Dropdown Binding
        public JsonResult GetDocGroup(string parentcode)
        {
            List<Attachment_Model> Docgroup = new List<Attachment_Model>();
            try
            {
                Docgroup = ServiceObject.dropdownvalues(parentcode);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Docgroup, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public void SingleFile_onchange(HttpPostedFileBase file)
        {
            Session["Single"] = null;
            Session["Single"] = file;
        }

        #region get document informations.
        public FileResult Show_documents(Int64 id)
        {
            try
            {
                dt = ServiceObject.Get_Document_Info(Convert.ToInt64(id));
                ModelObject.result = JsonConvert.SerializeObject(dt);
                byte[] fileBytes = System.IO.File.ReadAllBytes(Path.Combine(Server.MapPath("~/Content/UploadedFiles"), Path.GetFileName(dt.Rows[0][0].ToString())));
                var response = new FileContentResult(fileBytes, "application/octet-stream");
                response.FileDownloadName = Path.Combine(Server.MapPath("~/Content/UploadedFiles"), Path.GetFileName(dt.Rows[0][0].ToString()));
                return response;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw ex;
            }
        }
        #endregion

        #endregion

        #region Audit Trail
        // Attachment Read
        public ActionResult Read_AuditTrail([DataSourceRequest]DataSourceRequest request, Int64 ECF_Gid = 0)
        {
            try
            { 
                return Json(ServiceObject.Read_AuditTrail(ECF_Gid).ToDataSourceResult(request), JsonRequestBehavior.AllowGet); 
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }
        }
        #endregion

        #region Batchwise attachments.
        public ActionResult GetBatchWiseAttachments([DataSourceRequest] DataSourceRequest request, Attachment_Model _ObjModel)
        {
            try
            {
                return Json(ServiceObject.GetAttachments(_ObjModel).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }
        }
        #endregion

    }
}