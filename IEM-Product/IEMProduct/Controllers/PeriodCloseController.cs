﻿using DataAccessHandler.Models.IEMProduct;
using IEMProduct.Service;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;

namespace IEMProduct.Controllers
{
    public class PeriodCloseController : Controller
    {
        #region

        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(TransactionController));             //Declaring Log4Net 
        PeriodClose_Model ModelObject = new PeriodClose_Model();                                   // PeriodClose model object
        List<PeriodClose_Model> ModelList = new List<PeriodClose_Model>();                         //PeriodClose list model object
        PeriodClose_Service ServiceObject = new PeriodClose_Service();                            // PeriodClose master service object
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        string result = "";


        #endregion
        // GET: PeriodClose
        public ActionResult PeriodClose()
        {
            return View();
        }

        //PeriodClose Read

        public ActionResult ReadGrid([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                return Json(ServiceObject.ReadRecord(User_GID).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }

        }

        //PeriodClose Insert
        [HttpPost]
        public ActionResult SaveGrid(PeriodClose_Model ModelObject)
        {
            try
            {
                Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                
                dt = ServiceObject.SaveRecord(ModelObject, User_GID);
                ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.result);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return Json(ModelObject.result);
            }
        }

        // PeriodClose update
        public ActionResult UpdateGrid(PeriodClose_Model ModelObject)
        {
            try
            {
                Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
              
                dt = ServiceObject.UpdateRecord(ModelObject, User_GID);
                ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.result);

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return Json(ModelObject.result);

            }

        }

        //Non Qc Master Dropdown Values
        //public ActionResult GetPeriodStatus(string flag)
        //{
        //    List<PeriodClose_Model> statuslist = new List<PeriodClose_Model>();
        //    try
        //    {
        //        dt = ServiceObject.dropdownvalues(flag);
        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            PeriodClose_Model periodstatus = new PeriodClose_Model();
        //           // periodstatus.id = Convert.ToInt64(dt.Rows[i][0].ToString());
        //           periodstatus.PeriodCloseStatus = dt.Rows[i][1].ToString();
        //            statuslist.Add(periodstatus);
        //        }
        //        var jsonResult = Json(statuslist, JsonRequestBehavior.AllowGet);
        //        jsonResult.MaxJsonLength = int.MaxValue;
        //        return jsonResult;
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex.ToString());
        //        return View();
        //    }

      //  }

        public JsonResult GetPeriodStatus(string flag)
        {
            List<PeriodClose_Model> dropdown = new List<PeriodClose_Model>();
            try
            {
                dropdown = ServiceObject.dropdownvalues(flag);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(dropdown, JsonRequestBehavior.AllowGet);
        }

    }
}