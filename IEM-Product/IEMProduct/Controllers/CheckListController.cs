﻿using DataAccessHandler.Models.IEMProduct;
using IEMProduct.Service;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Kendo.Mvc.Extensions;
using System.Web.Mvc;

namespace IEMProduct.Controllers
{
    public class CheckListController : Controller
    {

        #region

        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(TransactionController));             //Declaring Log4Net 
        Checklist_Model ModelObject = new Checklist_Model();                                   // Checklist model object
        List<Checklist_Model> ModelList = new List<Checklist_Model>();                         //Checklist list model object
        CheckList_Service ServiceObject = new CheckList_Service();                            // Checklist master service object
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        string result = "";
        string Tran_SubType = string.Empty;

        #endregion
        // GET: CheckList
        public ActionResult CheckList()
        {
            return View();
        }
        //Checklist Read

        public ActionResult ReadGrid([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                return Json(ServiceObject.ReadRecord().ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }

        }

        //Checklist Insert
    
        public ActionResult SaveGrid(Checklist_Model ModelObject, string _status = "0", Int64 Typename = 0, Int64 subtypename = 0)
        {
            try
            {
                
                    ModelObject.TranId = Typename;
                    ModelObject.TransubId = subtypename;
                    ModelObject.status = _status;
                    dt = ServiceObject.SaveRecord(ModelObject);
                    ModelObject.result = JsonConvert.SerializeObject(dt);
                    return Json(ModelObject.result);
                
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return Json(ModelObject.result);
            }
        }

        //  Checklist Update

        public ActionResult UpdateGrid(Checklist_Model ModelObject, string _status = "0", Int64 Typename = 0, Int64 subtypename = 0)
        {
            try
            {
                ModelObject.TranId = Typename;
                ModelObject.TransubId = subtypename;
                ModelObject.status = _status;
                dt = ServiceObject.UpdateRecord(ModelObject);
                ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.result);

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return Json(ModelObject.result);

            }

        }


        // Checklist delete

        public ActionResult DeleteGrid(Checklist_Model ModelObject)
        {
            try
            {
                
                dt = ServiceObject.DeleteRecord(ModelObject);
                if (dt.Rows.Count > 0)
                {
                    result = dt.Rows[0][0].ToString();
                }
                ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return Json(ModelObject.result);
            }

        }




        //binding   Type dropdown

        public JsonResult GetTranType(string parentcode)
        {
            List<Checklist_Model> trantype = new List<Checklist_Model>();
            try
            {
                trantype = ServiceObject.Getdropdown(parentcode);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(trantype, JsonRequestBehavior.AllowGet);
        }


        //binding    Sub Type dropdown

        public JsonResult GetTranSubType(string parentcode)
        {
            List<Checklist_Model> transubtype = new List<Checklist_Model>();
            try
            {
                transubtype = ServiceObject.GetTranSubType(parentcode);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(transubtype, JsonRequestBehavior.AllowGet);
        }

        //dropdown get values
      // [HttpPost]
        public ActionResult GetSubType(string TranId, string parentcode)
        {
            List<Checklist_Model> sub_TypeList = new List<Checklist_Model>();

            try
            {
                dt = ServiceObject.GetValues(TranId, parentcode);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Checklist_Model SubType = new Checklist_Model();
                    SubType.TransubId = Convert.ToInt64(dt.Rows[i][0].ToString());
                    SubType.Tran_SubType = dt.Rows[i][1].ToString();
                    sub_TypeList.Add(SubType);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(sub_TypeList, JsonRequestBehavior.AllowGet);
        }


        //Non QC Master DropDown Values

        public JsonResult dropdowns_values_binding(string flag)
        {
            List<Checklist_Model> dropdown = new List<Checklist_Model>();
            try
            {
                dropdown = ServiceObject.dropdownvalues(flag);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(dropdown, JsonRequestBehavior.AllowGet);
        }


    }
}