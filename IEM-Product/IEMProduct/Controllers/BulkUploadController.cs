﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using Excel;
using ClosedXML.Excel;
using System.IO;
using Newtonsoft.Json;
using DataAccessHandler.Models.IEMProduct;
using System.Threading.Tasks;
using IEMProduct.Service;
using DBHelper;
using EOW.Service;
using DataAccessHandler.Models.EOW;

namespace IEMProduct.Controllers
{
    public class BulkUploadController : Controller
    {
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(BulkUploadController));
        BulkUpload_Model ObjModel = new BulkUpload_Model();
        BulkUpload_Service ObjService = new BulkUpload_Service();
        SupplierInvoiceService serviceObj = new SupplierInvoiceService();
        Claim_Service objClaimService = new Claim_Service();
        // GET: BulkUpload
        public string UploadMethodName = "";

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult downloadsexcel()
        {
            DataTable dtnew = (DataTable)Session["Errdatatablesu"];
            string downloaderrorfileName = dtnew.TableName.ToString();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", downloaderrorfileName + ".xls"));
            Response.ContentType = "application/vnd.ms-excel";
            DataTable dt = dtnew;
            string str = string.Empty;
            foreach (DataColumn dtcol in dt.Columns)
            {
                Response.Write(str + dtcol.ColumnName);
                str = "\t";
            }
            Response.Write("\n");
            foreach (DataRow dr in dt.Rows)
            {
                str = "";
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    Response.Write(str + Convert.ToString(dr[j]));
                    str = "\t";
                }
                Response.Write("\n");
            }
            Response.End();
            return View();
        }

        [HttpPost]
        public void UploadFilesnew()
        {
            try
            {
                foreach (string filenew in Request.Files)
                {
                    var fileContent = Request.Files[filenew];

                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        HttpPostedFileBase hpf = Request.Files[filenew] as HttpPostedFileBase;
                        TempData["_supplierattmtfileexcel"] = hpf;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }

        public string FileUpload()
        {
            string x = "";
            try
            {
                x = System.Configuration.ConfigurationManager.AppSettings["FileUpload"].ToString();
            }
            catch { x = ""; }
            return (x == null || x.Trim() == "") ? "" : x;
        }

        [HttpPost]
        public async Task<JsonResult> UploadFiles()
        {
            List<SheetData> objparent = new List<SheetData>();
            string Extensionnew = "";
            string Extension1 = "";
            string error = "";
            try
            {
                string path1 = "";
                if (TempData["_supplierattmtfileexcel"] != null)
                {
                    //HttpPostedFileBase savefile = (HttpPostedFileBase)Session["supplierattmtfileexcel"];
                    HttpPostedFileBase savefile = TempData["_supplierattmtfileexcel"] as HttpPostedFileBase;
                    string Extension = Path.GetFileName(savefile.FileName);
                    string LoginUser_Gid = Session["Employee_Gid"].ToString();
                    string n = string.Format(LoginUser_Gid + "Local-{0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now);
                    Extension1 = System.IO.Path.GetExtension(savefile.FileName);
                    //path1 = objModel.HoldFileUploadUrl() + n + Extension;
                    Extensionnew = n + Extension1;
                    path1 = string.Format("{0}{1}", FileUpload(), Extensionnew);
                    if (System.IO.File.Exists(path1))
                    {
                        System.IO.File.Delete(path1);
                    }
                    savefile.SaveAs(path1);

                    SheetData objModel;
                    int count = 0;
                    string sheets = "";
                    FileStream stream = new FileStream(path1, FileMode.Open, FileAccess.Read);
                    DataSet result1 = new DataSet();
                    if (Extension1.ToLower() == ".xlsx")
                    {
                        IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                        excelReader.IsFirstRowAsColumnNames = true;
                        result1 = excelReader.AsDataSet();
                        excelReader.Close();
                    }
                    else if (Extension1.ToLower() == ".xls")
                    {
                        IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                        excelReader.IsFirstRowAsColumnNames = true;
                        result1 = excelReader.AsDataSet();
                        excelReader.Close();
                    }
                    else
                    {
                        error = "Error";
                        count++;
                        objModel = new SheetData();
                        objModel.SheetId = count;
                        objModel.SheetName = error.ToString();
                        objparent.Add(objModel);
                    }

                    if (result1 != null && result1.Tables.Count > 0)
                    {
                        foreach (DataTable dt in result1.Tables)
                        {
                            sheets = dt.TableName.ToString().Trim();
                            count++;
                            objModel = new SheetData();
                            objModel.SheetId = count;
                            objModel.SheetName = sheets.ToString();
                            objparent.Add(objModel);
                        }
                    }

                    Session["Tempdataset"] = result1;
                    TempData.Remove("_supplierattmtfileexcel");
                }
                return Json(objparent.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return Json("Error occurred..." + ex.Message.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult Localconveyancestatuserrs(BulkUpload_Model objmodel)
        {
            string msg = "";
            string strCols = "";
            string[] strColArr;
            string ExcelResult = "";
            try
            {
                string datatableName = objmodel.UploadMethod.ToString();
                DataSet result1 = new DataSet();
                result1 = (DataSet)Session["Tempdataset"];
                var dataTable = new DataTable();
                dataTable = result1.Tables[0];
                Session["Tempdatatablesu"] = dataTable;

                foreach (DataColumn dtColumn in dataTable.Columns)
                {
                    strCols = strCols + dtColumn.ColumnName.Trim() + ":";
                }
                strCols = strCols.Substring(0, strCols.Length - 1);
                strColArr = strCols.Split(':');
                if (datatableName.ToString() == "SupplierDebitline")
                {
                    if (strColArr[0].ToString() == "SNo"
                        && strColArr[1].ToString() == "Reference No"
                        && strColArr[2].ToString() == "Nature of Expenses"
                        && strColArr[3].ToString() == "Main Category"
                        && strColArr[4].ToString() == "Sub Category"
                        && strColArr[5].ToString() == "Description"
                        //&& strColArr[6].ToString() == "Function Code"
                        //&& strColArr[7].ToString() == "Cost Code"
                        //&& strColArr[8].ToString() == "Product Code"
                        //&& strColArr[9].ToString() == "OU Code"
                        && strColArr[6].ToString() == "Amount"
                        && strColArr[7].ToString() == "hsncode"
                        && strColArr.Count().ToString() == "8")
                    {
                        if (Session["Tempdatatablesu"] != null)
                        {
                            DataTable errdataval = new DataTable();
                            errdataval = (DataTable)Session["Tempdatatablesu"];
                            ExcelResult = datasupload(errdataval, datatableName);
                            errdataval = (DataTable)Session["Errdatatablesu"];

                            msg = "Success";
                        }
                    }
                    else
                    {
                        msg = "Faild";
                        ExcelResult = "";
                    }
                }
                else if (datatableName.ToString() == "SupplierInvoice")
                {
                    if (strColArr[0].ToString() == "SNo"
                        && strColArr[1].ToString() == "Invoice No"
                        && strColArr[2].ToString() == "Supplier Code"
                        && strColArr[3].ToString() == "Supplier Name"
                        && strColArr[4].ToString() == "Invoice Date"
                        && strColArr[5].ToString() == "Provider Location"
                        && strColArr[6].ToString() == "Receiver Location"
                        && strColArr[7].ToString() == "Invoice Amount"
                        && strColArr[8].ToString() == "Tax Type"
                        && strColArr[9].ToString() == "Descrition"
                        && strColArr.Count().ToString() == "10")
                    {
                        if (Session["Tempdatatablesu"] != null)
                        {
                            DataTable errdataval = new DataTable();
                            errdataval = (DataTable)Session["Tempdatatablesu"];
                            //ExcelResult = datasupload(errdataval);
                            ExcelResult = datasupload_SupplierInvoice(errdataval, datatableName);
                            errdataval = (DataTable)Session["Errdatatablesu"];

                            msg = "Success";

                        }
                    }
                    else
                    {
                        msg = "Faild";
                        ExcelResult = "";
                    }
                }
                if (datatableName.ToString() == "EmployeeECF_Others")
                {
                    if (strColArr[0].ToString() == "S No"
                        && strColArr[1].ToString() == "Raiser Code"
                        && strColArr[2].ToString() == "Raiser Name"
                        && strColArr[3].ToString() == "Claim Date"
                        && strColArr[4].ToString() == "Claim Amount"
                        && strColArr[5].ToString() == "Claim Description"
                        && strColArr.Count().ToString() == "6")
                    {
                        if (Session["Tempdatatablesu"] != null)
                        {
                            DataTable errdataval = new DataTable();
                            errdataval = (DataTable)Session["Tempdatatablesu"];
                            ExcelResult = datasupload_EmployeeECF(errdataval, datatableName);
                            errdataval = (DataTable)Session["Errdatatablesu"];

                            msg = "Success";
                        }
                    }
                    else
                    {
                        msg = "Faild";
                        ExcelResult = "";
                    }
                }
                if (datatableName.ToString() == "EmployeeECF_Travel")
                {
                    if (strColArr[0].ToString() == "S No"
                        && strColArr[1].ToString() == "Raiser Code"
                        && strColArr[2].ToString() == "Raiser Name"
                        && strColArr[3].ToString() == "Claim Date"
                        && strColArr[4].ToString() == "Claim Amount"
                        && strColArr[5].ToString() == "Claim Description"
                        && strColArr[6].ToString() == "Additional Employee Codes"
                        && strColArr.Count().ToString() == "7")
                    {
                        if (Session["Tempdatatablesu"] != null)
                        {
                            DataTable errdataval = new DataTable();
                            errdataval = (DataTable)Session["Tempdatatablesu"];
                            ExcelResult = datasupload_EmployeeECF(errdataval, datatableName);
                            errdataval = (DataTable)Session["Errdatatablesu"];

                            msg = "Success";
                        }
                    }
                    else
                    {
                        msg = "Faild";
                        ExcelResult = "";
                    }
                }
                if (datatableName.ToString() == "EmployeeDebitline")
                {
                    if (strColArr[0].ToString() == "SNo"
                        && strColArr[1].ToString() == "Reference No"
                        && strColArr[2].ToString() == "Nature of Expenses"
                        && strColArr[3].ToString() == "Main Category"
                        && strColArr[4].ToString() == "Sub Category"
                        && strColArr[5].ToString() == "Tax Type"
                        && strColArr[6].ToString() == "Amount"
                        && strColArr[7].ToString() == "hsncode"
                        && strColArr.Count().ToString() == "8")
                    {
                        if (Session["Tempdatatablesu"] != null)
                        {
                            DataTable errdataval = new DataTable();
                            errdataval = (DataTable)Session["Tempdatatablesu"];
                            ExcelResult = datasupload_EmployeeDebitline(errdataval, datatableName);
                            errdataval = (DataTable)Session["Errdatatablesu"];

                            msg = "Success";
                        }
                    }
                    else
                    {
                        msg = "Faild";
                        ExcelResult = "";
                    }
                }
                if (datatableName.ToString() == "EmployeeCOA" || datatableName.ToString() == "SupplierCOA")
                {
                    if (strColArr[0].ToString() == "S No"
                        && strColArr[1].ToString() == "Reference No"
                        && strColArr[2].ToString() == "GL Code"
                        && strColArr[3].ToString() == "Function Code"
                        && strColArr[4].ToString() == "CC Code"
                        && strColArr[5].ToString() == "Product Code"
                        && strColArr[6].ToString() == "OU Code"
                        && strColArr[7].ToString() == "Amount"
                        && strColArr.Count().ToString() == "8")
                    {
                        if (Session["Tempdatatablesu"] != null)
                        {
                            DataTable errdataval = new DataTable();
                            errdataval = (DataTable)Session["Tempdatatablesu"];
                            ExcelResult = datasupload_EmployeeCOA(errdataval, datatableName);
                            errdataval = (DataTable)Session["Errdatatablesu"];

                            msg = "Success";
                        }
                    }
                    else
                    {
                        msg = "Faild";
                        ExcelResult = "";
                    }
                }
                if (datatableName.ToString() == "EmployeeConveyance")
                {
                    if (strColArr[0].ToString() == "S No"
                        && strColArr[1].ToString() == "Reference No"
                        && strColArr[2].ToString() == "Employee Code"
                        && strColArr[3].ToString() == "Employee Name"
                        && strColArr[4].ToString() == "Transport Type"
                        && strColArr[5].ToString() == "Date"
                        && strColArr[6].ToString() == "Location From"
                        && strColArr[7].ToString() == "Location To"
                        && strColArr[8].ToString() == "Km"
                        && strColArr[9].ToString() == "Rate"
                        && strColArr[10].ToString() == "Amount"
                        && strColArr.Count().ToString() == "11")
                    {
                        if (Session["Tempdatatablesu"] != null)
                        {
                            DataTable errdataval = new DataTable();
                            errdataval = (DataTable)Session["Tempdatatablesu"];
                            ExcelResult = datasupload_LocalConveyance(errdataval, datatableName);
                            errdataval = (DataTable)Session["Errdatatablesu"];

                            msg = "Success";
                        }
                    }
                    else
                    {
                        msg = "Faild";
                        ExcelResult = "";
                    }
                }
                return Json(new { msg, ExcelResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return Json("Error occurred..." + ex.Message.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        private string datasupload(DataTable dataTable, string datatableName)
        {
            int sno = 0;
            int totalrecord = 0;
            int invalid = 0;
            int valid = 0;
            string Result = "";
            DataTable maindatatable = new DataTable();
            maindatatable = dataTable;
            totalrecord = maindatatable.Rows.Count;
            DataTable Errdatatable = new DataTable();
            Errdatatable.Columns.Add("SNo");
            Errdatatable.Columns.Add("Line");
            Errdatatable.Columns.Add("Error Description");
            try
            {
                if (dataTable.Rows.Count > 0)
                {
                    string exceltext = "";
                    string status = "";
                    string errs = "";
                    int RowNo = 0;
                    decimal vales = 0;
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        if (dataTable.Rows[i][0].ToString() != "")
                        {
                            errs = "";
                            RowNo = i + 1;
                            if (dataTable.Rows[i][1].ToString() == "")
                            {
                                errs = "Reference No Should Not be Empty ";
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][1].ToString();

                                status = ObjService.ValidateExcelMasterData(exceltext, "", "ReferenceNo");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Nature of Expenses Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Nature of Expenses Was Not Found ";
                                    }
                                }
                            }
                            if (dataTable.Rows[i][2].ToString() == "")
                            {
                                errs = "Nature of Expenses Should Not be Empty ";
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][2].ToString();

                                status = ObjService.ValidateExcelMasterData(exceltext, "", "NatureofExpenses");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Nature of Expenses Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Nature of Expenses Was Not Found ";
                                    }
                                }
                            }

                            if (dataTable.Rows[i][3].ToString() == "")
                            {

                                if (errs == "")
                                {
                                    errs = "Main Category Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Main Category Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][3].ToString();
                                string expnat = dataTable.Rows[i][2].ToString();
                                status = ObjService.ValidateExcelMasterData(exceltext, expnat, "MainCategorycode");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Main Category Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Main Category Was Not Found ";
                                    }
                                }

                            }
                            if (dataTable.Rows[i][4].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Sub Category Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Sub Category Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][4].ToString();
                                string ExpCat = dataTable.Rows[i][3].ToString();
                                status = ObjService.ValidateExcelMasterData(exceltext, ExpCat, "SubCategorycode");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Sub Category Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Sub Category Was Not Found ";
                                    }
                                }
                            }
                            if (dataTable.Rows[i][6].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Amount Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Amount Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][6].ToString();

                                vales = Convert.ToDecimal(exceltext.ToString().Trim());
                                if (vales <= 0)
                                {
                                    if (errs == "")
                                    {
                                        errs = "Amount Should Not be Less Then Zero";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Amount Should Not be Less Then Zero";
                                    }
                                }
                            }

                            if (dataTable.Rows[i][7].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "HSN Code Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "HSN Code Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][7].ToString();

                                status = ObjService.ValidateExcelMasterData(exceltext, "", "HSNCode");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "HSN Code Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "HSN Code Was Not Found ";
                                    }
                                }
                            }


                            /*
                            if (dataTable.Rows[i][6].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Function Code Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Function Code Should Not be Empty";
                                }
                            }
                            /* else
                             {
                                 if (dataTable.Rows[i][9].ToString() != "")
                                 {
                                     exceltext = dataTable.Rows[i][6].ToString();
                                     // dataTable.Rows[i][9].ToString() == ""
                                     string exceloucode = dataTable.Rows[i][9].ToString();
                                     //string login = objCmnFunctions.GetLoginUserGid().ToString();
                                     status = ObjService.ValidateExcelMasterData(exceltext,"", "FunctionCodewithou");
                                     if (status == "notexists")
                                     {
                                         if (errs == "")
                                         {
                                             errs = "Function Code Not match with the OU code  ";
                                         }
                                         else
                                         {
                                             errs = errs + "," + "Function Code Not match with the OU code ";
                                         }
                                     }
                                 }
                                 else
                                 {
                                     errs = "OU Code Was Not Found ";
                                 }

                             }
                            if (dataTable.Rows[i][7].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Cost Code Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Cost Code Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][7].ToString();

                                status = ObjService.ValidateExcelMasterData(exceltext, "", "CostCode");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Cost Code Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Cost Code Was Not Found ";
                                    }
                                }
                            }
                            /* if (dataTable.Rows[i][7].ToString() != "" && dataTable.Rows[i][6].ToString() != "")
                             {
                                 status = ObjService.ValidateExcelMasterData(dataTable.Rows[i][7].ToString(), dataTable.Rows[i][6].ToString());
                                 if (status == "notexists")
                                 {
                                     if (errs == "")
                                     {
                                         errs = "Invalid Function Code and Cost Code";
                                     }
                                     else
                                     {
                                         errs = errs + "," + "Invalid Function Code and Cost Code";
                                     }
                                 }
                             }
                            if (dataTable.Rows[i][8].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Product Code Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Product Code Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][8].ToString();

                                status = ObjService.ValidateExcelMasterData(exceltext, "", "ProductCode");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Product Code Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Product Code Was Not Found ";
                                    }
                                }
                            }
                            if (dataTable.Rows[i][9].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "OU Code Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "OU Code Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][9].ToString();

                                status = ObjService.ValidateExcelMasterData(exceltext, "", "OUCode");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "OU Code Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "OU Code Was Not Found ";
                                    }
                                }
                            } 

                            */

                            if (errs != "")
                            {
                                sno++;
                                Errdatatable.Rows.Add(sno, RowNo, errs);
                            }
                        }
                    }
                }
                else
                {
                    Errdatatable.Rows.Add(1, "Please  Select Valid Sheet");
                }
                invalid = Errdatatable.Rows.Count;
                valid = totalrecord - invalid;

                Result = "Total No of Vaild Record :" + valid + " - ";
                Result = Result + "Total No of InVaild Record :" + invalid + " - ";
                Result = Result + "Total No of Record Excel File :" + totalrecord;

                Errdatatable.TableName = datatableName;
                maindatatable.TableName = datatableName;

                Session["Errdatatablesu"] = Errdatatable;
                Session["maindatatablesu"] = maindatatable;
            }
            catch (Exception ex)
            {
                sno++;
                Errdatatable.Rows.Add(sno, Errdatatable.Rows.Count + 1, ex.Message.ToString() + " Please ckeck Excel File Error While Reading Data" + ex.ToString());

                invalid = Errdatatable.Rows.Count;
                valid = totalrecord - invalid;
                //ViewBag.vbvalid = "Total No of Vaild Record :" + valid;
                //ViewBag.vbinvalid = "Total No of InVaild Record :" + invalid;
                //ViewBag.vbtotalrecord = "Total No of Record Excel File :" + totalrecord;
                Result = "Total No of Vaild Record :" + valid + " - ";
                Result = Result + "Total No of InVaild Record :" + invalid + " - ";
                Result = Result + "Total No of Record Excel File :" + totalrecord;

                Session["Errdatatablesu"] = Errdatatable;
                Session["maindatatablesu"] = maindatatable;
                logger.Error(ex.ToString());
            }

            return Result;
        }

        private string datasupload_EmployeeDebitline(DataTable dataTable, string datatableName)
        {
            int sno = 0;
            int totalrecord = 0;
            int invalid = 0;
            int valid = 0;
            string Result = "";
            DataTable maindatatable = new DataTable();
            maindatatable = dataTable;
            totalrecord = maindatatable.Rows.Count;
            DataTable Errdatatable = new DataTable();
            Errdatatable.Columns.Add("SNo");
            Errdatatable.Columns.Add("Line");
            Errdatatable.Columns.Add("Error Description");
            try
            {
                if (dataTable.Rows.Count > 0)
                {
                    string exceltext = "";
                    string status = "";
                    string errs = "";
                    int RowNo = 0;
                    decimal vales = 0;
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        if (dataTable.Rows[i][0].ToString() != "")
                        {
                            errs = "";
                            RowNo = i + 1;
                            if (dataTable.Rows[i][1].ToString() == "")
                            {
                                errs = "Reference No Should Not be Empty ";
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][1].ToString();

                                status = ObjService.ValidateExcelMasterData(exceltext, "", "ReferenceNo");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Reference No Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Reference No Was Not Found ";
                                    }
                                }
                            }
                            if (dataTable.Rows[i][2].ToString() == "")
                            {
                                errs = "Nature of Expenses Should Not be Empty ";
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][2].ToString();

                                status = ObjService.ValidateExcelMasterData(exceltext, "", "NatureofExpenses");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Nature of Expenses Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Nature of Expenses Was Not Found ";
                                    }
                                }
                            }

                            if (dataTable.Rows[i][3].ToString() == "")
                            {

                                if (errs == "")
                                {
                                    errs = "Main Category Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Main Category Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][3].ToString();
                                string expnat = dataTable.Rows[i][2].ToString();
                                status = ObjService.ValidateExcelMasterData(exceltext, expnat, "MainCategorycode");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Main Category Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Main Category Was Not Found ";
                                    }
                                }

                            }
                            if (dataTable.Rows[i][4].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Sub Category Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Sub Category Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][4].ToString();
                                string ExpCat = dataTable.Rows[i][3].ToString();
                                status = ObjService.ValidateExcelMasterData(exceltext, ExpCat, "SubCategorycode");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Sub Category Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Sub Category Was Not Found ";
                                    }
                                }
                            }
                            if (dataTable.Rows[i][5].ToString() == "")
                            {
                                errs = "Tax Type Should Not be Empty ";
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][5].ToString();

                                if (exceltext != "GST" && exceltext != "RCM" && exceltext == "None")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Invalid Tax Type ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Invalid Tax Type ";
                                    }
                                }
                            }

                            if (dataTable.Rows[i][6].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Amount Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Amount Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][6].ToString();

                                vales = Convert.ToDecimal(exceltext.ToString().Trim());
                                if (vales <= 0)
                                {
                                    if (errs == "")
                                    {
                                        errs = "Amount Should Not be Less Then Zero";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Amount Should Not be Less Then Zero";
                                    }
                                }
                            }

                            if (dataTable.Rows[i][7].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "HSN Code Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "HSN Code Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][7].ToString();

                                status = ObjService.ValidateExcelMasterData(exceltext, "", "HSNCode");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "HSN Code Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "HSN Code Was Not Found ";
                                    }
                                }
                            }


                            if (errs != "")
                            {
                                sno++;
                                Errdatatable.Rows.Add(sno, RowNo, errs);
                            }
                        }
                    }
                }
                else
                {
                    Errdatatable.Rows.Add(1, "Please  Select Valid Sheet");
                }
                invalid = Errdatatable.Rows.Count;
                valid = totalrecord - invalid;

                Result = "Total No of Vaild Record :" + valid + " - ";
                Result = Result + "Total No of InVaild Record :" + invalid + " - ";
                Result = Result + "Total No of Record Excel File :" + totalrecord;

                Errdatatable.TableName = datatableName;
                maindatatable.TableName = datatableName;

                Session["Errdatatablesu"] = Errdatatable;
                Session["maindatatablesu"] = maindatatable;
            }
            catch (Exception ex)
            {
                sno++;
                Errdatatable.Rows.Add(sno, Errdatatable.Rows.Count + 1, ex.Message.ToString() + " Please ckeck Excel File Error While Reading Data" + ex.ToString());

                invalid = Errdatatable.Rows.Count;
                valid = totalrecord - invalid;
                //ViewBag.vbvalid = "Total No of Vaild Record :" + valid;
                //ViewBag.vbinvalid = "Total No of InVaild Record :" + invalid;
                //ViewBag.vbtotalrecord = "Total No of Record Excel File :" + totalrecord;
                Result = "Total No of Vaild Record :" + valid + " - ";
                Result = Result + "Total No of InVaild Record :" + invalid + " - ";
                Result = Result + "Total No of Record Excel File :" + totalrecord;

                Session["Errdatatablesu"] = Errdatatable;
                Session["maindatatablesu"] = maindatatable;
                logger.Error(ex.ToString());
            }

            return Result;
        }

        private string datasupload_SupplierInvoice(DataTable dataTable, string datatableName)
        {
            int sno = 0;
            int totalrecord = 0;
            int invalid = 0;
            int valid = 0;
            string Result = "";
            DataTable maindatatable = new DataTable();
            maindatatable = dataTable;
            totalrecord = maindatatable.Rows.Count;
            DataTable Errdatatable = new DataTable();
            Errdatatable.Columns.Add("SNo");
            Errdatatable.Columns.Add("Line");
            Errdatatable.Columns.Add("Error Description");
            try
            {
                if (dataTable.Rows.Count > 0)
                {
                    string exceltext = "";
                    string status = "";
                    string errs = "";
                    int RowNo = 0;
                    decimal vales = 0;
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        if (dataTable.Rows[i][0].ToString() != "")
                        {
                            errs = "";
                            RowNo = i + 1;
                            if (dataTable.Rows[i][1].ToString() == "")
                            {
                                errs = "Invoice No Should Not be Empty ";
                            }
                            if (dataTable.Rows[i][2].ToString() == "")
                            {
                                errs = "Supplier Code Should Not be Empty ";
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][2].ToString();

                                status = ObjService.ValidateExcelMasterData(exceltext, "", "SupplierCode");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Supplier Code Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Supplier Code Was Not Found ";
                                    }
                                }
                            }

                            if (dataTable.Rows[i][3].ToString() == "")
                            {

                                if (errs == "")
                                {
                                    errs = "Supplier Name Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Supplier Name Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][3].ToString();
                                string Suppliercode = dataTable.Rows[i][2].ToString();
                                status = ObjService.ValidateExcelMasterData(exceltext, Suppliercode, "SupplierName");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Supplier Name Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Supplier Name Was Not Found ";
                                    }
                                }

                            }
                            if (dataTable.Rows[i][4].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Invoice Date Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Invoice Date Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][4].ToString();
                                //check valid format or not
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Invalid Invoice Date ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Invalid Invoice Date ";
                                    }
                                }
                            }
                            if (dataTable.Rows[i][5].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Provider Location Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Provider Location Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][5].ToString();
                                string Suppliercode = dataTable.Rows[i][2].ToString();

                                status = ObjService.ValidateExcelMasterData(exceltext, Suppliercode, "ProviderLocation");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Provider Location Should Not be Less Then Zero";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Provider Location Should Not be Less Then Zero";
                                    }
                                }
                            }

                            if (dataTable.Rows[i][6].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Receiver Location Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Receiver Location Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][6].ToString();

                                status = ObjService.ValidateExcelMasterData(exceltext, "", "ReceiverLocation");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Receiver Location Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Receiver Location Was Not Found ";
                                    }
                                }
                            }
                            if (dataTable.Rows[i][7].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Invoice Amount Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Invoice Amount Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][7].ToString();
                                vales = Convert.ToDecimal(exceltext.ToString().Trim());
                                if (vales <= 0)
                                {
                                    if (errs == "")
                                    {
                                        errs = "Invoice Amount Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Invoice Amount Was Not Found ";
                                    }
                                }
                            }
                            if (dataTable.Rows[i][8].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Tax Type Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Tax Type Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][8].ToString();
                                if (!exceltext.Equals("GST") && !exceltext.Equals("RCM") && !exceltext.Equals("NONE"))
                                {
                                    if (errs == "")
                                    {
                                        errs = "Invalid Tax Type ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Invalid Tax Type ";
                                    }
                                }
                            }
                            if (errs != "")
                            {
                                sno++;
                                Errdatatable.Rows.Add(sno, RowNo, errs);
                            }
                        }
                    }
                }
                else
                {
                    Errdatatable.Rows.Add(1, "Please  Select Valid Sheet");
                }
                invalid = Errdatatable.Rows.Count;
                valid = totalrecord - invalid;

                Result = "Total No of Vaild Record :" + valid + " - ";
                Result = Result + "Total No of InVaild Record :" + invalid + " - ";
                Result = Result + "Total No of Record Excel File :" + totalrecord;

                Errdatatable.TableName = datatableName;
                maindatatable.TableName = datatableName;

                Session["Errdatatablesu"] = Errdatatable;
                Session["maindatatablesu"] = maindatatable;
            }
            catch (Exception ex)
            {
                sno++;
                Errdatatable.Rows.Add(sno, Errdatatable.Rows.Count + 1, ex.Message.ToString() + " Please ckeck Excel File Error While Reading Data" + ex.ToString());

                invalid = Errdatatable.Rows.Count;
                valid = totalrecord - invalid;
                //ViewBag.vbvalid = "Total No of Vaild Record :" + valid;
                //ViewBag.vbinvalid = "Total No of InVaild Record :" + invalid;
                //ViewBag.vbtotalrecord = "Total No of Record Excel File :" + totalrecord;
                Result = "Total No of Vaild Record :" + valid + " - ";
                Result = Result + "Total No of InVaild Record :" + invalid + " - ";
                Result = Result + "Total No of Record Excel File :" + totalrecord;

                Session["Errdatatablesu"] = Errdatatable;
                Session["maindatatablesu"] = maindatatable;
                logger.Error(ex.ToString());
            }

            return Result;
        }

        private string datasupload_EmployeeECF(DataTable dataTable, string datatableName)
        {
            int sno = 0;
            int totalrecord = 0;
            int invalid = 0;
            int valid = 0;
            string Result = "";
            DataTable maindatatable = new DataTable();
            maindatatable = dataTable;
            totalrecord = maindatatable.Rows.Count;
            DataTable Errdatatable = new DataTable();
            Errdatatable.Columns.Add("SNo");
            Errdatatable.Columns.Add("Line");
            Errdatatable.Columns.Add("Error Description");
            try
            {
                if (dataTable.Rows.Count > 0)
                {
                    string exceltext = "";
                    string status = "";
                    string errs = "";
                    int RowNo = 0;
                    decimal vales = 0;
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        if (dataTable.Rows[i][0].ToString() != "")
                        {
                            errs = "";
                            RowNo = i + 1;
                            if (dataTable.Rows[i][1].ToString() == "")
                            {
                                errs = "Raiser Code Should Not be Empty ";
                            }
                            if (dataTable.Rows[i][2].ToString() == "")
                            {
                                errs = "Employee Name Should Not be Empty ";
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][2].ToString();
                                string employeecode = dataTable.Rows[i][1].ToString();
                                status = ObjService.ValidateExcelMasterData(exceltext, employeecode, "EmployeeCodeName");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Employee Code/Name Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Employee Code/Name Was Not Found ";
                                    }
                                }
                            }
                            if (dataTable.Rows[i][3].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Claim Date Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Claim Date Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][3].ToString();
                                //check valid format or not
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Invalid Claim Date ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Invalid Claim Date ";
                                    }
                                }
                            }
                            if (dataTable.Rows[i][4].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Claim Amount Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Claim Amount Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][4].ToString();
                                vales = Convert.ToDecimal(exceltext.ToString().Trim());
                                if (vales <= 0)
                                {
                                    if (errs == "")
                                    {
                                        errs = "Claim Amount Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Claim Amount Was Not Found ";
                                    }
                                }
                            }
                            if (dataTable.Rows[i][5].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Claim Description Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Claim Description Should Not be Empty";
                                }
                            }

                            if (errs != "")
                            {
                                sno++;
                                Errdatatable.Rows.Add(sno, RowNo, errs);
                            }
                        }
                    }
                }
                else
                {
                    Errdatatable.Rows.Add(1, "Please  Select Valid Sheet");
                }
                invalid = Errdatatable.Rows.Count;
                valid = totalrecord - invalid;

                Result = "Total No of Vaild Record :" + valid + " - ";
                Result = Result + "Total No of InVaild Record :" + invalid + " - ";
                Result = Result + "Total No of Record Excel File :" + totalrecord;

                Errdatatable.TableName = datatableName;
                maindatatable.TableName = datatableName;

                Session["Errdatatablesu"] = Errdatatable;
                Session["maindatatablesu"] = maindatatable;
            }
            catch (Exception ex)
            {
                sno++;
                Errdatatable.Rows.Add(sno, Errdatatable.Rows.Count + 1, ex.Message.ToString() + " Please ckeck Excel File Error While Reading Data" + ex.ToString());

                invalid = Errdatatable.Rows.Count;
                valid = totalrecord - invalid;
                //ViewBag.vbvalid = "Total No of Vaild Record :" + valid;
                //ViewBag.vbinvalid = "Total No of InVaild Record :" + invalid;
                //ViewBag.vbtotalrecord = "Total No of Record Excel File :" + totalrecord;
                Result = "Total No of Vaild Record :" + valid + " - ";
                Result = Result + "Total No of InVaild Record :" + invalid + " - ";
                Result = Result + "Total No of Record Excel File :" + totalrecord;

                Session["Errdatatablesu"] = Errdatatable;
                Session["maindatatablesu"] = maindatatable;
                logger.Error(ex.ToString());
            }

            return Result;
        }

        private string datasupload_EmployeeCOA(DataTable dataTable, string datatableName)
        {
            int sno = 0;
            int totalrecord = 0;
            int invalid = 0;
            int valid = 0;
            string Result = "";
            DataTable maindatatable = new DataTable();
            maindatatable = dataTable;
            totalrecord = maindatatable.Rows.Count;
            DataTable Errdatatable = new DataTable();
            Errdatatable.Columns.Add("SNo");
            Errdatatable.Columns.Add("Line");
            Errdatatable.Columns.Add("Error Description");
            try
            {
                if (dataTable.Rows.Count > 0)
                {
                    string exceltext = "";
                    string status = "";
                    string errs = "";
                    int RowNo = 0;
                    decimal vales = 0;
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        if (dataTable.Rows[i][0].ToString() != "")
                        {
                            errs = "";
                            RowNo = i + 1;
                            if (dataTable.Rows[i][1].ToString() == "")
                            {
                                errs = "Reference No Should Not be Empty ";
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][1].ToString();

                                status = ObjService.ValidateExcelMasterData(exceltext, "", "ReferenceNo");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Reference No Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Reference No Was Not Found ";
                                    }
                                }
                            }
                            if (dataTable.Rows[i][2].ToString() == "")
                            {
                                errs = "GL Number Should Not be Empty ";
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][2].ToString();
                                string ecfno = dataTable.Rows[i][1].ToString();
                                status = ObjService.ValidateExcelMasterData(exceltext, ecfno, "GLNo");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "GL Number Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "GL Number Was Not Found ";
                                    }
                                }
                            }
                            if (dataTable.Rows[i][3].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Function Code Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Function Code Should Not be Empty";
                                }
                            }
                            else
                            {
                                if (dataTable.Rows[i][3].ToString() != "")
                                {
                                    exceltext = dataTable.Rows[i][3].ToString();
                                    string exceloucode = dataTable.Rows[i][6].ToString();
                                    status = ObjService.ValidateExcelMasterData(exceltext, "", "FCCode");
                                    if (status == "notexists")
                                    {
                                        if (errs == "")
                                        {
                                            errs = "Function Code Not match with the OU code  ";
                                        }
                                        else
                                        {
                                            errs = errs + "," + "Function Code Not match with the OU code ";
                                        }
                                    }
                                }
                                else
                                {
                                    errs = "OU Code Was Not Found ";
                                }

                            }
                            if (dataTable.Rows[i][4].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Cost Code Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Cost Code Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][4].ToString();

                                status = ObjService.ValidateExcelMasterData(exceltext, "", "CostCode");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Cost Code Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Cost Code Was Not Found ";
                                    }
                                }
                            }

                            if (dataTable.Rows[i][5].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Product Code Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Product Code Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][5].ToString();

                                status = ObjService.ValidateExcelMasterData(exceltext, "", "ProductCode");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Product Code Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Product Code Was Not Found ";
                                    }
                                }
                            }
                            if (dataTable.Rows[i][6].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "OU Code Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "OU Code Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][6].ToString();

                                status = ObjService.ValidateExcelMasterData(exceltext, "", "OUCode");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "OU Code Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "OU Code Was Not Found ";
                                    }
                                }
                            }
                            if (dataTable.Rows[i][7].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Amount Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Amount Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][7].ToString();

                                vales = Convert.ToDecimal(exceltext.ToString().Trim());
                                if (vales <= 0)
                                {
                                    if (errs == "")
                                    {
                                        errs = "Amount Should Not be Less Then Zero";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Amount Should Not be Less Then Zero";
                                    }
                                }
                            }


                            if (errs != "")
                            {
                                sno++;
                                Errdatatable.Rows.Add(sno, RowNo, errs);
                            }
                        }
                    }
                }
                else
                {
                    Errdatatable.Rows.Add(1, "Please  Select Valid Sheet");
                }
                invalid = Errdatatable.Rows.Count;
                valid = totalrecord - invalid;

                Result = "Total No of Vaild Record :" + valid + " - ";
                Result = Result + "Total No of InVaild Record :" + invalid + " - ";
                Result = Result + "Total No of Record Excel File :" + totalrecord;

                Errdatatable.TableName = datatableName;
                maindatatable.TableName = datatableName;

                Session["Errdatatablesu"] = Errdatatable;
                Session["maindatatablesu"] = maindatatable;
            }
            catch (Exception ex)
            {
                sno++;
                Errdatatable.Rows.Add(sno, Errdatatable.Rows.Count + 1, ex.Message.ToString() + " Please ckeck Excel File Error While Reading Data" + ex.ToString());

                invalid = Errdatatable.Rows.Count;
                valid = totalrecord - invalid;
                //ViewBag.vbvalid = "Total No of Vaild Record :" + valid;
                //ViewBag.vbinvalid = "Total No of InVaild Record :" + invalid;
                //ViewBag.vbtotalrecord = "Total No of Record Excel File :" + totalrecord;
                Result = "Total No of Vaild Record :" + valid + " - ";
                Result = Result + "Total No of InVaild Record :" + invalid + " - ";
                Result = Result + "Total No of Record Excel File :" + totalrecord;

                Session["Errdatatablesu"] = Errdatatable;
                Session["maindatatablesu"] = maindatatable;
                logger.Error(ex.ToString());
            }

            return Result;
        }

        private string datasupload_LocalConveyance(DataTable dataTable, string datatableName)
        {
            int sno = 0;
            int totalrecord = 0;
            int invalid = 0;
            int valid = 0;
            string Result = "";
            DataTable maindatatable = new DataTable();
            maindatatable = dataTable;
            totalrecord = maindatatable.Rows.Count;
            DataTable Errdatatable = new DataTable();
            Errdatatable.Columns.Add("SNo");
            Errdatatable.Columns.Add("Line");
            Errdatatable.Columns.Add("Error Description");
            try
            {
                if (dataTable.Rows.Count > 0)
                {
                    string exceltext = "";
                    string status = "";
                    string errs = "";
                    int RowNo = 0;
                    decimal vales = 0;
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        if (dataTable.Rows[i][0].ToString() != "")
                        {
                            errs = "";
                            RowNo = i + 1;
                            if (dataTable.Rows[i][1].ToString() == "")
                            {
                                errs = "Reference No Should Not be Empty ";
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][1].ToString();

                                status = ObjService.ValidateExcelMasterData(exceltext, "", "ReferenceNo");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Reference No Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Reference No Was Not Found ";
                                    }
                                }
                            }
                            if (dataTable.Rows[i][2].ToString() == "")
                            {
                                errs = "Employee Code Should Not be Empty ";
                            }
                            if (dataTable.Rows[i][3].ToString() == "")
                            {
                                errs = "Employee Name Should Not be Empty ";
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][3].ToString();
                                string employeecode = dataTable.Rows[i][2].ToString();
                                status = ObjService.ValidateExcelMasterData(exceltext, employeecode, "EmployeeCodeName");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Employee Code/Name Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Employee Code/Name Was Not Found ";
                                    }
                                }
                            }
                            if (dataTable.Rows[i][4].ToString() == "")
                            {
                                errs = "Transport Type Should Not be Empty ";
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][4].ToString();
                                status = ObjService.ValidateExcelMasterData(exceltext, "", "Transport");
                                if (status == "notexists")
                                {
                                    if (errs == "")
                                    {
                                        errs = "Transport Type Was Not Found ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Transport Type Was Not Found ";
                                    }
                                }
                            }
                            if (dataTable.Rows[i][5].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Travelled Date Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Travelled Date Should Not be Empty";
                                }
                            }
                            if (dataTable.Rows[i][6].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Location From Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Location From Should Not be Empty";
                                }
                            }
                            if (dataTable.Rows[i][7].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Location To Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Location To Should Not be Empty";
                                }
                            }
                            if (dataTable.Rows[i][8].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "KM Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "KM Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][8].ToString();
                                decimal KM = Convert.ToDecimal(exceltext);
                                if (KM <= 0)
                                {
                                    if (errs == "")
                                    {
                                        errs = "KM should not be less or equal to Zero ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "KM should not be less or equal to Zero ";
                                    }
                                }
                            }
                            if (dataTable.Rows[i][9].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Rate Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Rate Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][9].ToString();
                                decimal Rate = Convert.ToDecimal(exceltext);
                                if (Rate <= 0)
                                {
                                    if (errs == "")
                                    {
                                        errs = "Rate should not be less or equal to Zero ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Rate should not be less or equal to Zero ";
                                    }
                                }
                            }
                            if (dataTable.Rows[i][10].ToString() == "")
                            {
                                if (errs == "")
                                {
                                    errs = "Amount Should Not be Empty";
                                }
                                else
                                {
                                    errs = errs + "," + "Amount Should Not be Empty";
                                }
                            }
                            else
                            {
                                exceltext = dataTable.Rows[i][10].ToString();
                                decimal Amount = Convert.ToDecimal(exceltext);
                                if (Amount <= 0)
                                {
                                    if (errs == "")
                                    {
                                        errs = "Amount should not be less or equal to Zero ";
                                    }
                                    else
                                    {
                                        errs = errs + "," + "Amount should not be less or equal to Zero ";
                                    }
                                }
                            }

                            if (errs != "")
                            {
                                sno++;
                                Errdatatable.Rows.Add(sno, RowNo, errs);
                            }
                        }
                    }
                }
                else
                {
                    Errdatatable.Rows.Add(1, "Please  Select Valid Sheet");
                }
                invalid = Errdatatable.Rows.Count;
                valid = totalrecord - invalid;

                Result = "Total No of Vaild Record :" + valid + " - ";
                Result = Result + "Total No of InVaild Record :" + invalid + " - ";
                Result = Result + "Total No of Record Excel File :" + totalrecord;

                Errdatatable.TableName = datatableName;
                maindatatable.TableName = datatableName;

                Session["Errdatatablesu"] = Errdatatable;
                Session["maindatatablesu"] = maindatatable;
            }
            catch (Exception ex)
            {
                sno++;
                Errdatatable.Rows.Add(sno, Errdatatable.Rows.Count + 1, ex.Message.ToString() + " Please ckeck Excel File Error While Reading Data" + ex.ToString());

                invalid = Errdatatable.Rows.Count;
                valid = totalrecord - invalid;
                //ViewBag.vbvalid = "Total No of Vaild Record :" + valid;
                //ViewBag.vbinvalid = "Total No of InVaild Record :" + invalid;
                //ViewBag.vbtotalrecord = "Total No of Record Excel File :" + totalrecord;
                Result = "Total No of Vaild Record :" + valid + " - ";
                Result = Result + "Total No of InVaild Record :" + invalid + " - ";
                Result = Result + "Total No of Record Excel File :" + totalrecord;

                Session["Errdatatablesu"] = Errdatatable;
                Session["maindatatablesu"] = maindatatable;
                logger.Error(ex.ToString());
            }

            return Result;
        }

        public JsonResult localdetailssu(SupplierInvoice_Model ObjModel)
        {
            try
            {
                Int64 Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                DataTable dt = (DataTable)Session["maindatatablesu"];

                if (ObjModel.UploadMethod.Equals("SupplierInvoice"))
                {
                    dt = serviceObj.Save_UploadInvoice(ObjModel, Loginid, dt);
                }
                else if (ObjModel.UploadMethod.Equals("SupplierDebitline"))
                {
                    dt = serviceObj.Save_UploadDebitline(ObjModel, Loginid, dt);
                }
                else if (ObjModel.UploadMethod.Equals("EmployeeDebitline"))
                {
                    dt = objClaimService.Save_Bulk_Expense(ObjModel, Loginid, dt);
                }
                else if (ObjModel.UploadMethod.Equals("EmployeeECF_Others") || ObjModel.UploadMethod.Equals("EmployeeECF_Travel"))
                {
                    dt = objClaimService.Save_Claim_BulkECF(ObjModel, Loginid, dt);
                }
                else if (ObjModel.UploadMethod.Equals("EmployeeCOA") || ObjModel.UploadMethod.Equals("SupplierCOA"))
                {
                    dt = serviceObj.Save_UploadCOA(ObjModel, Loginid, dt);
                }
                else if (ObjModel.UploadMethod.Equals("EmployeeConveyance"))
                {
                    dt = objClaimService.Save_UploadLocalConveyance(ObjModel, Loginid, dt);
                }
                return Json(JsonConvert.SerializeObject(dt), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return Json("Error occurred..." + ex.Message.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public FileResult DownloadTemplate(string TemplateName)
        {
            string fullPath = System.Configuration.ConfigurationManager.AppSettings["DownloadTempate"];
            try
            {
                //SupplierInvoice_Model obj = new SupplierInvoice_Model(); 
                //get the temp folder and file path in server             

                //return the file for download, this is an Excel 
                //so I set the file content type to "application/vnd.ms-excel"
                return File(fullPath + TemplateName + ".xlsx", "application/vnd.ms-excel", TemplateName + ".xlsx");
            }
            catch(Exception ex)
            {
                logger.Error(ex.ToString());
                return File(fullPath + "Error.xlsx", "application/vnd.ms-excel", "Error.xlsx");
            }
        }
    }
}