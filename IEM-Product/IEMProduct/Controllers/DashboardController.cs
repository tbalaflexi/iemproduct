﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using System.Data;
using Newtonsoft.Json;
using Kendo.Mvc.UI;
using IEMProduct.Service;
using DataAccessHandler.Models.IEMProduct;

namespace IEMProduct.Controllers
{
    public class DashboardController : Controller
    {
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(DashboardController));
        Dashboard_Service objService = new Dashboard_Service();
        DataTable dt = new DataTable();
        Dashboard_Model objModel = new Dashboard_Model();

        // GET: Dashboard
        public ActionResult Dashboard()
        {
            return View();
        }


        public ActionResult GetRaiserCountList([DataSourceRequest]DataSourceRequest request)
        {
            try
            {

                Int64 userId = Convert.ToInt64(Session["Employee_Gid"].ToString());
                return Json(objService.GetRaiserCountList(userId).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return View();
        }


        public ActionResult IsCheckApFlag()
        {
            try
            {

                Int64 userId = Convert.ToInt64(Session["Employee_Gid"].ToString());
                return Json(objService.ApCheckFlag(userId));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return View();
        } 

        public ActionResult GetAPCountList([DataSourceRequest]DataSourceRequest request)
        {
            try
            {

                Int64 userId = Convert.ToInt64(Session["Employee_Gid"].ToString());
                return Json(objService.GetAPCountList(userId).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return View();
        }
        [HttpPost]
        public ActionResult ReadRaiserChartList(string dashDateFrom, string dashDateTo)
        {
            try
            {
                Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = objService.ReadRaiserChartList(User_GID, dashDateFrom, dashDateTo);
                //return Json(lstService.ReadFlowAssignment(User_GID).ToDataSourceResult(request));
                string Data = JsonConvert.SerializeObject(dt);
                return Json(Data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }
        }
        [HttpPost]
        public ActionResult ReadAPChartList(string dashDateFrom, string dashDateTo)
        {
            try
            {
                Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = objService.ReadAPChartList(User_GID, dashDateFrom, dashDateTo);
                //return Json(lstService.ReadFlowAssignment(User_GID).ToDataSourceResult(request));
                string Data = JsonConvert.SerializeObject(dt);
                return Json(Data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }

        }

    }
}