﻿using DataAccessHandler.Models.IEMProduct;
using IEMProduct.Service;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Kendo.Mvc.Extensions;
using System.Web.Mvc;

namespace IEMProduct.Controllers
{
    public class QueryController : Controller
    {

        #region

        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(TransactionController));             //Declaring Log4Net 
        Query_Model ModelObject = new Query_Model();                                   // Query model object
        List<Query_Model> ModelList = new List<Query_Model>();                         //Query list model object
        Query_Service ServiceObject = new Query_Service();                            // Query master service object
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        //string result = "";


        #endregion
        // GET: Query 
        public ActionResult Query()
        {
            return View();
        }

       // Read Query

        public ActionResult ReadGrid([DataSourceRequest]DataSourceRequest request, Query_Model ModelObject, string Ecfno = "", Int64 Deptid = 0, Int64 Statusid = 0, string Fromdate = "", string Todate = "")
        {
           try
            {
             // Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                ModelObject.ecf_no = Ecfno;
                ModelObject.deptid = Deptid;
                ModelObject.statusid = Statusid;
                ModelObject.fromdate = Fromdate;
                ModelObject.todate = Todate;

                //return Json(ServiceObject.ReadRecord(User_GID));
                // return Json(ServiceObject.ReadRecord().ToDataSourceResult(request));, User_GID

                return Json(ServiceObject.ReadRecord(ModelObject).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }

        }

        //Dropdown Binding Department

        public JsonResult GetDepartment(string parentcode)
        {
            List<Query_Model> delmattype = new List<Query_Model>();
            try
            {
                delmattype = ServiceObject.GetDepartment(parentcode);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(delmattype, JsonRequestBehavior.AllowGet);
        }

        //Dropdown Binding Status

        //public JsonResult Getstatus(string parentcode)
        //{
        //    List<Query_Model> delmattype = new List<Query_Model>();
        //    try
        //    {
        //        delmattype = ServiceObject.Getstatus(parentcode);
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex.ToString());
        //    }
        //    return Json(delmattype, JsonRequestBehavior.AllowGet);
        //}

        

        //Non QC Master DropDown Values

        public JsonResult dropdowns_values_binding(string flag)
        {
            List<Query_Model> dropdown = new List<Query_Model>();
            try
            {
                dropdown = ServiceObject.dropdownvalues(flag);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(dropdown, JsonRequestBehavior.AllowGet);
        }
    }
}