﻿//using DataAccessHandler.Models.IEMProduct;
using IEMProduct.Service;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using DBHelper;
using Newtonsoft.Json;

namespace IEMProduct.Controllers
{
    public class TransactionController : Controller
    {

        #region

        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(TransactionController));             //Declaring Log4Net 
        Transaction_Model ModelObject = new Transaction_Model();                                   // Transaction model object
        List<Transaction_Model> ModelList = new List<Transaction_Model>();                         //Transaction list model object
        Transaction_Service ServiceObject = new Transaction_Service();                            // Transaction master service object
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        string result = "";
        string Tran_SubType = string.Empty;

        #endregion


        // GET: Transaction
        public ActionResult Transaction()
        {
            return View();
        }


        //Transaction Read

        public ActionResult ReadGrid([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                return Json(ServiceObject.ReadRecord().ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }

        }

        //Transaction Insert

        public ActionResult SaveGrid([DataSourceRequest] DataSourceRequest request, Transaction_Model ModelObject)
        {
            try
            {
                dt = ServiceObject.SaveRecord(ModelObject);
                ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.result);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return Json(ModelObject.result);
            }
        }

        //  Transaction Update

        public ActionResult UpdateGrid([DataSourceRequest] DataSourceRequest request, Transaction_Model ModelObject)
        {
            try
            {
                dt = ServiceObject.UpdateRecord(ModelObject);
                ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.result);

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return Json(ModelObject.result);

            }

        }


        // Transaction delete

        public ActionResult DeleteGrid(Transaction_Model ModelObject)
        {
            try
            {
                dt = ServiceObject.DeleteRecord(ModelObject);
                if (dt.Rows.Count > 0)
                {
                    result = dt.Rows[0][0].ToString();
                }
                ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return Json(ModelObject.result);
            }

        }




        //binding Tran Type dropdown

        public JsonResult GetTranType(string parentcode)
        {
            List<Transaction_Model> trantype = new List<Transaction_Model>();
            try
            {
                trantype = ServiceObject.Getdropdown(parentcode);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(trantype, JsonRequestBehavior.AllowGet);
        }


        //binding  Tran Sub Type dropdown

        public JsonResult GetTranSubType(string parentcode)
        {
            List<Transaction_Model> transubtype = new List<Transaction_Model>();
            try
            {
                transubtype = ServiceObject.GetTranSubType(parentcode);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(transubtype, JsonRequestBehavior.AllowGet);
        }

        //dropdown get values

        public ActionResult GetSubType(string TranId, string parentcode)
        {
            List<Transaction_Model> sub_TypeList = new List<Transaction_Model>();

            try
            {
                dt = ServiceObject.GetValues(TranId, parentcode);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Transaction_Model SubType = new Transaction_Model();
                    SubType.TransubId = Convert.ToInt64(dt.Rows[i][0].ToString());
                    SubType.Tran_SubType = dt.Rows[i][1].ToString();
                    sub_TypeList.Add(SubType);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(sub_TypeList, JsonRequestBehavior.AllowGet);
        }


        //Non QC Master DropDown Values

        public JsonResult dropdowns_values_binding(string flag)
        {
            List<Transaction_Model> dropdown = new List<Transaction_Model>();
            try
            {
                dropdown = ServiceObject.dropdownvalues(flag);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(dropdown, JsonRequestBehavior.AllowGet);
        }


    }
}