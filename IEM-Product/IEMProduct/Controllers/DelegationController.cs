﻿using DataAccessHandler.Models.IEMProduct;
using IEMProduct.Service;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using System.Globalization;

namespace IEMProduct.Controllers
{
    public class DelegationController : Controller
    {
        #region

        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(TransactionController));             //Declaring Log4Net 
        Delegation_Model ModelObject = new Delegation_Model();                                   // Delegation model object
        List<Delegation_Model> ModelList = new List<Delegation_Model>();                         //Delegation list model object
        Delegation_Service ServiceObject = new Delegation_Service();                            // Delegation master service object
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        string result = "";
       

        #endregion


        // GET: Delegation
        public ActionResult Delegation()
        {
           // ModelObject.delegate_by = Session["Employee_Code"].ToString() + "-" + Session["Employee_Name"].ToString();
           // ModelObject.delegate_by = (Session["Employee_Code"].ToString());
            return View();
        }

        //Delegation Read

        public ActionResult ReadGrid([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                return Json(ServiceObject.ReadRecord(User_GID).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }

        }

        //Delegation Insert
        [HttpPost]
        public ActionResult SaveGrid( Delegation_Model ModelObject, string Employee_Ids)
        {
            try
            {
                Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                ModelObject.delegate_workflow_Role = (Employee_Ids);
                dt = ServiceObject.SaveRecord(ModelObject, User_GID);
                ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.result);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return Json(ModelObject.result);
            }
        }

        // Delegation update
        public ActionResult UpdateGrid(Delegation_Model ModelObject, string Employee_Ids)
        {
            try
            {
                Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
               // ModelObject.loginid = Convert.ToInt64(Session["Employee_Gid"]);
                ModelObject.delegate_workflow_Role = (Employee_Ids);
                dt = ServiceObject.UpdateRecord(ModelObject, User_GID);
                ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.result);

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return Json(ModelObject.result);

            }

        }

        // Delegation Delete

        public ActionResult DeleteGrid(Delegation_Model ModelObject)
        {
            try
            {
                Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = ServiceObject.DeleteRecord(ModelObject, User_GID);
                if (dt.Rows.Count > 0)
                {
                    result = dt.Rows[0][0].ToString();
                }
                ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return Json(ModelObject.result);
            }

        }

        //dropdownvalues for delmat type

        public JsonResult GetDelmatType(string parentcode)
        {
            List<Delegation_Model> delmattype = new List<Delegation_Model>();
            try
            {
                delmattype = ServiceObject.Getdropdown(parentcode);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(delmattype, JsonRequestBehavior.AllowGet);
        }

        // dropdown values for Role


        public JsonResult GetRoleType(string parentcode)
        {
            List<Delegation_Model> roletype = new List<Delegation_Model>();
            try
            {
                roletype = ServiceObject.GetRole(parentcode);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(roletype, JsonRequestBehavior.AllowGet);
        }


        //Non QC Master DropDown Values  empcode,empname

        //public JsonResult GetEmpCodeName(string flag, string EmployeeName)
        //{
        //    List<Delegation_Model> dropdown = new List<Delegation_Model>();
        //    try
        //    {
        //        dropdown = ServiceObject.dropdownvalues(flag, EmployeeName);
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex.ToString());
        //    }
        //    return Json(dropdown, JsonRequestBehavior.AllowGet);
        //}

        //Employee Name Code Non QC Master
        public ActionResult GetEmpCodeName(string EmployeeName, string Master, string flag = "")
        {
            List<Delegation_Model> emplist = new List<Delegation_Model>();
            try
            {
                dt = ServiceObject.GetEmployee_ByCodeorName(EmployeeName, flag, Master);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Delegation_Model emploee = new Delegation_Model();
                    emploee.empid = Convert.ToInt64(dt.Rows[i][0].ToString());
                    emploee.empname = dt.Rows[i][1].ToString();
                    emplist.Add(emploee);
                }
                //var jsonResult = Json(emplist, JsonRequestBehavior.AllowGet);
                //jsonResult.MaxJsonLength = int.MaxValue;
                //return jsonResult;
                return Json(emplist, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }

        }
     
    }
}