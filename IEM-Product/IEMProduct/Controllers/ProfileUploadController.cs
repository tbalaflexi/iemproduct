﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMProduct.Service;
using Kendo.Mvc.Extensions;
using DataAccessHandler.Models.IEMProduct;
using System.Data;
using Newtonsoft.Json;
using DBHelper;
using System.Configuration;
using System.IO;
using System.Data.OleDb;
using System.Security.Cryptography;

namespace IEMProduct.Controllers
{
    public class ProfileUploadController : Controller
    {
        #region

        ProfileUpload_Service serviceObj = new ProfileUpload_Service();
        DataTable dt = new DataTable();
        DataTable result = new DataTable();
        DataRow dr;
        DataSet ds = new DataSet();
        ProfileUpload_Model Obj_Model = new ProfileUpload_Model();
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(ProfileController));
        #endregion

        // GET: ProfileUpload
        public ActionResult ProfileUpload()
        {
            return View();
        }
        public ActionResult Read_ProfileUpload([DataSourceRequest]DataSourceRequest request, ProfileUpload_Model Obj_Model)
        {
            try
            {
                return Json(serviceObj.Get_ProfileUPloadDetailList(Obj_Model).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
        }

        public ActionResult Upload_FileImport()
        {
            string result = string.Empty;
            string[] result_array = { };
            try
            {
                HttpFileCollectionBase File = Request.Files;
                HttpPostedFileBase excelfile = File[0];
                DataSet ds = new DataSet();
                Obj_Model.LoginId = Convert.ToInt64(Session["Employee_Gid"]);
                string ServerSavePath = "";
                if (excelfile != null && excelfile.ContentLength > 0)
                {
                    string fileExtension = System.IO.Path.GetExtension(excelfile.FileName);//get file selected file extension.
                    string filePath = ConfigurationManager.AppSettings["ProfileUploadTemplate"].ToString();
                    var InputFileName = Path.GetFileName(excelfile.FileName);
                    Obj_Model.POUpload_FileName = InputFileName;
                    Int64 FileCount = serviceObj.Get_FileCount(Obj_Model); //Get File Count.
                    FileInfo fi = new FileInfo(excelfile.FileName);
                    string FileType = fi.Extension;
                    //string FileType = "Excel";
                    if (FileCount > 0)
                    {
                        string FileName_withoutext = Path.GetFileNameWithoutExtension(excelfile.FileName);

                        // string FileType = ".csv";
                        string NewFileName = FileName_withoutext + "(" + FileCount + ")" + FileType;
                        Obj_Model.POUpload_NewFileName = NewFileName;
                        Obj_Model.POUpload_FileType = FileType;
                        ServerSavePath = Path.Combine(Path.Combine(filePath, Path.GetFileName(NewFileName)));
                        Obj_Model.ServerPath = ServerSavePath;
                    }
                    else
                    {
                        Obj_Model.POUpload_NewFileName = InputFileName;
                        Obj_Model.POUpload_FileType = FileType;
                        ServerSavePath = Path.Combine(Path.Combine(filePath, Path.GetFileName(excelfile.FileName)));
                        Obj_Model.ServerPath = ServerSavePath;
                    }

                    string fileLocation = ServerSavePath;
                    //Delete the file if already exist in same name.
                    if (System.IO.File.Exists(fileLocation))
                    {
                        System.IO.File.Delete(fileLocation);
                    }
                    //Save file to server folder.  
                    excelfile.SaveAs(ServerSavePath);

                    //result = serviceObj.SavePoUploadData(Obj_Model);


                    string excelConnectionString = string.Empty;
                    excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=PathToExcelFileWithExtension;Extended Properties='Excel 8.0;HDR=YES'";
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    else if (fileExtension == ".xlsx")
                    {
                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);//connection for excel.
                    excelConnection.Close();//excel connection close.
                    excelConnection.Open();//excel connection open.
                    DataTable dtexcel = new DataTable();//datatable object.
                    dtexcel = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dtexcel == null)
                    {
                        return null;
                    }
                    String[] excelSheets = new String[dtexcel.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dtexcel.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    int excel = excelSheets.Count();
                    if (excel == 0)
                    {
                        result = "invalid file";
                        excelConnection.Close();
                    }
                    else
                    {
                        string query = string.Format("Select * from [{0}]", excelSheets[0]);
                        using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection))
                        {
                            dataAdapter.Fill(ds);
                        }
                        DataTable dtexceldata = new DataTable();
                        if (ds.Tables.Count > 0)
                        {
                            dtexceldata = ds.Tables[0]; //Excel Data
                            //code for removing invalid empty rows from datatable.
                            //dtexceldata = dtexceldata.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field =>
                            //field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
                        }
                        if (dtexceldata.Rows.Count == 0)
                        {
                            result = "mandatory fields empty";
                            excelConnection.Close();
                            return Json(result, JsonRequestBehavior.AllowGet);
                        }

                        if (dtexceldata.Columns[0].ToString().Trim().ToLower() == "employee code" && dtexceldata.Columns[1].ToString().Trim().ToLower() == "employee name" && dtexceldata.Columns[2].ToString().Trim().ToLower() == "gender" &&
                            dtexceldata.Columns[3].ToString().Trim().ToLower() == "date of birth" && dtexceldata.Columns[4].ToString().Trim().ToLower() == "date of join" && dtexceldata.Columns[5].ToString().Trim().ToLower() == "designation code" &&
                            dtexceldata.Columns[6].ToString().Trim().ToLower() == "grade code" && dtexceldata.Columns[7].ToString().Trim().ToLower() == "department code" && dtexceldata.Columns[8].ToString().Trim().ToLower() == "branch code" &&
                            dtexceldata.Columns[9].ToString().Trim().ToLower() == "fc code" && dtexceldata.Columns[10].ToString().Trim().ToLower() == "cc code" && dtexceldata.Columns[11].ToString().Trim().ToLower() == "product code" &&
                            dtexceldata.Columns[12].ToString().Trim().ToLower() == "address 1" && dtexceldata.Columns[13].ToString().Trim().ToLower() == "address 2" && dtexceldata.Columns[14].ToString().Trim().ToLower() == "city code" &&
                            dtexceldata.Columns[15].ToString().Trim().ToLower() == "pincode" && dtexceldata.Columns[16].ToString().Trim().ToLower() == "personal email" && dtexceldata.Columns[17].ToString().Trim().ToLower() == "office email" &&
                            dtexceldata.Columns[18].ToString().Trim().ToLower() == "mobile no" && dtexceldata.Columns[19].ToString().Trim().ToLower() == "bank code" && dtexceldata.Columns[20].ToString().Trim().ToLower() == "bank branch code" &&
                            dtexceldata.Columns[21].ToString().Trim().ToLower() == "acc no" && dtexceldata.Columns[22].ToString().Trim().ToLower() == "acc type" && dtexceldata.Columns[23].ToString().Trim().ToLower() == "card no" &&
                            dtexceldata.Columns[24].ToString().Trim().ToLower() == "employee permanent addr" && dtexceldata.Columns[25].ToString().Trim().ToLower() == "supervisor code" && dtexceldata.Columns[26].ToString().Trim().ToLower() == "employee dor" &&
                            dtexceldata.Columns[27].ToString().Trim().ToLower() == "last working date" && dtexceldata.Columns[28].ToString().Trim().ToLower() == "effecitive date")
                        {

                            Int64 File_Gid = serviceObj.FileInst_Data(Obj_Model);
                            Int64 TruncateTemp = serviceObj.TruncateTempData(Obj_Model);
                            if (File_Gid > 0)
                            {
                                int Clear_Count = 0;

                                for (int j = 0; j < dtexceldata.Rows.Count; j++)
                                {
                                    Obj_Model.File_Gid = File_Gid;
                                    Obj_Model.Emp_Code = dtexceldata.Rows[j]["employee code"].ToString().Trim();
                                    Obj_Model.Emp_Name = dtexceldata.Rows[j]["employee name"].ToString().Trim();
                                    Obj_Model.Emp_Gender = dtexceldata.Rows[j]["gender"].ToString().Trim();
                                    string DOB = dtexceldata.Rows[j]["date of birth"].ToString().Trim();

                                    if (DOB == "" || DOB == null)
                                    {
                                        DOB = null;
                                    }
                                    else
                                    {
                                        DateTime dt;
                                        if (DateTime.TryParse(DOB, out dt))
                                        {
                                            DOB = dt.ToString("yyyy-MM-dd");
                                        }
                                        else
                                        {
                                            result = "Employee DOB Date Format is Invalid";
                                            break;
                                        }
                                        //DateTime dt = Convert.ToDateTime(DOB);
                                        //DOB = dt.ToString("yyyy-MM-dd");
                                    }
                                    Obj_Model.Emp_Dob = DOB;

                                    string DOJ = dtexceldata.Rows[j]["date of join"].ToString().Trim();
                                    if (DOJ == "" || DOJ == null)
                                    {
                                        DOJ = null;
                                    }
                                    else
                                    {
                                        DateTime dt;
                                        if (DateTime.TryParse(DOJ, out dt))
                                        {
                                            DOJ = dt.ToString("yyyy-MM-dd");
                                        }
                                        else
                                        {
                                            result = "Employee DOJ Date Format is Invalid";
                                            break;
                                        }
                                        //DateTime dt = Convert.ToDateTime(DOJ);
                                        //DOJ = dt.ToString("yyyy-MM-dd");
                                    }
                                    Obj_Model.Emp_Doj = DOJ;
                                    Obj_Model.Designation_code = dtexceldata.Rows[j]["designation code"].ToString().Trim();
                                    Obj_Model.Grade_code = dtexceldata.Rows[j]["grade code"].ToString().Trim();
                                    Obj_Model.Dept_Code = dtexceldata.Rows[j]["department code"].ToString().Trim();
                                    Obj_Model.Emp_Branch_Code = dtexceldata.Rows[j]["branch code"].ToString().Trim();
                                    Obj_Model.FC_Code = dtexceldata.Rows[j]["fc code"].ToString().Trim();
                                    Obj_Model.CC_Code = dtexceldata.Rows[j]["cc code"].ToString().Trim();
                                    Obj_Model.Product_Code = dtexceldata.Rows[j]["product code"].ToString().Trim();
                                    Obj_Model.Addr1 = dtexceldata.Rows[j]["address 1"].ToString().Trim();
                                    Obj_Model.Addr2 = dtexceldata.Rows[j]["address 2"].ToString().Trim();
                                    Obj_Model.City_Code = dtexceldata.Rows[j]["city code"].ToString().Trim();
                                    Obj_Model.Pincode = dtexceldata.Rows[j]["pincode"].ToString().Trim();
                                    Obj_Model.Personal_email = dtexceldata.Rows[j]["personal email"].ToString().Trim();
                                    Obj_Model.Office_Email = dtexceldata.Rows[j]["office email"].ToString().Trim();
                                    Obj_Model.Mobil_No = dtexceldata.Rows[j]["mobile no"].ToString().Trim();
                                    Obj_Model.Bank_Code = dtexceldata.Rows[j]["bank code"].ToString().Trim();
                                    Obj_Model.Bank_Branch_Code = dtexceldata.Rows[j]["bank branch code"].ToString().Trim();
                                    Obj_Model.Acc_No = dtexceldata.Rows[j]["acc no"].ToString().Trim();
                                    Obj_Model.Acc_Type = dtexceldata.Rows[j]["acc type"].ToString().Trim();
                                    Obj_Model.Emp_Card_No = dtexceldata.Rows[j]["card no"].ToString().Trim();
                                    Obj_Model.Emp_Parmanent_Addr = dtexceldata.Rows[j]["employee permanent addr"].ToString().Trim();
                                    Obj_Model.Supervisor_Code = dtexceldata.Rows[j]["supervisor code"].ToString().Trim();
                                    string emp_dor = dtexceldata.Rows[j]["employee dor"].ToString().Trim();
                                    if (emp_dor == "" || emp_dor == null)
                                    {
                                        emp_dor = null;
                                    }
                                    else
                                    {
                                        DateTime dt;
                                        if (DateTime.TryParse(emp_dor, out dt))
                                        {
                                            emp_dor = dt.ToString("yyyy-MM-dd");
                                        }
                                        else
                                        {
                                            result = "Employee DOR Date Format is Invalid";
                                            break;
                                        }
                                        
                                        //DateTime dt = Convert.ToDateTime(emp_dor);
                                        //emp_dor = dt.ToString("yyyy-MM-dd");
                                    }
                                    Obj_Model.Employee_dor = emp_dor;
                                    string lastwrk_date = dtexceldata.Rows[j]["last working date"].ToString().Trim();
                                    if (lastwrk_date == "" || lastwrk_date == null)
                                    {
                                        lastwrk_date = null;
                                    }
                                    else
                                    {
                                        DateTime dt;
                                        if (DateTime.TryParse(lastwrk_date, out dt))
                                        {
                                            lastwrk_date = dt.ToString("yyyy-MM-dd");
                                        }
                                        else
                                        {
                                            result = "Last Working Date Format is Invalid";
                                            break;
                                        }
                                        //DateTime dt = Convert.ToDateTime(lastwrk_date);
                                        //lastwrk_date = dt.ToString("yyyy-MM-dd");
                                    }
                                    Obj_Model.Last_Working_date = lastwrk_date;
                                    string Effectivedate = dtexceldata.Rows[j]["effecitive date"].ToString().Trim();
                                    if (Effectivedate == "" || Effectivedate == null )
                                    {
                                        Effectivedate = null;
                                    }
                                    else
                                    {
                                        DateTime dt;
                                        if (DateTime.TryParse(Effectivedate, out dt))
                                        {
                                            Effectivedate = dt.ToString("yyyy-MM-dd");
                                        }
                                        else
                                        {
                                            result = "Effective Date Format is Invalid";
                                            break;
                                        }
                                        //DateTime dt = Convert.ToDateTime(Effectivedate);
                                        //Effectivedate = dt.ToString("yyyy-MM-dd");
                                    }
                                    Obj_Model.Effictive_date = Effectivedate;

                                    DataTable dt_data = serviceObj.SaveTempProfileUploadData(Obj_Model);

                                    for (int i = 0; i < dt_data.Rows.Count; i++)
                                    {
                                        Clear_Count = Convert.ToInt16(dt_data.Rows[i]["Clear"]);
                                        result = "inserted";
                                    }
                                }
                                return Json(result, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                result = "Profile Template File Not Stored.";
                                return Json(result, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            result = "invalid header name";
                            return Json(result, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    result = "invalid file";
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = "failed";
                log.Error(ex.ToString());
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult Upload_FileClear()
        {
            string result = "";
            Int64 TruncateTemp = serviceObj.TruncateTempData(Obj_Model);
            result = "1"; 
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Upload_FileConfirm()
        {
            try
            {
                Obj_Model.LoginId = Convert.ToInt64(Session["Employee_Gid"]);
                Obj_Model.Emp_Password = CheckUserCredential("admin");
                
                dt = serviceObj.SetFileConfirm_Data(Obj_Model);
                return Json(JsonConvert.SerializeObject(dt), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string  CheckUserCredential( string userpassword)
        {
            string EncryptionKey = "abc123";
            string encryptedpassword = string.Empty;
            try
            {
                byte[] clearBytes = System.Text.Encoding.Unicode.GetBytes(userpassword);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        encryptedpassword = Convert.ToBase64String(ms.ToArray());
                    }
                }
                return encryptedpassword;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}