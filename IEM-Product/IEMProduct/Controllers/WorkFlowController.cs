﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using Newtonsoft.Json;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using DBHelper;
using IEMProduct.Service;
using DataAccessHandler.Models.IEMProduct;

namespace IEMProduct.Controllers
{
    public class WorkFlowController : Controller
    {
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(WorkFlowController));
        WorkFlow_Service serviceObj = new WorkFlow_Service();
        DataTable dt = new DataTable();
        // GET: WorkFlow
        public ActionResult WorkFlow()
        {
            return View();
        }

        #region Get summery list.

        public ActionResult Read_WorkFlow([DataSourceRequest]DataSourceRequest request, QCD_Model ObjModel)
        {
            try
            {
                return Json(serviceObj.Get_DropDownList(ObjModel).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return View();
        }

        public ActionResult Read_WorkFlow_New([DataSourceRequest]DataSourceRequest request, string Master, string Depend_Code, string TypeCode, string SubTypeCode, string AttributeParent, string AttributeChild)
        {
            try
            {
                return Json(serviceObj.Read_WorkFlow(Master, Depend_Code, TypeCode, SubTypeCode, AttributeParent, AttributeChild).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return View();
        }

        public ActionResult Read_WorkFlow_Summery([DataSourceRequest]DataSourceRequest request, string Master, string Depend_Code, string TypeCode, string SubTypeCode, string AttributeParent, string AttributeChild)
        {
            try
            {
                return Json(serviceObj.Read_WorkFlow(Master, Depend_Code, TypeCode, SubTypeCode, AttributeParent, AttributeChild).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }
        }

        public ActionResult Read_Type(string Master, string Depend_Code, string TypeCode, string SubTypeCode, string AttributeParent, string AttributeChild)
        {
            List<WorkFlow_Model> lst = new List<WorkFlow_Model>();
            try
            {
                lst = serviceObj.Read_WorkFlow(Master, Depend_Code, TypeCode, SubTypeCode, AttributeParent, AttributeChild);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Common method - dropdownlist datasource binding.
        public ActionResult Get_DropDownList(QCD_Model ObjModel)
        {
            List<QCD_Model> lst = new List<QCD_Model>();
            try
            {
                lst = serviceObj.Get_DropDownList(ObjModel);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Create Flow.
        [HttpPost]
        public ActionResult UpdateHeaderDetails(WorkFlow_Model objmodel)
        {
            string Result = string.Empty;
            try
            {
                objmodel.Logged_UserGID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = serviceObj.UpdateHeaderDetails(objmodel);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Save_WorkFlow(WorkFlow_Model objmodel, Int64 workflowheader_gid)
        {
            string Result = string.Empty;
            try
            {
                objmodel.workflow_id = workflowheader_gid;
                objmodel.Logged_UserGID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = serviceObj.Save_WorkFlow(objmodel);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Edit Flow.
        public ActionResult Update_WorkFlow(WorkFlow_Model objmodel, Int64 workflowheader_gid)
        {
            string Result = string.Empty;
            try
            {
                objmodel.workflow_id = workflowheader_gid;
                objmodel.Logged_UserGID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = serviceObj.Update_WorkFlow(objmodel);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Remove workflow.
        public ActionResult Remove_WorkFlow(Int64 Master_Gid)
        {
            string Result = string.Empty;
            try
            {
                Int64 Logged_UserGID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = serviceObj.Remove_WorkFlow(Master_Gid, Logged_UserGID);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Show attribute Partial view.
        [HttpPost]
        public ActionResult Show_Attribute_PartialView(Int64 Type_Gid)
        {
            try
            {
                ViewBag.ParentAttributes = serviceObj.Get_Attributes(Type_Gid);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return PartialView("AttributesPartialView", ViewBag.ParentAttributes);
        }

        [HttpPost]
        public ActionResult Show_Attribute_PartialView_ForWorkFlow(string Type_Code)
        {
            try
            {
                Int64 Logged_UserGid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                ViewBag.ParentAttributes = serviceObj.Get_Attributes_ForWorkFlow(Type_Code, Logged_UserGid);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return PartialView("AttributesPartialView", ViewBag.ParentAttributes);
        }
        #endregion

        #region Get department records.
        public ActionResult Get_DropDownList_Dept(Delmat_Model ObjModel)
        {
            List<Delmat_Model> lst = new List<Delmat_Model>();
            try
            {
                lst = serviceObj.Get_DropDownList_Dept(ObjModel);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}