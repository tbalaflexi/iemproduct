﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMProduct.Service;
using Kendo.Mvc.Extensions;
using DataAccessHandler.Models.IEMProduct;
using System.Data;
using Newtonsoft.Json;
namespace IEMProduct.Controllers
{
    public class ProxyController : Controller
    {

        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(ProxyController));
        Proxy_Service objService = new Proxy_Service();
        DataTable dt = new DataTable();
        // GET: Proxy
        public ActionResult Proxy()
        {
            return View();
        }


        #region common method for fetching grid records.
        public ActionResult GetProxyDetails([DataSourceRequest]DataSourceRequest request)
        {
            try
            {

                Int64 userId = Convert.ToInt64(Session["Employee_Gid"].ToString());
                return Json(objService.ReadRecord(userId).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return View();
        }


        //public ActionResult Get_EmployeeList()
        //{
        //    List<Proxy_Model> _lst = new List<Proxy_Model>();
        //    try
        //    {
        //        Int64 userId = Convert.ToInt64(Session["Employee_Gid"].ToString());
        //        _lst = objService.Get_EmployeeList(userId);
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex.ToString());
        //    }
        //    return Json(_lst, JsonRequestBehavior.AllowGet);
        //}
        public JsonResult Get_EmployeeList()
        {
            List<Proxy_Model> _lst = new List<Proxy_Model>();
            try
            {
                Int64 userId = Convert.ToInt64(Session["Employee_Gid"].ToString());
                _lst = objService.Get_EmployeeList(userId);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            var jsonResult = Json(_lst, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
            //return Json(_lst, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Save_Update_ProxyDetails(Proxy_Model ObjModel, string ActionType = "", Int64 ProxyFromId = 0, Int64 ProxyToId = 0,string Status="")
        {
            string Result = string.Empty;
            try
            {
                ObjModel.Action = ActionType;
                ObjModel.proxyFromId = Convert.ToInt64(Session["Employee_Gid"].ToString());
                ObjModel.proxyToId = Convert.ToInt64(ProxyToId);
                ObjModel.proxyActiveId = Status;
                dt = objService.Save_Update_ProxyDetails(ObjModel);
                Result = JsonConvert.SerializeObject(dt);
                return Json(Result);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                Result = ex.Message.ToString();
                return Json(Result);
            }
            //  return Json(Result, JsonRequestBehavior.AllowGet);
        } 
     public ActionResult Remove_ProxyDetail(Proxy_Model ObjModel, string ActionType = "", Int64 proxyGid = 0)
        {
            string Result = string.Empty;
            try
            {
                ObjModel.Action = ActionType;
                ObjModel.proxyGid = proxyGid;
                
                dt = objService.Remove_ProxyDetails(ObjModel);
                Result = JsonConvert.SerializeObject(dt);
                return Json(Result);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                Result = ex.Message.ToString();
                return Json(Result);
            }
            //  return Json(Result, JsonRequestBehavior.AllowGet);
        }


        #endregion
     ////Employee Name Code Non QC Master
     //public ActionResult GetEmpCodeName(string EmployeeName, string Master, string flag = "")
     //{
     //    List<Proxy_Model> emplist = new List<Proxy_Model>();
     //    try
     //    {
     //        dt = objService.GetEmployee_ByCodeorName(EmployeeName, flag, Master);
     //        for (int i = 0; i < dt.Rows.Count; i++)
     //        {
     //            Proxy_Model emploee = new Proxy_Model();
     //            emploee.proxyToId = Convert.ToInt64(dt.Rows[i][0].ToString());
     //            emploee.proxyTo = dt.Rows[i][1].ToString();
     //            emplist.Add(emploee);
     //        }
     //        //var jsonResult = Json(emplist, JsonRequestBehavior.AllowGet);
     //        //jsonResult.MaxJsonLength = int.MaxValue;
     //        //return jsonResult;
     //        return Json(emplist, JsonRequestBehavior.AllowGet);
     //    }
     //    catch (Exception ex)
     //    {
     //        logger.Error(ex.ToString());
     //        return View();
     //    }

    // }

    }
}