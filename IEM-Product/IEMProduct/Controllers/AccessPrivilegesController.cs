﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using DataAccessHandler.Models.IEMProduct;
using IEMProduct.Service;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;

namespace IEMProduct.Controllers
{
    public class AccessPrivilegesController : Controller
    {
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(AccessPrivilegesController));
        AccessPrivileges_Service serviceObj = new AccessPrivileges_Service();
        // GET: AccessPrivileges
        public ActionResult AccessPrivileges()
        {
            return View();
        }

        public ActionResult AccessPrivileges_Read([DataSourceRequest]DataSourceRequest request)
        {
            try
            {
                Int64 LoggedUser_ID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                return Json(serviceObj.AccessPrivileges_GridDatas(LoggedUser_ID).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }
        }

        public PartialViewResult TreeViewPartial(Int64 Role_Gid)
        {
            AccessPrivileges_Model.UserGroups ug = new AccessPrivileges_Model.UserGroups();
            try
            {
                ug = AccessPrivileges_Service.GetTreeview(Role_Gid);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return PartialView("TreeviewPartial", ug);
        }

        [HttpPost]
        public ActionResult SetAccessPrivileges(Int64 Role_Gid, string menuchecked, string menunotchecked)
        {
            DataTable dt = new DataTable();
            string Result = string.Empty;
            try
            {
                Int64 LoggedUser_ID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = serviceObj.Update_MenueRights(Role_Gid, menuchecked, menunotchecked, LoggedUser_ID);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
    }
}