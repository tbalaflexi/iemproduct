﻿using System.Web.Mvc;

namespace IEMProduct
{
    public class FlexiBuyAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "FlexiBuy";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "FlexiBuy_default",
                "FlexiBuy/{controller}/{action}/{id}",
                new { controller = "FlexiBuyHome", action = "FlexiBuyHome", id = UrlParameter.Optional },
                new string[] { "FlexiBuy.Controllers" }
            );
        }
    }
}