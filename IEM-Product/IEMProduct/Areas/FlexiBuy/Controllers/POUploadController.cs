﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlexiBuy.Service;
using Kendo.Mvc.Extensions;
using DataAccessHandler.Models.FlexiBuy;
using System.Data;
using System.Data.OleDb;
using System.IO;
using Newtonsoft.Json;
using System.Configuration;


namespace FlexiBuy.Controllers
{
    public class POUploadController : Controller
    {
        #region

        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        POUpload_Model Obj_Model = new POUpload_Model();
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(POUploadController));
        POUpload_Service serviceObj = new POUpload_Service();
        #endregion

        // GET: POUpload
        public ActionResult POUpload()
        {
            return View();
        }
        public ActionResult Read_PoUpload([DataSourceRequest]DataSourceRequest request, POUpload_Model Obj_Model)
        {
            try
            {
                return Json(serviceObj.Get_POUPloadDetailList(Obj_Model).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }

        }
        public ActionResult Upload_FileImport()
        {
            string result = string.Empty;
            string[] result_array = { };
            try
            {
                HttpFileCollectionBase File = Request.Files;
                HttpPostedFileBase excelfile = File[0];
                DataSet ds = new DataSet();
                Obj_Model.LoginId = Convert.ToInt64(Session["Employee_Gid"]);
                string ServerSavePath = "";
                if (excelfile != null && excelfile.ContentLength > 0)
                {
                    string fileExtension = System.IO.Path.GetExtension(excelfile.FileName);//get file selected file extension.
                    string filePath = ConfigurationManager.AppSettings["POUploadTemplate"].ToString();
                    var InputFileName = Path.GetFileName(excelfile.FileName);
                    Obj_Model.POUpload_FileName = InputFileName;
                    Int64 FileCount = serviceObj.Get_FileCount(Obj_Model); //Get File Count.
                    FileInfo fi = new FileInfo(excelfile.FileName);
                    string FileType = fi.Extension;
                    //string FileType = "Excel";
                    if (FileCount > 0)
                    {
                        string FileName_withoutext = Path.GetFileNameWithoutExtension(excelfile.FileName);

                        // string FileType = ".csv";
                        string NewFileName = FileName_withoutext + "(" + FileCount + ")" + FileType;
                        Obj_Model.POUpload_NewFileName = NewFileName;
                        Obj_Model.POUpload_FileType = FileType;
                        ServerSavePath = Path.Combine(Path.Combine(filePath, Path.GetFileName(NewFileName)));
                        Obj_Model.ServerPath = ServerSavePath;
                    }
                    else
                    {
                        Obj_Model.POUpload_NewFileName = InputFileName;
                        Obj_Model.POUpload_FileType = FileType;
                        ServerSavePath = Path.Combine(Path.Combine(filePath, Path.GetFileName(excelfile.FileName)));
                        Obj_Model.ServerPath = ServerSavePath;
                    }

                    string fileLocation = ServerSavePath;
                    //Delete the file if already exist in same name.
                    if (System.IO.File.Exists(fileLocation))
                    {
                        System.IO.File.Delete(fileLocation);
                    }
                    //Save file to server folder.  
                    excelfile.SaveAs(ServerSavePath);

                    //result = serviceObj.SavePoUploadData(Obj_Model);


                    string excelConnectionString = string.Empty;
                    excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=PathToExcelFileWithExtension;Extended Properties='Excel 8.0;HDR=YES'";
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    else if (fileExtension == ".xlsx")
                    {
                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);//connection for excel.
                    excelConnection.Close();//excel connection close.
                    excelConnection.Open();//excel connection open.
                    DataTable dtexcel = new DataTable();//datatable object.
                    dtexcel = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dtexcel == null)
                    {
                        return null;
                    }
                    String[] excelSheets = new String[dtexcel.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dtexcel.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    int excel = excelSheets.Count();
                    if (excel == 0)
                    {
                        result = "invalid file";
                        excelConnection.Close();
                    }
                    else
                    {
                        string query = string.Format("Select * from [{0}]", excelSheets[0]);
                        using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection))
                        {
                            dataAdapter.Fill(ds);
                        }
                        DataTable dtexceldata = new DataTable();
                        if (ds.Tables.Count > 0)
                        {
                            dtexceldata = ds.Tables[0]; //Excel Data
                            //code for removing invalid empty rows from datatable.
                            //dtexceldata = dtexceldata.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field =>
                            //field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
                        }
                        if (dtexceldata.Rows.Count == 0)
                        {
                            result = "mandatory fields empty";
                            excelConnection.Close();
                            return Json(result, JsonRequestBehavior.AllowGet);
                        }

                        if (dtexceldata.Columns[0].ToString().Trim().ToLower() == "po number" && dtexceldata.Columns[1].ToString().Trim().ToLower() == "po header description" && dtexceldata.Columns[2].ToString().Trim().ToLower() == "po period from" &&
                            dtexceldata.Columns[3].ToString().Trim().ToLower() == "po period to" && dtexceldata.Columns[4].ToString().Trim().ToLower() == "vendor code" && dtexceldata.Columns[5].ToString().Trim().ToLower() == "vendor contact code" &&
                            dtexceldata.Columns[6].ToString().Trim().ToLower() == "po type" && dtexceldata.Columns[7].ToString().Trim().ToLower() == "po request type" && dtexceldata.Columns[8].ToString().Trim().ToLower() == "raiser code" &&
                            dtexceldata.Columns[9].ToString().Trim().ToLower() == "po line item description" && dtexceldata.Columns[10].ToString().Trim().ToLower() == "po line item quantity" && dtexceldata.Columns[11].ToString().Trim().ToLower() == "po line item unit price" &&
                            dtexceldata.Columns[12].ToString().Trim().ToLower() == "po total amount" && dtexceldata.Columns[13].ToString().Trim().ToLower() == "po product service code" && dtexceldata.Columns[14].ToString().Trim().ToLower() == "uom code" &&
                            dtexceldata.Columns[15].ToString().Trim().ToLower() == "po shipment type" && dtexceldata.Columns[16].ToString().Trim().ToLower() == "po shipment branch id" && dtexceldata.Columns[17].ToString().Trim().ToLower() == "po shipment employee" &&
                            dtexceldata.Columns[18].ToString().Trim().ToLower() == "grn inward doc no" && dtexceldata.Columns[19].ToString().Trim().ToLower() == "grn inward invoice no" && dtexceldata.Columns[20].ToString().Trim().ToLower() == "grn inward received qty" &&
                            dtexceldata.Columns[21].ToString().Trim().ToLower() == "asset sl no" && dtexceldata.Columns[22].ToString().Trim().ToLower() == "put to used date" && dtexceldata.Columns[23].ToString().Trim().ToLower() == "manufacture name" &&
                            dtexceldata.Columns[24].ToString().Trim().ToLower() == "po details gst applicable" && dtexceldata.Columns[25].ToString().Trim().ToLower() == "hsn code" &&
                            dtexceldata.Columns[26].ToString().Trim().ToLower() == "provider location" && dtexceldata.Columns[27].ToString().Trim().ToLower() == "reciver location" && dtexceldata.Columns[28].ToString().Trim().ToLower() == "cwip release branch code" &&
                            dtexceldata.Columns[29].ToString().Trim().ToLower() == "cwip release employee code")
                        {

                            Int64 File_Gid = serviceObj.FileInst_Data(Obj_Model);
                            Int64 TruncateTemp = serviceObj.TruncateTempData(Obj_Model);
                            if (File_Gid > 0)
                            {
                                int Clear_Count = 0;

                                for (int j = 0; j < dtexceldata.Rows.Count; j++)
                                {
                                    Obj_Model.File_Gid = File_Gid;
                                    Obj_Model.Poheader_pono = dtexceldata.Rows[j]["po number"].ToString().Trim();
                                    Obj_Model.poheader_desc = dtexceldata.Rows[j]["po header description"].ToString().Trim();
                                    string periodfrom = dtexceldata.Rows[j]["po period from"].ToString().Trim();
                                    if (periodfrom == "" || periodfrom == null)
                                    {
                                        periodfrom = null;
                                    }
                                    else
                                    {

                                        DateTime dt;
                                        if (DateTime.TryParse(periodfrom, out dt))
                                        {
                                            periodfrom = dt.ToString("yyyy-MM-dd");
                                        }
                                        else
                                        {
                                            result = "PO Period From Date Format is Invalid";                                          
                                            break;
                                        }
                                        //DateTime dt = Convert.ToDateTime(periodfrom);
                                        //periodfrom = dt.ToString("yyyy-MM-dd");
                                    }
                                    Obj_Model.poheader_period_from = periodfrom;

                                    string periodto = dtexceldata.Rows[j]["po period to"].ToString().Trim();
                                    if (periodto == "" || periodto == null)
                                    {
                                        periodto = null;
                                    }
                                    else
                                    {
                                        DateTime dt;
                                        if (DateTime.TryParse(periodto, out dt))
                                        {
                                            periodto = dt.ToString("yyyy-MM-dd");
                                        }
                                        else
                                        {
                                            result = "PO Period To Date Format is Invalid";                                           
                                            break;
                                        }
                                        //DateTime dt = Convert.ToDateTime(periodto);
                                        //periodto = dt.ToString("yyyy-MM-dd");
                                    }
                                    Obj_Model.poheader_period_to = periodto;

                                    Obj_Model.poheader_vendor_code = dtexceldata.Rows[j]["vendor code"].ToString().Trim();
                                    Obj_Model.poheader_vendor_contact_code = dtexceldata.Rows[j]["vendor contact code"].ToString().Trim();
                                    Obj_Model.poheader_type = dtexceldata.Rows[j]["po type"].ToString().Trim();
                                    Obj_Model.poheader_request = dtexceldata.Rows[j]["po request type"].ToString().Trim();
                                    Obj_Model.poheader_raiser_code = dtexceldata.Rows[j]["raiser code"].ToString().Trim();
                                    Obj_Model.podts_item_desc = dtexceldata.Rows[j]["po line item description"].ToString().Trim();
                                    Double POItem_Qty = 0.00;
                                    if (dtexceldata.Rows[j]["po line item quantity"].ToString().Trim() == "")
                                    {
                                        Obj_Model.podts_item_Qty = "0";
                                    }
                                    else
                                    {
                                        POItem_Qty = Convert.ToDouble(dtexceldata.Rows[j]["po line item quantity"].ToString().Trim());
                                        Obj_Model.podts_item_Qty = POItem_Qty.ToString("0.00");
                                    }
                                    Double UnitPrice = 0.00;
                                    if (dtexceldata.Rows[j]["po line item unit price"].ToString().Trim() == "")
                                    {
                                        Obj_Model.podts_unit_price = "0";
                                    }
                                    else
                                    {
                                        UnitPrice = Convert.ToDouble(dtexceldata.Rows[j]["po line item unit price"].ToString().Trim());
                                        Obj_Model.podts_unit_price = UnitPrice.ToString("0.00");
                                    }
                                    Double Item_Total = 0.00;
                                    if (dtexceldata.Rows[j]["po total amount"].ToString().Trim() == "")
                                    {
                                        Obj_Model.podts_item_total = "0";
                                    }
                                    else
                                    {
                                        Item_Total = Convert.ToDouble(dtexceldata.Rows[j]["po total amount"].ToString().Trim());
                                        Obj_Model.podts_item_total = Item_Total.ToString("0.00");
                                    }

                                    Obj_Model.podts_service_code = dtexceldata.Rows[j]["po product service code"].ToString().Trim();
                                    Obj_Model.podts_uom_code = dtexceldata.Rows[j]["uom code"].ToString().Trim();
                                    Obj_Model.podts_shipment_type = dtexceldata.Rows[j]["po shipment type"].ToString().Trim();
                                    Obj_Model.podts_shipment_branch_code = dtexceldata.Rows[j]["po shipment branch id"].ToString().Trim();
                                    Obj_Model.podts_shipment_emp_code = dtexceldata.Rows[j]["po shipment employee"].ToString().Trim();
                                    Obj_Model.grninwrdheader_dcno = dtexceldata.Rows[j]["grn inward doc no"].ToString().Trim();
                                    Obj_Model.grninwrdheader_invoiceno = dtexceldata.Rows[j]["grn inward invoice no"].ToString().Trim();
                                    Double Inward_Qty = 0.00;
                                    if (dtexceldata.Rows[j]["grn inward received qty"].ToString().Trim() == "")
                                    {
                                        Obj_Model.grn_inward_qty = "0";
                                    }
                                    else
                                    {
                                        Inward_Qty = Convert.ToDouble(dtexceldata.Rows[j]["grn inward received qty"].ToString().Trim());
                                        Obj_Model.grn_inward_qty = Inward_Qty.ToString("0.00");
                                    }


                                    Obj_Model.asset_sl_no = dtexceldata.Rows[j]["asset sl no"].ToString().Trim();

                                    string PutUseDate = dtexceldata.Rows[j]["put to used date"].ToString().Trim();

                                    if (PutUseDate == "" || PutUseDate == null)
                                    {
                                        PutUseDate = null;
                                    }
                                    else
                                    {
                                        DateTime dt;
                                        if (DateTime.TryParse(PutUseDate, out dt))
                                        {
                                            PutUseDate = dt.ToString("yyyy-MM-dd");
                                        }
                                        else
                                        {
                                            result = "PO PutUse Date Format is Invalid";
                                            break;
                                        }
                                        //DateTime dt = Convert.ToDateTime(PutUseDate);
                                        //PutUseDate = dt.ToString("yyyy-MM-dd");
                                    }
                                    Obj_Model.grn_putuseddate = PutUseDate;
                                    Obj_Model.grn_mft_name = dtexceldata.Rows[j]["manufacture name"].ToString().Trim();
                                    Obj_Model.podts_gst_applicable = dtexceldata.Rows[j]["po details gst applicable"].ToString().Trim();
                                    Obj_Model.HSN_Code = dtexceldata.Rows[j]["hsn code"].ToString().Trim();

                                    //Double Tax_Amount = 0.00;
                                    Obj_Model.podts_tax_amount = "0";
                                    //if (dtexceldata.Rows[j][26].ToString().Trim()=="")
                                    //{
                                    //    Obj_Model.podts_tax_amount = "0";
                                    //}
                                    //else
                                    //{
                                    //    Tax_Amount = Convert.ToDouble(dtexceldata.Rows[j][26].ToString().Trim());
                                    //    Obj_Model.podts_tax_amount = Tax_Amount.ToString("0.00");
                                    //}

                                    Obj_Model.provider_loc = dtexceldata.Rows[j]["provider location"].ToString().Trim();
                                    Obj_Model.receiver_loc = dtexceldata.Rows[j]["reciver location"].ToString().Trim();
                                    Obj_Model.cwiprelease_branch_code = dtexceldata.Rows[j]["cwip release branch code"].ToString().Trim();
                                    Obj_Model.cwiprelease_emp_code = dtexceldata.Rows[j]["cwip release employee code"].ToString().Trim();

                                    DataTable dt_data = serviceObj.SaveTempPOUploadData(Obj_Model);

                                    for (int i = 0; i < dt_data.Rows.Count; i++)
                                    {
                                        Clear_Count = Convert.ToInt16(dt_data.Rows[i]["Clear"]);
                                        result = "inserted";
                                    }
                                }
                                return Json(result, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                result = "PO Template File Not Stored.";
                                return Json(result, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            result = "invalid header name";
                            return Json(result, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    result = "invalid file";
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = "failed";
                log.Error(ex.ToString());
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult Upload_FileClear()
        {
            string result = "";
            Int64 TruncateTemp = serviceObj.TruncateTempData(Obj_Model);
            result = "1";
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Upload_FileConfirm()
        {
            try
            {
                Obj_Model.LoginId = Convert.ToInt64(Session["Employee_Gid"]);
                dt = serviceObj.SetFileConfirm_Data(Obj_Model);
                return Json(JsonConvert.SerializeObject(dt), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}