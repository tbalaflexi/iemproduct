﻿using System.Web.Mvc;

namespace IEMProduct
{
    public class FAAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "FA";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "FA_default",
                "FA/{controller}/{action}/{id}",
                new { controller = "FAHome", action = "FAHome", id = UrlParameter.Optional },
                new string[] { "FA.Controllers" }
            );
        }
    }
}