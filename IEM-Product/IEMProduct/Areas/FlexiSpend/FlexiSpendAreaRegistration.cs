﻿using System.Web.Mvc;

namespace IEMProduct
{
    public class FlexiSpendAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "FlexiSpend";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "FlexiSpend_default",
                "FlexiSpend/{controller}/{action}/{id}",
                new { controller = "FlexiSpendHome", action = "FlexiSpendHome", id = UrlParameter.Optional },
                new string[] { "FlexiSpend.Controllers" }
            );
        }
    }
}