﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccessHandler.Models.FlexiSpend;
using FlexiSpend.Service;
using EOW.Service;
using DataAccessHandler.Models.EOW;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Data;
using Newtonsoft.Json;
using Excel;
using ClosedXML.Excel;
using IEM.Areas.FLEXISPEND.CreatePDF;
using System.IO;
using System.Text;
using System.Security.Cryptography;
namespace FlexiSpend.Controllers
{
    public class EFTController : Controller
    {
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(PaymentRunController));
        EFTFileGeneration_Model objModel = new EFTFileGeneration_Model();
        PaymentRun_Service ObjPayRun_Service = new PaymentRun_Service();
        Claim_Service objClaim_Service = new Claim_Service();
        EFTFileGeneration_Service Objservice = new EFTFileGeneration_Service();
        HtmlViewRenderer ObjHtmlViewRenderer = new HtmlViewRenderer();
        DataTable dt = new DataTable();
        // GET: EFT
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult EFT()
        {
            return View();
        }

        public ActionResult Read_PayBank()
        {
            List<EFTFileGeneration_Model> lst = new List<EFTFileGeneration_Model>();
            try
            {
                lst = Objservice.Read_PayBank();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Get_PayMode(string Master, string Master_Code, Int64 Master_Gid, string EmpName)
        {
            List<Claim_Model> lst = new List<Claim_Model>();
            try
            {
                //lst = objService.Get_DropDown_List(Master, Master_Code, Master_Gid);
                lst = objClaim_Service.Get_DropDown_List(Master, Master_Code, Master_Gid, EmpName);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            // return Json(lst, JsonRequestBehavior.AllowGet);
            var jsonResult = Json(lst, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public ActionResult Get_ClaimType()
        {
            List<EFTFileGeneration_Model> lst = new List<EFTFileGeneration_Model>();
            try
            {
                lst = Objservice.Get_ClaimType();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Read_EFTGrid([DataSourceRequest] DataSourceRequest request, EFTFileGeneration_Model ObjModel)
        {
            try
            {
                Int64 User_Gid = Convert.ToInt64(Session["Employee_Gid"]);

                return Json(Objservice.Read_EFTGrid(ObjModel, User_Gid).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }
        }
        public ActionResult SetEFTMemoDetails(EFTFileGeneration_Model ObjModel)
        {
            try
            {
                string Result = string.Empty;
                Int64 Login_User_Gid = 0;
                try
                {
                    Login_User_Gid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                    dt = Objservice.SetEFTMemoDetails(ObjModel, Login_User_Gid);
                    Result = JsonConvert.SerializeObject(dt);
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                }
                return Json(Result, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return null;
            }
        }

        #region Generate Memo and Online File

        public void GenerateNotePadFile(string NormalFile, string Encrypted, DataTable dt)
        {
            //string delemited = ",";
            //NORMAL FILE -- Create Notepad
            //clear the previous data's
            string textToEncrypt = "";
            FileStream fs = new FileStream(NormalFile, FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter _fsstr = new StreamWriter(fs);
            _fsstr.WriteLine(string.Empty);
            _fsstr.Flush(); _fsstr.Close();

            StringBuilder sb = new StringBuilder();



            //update the notepad content for downloading.
            if (dt.Rows.Count > 0)
            {
                StreamWriter str = new StreamWriter(NormalFile, false, System.Text.Encoding.Default);
                foreach (DataRow datarow in dt.Rows)
                {
                    string row = string.Empty;
                    row = datarow["TransactionType"].ToString() + "," + datarow["SupplierCode"].ToString() + "," + datarow["AccNo"].ToString() + ","
                        + datarow["PVAmount"].ToString() + "," + datarow["SupplierName"].ToString() + ",,,,,,,,," + datarow["PVNo"].ToString() + ",,,,,,,,,"
                        + datarow["FileSpoolingDate"].ToString() + ",," + datarow["IFSCCode"].ToString() + "," + datarow["Bankname"].ToString() + ",,";
                    str.WriteLine(row);
                    sb.AppendLine(row);
                }

                textToEncrypt = sb.ToString();
                str.Flush();
                str.Close();
            }
            //string textToEncrypt = File.ReadAllText(NormalFile); 
            //ENCRYPT FILE -- Create Notepad
            //clear the previous data's
            if (Encrypted != "")
            {
                FileStream efs = new FileStream(Encrypted, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter _efsstr = new StreamWriter(efs);
                _efsstr.WriteLine(string.Empty);
                _efsstr.Flush(); _efsstr.Close();
                string EncryptedText = EncryptedData(textToEncrypt);
                StreamWriter str = new StreamWriter(Encrypted, false, System.Text.Encoding.Default);
                //update the notepad content for downloading.
                /* if (dt.Rows.Count > 0)
                 {
                     StreamWriter str = new StreamWriter(Encrypted, false, System.Text.Encoding.Default);
                     foreach (DataRow datarow in dt.Rows)
                     {
                         string row = string.Empty;
                         row = datarow["TransactionType"].ToString() + "," + datarow["SupplierCode"].ToString() + "," + datarow["AccNo"].ToString() + ","
                             + datarow["PVAmount"].ToString() + "," + datarow["SupplierName"].ToString() + ",,,,,,,,," + datarow["PVNo"].ToString() + ",,,,,,,,,"
                             + datarow["FileSpoolingDate"].ToString() + ",," + datarow["IFSCCode"].ToString() + "," + datarow["Bankname"].ToString() + ",,";
                         str.WriteLine(row);
                     }*/
                str.WriteLine(EncryptedText);
                str.Flush();
                str.Close();
                //}
            }
        }
        public string IsCheckFolder(string Date, string SerialNo, string PayMode, string SubFolder, string BankName, string Mode)
        {
            try
            {
                bool _MFolder = false, _DFolder = false, _SerFolder = false, _SBFolder = false, _SBFolder1 = false, _SUBFolder2 = false;
                string _mainFolder = "", _dateFolder = "", _serialFolder = "", _subFolder = "", _subFolder1 = "", _subFolder2 = "";

                string DownloadMemoUrl = System.Configuration.ConfigurationManager.AppSettings["DownloadMemoLocal"].ToString();
                //Create Root Folder
                _mainFolder = DownloadMemoUrl;
                _MFolder = FolderCreation(_mainFolder);

                //Create Date Folder
                _dateFolder = string.Format("{0}/{1}", _mainFolder, Date);
                _DFolder = FolderCreation(_dateFolder);

                //Create Serial No Folder
                _serialFolder = string.Format("{0}/{1}", _dateFolder, SerialNo);
                _SerFolder = FolderCreation(_serialFolder);

                if ((PayMode.ToLower().Trim() == "eft" || PayMode.ToLower().Trim() == "era") && SubFolder == "0")
                {
                    _subFolder1 = string.Format("{0}/{1}", _serialFolder, "Online");
                    _SBFolder1 = FolderCreation(_subFolder1);
                }
                else if (PayMode.ToLower().Trim() == "dd")
                {
                    _subFolder1 = string.Format("{0}/{1}", _serialFolder, "DD");
                    _SBFolder1 = FolderCreation(_subFolder1);
                }
                else if (PayMode.ToLower().Trim() == "rrp")
                {
                    _subFolder1 = string.Format("{0}/{1}", _serialFolder, "RRP");
                    _SBFolder1 = FolderCreation(_subFolder1);
                }
                else
                {
                    _subFolder = string.Format("{0}/{1}", _serialFolder, "MEMO");
                    _SBFolder = FolderCreation(_subFolder);

                    //Create Memo Folder
                    _subFolder2 = string.Format("{0}/{1}", _subFolder, PayMode);
                    _SUBFolder2 = FolderCreation(_subFolder2);

                    if (PayMode.ToLower().Trim() == "era")
                    {
                        if (BankName.ToLower().Contains("citi"))
                        {
                            _subFolder1 = string.Format("{0}/{1}", _subFolder2, "CITI");
                            _SBFolder1 = FolderCreation(_subFolder1);
                        }
                        else if (BankName.ToLower().Contains("icici"))
                        {
                            _subFolder1 = string.Format("{0}/{1}", _subFolder2, "ICICI");
                            _SBFolder1 = FolderCreation(_subFolder1);
                        }
                        else if (BankName.ToLower().Contains("sbi") || BankName.ToUpper().Contains("STATE BANK OF INDIA"))
                        {
                            _subFolder1 = string.Format("{0}/{1}", _subFolder2, "SBI");
                            _SBFolder1 = FolderCreation(_subFolder1);
                        }
                        else if (BankName.ToLower().Contains("uti") || BankName.ToUpper().Contains("AXIS"))
                        {
                            _subFolder1 = string.Format("{0}/{1}", _subFolder2, "AXIS");
                            _SBFolder1 = FolderCreation(_subFolder1);
                        }
                        else
                        {
                            _subFolder1 = string.Format("{0}/{1}", _subFolder2, "HDFC");
                            _SBFolder1 = FolderCreation(_subFolder1);
                        }
                    }
                    else if (PayMode.ToLower().Trim() == "eft")
                    {
                        if (Mode.ToLower().Contains("rtgs"))
                        {
                            _subFolder1 = string.Format("{0}/{1}", _subFolder2, "HDFC - RTGS");
                            _SBFolder1 = FolderCreation(_subFolder1);
                        }
                        else if (Mode.ToLower().Contains("neft"))
                        {
                            _subFolder1 = string.Format("{0}/{1}", _subFolder2, "HDFC - NEFT");
                            _SBFolder1 = FolderCreation(_subFolder1);
                        }
                    }
                }

                return _subFolder1;
            }
            catch
            {
                return "";
            }
        }

        public bool FolderCreation(string FolderName)
        {
            try
            {
                DirectoryInfo dir = new DirectoryInfo(FolderName);

                //check folder Presence
                if (!dir.Exists)
                {
                    dir.Create();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public EFTFileGeneration_Model GetMemoDetailsPrint(DataTable dtDet, DataTable dtRem)
        {
            EFTFileGeneration_Model rec = new EFTFileGeneration_Model();
            DataTable dt = null;
            if (dtDet != null)
            {
                //Top Details
                rec.Mode = dtDet.Rows[0]["TransactionType"].ToString();
                rec.AccountType = dtDet.Rows[0]["AccountType"].ToString();
                rec.BranchDetails = dtDet.Rows[0]["branchCode"].ToString() != "" ? dtDet.Rows[0]["branchCode"].ToString() : dtDet.Rows[0]["branchName"].ToString();
                rec.MemoNo = dtDet.Rows[0]["MemoNo"].ToString();
                rec.MemoTime = dtDet.Rows[0]["Time"].ToString();
                rec.MemoDate = dtDet.Rows[0]["Date"].ToString();

                //Beneficiary Details
                if (dtRem.Rows.Count > 1)
                {
                    rec.BenName = "AS PER LIST";
                    rec.BenAccNo = "AS PER LIST";
                    rec.BenAddress = "AS PER LIST";
                    rec.BenBankname = "AS PER LIST";
                    rec.BenBankIfscCode = "AS PER LIST";
                    rec.AmountInWords = "AS PER LIST";
                    rec.AmountInFigures = "AS PER LIST";
                }
                else
                {
                    rec.BenName = dtDet.Rows[0]["BenName"].ToString();
                    rec.BenAccNo = dtDet.Rows[0]["BenAccNo"].ToString();
                    rec.BenAddress = dtDet.Rows[0]["BenAddress"].ToString();
                    rec.BenBankname = dtDet.Rows[0]["BenBankname"].ToString();
                    rec.BenBankIfscCode = dtDet.Rows[0]["BenBankIfscCode"].ToString();
                    rec.AmountInWords = dtDet.Rows[0]["AmountInWords"].ToString();
                    rec.AmountInFigures = dtDet.Rows[0]["Amount"].ToString();
                }

                //Our Details
                if (dtRem.Rows.Count > 1)
                {
                    rec.RemitterName = "AS PER LIST";
                    rec.RemitterAccNo = "AS PER LIST";
                    rec.RemitterCashDeposited = "AS PER LIST";
                    rec.RemitterMobilePhoneNo = "AS PER LIST";
                    rec.RemitterEmailId = "AS PER LIST";
                    rec.RemitterAddress = "AS PER LIST";
                    rec.Remarks = "AS PER LIST";
                }
                else
                {
                    rec.RemitterName = dtDet.Rows[0]["RemitterName"].ToString();
                    rec.RemitterAccNo = dtDet.Rows[0]["RemitterAccNo"].ToString();
                    rec.RemitterCashDeposited = dtDet.Rows[0]["RemitterCashDeposited"].ToString();
                    rec.RemitterMobilePhoneNo = dtDet.Rows[0]["RemitterMobilePhoneNo"].ToString();
                    rec.RemitterEmailId = dtDet.Rows[0]["RemitterEmailId"].ToString();
                    rec.RemitterAddress = dtDet.Rows[0]["RemitterAddress"].ToString();
                    rec.Remarks = dtDet.Rows[0]["Remarks"].ToString();
                }

                dt = dtRem;

                List<EFTFileGeneration_Model> childArray = new List<EFTFileGeneration_Model>();
                if (dt != null && dt.Rows.Count > 1)
                {
                    foreach (DataRow cdr in dt.Rows)
                    {
                        EFTFileGeneration_Model child = new EFTFileGeneration_Model();
                        child.NameOfVendor = cdr["BenName"].ToString();
                        child.BankAccNo = cdr["BenAccNo"].ToString();
                        child.BankName = cdr["BenBankname"].ToString();
                        child.IFSCCode = cdr["BenBankIfscCode"].ToString();
                        child.Amount = cdr["Amount"].ToString() == "" ? "0" : cdr["Amount"].ToString();
                        child.RemitterDetails = cdr["RemitterDetails"].ToString();
                        childArray.Add(child);
                    }
                    rec.TotalAmount = childArray.Sum(item => Convert.ToDecimal(item.Amount));
                }
                else
                {
                    childArray = null;
                    string _tmpVal = "";
                    _tmpVal = dtDet.Rows[0]["Amount"].ToString() == "" ? "0" : dtDet.Rows[0]["Amount"].ToString();
                    rec.TotalAmount = Convert.ToDecimal(_tmpVal);
                }

                rec.DetailArray = childArray;

            }
            return rec;
        }
        public string EncryptedData(string DataToEncrypt)
        {
            string EncryptionKey = "abc123";
            string encrypteddata = string.Empty;
            try
            {
                byte[] clearBytes = System.Text.Encoding.Unicode.GetBytes(DataToEncrypt);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        encrypteddata = Convert.ToBase64String(ms.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                encrypteddata = "";
            }
            return encrypteddata;
        }
        public JsonResult GenerateDocuments(EFTFileGeneration_Model ObjGenModel)
        {
            Int64 LoginUserId = Convert.ToInt64(Session["Employee_Gid"]);
            string IsFileGenerated = "";
            string _Date = "", _SerialNo = "", _PayMode = "", _ViewType = "", _BankName = "";
            _PayMode = ObjGenModel.Pay_paymodemode;
            _ViewType = ObjGenModel.ViewType.ToString() == "" || ObjGenModel.ViewType.ToString() == null ? "0" : ObjGenModel.ViewType.ToString();

            DataSet ds = Objservice.PrintEFTMemoDetails(ObjGenModel, LoginUserId);
            if (ds != null)
            {
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    _Date = ds.Tables[0].Rows[0]["CurrentDate"].ToString();
                    _SerialNo = ds.Tables[0].Rows[0]["SerialNo"].ToString();
                    if (_PayMode.ToLower().Trim() != "rrp")
                        _BankName = ds.Tables[0].Rows[0]["PayBankname"].ToString();
                }

                if (_Date != string.Empty && _SerialNo != string.Empty)
                {
                    IsFileGenerated = IsCheckFolder(_Date, _SerialNo, _PayMode, _ViewType, _BankName, "");
                }

                //Generate Online Template for EFT Template.
                if (_PayMode.ToLower().Trim() == "eft" && _ViewType == "0" && IsFileGenerated != string.Empty)
                {
                    string _NormalFile = "", _EncryptedFile = "";
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        _NormalFile = string.Format("{0}/{1}.txt", IsFileGenerated, ds.Tables[0].Rows[0]["NormalTextFile"].ToString());

                        /* @@@@@@@@@@@@@@@@@@@@ ENCRYPTION @@@@@@@@@@@@@@@@@@@@@@@@*/
                        //string _encrypt = EncryptedFile(_NormalFile);
                        /*   DirectoryInfo dir = new DirectoryInfo(_encrypt);

                           //check folder Presence
                           if (dir.Exists)
                           {
                               _EncryptedFile = string.Format("{0}/{1}~{2}~Online~{3}.txt", _encrypt, _Date, _SerialNo, ds.Tables[0].Rows[0]["Encrypted"].ToString());
                           }
                           else
                           {
                               _EncryptedFile = "";
                           }*/
                        //_EncryptedFile = string.Format("{0}/{1}.txt", IsFileGenerated, ds.Tables[0].Rows[0]["NormalTextFile"].ToString());
                        _EncryptedFile = string.Format("{0}/{1}.txt", IsFileGenerated, ds.Tables[0].Rows[0]["Encrypted"].ToString());
                    }

                    GenerateNotePadFile(_NormalFile, _EncryptedFile, ds.Tables[1]);
                }

                //Ramya added for Employee Claim
                //Generate Online Template for EFT Template.
                if (_PayMode.ToLower().Trim() == "era" && _ViewType == "0" && IsFileGenerated != string.Empty)
                {
                    string _NormalFile = "", _EncryptedFile = "";
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        _NormalFile = string.Format("{0}/{1}.txt", IsFileGenerated, ds.Tables[0].Rows[0]["NormalTextFile"].ToString());
                        //string EncryptMemoDownloadUrl=System.Configuration.ConfigurationManager.AppSettings["MemoLocalEncrypt"].ToString();

                        //string _encrypt = EncryptedFile(_NormalFile);
                        /* DirectoryInfo dir = new DirectoryInfo(_encrypt);

                         //check folder Presence
                         if (dir.Exists)
                         {
                             _EncryptedFile = string.Format("{0}/{1}~{2}~Online~{3}.txt", _encrypt, _Date, _SerialNo, ds.Tables[0].Rows[0]["Encrypted"].ToString());
                         }
                         else
                         {
                             _EncryptedFile = "";
                         }*/
                        //_EncryptedFile = string.Format("{0}/{1}~{2}~Online~{3}.txt", _encrypt, _Date, _SerialNo, ds.Tables[0].Rows[0]["Encrypted"].ToString());
                        _EncryptedFile = string.Format("{0}/{1}.txt", IsFileGenerated, ds.Tables[0].Rows[0]["Encrypted"].ToString());
                    }

                    GenerateNotePadFile(_NormalFile, _EncryptedFile, ds.Tables[1]);
                }

                //EFT Template Work [RTGS/NEFT]
                if (_PayMode.ToLower().Trim() == "eft" && _ViewType == "1")
                {
                    XLWorkbook xl = null;
                    if (ds.Tables[1].Rows.Count > 0)
                    {

                        DataTable dt = new DataTable();
                        dt.Columns.Add("SL No", typeof(string));
                        dt.Columns.Add("Name of Vendor", typeof(string));
                        dt.Columns.Add("Account No", typeof(string));
                        dt.Columns.Add("Bank Name", typeof(string));
                        dt.Columns.Add("IFSC Code", typeof(string));
                        dt.Columns.Add("Amount", typeof(string));
                        for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                        {
                            dt.Rows.Add(new object[] { i + 1, ds.Tables[1].Rows[i]["BenName"].ToString(), ds.Tables[1].Rows[i]["BenAccNo"].ToString(), ds.Tables[1].Rows[i]["BenBankname"].ToString(), ds.Tables[1].Rows[i]["BenBankIfscCode"].ToString(), ds.Tables[1].Rows[i]["Amount"].ToString() });
                        }

                        EFTFileGeneration_Model result = new EFTFileGeneration_Model();
                        result = GetMemoDetailsPrint(ds.Tables[1], ds.Tables[1]);

                        IsFileGenerated = IsCheckFolder(_Date, _SerialNo, _PayMode, _ViewType, _BankName, "NEFT");

                        xl = new XLWorkbook();
                        xl.Worksheets.Add(dt, "HDFC_NEFT");
                        xl.SaveAs(string.Format("{0}/HDFC_NEFT.xlsx", IsFileGenerated));

                        //save the DD Content to local folder.
                        string fileName = "";
                        fileName = string.Format("{0}/NEFT.html", IsFileGenerated);

                        // Render the view html to a string.
                        string htmlText = ObjHtmlViewRenderer.RenderViewToString(this, "TmpRTGS_HDFC_A_NEW", result);

                        //save the document as html.
                        using (FileStream fs = new FileStream(fileName, FileMode.Create))
                        {
                            using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                            {
                                w.Write(htmlText);
                            }
                        }
                        // Let the html be rendered into a PDF document through iTextSharp.
                        //byte[] buffer = standardPdfRenderer.Render(htmlText, "");

                        //using (FileStream fs = new FileStream(fileName, FileMode.Create))
                        //{
                        //    fs.Write(buffer, 0, buffer.Length);
                        //}
                    }

                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.Add("SL No", typeof(string));
                        dt.Columns.Add("Name of Vendor", typeof(string));
                        dt.Columns.Add("Account No", typeof(string));
                        dt.Columns.Add("Bank Name", typeof(string));
                        dt.Columns.Add("IFSC Code", typeof(string));
                        dt.Columns.Add("Amount", typeof(string));
                        for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
                        {
                            dt.Rows.Add(new object[] { i + 1, ds.Tables[2].Rows[i]["BenName"].ToString(), ds.Tables[2].Rows[i]["BenAccNo"].ToString(), ds.Tables[2].Rows[i]["BenBankname"].ToString(), ds.Tables[2].Rows[i]["BenBankIfscCode"].ToString(), ds.Tables[2].Rows[i]["Amount"].ToString() });
                        }

                        EFTFileGeneration_Model result = new EFTFileGeneration_Model();
                        result = GetMemoDetailsPrint(ds.Tables[2], ds.Tables[2]);

                        IsFileGenerated = IsCheckFolder(_Date, _SerialNo, _PayMode, _ViewType, _BankName, "RTGS");

                        //save the DD Content to local folder.
                        string fileName = "";
                        fileName = string.Format("{0}/RTGS.html", IsFileGenerated);

                        xl = new XLWorkbook();
                        xl.Worksheets.Add(dt, "HDFC_NEFT");
                        xl.SaveAs(string.Format("{0}/HDFC_NEFT.xlsx", IsFileGenerated));

                        // Render the view html to a string.
                        string htmlText = ObjHtmlViewRenderer.RenderViewToString(this, "TmpRTGS_HDFC_A_NEW", result);

                        //save the document as html.
                        using (FileStream fs = new FileStream(fileName, FileMode.Create))
                        {
                            using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                            {
                                w.Write(htmlText);
                            }
                        }
                        // Let the html be rendered into a PDF document through iTextSharp.
                        //byte[] buffer = standardPdfRenderer.Render(htmlText, "");

                        //using (FileStream fs = new FileStream(fileName, FileMode.Create))
                        //{
                        //    fs.Write(buffer, 0, buffer.Length);
                        //}
                    }
                }

                //DD Template Work
                if (_PayMode.ToLower().Trim() == "dd" && IsFileGenerated != string.Empty)
                {
                    XLWorkbook xl = null;
                    DataTable dt = new DataTable();
                    dt.Columns.Add("SL No", typeof(string));
                    dt.Columns.Add("DD Favoring", typeof(string));
                    dt.Columns.Add("Payable at", typeof(string));
                    dt.Columns.Add("Amount", typeof(string));

                    for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                    {
                        dt.Rows.Add(new object[] { i + 1, ds.Tables[1].Rows[i]["DDFavoring"].ToString(), ds.Tables[1].Rows[i]["PayableAt"].ToString(), ds.Tables[1].Rows[i]["Amount"].ToString() });
                    }
                    DDTemplate result = new DDTemplate();
                    result = GetDDTemplate(ds.Tables[0], ds.Tables[1]);

                    xl = new XLWorkbook();
                    xl.Worksheets.Add(dt, "HDFC_DD");
                    xl.SaveAs(string.Format("{0}/HDFC_DD.xlsx", IsFileGenerated));

                    //save the DD Content to local folder.
                    string fileName = "";
                    fileName = string.Format("{0}/HDFC_DD.html", IsFileGenerated);

                    // Render the view html to a string.
                    string htmlText = ObjHtmlViewRenderer.RenderViewToString(this, "TmpDD_HDFC", result);

                    //save the document as html.
                    using (FileStream fs = new FileStream(fileName, FileMode.Create))
                    {
                        using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                        {
                            w.Write(htmlText);
                        }
                    }
                    // Let the html be rendered into a PDF document through iTextSharp.
                    //byte[] buffer = standardPdfRenderer.Render(htmlText, "");

                    //using (FileStream fs = new FileStream(fileName, FileMode.Create))
                    //{
                    //    fs.Write(buffer, 0, buffer.Length);
                    //}
                }

                if (_PayMode.ToLower().Trim() == "rrp" && IsFileGenerated != string.Empty)
                {
                    XLWorkbook xl = null;
                    DataTable dt = new DataTable();
                    dt.Columns.Add("SL No", typeof(string));
                    dt.Columns.Add("Pay Number", typeof(string));
                    dt.Columns.Add("Employee Code", typeof(string));
                    dt.Columns.Add("Employee Name", typeof(string));
                    dt.Columns.Add("Location", typeof(string));
                    dt.Columns.Add("Amount", typeof(string));
                    dt.Columns.Add("ECF No", typeof(string));

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        dt.Rows.Add(new object[] { i + 1, ds.Tables[0].Rows[i]["PVNo"].ToString(), ds.Tables[0].Rows[i]["EmployeeSupplierCode"].ToString(), ds.Tables[0].Rows[i]["EmployeeSupplierName"].ToString()
                        , ds.Tables[0].Rows[i]["Location"].ToString(), ds.Tables[0].Rows[i]["Amount"].ToString(), ds.Tables[0].Rows[i]["ECFNo"].ToString()});
                    }

                    xl = new XLWorkbook();
                    xl.Worksheets.Add(dt, "RRP");
                    xl.SaveAs(string.Format("{0}/" + @System.Configuration.ConfigurationManager.AppSettings["CompanyName"].ToString() + "-RRP-REG-" + _Date.Replace("-", "") + _SerialNo + ".xlsx", IsFileGenerated));
                }

                //ERA Template Work
                if (_PayMode.ToLower().Trim() == "era" && IsFileGenerated != string.Empty)
                {
                    string _ViewName = "";
                    XLWorkbook xl = null;
                    ERATemplate result = new ERATemplate();

                    result = GetERATemplate(ds.Tables[0], ds.Tables[2]);

                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && result != null)
                    {

                        if (_BankName.ToLower().Contains("citi"))
                        {
                            _ViewName = "TmpDT_CITI";
                        }

                        if (_BankName.ToLower().Contains("hdfc"))
                        {
                            _ViewName = "TmpDT_HDFC_ERA";
                        }

                        if (_BankName.ToLower().Contains("icici"))
                        {
                            _ViewName = "TmpDT_ICICI";
                        }
                        if (_BankName.ToLower().Contains("sbi") || _BankName.ToUpper().Contains("STATE BANK OF INDIA"))
                        {
                            _ViewName = "TmpDT_SBI";
                        }
                        if (_BankName.ToLower().Contains("uti") || _BankName.ToUpper().Contains("AXIS"))
                        {
                            _ViewName = "TmpDT_UTI";
                        }
                        if (_ViewName == "")
                        {
                            _ViewName = "TmpDT_HDFC_ERA";
                        }
                    }
                    if (_ViewName != "")
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.Add("SL No", typeof(string));
                        dt.Columns.Add("Employee Code", typeof(string));
                        dt.Columns.Add("Employee Name", typeof(string));
                        dt.Columns.Add("Account No", typeof(string));
                        dt.Columns.Add("Amount", typeof(string));
                        //RAMYA for IFSC Code
                        dt.Columns.Add("IFSC Code", typeof(string));
                        for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
                        {
                            dt.Rows.Add(new object[] { i + 1, ds.Tables[2].Rows[i]["EmployeeSupplierCode"].ToString(), ds.Tables[2].Rows[i]["EmployeeSupplierName"].ToString(), ds.Tables[2].Rows[i]["AccNo"].ToString(), ds.Tables[2].Rows[i]["Amount"].ToString(), ds.Tables[2].Rows[i]["IFSCCode"].ToString() });
                        }
                        //save the DD Content to local folder.
                        string fileName = "";
                        if (_ViewName == "TmpDT_CITI")
                        {
                            fileName = string.Format("{0}/CITI.html", IsFileGenerated);
                            xl = new XLWorkbook();
                            xl.Worksheets.Add(dt, "CITI");
                            xl.SaveAs(string.Format("{0}/CITI.xlsx", IsFileGenerated));
                        }
                        else if (_ViewName == "TmpDT_HDFC_ERA")
                        {
                            fileName = string.Format("{0}/HDFC.html", IsFileGenerated);
                            xl = new XLWorkbook();
                            xl.Worksheets.Add(dt, "HDFC");
                            xl.SaveAs(string.Format("{0}/HDFC.xlsx", IsFileGenerated));
                        }
                        else if (_ViewName == "TmpDT_ICICI")
                        {
                            fileName = string.Format("{0}/ICICI.html", IsFileGenerated);
                            xl = new XLWorkbook();
                            xl.Worksheets.Add(dt, "ICICI");
                            xl.SaveAs(string.Format("{0}/ICICI.xlsx", IsFileGenerated));
                        }
                        else if (_ViewName == "TmpDT_SBI")
                        {
                            fileName = string.Format("{0}/SBIDT.txt", IsFileGenerated);
                            GenerateNotePad(fileName, ds.Tables[1]);
                            fileName = string.Format("{0}/SBI.html", IsFileGenerated);
                            xl = new XLWorkbook();
                            xl.Worksheets.Add(dt, "SBI");
                            xl.SaveAs(string.Format("{0}/SBI.xlsx", IsFileGenerated));
                        }
                        else if (_ViewName == "TmpDT_UTI")
                        {
                            fileName = string.Format("{0}/UTI.html", IsFileGenerated);
                            xl = new XLWorkbook();
                            xl.Worksheets.Add(dt, "UTI");
                            xl.SaveAs(string.Format("{0}/UTI.xlsx", IsFileGenerated));
                        }


                        // Render the view html to a string.
                        string htmlText = ObjHtmlViewRenderer.RenderViewToString(this, _ViewName, result);

                        //save the document as html.
                        using (FileStream fs = new FileStream(fileName, FileMode.Create))
                        {
                            using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                            {
                                w.Write(htmlText);
                            }
                        }

                    }

                }
            }
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public DDTemplate GetDDTemplate(DataTable dtDet, DataTable dtRem)
        {
            DDTemplate rec = new DDTemplate();
            DataTable dt = null;
            if (dtDet != null)
            {
                if (dtDet.Rows.Count > 0)
                {
                    rec.Date = dtDet.Rows[0]["Date"].ToString();
                    rec.LetterNo = dtDet.Rows[0]["MemoNo"].ToString();
                    rec.BankAddress = dtDet.Rows[0]["BankAddress"].ToString();
                    rec.CompanyAccountNo = dtDet.Rows[0]["CompanyAccNo"].ToString();
                    //kavitha for dd favor ,pay at shows blank
                    rec.DDFavoring = dtDet.Rows[0]["DDFavoring"].ToString();
                    rec.PayableAt = dtDet.Rows[0]["PayableAt"].ToString();
                    rec.Amount = dtDet.Rows[0]["Amount"].ToString();
                    rec.AmountInWords = dtDet.Rows[0]["AmountInWords"].ToString();

                    dt = dtRem;//LoadChildList(dtRem, dtDet.Rows[i]["PvId"].ToString());

                    List<DDInnerDetails> childArray = new List<DDInnerDetails>();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow cdr in dt.Rows)
                        {
                            DDInnerDetails child = new DDInnerDetails();
                            child.DDFavoring = cdr["DDFavoring"].ToString();
                            child.PayableAt = cdr["PayableAt"].ToString();
                            child.Amount = cdr["Amount"].ToString();
                            childArray.Add(child);
                        }
                        rec.Totalamount = childArray.Sum(item => Convert.ToDecimal(item.Amount));
                    }
                    rec.DetailArray = childArray;
                }
            }
            return rec;
        }
        public void GenerateNotePad(string NormalFile, DataTable dt)
        {
            FileStream fs = new FileStream(NormalFile, FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter _fsstr = new StreamWriter(fs);
            _fsstr.WriteLine(string.Empty);
            _fsstr.Flush(); _fsstr.Close();

            //update the notepad content for downloading.
            if (dt.Rows.Count > 0)
            {
                StreamWriter str = new StreamWriter(NormalFile, false, System.Text.Encoding.Default);
                foreach (DataRow datarow in dt.Rows)
                {
                    string row = string.Empty;
                    row = datarow["AccNo"].ToString() + "          " + datarow["PVAmount"].ToString();
                    str.WriteLine(row);
                }
                str.Flush();
                str.Close();
            }
        }
        public ERATemplate GetERATemplate(DataTable dtDet, DataTable dtRem)
        {
            ERATemplate rec = new ERATemplate();
            DataTable dt = null;
            if (dtDet != null)
            {
                if (dtDet.Rows.Count > 0)
                {
                    rec.Date = dtDet.Rows[0]["Date"].ToString();
                    rec.LetterNo = dtDet.Rows[0]["MemoNo"].ToString();
                    rec.BankAddress = dtDet.Rows[0]["BankAddress"].ToString();
                    rec.CompanyAccountNo = dtDet.Rows[0]["CompanyAccNo"].ToString();
                    rec.CompanyAccountNo = dtDet.Rows[0]["CompanyAccNo"].ToString();
                    rec.KindAttan = dtDet.Rows[0]["KindAttan"].ToString();
                    rec.Amount = dtDet.Rows[0]["Amount"].ToString();
                    rec.AmountInWords = dtDet.Rows[0]["AmountInWords"].ToString();

                    dt = dtRem;
                    List<ERAInnerDetails> childArray = new List<ERAInnerDetails>();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow cdr in dt.Rows)
                        {
                            ERAInnerDetails child = new ERAInnerDetails();
                            child.VendorCode = cdr["EmployeeSupplierCode"].ToString();
                            child.NameOfVendor = cdr["EmployeeSupplierName"].ToString();
                            child.BankAccountNo = cdr["AccNo"].ToString();
                            child.IFSCCode = cdr["IFSCCode"].ToString();
                            child.Amount = cdr["Amount"].ToString();
                            child.RemittanceDetails = cdr["RemitterDetails"].ToString();
                            childArray.Add(child);
                        }
                    }
                    rec.DetailArray = childArray;
                }
            }
            return rec;
        }

        #endregion
    }
}