﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlexiSpend.Service;
using Kendo.Mvc.Extensions;
using DataAccessHandler.Models.FlexiSpend;

namespace FlexiSpend.Controllers
{
    public class APCheckerController : Controller
    {
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(APCheckerController));
        APChecker_Service objService = new APChecker_Service();
        // GET: APChecker
        public ActionResult APChecker()
        {
            return View();
        }

        public ActionResult Get_APCheckerList([DataSourceRequest]DataSourceRequest request, string GridAction)
        {
            try
            {
                return Json(objService.Get_APCheckerList(GridAction).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            } 
        }

        // GET: APChecker
        public ActionResult APAuthorizer()
        {
            return View();
        }

    }
}