﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using Newtonsoft.Json;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using DataAccessHandler.Models.FlexiSpend;
using FlexiSpend.Service;

namespace FlexiSpend.Controllers
{
    public class InwardController : Controller
    {
        #region
        InwardService serviceObj = new InwardService();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(InwardController));
        #endregion
        // GET: Inward
       /* public ActionResult Index()
        {
            return View();
        }*/
        public ActionResult Inward()
        {
            return View();
        }

        #region
        public ActionResult GetInwardDetails(Inward_Model _data)
        {
            try
            {
                _data.LoginId = Convert.ToInt64(Session["Employee_Gid"]);
                return Json(serviceObj.GetInwardDetails(_data));
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
        }

        public ActionResult Read_Inward([DataSourceRequest]DataSourceRequest request, Inward_Model _data)
        {
            try
            {
                //_data._Action = _Action; _data.Gridtype = Gridtype;
                _data.LoginId = Convert.ToInt64(Session["Employee_Gid"]);
                return Json(serviceObj.GetInwardDetails(_data).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
        }

        public ActionResult Read_EcfDropDownList()
        {
            try
            {
                Inward_Model _data = new Inward_Model();
                _data.LoginId = Convert.ToInt64(Session["Employee_Gid"]);
                _data._Action = "GetEcf";
                return Json(serviceObj.GetEcf(_data), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
        }

        public ActionResult ApprovedEcfDropdown()
        {
            try
            {
                Inward_Model _data = new Inward_Model();
                _data.LoginId = Convert.ToInt64(Session["Employee_Gid"]);
                _data._Action = "ApprovedEcf";
                return Json(serviceObj.GetEcf(_data), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
        }

        public ActionResult GetCourier()
        {
            try
            {
                Inward_Model _data = new Inward_Model();
                _data.LoginId = Convert.ToInt64(Session["Employee_Gid"]);
                _data._Action = "Courier";
                return Json(serviceObj.GetCourier(_data), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
        }

        public ActionResult SetInwardDetails(Inward_Model _data)
        {
            _data.LoginId = Convert.ToInt64(Session["Employee_Gid"]);
            dt = serviceObj.SetInwardDetails(_data);
            return Json(JsonConvert.SerializeObject(dt), JsonRequestBehavior.AllowGet);
        }

        public ActionResult SetBatchDetails(Inward_Model _data) //Batch no Update
        {
            try
            {
                //Inward_Model _data = new Inward_Model();
                _data.LoginId = Convert.ToInt64(Session["Employee_Gid"]);
                dt = serviceObj.SetBatchDetails(_data);
                return Json(JsonConvert.SerializeObject(dt), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
        }
        #endregion
    }
}