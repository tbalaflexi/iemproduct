﻿using DataAccessHandler.Models.FlexiSpend;
using FlexiSpend.Service;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;

namespace FlexiSpend.Controllers
{
    public class ChequeDespatchController : Controller
    {
        #region

        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(ChequePaymentController));             //Declaring Log4Net 
        ChequePayment_Model ModelObject = new ChequePayment_Model();                                   // ChequeDespatch model object
        List<ChequePayment_Model> ModelList = new List<ChequePayment_Model>();                         //ChequeDespatch list model object
        ChequeDespatch_Service ServiceObject = new ChequeDespatch_Service();                            // ChequeDespatch master service object
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        string result = "";


        #endregion
        // GET: ChequeDespatch
        public ActionResult ChequeDespatch()
        {
            return View();
        }

        //ChequeDespatch Read
        public ActionResult ReadGrid([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                // Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                return Json(ServiceObject.ReadRecord().ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }

        }
        //
        public ActionResult UpdateGrid(ChequePayment_Model ModelObject)
        {

            try
            {
                 
                ModelObject.logingid = Convert.ToInt64(Session["Employee_Gid"]);
                dt = ServiceObject.UpdateRecord(ModelObject);
                ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.result);

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return Json(ModelObject.result);

            }

        }

        //despatch mode dropdown binding

        public JsonResult GetDespatchMode(string parentcode)
        {
            List<ChequePayment_Model> trantype = new List<ChequePayment_Model>();
            try
            {
                trantype = ServiceObject.Getdropdown(parentcode);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(trantype, JsonRequestBehavior.AllowGet);
        }
 
        //onchange values
        public ActionResult GetSubType(string TranId, string parentcode)
        {
            List<ChequePayment_Model> sub_TypeList = new List<ChequePayment_Model>();

            try
            {
                dt = ServiceObject.GetValues(TranId, parentcode);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ChequePayment_Model courier = new ChequePayment_Model();
                    courier.courierid= Convert.ToInt64(dt.Rows[i][0].ToString());
                    courier.chq_courier_name = dt.Rows[i][1].ToString();
                    sub_TypeList.Add(courier);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(sub_TypeList, JsonRequestBehavior.AllowGet);
        }
    }
}