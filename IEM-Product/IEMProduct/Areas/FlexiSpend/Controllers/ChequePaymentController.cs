﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccessHandler.Models.FlexiSpend;
using System.Data;
using FlexiSpend.Service;
//using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
//using Kendo.Mvc.Extensions;
 
using System.Globalization;
using Kendo.Mvc.UI;

 
  
//using Newtonsoft.Json;

namespace FlexiSpend.Controllers
{
    public class ChequePaymentController : Controller
    {
        #region

        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(ChequePaymentController));             //Declaring Log4Net 
        ChequePayment_Model ModelObject = new ChequePayment_Model();                                   // ChequePayment model object
        List<ChequePayment_Model> ModelList = new List<ChequePayment_Model>();                         //ChequePayment list model object
        ChequePayment_Service ServiceObject = new ChequePayment_Service();                            // ChequePayment master service object
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        string result = "";


        #endregion
        // GET: ChequePayment
        public ActionResult ChequePayment()
        {
            return View();
        }

        //ChequePayment Read
      //  [HttpGet]
        public ActionResult ReadGrid([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
               // Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                return Json(ServiceObject.ReadRecord().ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }

        }


        //ChequePayment Insert
       [HttpPost]
        public ActionResult Save_Grid(ChequePayment_Model ModelObject)
        {
            try
            {
               // ChequePayment_Model obj = new ChequePayment_Model();
                ModelObject.logingid = Convert.ToInt64(Session["Employee_Gid"]);
                dt = ServiceObject.SaveRecord(ModelObject);
                ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.result);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return View();
                 
            }
        }


        
        //ChequePayment Update
        //[HttpPost]
        public ActionResult UpdateGrid([DataSourceRequest] DataSourceRequest request, ChequePayment_Model ModelObject)
        {
              
            try
            {
                
                ModelObject.logingid = Convert.ToInt64(Session["Employee_Gid"]);
                dt = ServiceObject.UpdateRecord(ModelObject);
                ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.result);

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return Json(ModelObject.result);

            }

        }
    }
}