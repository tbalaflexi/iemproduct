﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccessHandler.Models.FlexiSpend;
using System.Data;
using FlexiSpend.Service;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using System.Globalization;
using Kendo.Mvc.UI;

namespace FlexiSpend.Controllers
{
    public class RejectDespatchController : Controller
    {
        #region
         log4net.ILog logger = log4net.LogManager.GetLogger(typeof(ChequePaymentController));             //Declaring Log4Net 
        RejectDespatch_Model ModelObject = new RejectDespatch_Model();                                   // RejectDespatch model object
        List<RejectDespatch_Model> ModelList = new List<RejectDespatch_Model>();                         //RejectDespatch list model object
        RejectDespatch_Service ServiceObject = new RejectDespatch_Service();                            // RejectDespatch master service object
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        string result = "";
         #endregion



        // GET: RejectDespatch
        public ActionResult RejectDespatch()
        {
            return View();
        }

        // RejectDespatch Read For Review
        public ActionResult ReadGrid([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                // Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                return Json(ServiceObject.ReadRecord().ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }

        }


   


        //RejectDespatch Update For Review

        public ActionResult UpdateGrid(RejectDespatch_Model ModelObject)
        {

            try
            {

               // ModelObject.inexid = ModelObject.inexid.Replace("'", "");
               
               
                ModelObject.logingid = Convert.ToInt64(Session["Employee_Gid"]);
                dt = ServiceObject.UpdateRecord(ModelObject);
                ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.result);

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return Json(ModelObject.result);

            }

        }

        //
        //binding Tran Type dropdown qc for Review

        public JsonResult GetTranType(string parentcode)
        {
            List<RejectDespatch_Model> trantype = new List<RejectDespatch_Model>();
            try
            {
                trantype = ServiceObject.Getdropdown(parentcode);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(trantype, JsonRequestBehavior.AllowGet);
        }

        //non qc dropdown values for Review
        public JsonResult dropdowns_values_binding(string flag)
        {
            List<RejectDespatch_Model> dropdown = new List<RejectDespatch_Model>();
            try
            {
                dropdown = ServiceObject.dropdownvalues(flag);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(dropdown, JsonRequestBehavior.AllowGet);
        }

         

        //despatch mode dropdown binding For Despatch

        public JsonResult GetDespatchMode(string parentcode)
        {
            List<RejectDespatch_Model> Despachmode = new List<RejectDespatch_Model>();
            try
            {
                Despachmode = ServiceObject.Getdropdown(parentcode);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Despachmode, JsonRequestBehavior.AllowGet);
        }






        //Reject Despatch Read For Despatch
       
        public ActionResult Read_Grid([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                // Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                return Json(ServiceObject.Read_Record().ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }

        }

        //RejectDespatch Update For Despatch

        public ActionResult Update_Grid(RejectDespatch_Model ModelObject)
        {

            try
            {
                ModelObject.logingid = Convert.ToInt64(Session["Employee_Gid"]);
                dt = ServiceObject.Update_Record(ModelObject);
                ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.result);

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return Json(ModelObject.result);

            }

        }

        //onchange despath to corrier name
        public ActionResult GetSubType(string TranId, string parentcode)
        {
            List<ChequePayment_Model> sub_TypeList = new List<ChequePayment_Model>();

            try
            {
                dt = ServiceObject.GetValues(TranId, parentcode);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ChequePayment_Model courier = new ChequePayment_Model();
                    courier.courierid = Convert.ToInt64(dt.Rows[i][0].ToString());
                    courier.chq_courier_name = dt.Rows[i][1].ToString();
                    sub_TypeList.Add(courier);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(sub_TypeList, JsonRequestBehavior.AllowGet);
        }
    }
}