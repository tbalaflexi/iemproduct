﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlexiSpend.Service;
using Kendo.Mvc.Extensions;
using DataAccessHandler.Models.FlexiSpend;
using System.Data;
using Newtonsoft.Json;

namespace FlexiSpend.Controllers
{
    public class PaymentConversionCheckerController : Controller
    {
        #region
        PaymentConversion_Service serviceObj = new PaymentConversion_Service();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        PaymentConversion_Model Obj_Model = new PaymentConversion_Model();
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(PulloutController));
        #endregion

        // GET: PaymentConversionChecker
        public ActionResult PaymentConversionChecker()
        {
            return View();
        }
        public ActionResult Read_PaymentConversionChecker([DataSourceRequest]DataSourceRequest request, PaymentConversion_Model Obj_Model)
        {
            try
            {
                return Json(serviceObj.Get_PaymentConversionList(Obj_Model).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
        }
        [HttpPost]
        public ActionResult SetPaymentConvChecker(PaymentConversion_Model Obj_Model)
        {
            try
            {
                Obj_Model.LoginId = Convert.ToInt64(Session["Employee_Gid"]);
                dt = serviceObj.SETPaymentConvChecker(Obj_Model);
                return Json(JsonConvert.SerializeObject(dt), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}