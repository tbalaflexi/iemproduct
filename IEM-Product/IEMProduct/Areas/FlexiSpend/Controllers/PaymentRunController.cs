﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using DataAccessHandler.Models.FlexiSpend;
using FlexiSpend.Service;
using System.Data;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
namespace FlexiSpend.Controllers
{
    public class PaymentRunController : Controller
    {
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(PaymentRunController));
        PaymentRun_Service objService = new PaymentRun_Service();
        PaymentRun_Model objModel = new PaymentRun_Model();
        DataTable dt = new DataTable();
        // GET: PaymentRun
        public ActionResult PaymentRun()
        {
            return View();
        }

        public ActionResult Read_FundPosition([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                Int64 User_Gid = Convert.ToInt64(Session["Employee_Gid"]);

                return Json(objService.Read_FundPosition(User_Gid).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }
        }

        public ActionResult Read_PaymentRun_BeforeGL([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                Int64 User_Gid = Convert.ToInt64(Session["Employee_Gid"]);

                return Json(objService.Read_PaymentRun_BeforeGL(User_Gid).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }
        }

        public ActionResult Read_PaymentRun_AfterGL([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                Int64 User_Gid = Convert.ToInt64(Session["Employee_Gid"]);

                return Json(objService.Read_PaymentRun_AfterGL(User_Gid).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }
        }

        public ActionResult Read_PayBank()
        {
            List<PaymentRun_Model> lst = new List<PaymentRun_Model>();
            try
            {
                lst = objService.Read_PayBank();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Reverse_Authorization(String EcfGID = "")
        {
            string Result = string.Empty;
            Int64 Login_User_Gid = 0;
            try
            {
                Login_User_Gid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = objService.Reverse_Authorization(EcfGID, Login_User_Gid);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update_GLValue_Date(string ecf_gids = "", string GL_date = "", Int64 PayBank_Gid = 0)
        {
            string Result = string.Empty;
            Int64 Login_User_Gid = 0;
            try
            {
                PaymentRun_Model ObjdataModel = new PaymentRun_Model();
                ObjdataModel.PaymentRunGL_ECF_Gids = ecf_gids;
                ObjdataModel.PaymentRunGL_GLDate = GL_date;
                ObjdataModel.PaymentRunGL_PayBank_Gid = PayBank_Gid;
                Login_User_Gid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = objService.Update_GLValue_Date(ObjdataModel, Login_User_Gid);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Undo_PaymentRunGLDate(Int64 EcfGID = 0)
        {
            string Result = string.Empty;
            Int64 Login_User_Gid = 0;
            try
            {
                Login_User_Gid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = objService.Undo_PaymentRunGLDate(EcfGID, Login_User_Gid);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Set_PaymentRun(string ecf_gids = "")
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            string Result = string.Empty;
            Int64 Login_User_Gid = 0;
            try
            {
                Login_User_Gid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                ds = objService.Set_PaymentRun(ecf_gids, Login_User_Gid);
                string Data1="";
                string Data2="";
                string Data3 = "";
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        dt = ds.Tables[0];
                        Data1 = JsonConvert.SerializeObject(dt);
                    }
                }
                if (ds.Tables.Count > 1)
                {
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        dt1 = ds.Tables[1];
                        Data2 = JsonConvert.SerializeObject(dt1);
                    }

                }
                if (ds.Tables.Count > 2)
                {
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        dt2 = ds.Tables[2];
                        Data3 = JsonConvert.SerializeObject(dt2);
                    }
                }

                return Json(new { Data1, Data2, Data3 }, JsonRequestBehavior.AllowGet);

                // Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
    }
}