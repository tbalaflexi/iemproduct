﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlexiSpend.Service;
using Kendo.Mvc.Extensions;
using DataAccessHandler.Models.FlexiSpend;
using System.Data;
using Newtonsoft.Json;

namespace FlexiSpend.Controllers
{
    public class UrgentController : Controller
    {
        #region
        Urgent_Service serviceObj = new Urgent_Service();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        Urgent_Model Obj_Model= new Urgent_Model ();
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(UrgentController));
        #endregion

        // GET: Urgent
        public ActionResult Urgent()
        {
            return View();
        }
        public ActionResult Read_Urgent([DataSourceRequest]DataSourceRequest request, Urgent_Model Obj_Model )
        {
            try
            {
                return Json(serviceObj.Get_UrgentDetailsList(Obj_Model).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
               
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
           
        }
        public ActionResult Read_EcfDropDownList()
        {
            List<Urgent_Model> lst = new List<Urgent_Model>();
            try
            {
                Urgent_Model data = new Urgent_Model();
                lst = serviceObj.GetEcf(data);
                //return Json(serviceObj.GetEcf(data), JsonRequestBehavior.AllowGet);
                var jsonResult = Json(lst, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
        }
        public ActionResult Read_EmpDropDownList(string EmployeeName)
        {
            List<Urgent_Model> lst = new List<Urgent_Model>();
            try
            {
                Urgent_Model data = new Urgent_Model();
                data.EmpName_Sr = EmployeeName;
                lst = serviceObj.GetEmp(data);
                //return Json(serviceObj.GetEmp(data), JsonRequestBehavior.AllowGet);
                var jsonResult = Json(lst, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
        }
        [HttpPost]
        public ActionResult UpdateUrgentDetails(Urgent_Model Obj_Model)
        {
            Obj_Model.Action = "Insert";
            Obj_Model.MakerId = Convert.ToInt64(Session["Employee_Gid"]);
            Obj_Model.LoginId = Convert.ToInt64(Session["Employee_Gid"]);
            //Obj_Model.UrgentECF_Gid = UrgentECF_Gid;
            //Obj_Model.Maker_Reason = Maker_Reason;
            dt = serviceObj.DMLUrgentDetails(Obj_Model);
            return Json(JsonConvert.SerializeObject(dt), JsonRequestBehavior.AllowGet);
        }
    }
}