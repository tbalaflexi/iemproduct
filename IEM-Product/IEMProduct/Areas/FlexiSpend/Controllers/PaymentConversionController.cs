﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlexiSpend.Service;
using Kendo.Mvc.Extensions;
using DataAccessHandler.Models.FlexiSpend;
using System.Data;
using Newtonsoft.Json;

namespace FlexiSpend.Controllers
{
    public class PaymentConversionController : Controller
    {
        #region
        PaymentConversion_Service serviceObj = new PaymentConversion_Service();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        PaymentConversion_Model Obj_Model = new PaymentConversion_Model();
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(PulloutController));
        #endregion

        // GET: PaymentConversion
        public ActionResult PaymentConversion()
        {
            return View();
        }
        public ActionResult Read_PaymentConversion([DataSourceRequest]DataSourceRequest request, PaymentConversion_Model Obj_Model)
        {
            try
            {
                return Json(serviceObj.Get_PaymentConversionList(Obj_Model).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
        }
        public ActionResult Update_PaymentConversion([DataSourceRequest] DataSourceRequest request, PaymentConversion_Model ModelObject)
        {
            try
            {
                //dt = ServiceObject.UpdateRecord(ModelObject);
                //ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.Action );

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                ModelObject.Action = ex.Message.ToString();
                return Json(ModelObject.Action);

            }
        }
        public ActionResult Read_PaymodeDropDownList()
        {
            try
            {
            //    Obj_Model.EMP_SUP_Type = Emp_Sup_Type;
            //    Obj_Model.ECFGid = Convert.ToInt64(ECF_Gid.ToString());
                return Json(serviceObj.GetPaymodeList(Obj_Model), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
        }
          [HttpPost]
        public ActionResult GetDropDownList(PaymentConversion_Model Obj_Model)
        {
            try
            {
                //Obj_Model.EMP_SUP_Type = Emp_Sup_Type;
                //Obj_Model.ECFGid = Convert.ToInt64(ECF_Gid.ToString());
                return Json(serviceObj.GetPaymodeDropDownList(Obj_Model), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
        }
        public ActionResult Read_BeneficiaryDropDownList()
        {
            try
            {

                return Json(serviceObj.GetBeneficiaryList(Obj_Model), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
        }
        [HttpPost]
        public ActionResult SetPaymentConvMaker(PaymentConversion_Model Obj_Model)
        {
            try
            {
                Obj_Model.LoginId = Convert.ToInt64(Session["Employee_Gid"]);
                dt = serviceObj.SETPaymentConvMaker(Obj_Model);
                return Json(JsonConvert.SerializeObject(dt), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}