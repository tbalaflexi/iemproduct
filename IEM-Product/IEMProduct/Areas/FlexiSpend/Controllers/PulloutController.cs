﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlexiSpend.Service;
using Kendo.Mvc.Extensions;
using DataAccessHandler.Models.FlexiSpend;
using System.Data;
using Newtonsoft.Json;

namespace FlexiSpend.Controllers
{
    public class PulloutController : Controller
    {

        #region
        Pullout_Service serviceObj = new Pullout_Service();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        Pullout_Model Obj_Model = new Pullout_Model();
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(PulloutController));
        #endregion

        // GET: Pullout

        public ActionResult Pullout()
        {
            return View();
        }

        public ActionResult Read_Pullout([DataSourceRequest]DataSourceRequest request, Pullout_Model Obj_Model)
        {
            try
            {
                return Json(serviceObj.Get_PulloutDetailList(Obj_Model).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }

        }
        public ActionResult Read_Interfiling([DataSourceRequest]DataSourceRequest request, Pullout_Model Obj_Model)
        {
            try
            {
                return Json(serviceObj.Get_PulloutInterFilingList(Obj_Model).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }

        }
        public ActionResult Read_EcfDropDownList()
        {
            try
            {               

                return Json(serviceObj.GetEcf(Obj_Model), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
        }
        public ActionResult Read_EmpDropDownList(string EmployeeName)
        {
            List<Pullout_Model> lst = new List<Pullout_Model>();

            try
            {
                Obj_Model.EmpName_Sr = EmployeeName;
                lst = serviceObj.GetEmp(Obj_Model);
            
                var jsonResult = Json(lst, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
        }
        public ActionResult Read_HandedDropDownList(string EmployeeName)
        {
            List<Pullout_Model> lst = new List<Pullout_Model>();
            try
            {
                Obj_Model.EmpName_Sr = EmployeeName;
                lst = serviceObj.GetHandeddetails(Obj_Model);
               
                var jsonResult = Json(lst, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
        }
        [HttpPost]
        public ActionResult UpdatePulloutDetails(Pullout_Model Obj_Model)
        {
            try
            {
                Obj_Model.LoginId = Convert.ToInt64(Session["Employee_Gid"]);
                dt = serviceObj.SETPulloutDetails(Obj_Model);
                return Json(JsonConvert.SerializeObject(dt), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}