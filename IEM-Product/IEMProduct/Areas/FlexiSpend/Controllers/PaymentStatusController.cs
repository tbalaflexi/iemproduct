﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using DataAccessHandler.Models.FlexiSpend;
using Kendo.Mvc.UI;
using FlexiSpend.Service;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;

namespace FlexiSpend.Controllers
{
    public class PaymentStatusController : Controller
    {
        // GET: PaymentStatus


        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(PaymentStatusController));
        PayementStatus_Service objService = new PayementStatus_Service();
        DataTable dt = new DataTable();

        public ActionResult PaymentStatus()
        {
            
            return View();
        }


        public ActionResult ReadPaymentStatus([DataSourceRequest]DataSourceRequest request )
         {
            try
            {

                Int64 userId = Convert.ToInt64(Session["Employee_Gid"].ToString());
                return Json(objService.ReadPaymentStatus().ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return View();
        }

        //[HttpPost]
        //public ActionResult GetPaymentStatus()
        //{
        //    List<PaymentStatus_Model> _lst = new List<PaymentStatus_Model>();
        //    try
        //    {
        //        Int64 userId = Convert.ToInt64(Session["Employee_Gid"].ToString());
        //        // return Json(objService.GetPaymentStatus(Obj_Model), JsonRequestBehavior.AllowGet);
        //        _lst = objService.GetPaymentStatus();
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex.ToString());
        //    }
        //    return Json(_lst, JsonRequestBehavior.AllowGet);
        //}


        //Payment Status Update
        [HttpPost]
        public ActionResult UpdatePayment(PaymentStatus_Model Obj_Model)
        {
            try
            {
                Obj_Model.LoginId = Convert.ToInt64(Session["Employee_Gid"]);
                dt = objService.UpdatePaymentstatus(Obj_Model);
                return Json(JsonConvert.SerializeObject(dt), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        ////get qc dropdown values
        //public JsonResult Getchequestatus(string parentcode)
        //{
        //    List<PaymentStatus_Model> trantype = new List<PaymentStatus_Model>();
        //    try
        //    {
        //        trantype = objService.Getdropdown(parentcode);
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex.ToString());
        //    }
        //    return Json(trantype, JsonRequestBehavior.AllowGet);
        //}


        //
        [HttpPost]
        public ActionResult GetDropDownList(PaymentStatus_Model Obj_Model)
        {
            try
            {
                //Obj_Model.EMP_SUP_Type = Emp_Sup_Type;
                //Obj_Model.ECFGid = Convert.ToInt64(ECF_Gid.ToString());
                return Json(objService.GetPaymodeDropDownList(Obj_Model), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
               // return View();
            }
        }

    }
}