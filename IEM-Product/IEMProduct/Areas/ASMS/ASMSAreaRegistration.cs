﻿using System.Web.Mvc;

namespace IEMProduct
{
    public class ASMSAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ASMS";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ASMS_default",
                "ASMS/{controller}/{action}/{id}",
                new { controller = "ASMSHome", action = "ASMSHome", id = UrlParameter.Optional },
                new string[] { "ASMS.Controllers" }
            );
        }
    }
}