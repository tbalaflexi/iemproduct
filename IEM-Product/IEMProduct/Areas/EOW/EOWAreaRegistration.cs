﻿using System.Web.Mvc;

namespace IEMProduct
{
    public class EOWAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "EOW";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "EOW_default",
                "EOW/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new string[] { "EOW.Controllers" }
            );
        }
    }
}