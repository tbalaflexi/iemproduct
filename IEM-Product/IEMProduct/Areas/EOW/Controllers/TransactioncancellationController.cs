﻿using DataAccessHandler.Models.EOW;
using EOW.Service;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
namespace EOW.Controllers
{
    public class TransactioncancellationController : Controller
    {


        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(TransactioncancellationController));             //Declaring Log4Net 
        TransactionCancellation_Model ModelObject = new TransactionCancellation_Model();                                   // TransactionCancellation model object
        List<TransactionCancellation_Model> ModelList = new List<TransactionCancellation_Model>();                         //TransactionCancellation list model object
        TransactionCancellation_Service ServiceObject = new TransactionCancellation_Service();                            // TransactionCancellation master service object
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        string result = "";

        // GET: Transactioncancellation
        public ActionResult Transactioncancellation()
        {
            return View();
        }
        //Transaction Cancellation
        public ActionResult ReadGrid([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                return Json(ServiceObject.ReadRecord(User_GID).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }

        }

        //Delete Transaction Cancellation
      //  [HttpPost]
        public ActionResult DeleteGrid(string claim)
        {
            try
            {
                claim = claim.Replace("'", "");
                dt = ServiceObject.DeleteRecord(claim);
                if (dt.Rows.Count > 0)
                {
                    result = dt.Rows[0][0].ToString();
                }
                ModelObject.result = JsonConvert.SerializeObject(dt);
                return Json(ModelObject.result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ModelObject.result = ex.Message.ToString();
                return Json(ModelObject.result);
            }

        }
    }
}