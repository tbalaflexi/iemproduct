﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using Newtonsoft.Json;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using DBHelper;
using EOW.Service;
using DataAccessHandler.Models.EOW;

namespace EOW.Controllers
{
    public class PhysicalDespatchController : Controller
    {
        #region
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(PhysicalDespatchController)); 
        PhysicalDespatch_Model ModelObject = new PhysicalDespatch_Model();                                   // WorkBench model object
        List<PhysicalDespatch_Model> ModelList = new List<PhysicalDespatch_Model>();                         //WorkBench list model object
        PhysicalDespatch_Service ServiceObject = new PhysicalDespatch_Service();                            // WorkBench master service object
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        string result = "";
        #endregion


        // GET: PhysicalDespatch
        public ActionResult PhysicalDespatch()
        {
            ModelObject.loginUserID = Convert.ToInt64(Session["Employee_Gid"].ToString());
            return View(ModelObject);
        }


        public ActionResult ReadPhysicalDespatch([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                return Json(ServiceObject.ReadPhysicalDespatch(User_GID).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }

        }


        public JsonResult Get_DespatchMode()
        {
            List<PhysicalDespatch_Model> _lst = new List<PhysicalDespatch_Model>();
            try
            {

                _lst = ServiceObject.Get_DespatchMode();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(_lst, JsonRequestBehavior.AllowGet);
        }

         //   string ecfIDs, Int64 despatchModeId, string despatchedDate, string despatchedAWBNumber
        [HttpPost]
        public ActionResult Save_EcfDespatchedList(PhysicalDespatch_Model objModel)
        {
            string Result = string.Empty;
            try
            {
                //ModelObject.ecfSelectedList = ecfIDs;
                //ModelObject.ecfDespatchModeID = despatchModeId;
                //ModelObject.ecfDespatchDate = despatchedDate;
                //ModelObject.despatchEWBNumber = despatchedAWBNumber;                
                objModel.loginUserID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = ServiceObject.Save_EcfDespatchedList(objModel);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }




        public ActionResult GetPhysicalDespatched([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                Int64 User_GID = Convert.ToInt64(Session["Employee_Gid"].ToString());
                return Json(ServiceObject.GetPhysicalDespatched(User_GID).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }

        }
 

    }
}