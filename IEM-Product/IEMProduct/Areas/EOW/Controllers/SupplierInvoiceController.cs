﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using Newtonsoft.Json;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using DBHelper;
using EOW.Service;
using DataAccessHandler.Models.EOW;

namespace EOW.Controllers
{
    public class SupplierInvoiceController : Controller
    {
        #region
        SupplierInvoiceService serviceObj = new SupplierInvoiceService();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(SupplierInvoiceController));
        #endregion
        // GET: SupplierInvoice
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SupplierinvoiceDetails(Int64 BatchGID = 0, Int64 RaiserGID = 0, Int64 EcfGID = 0, string ClaimStatus = "Draft", string Display_Status = "Draft")
        {
            SupplierInvoice_Model _data = new SupplierInvoice_Model();
            _data.BatchGid = BatchGID;
            //_data.BatchGid = 188;
            _data.Refid = EcfGID;
            //_data.PageFor = ClaimStatus;
            _data._Action = "Batch";
            _data.Expenseid = 0;
            _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
            
            if (BatchGID != 0 || EcfGID != 0)
            {
                _data = serviceObj.GetBatch(_data);
                // _data.Raiserid = RaiserGID;
                // _data.RaiserName = _data.RaiserName;
                //_data.PageFor = ClaimStatus; 
                _data.HDR_EcfGId = EcfGID;
                _data.HDR_Raiser_Gid = _data.Raiserid;
                //_data._Action = "Batch";
                //_data.Expenseid = 0;
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
            }
            else
            {
                _data.BatchGid = BatchGID; 
                _data.Refid = EcfGID;
                
                //_data._Action = "Batch";
                //_data.Expenseid = 0;
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                _data.HDR_Raiser_Gid = Convert.ToInt64(Session["Employee_Gid"]);
            }
            if (ClaimStatus == "Draft")
            {
                ViewBag.IsApprover = false;
                _data.Raiserid = Convert.ToInt64(Session["Employee_Gid"]);
                _data.RaiserName = Convert.ToString(Session["Employee_Code"]) + '-' + Session["Employee_Name"];
            }
            _data.PageFor = ClaimStatus;
            _data.Display_Batch_Status = Display_Status;
            return View(_data);
        }


        #region Get summery list.
        public ActionResult Read_Invoice([DataSourceRequest]DataSourceRequest request, SupplierInvoice_Model _data, Int64 Batchid = 0, Int64 Raiserid = 0, Int64 Expenseid = 0, Int64 Refid = 0)
        {
            _data.BatchGid = Batchid; _data.Raiserid = Raiserid; _data.Expenseid = Expenseid; _data.Refid = Refid;
            try
            {
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                return Json(serviceObj.GetInvoices(_data).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
        }
        #endregion

        #region Create Data
        //, Int64 Batchid, Int64 TypeGid, Int64 SubTypeGid, string Gridtype
        public ActionResult Save_Invoice(SupplierInvoice_Model _data, Int64 Batchid = 0, Int64 TypeGid = 0, Int64 SubTypeGid = 0, string Gridtype = "", Int64 Raiser = 0)
        {
            List<SupplierInvoice_Model> lstInv = new List<SupplierInvoice_Model>();
            SupplierInvoice_Model Objdata = new SupplierInvoice_Model();
            try
            {
                _data.BatchGid = Batchid; _data.GridType = Gridtype; _data.Type_Gid = TypeGid; _data.SubType_Gid = SubTypeGid;
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]); _data.Raiserid = Raiser;
                dt = serviceObj.SaveInvoice(_data);
                return Json(JsonConvert.SerializeObject(dt));
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();;
            }
        }
        #endregion

        #region Update Invoice Details

        public ActionResult Update_Invoice(SupplierInvoice_Model _data, Int64 TypeGid = 0, Int64 Batchid = 0, Int64 SubTypeGid = 0, Int64 Raiser = 0)
        {
            List<SupplierInvoice_Model> lstInv = new List<SupplierInvoice_Model>();
            try
            {
                _data.BatchGid = Batchid; _data.Type_Gid = TypeGid; _data.SubType_Gid = SubTypeGid; _data.Raiserid = Raiser;
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                dt = serviceObj.UpdateInvoice(_data);
                
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                //return View();;
            }
            return Json(JsonConvert.SerializeObject(dt));
        }
        #endregion


        #region Delete Invoice Details
        [HttpPost]
        public ActionResult Delete_Invoice(SupplierInvoice_Model _data, Int64 Refid = 0, Int64 Subtypeid = 0, Int64 Typeid = 0)
        {
            List<SupplierInvoice_Model> lstInv = new List<SupplierInvoice_Model>();
            try
            {
                _data.Refid = Refid; _data.Type_Gid = Typeid; _data.SubType_Gid = Subtypeid;
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                dt = serviceObj.DeleteInvoice(_data);
                return Json(JsonConvert.SerializeObject(dt));
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();;
            }
        }
        #endregion

        #region Delete Expense Details

        public ActionResult Delete_Invoiceexp(SupplierInvoice_Model _data)
        {
            List<SupplierInvoice_Model> lstInv = new List<SupplierInvoice_Model>();
            try
            {
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                dt = serviceObj.DeleteInvoiceExp(_data);
                return Json(JsonConvert.SerializeObject(dt));
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();;
            }
        }
        #endregion

        #region Get Dropdown list.
        public ActionResult Read_DropDownList(SupplierInvoice_Model _data)
        {
            try
            {
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                return Json(serviceObj.Get_DropDownList(_data), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
        }
        #endregion

        #region Get Raiser
        public ActionResult GetRaiser(SupplierInvoice_Model _data)
        {
            try
            {
                /*_data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                return Json(serviceObj.GetRaiser(_data), JsonRequestBehavior.AllowGet);*/
                List<SupplierInvoice_Model> Obj = new List<SupplierInvoice_Model>();
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);                
                var jsonResult = Json(serviceObj.GetRaiser(_data), JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
        }
        public ActionResult Get_Raiser_DropDown_List(string Master, string EmployeeName, Int64 Master_Gid = 0)
        {
            try
            {
                List<SupplierInvoice_Model> Obj = new List<SupplierInvoice_Model>();
                var jsonResult = Json(serviceObj.Get_Raiser_DropDown_List(Master, Master_Gid, EmployeeName), JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
        }
        #endregion

        #region Get Supplier
        public ActionResult GetSupplier(SupplierInvoice_Model _data)
        {
            try
            {
                List<SupplierInvoice_Model> Obj = new List<SupplierInvoice_Model>();
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                var jsonResult = Json(serviceObj.GetSupplier(_data), JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
                /*_data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                return Json(serviceObj.GetSupplier(_data), JsonRequestBehavior.AllowGet);*/
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();
            }
        }
        #endregion

        #region GetProvider Location
        public ActionResult GetGstnLoc(SupplierInvoice_Model _data)
        {
            try
            {
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                return Json(serviceObj.GetGstnLoc(_data), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();;
            }
        }
        #endregion

        #region GetCurrency
        public ActionResult GetCurrency(SupplierInvoice_Model _data)
        {
            try
            {
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                return Json(serviceObj.GetGstnLoc(_data), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();;
            }
        }

        public ActionResult GetCurrencyRate(SupplierInvoice_Model _data)
        {
            _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
            dt = serviceObj.GetCurrencyRate(_data);
            _data.Data = JsonConvert.SerializeObject(dt);
            return Json(_data.Data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region DropDown Bindings For Expense Grid
        public ActionResult GetExpDropDown(SupplierInvoice_Model _data, Int64 Batchid)
        {
            try
            {
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                _data.BatchGid = Batchid;
                return Json(serviceObj.GetExpDropDown(_data), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();;
            }
        }

        #endregion

        #region DropDown Bindings for Credit Grid
        public ActionResult GetCrDropDown(SupplierInvoice_Model _data, Int64 Batchid)
        {
            try
            {
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                _data.BatchGid = Batchid; 
                //return Json(serviceObj.GetCrDropDown(_data), JsonRequestBehavior.AllowGet);
                var jsonResult = Json(serviceObj.GetCrDropDown(_data), JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();;
            }
        }
        #endregion

        #region Submit Invoice Details
        [HttpPost]
        public ActionResult ApproveInvoice(SupplierInvoice_Model _data)
        {
            try
            {
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                dt = serviceObj.ApproveInvoice(_data);
                _data.Data = JsonConvert.SerializeObject(dt);
                return Json(_data.Data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SubmitInvoice(SupplierInvoice_Model _data)
        {
            try
            {
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                dt = serviceObj.SubmitInvoice(_data);
                _data.Data = JsonConvert.SerializeObject(dt);
                return Json(_data.Data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region COA Grid
        public ActionResult Read_COA([DataSourceRequest]DataSourceRequest request, SupplierInvoice_Model _data, Int64 COA_Expense_Gid)
        {
            _data.Expenseid = COA_Expense_Gid;
            try
            {
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                //ViewBag.GlName = "124578";
                return Json(serviceObj.GetCOA(_data).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();;
            }
        }

        public ActionResult GetReferenceNoDropDown(SupplierInvoice_Model _data, string COA_Action, Int64 CAO_Batch_Gid, Int64 COA_ECF_Gid)
        {
            try
            {
                _data.BatchGid = CAO_Batch_Gid;
                _data._Action = COA_Action;
                _data.Refid = COA_ECF_Gid;
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                return Json(serviceObj.GetReferenceNoDropDown(_data), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();;
            }
        }
        public ActionResult GetGLNoDropDown(SupplierInvoice_Model _data, string COA_Action, Int64 CAO_Batch_Gid, Int64 COA_ECF_Gid)
        {
            try
            {
                _data.BatchGid = CAO_Batch_Gid;
                _data._Action = COA_Action;
                _data.Refid = COA_ECF_Gid;
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                return Json(serviceObj.GetReferenceNoDropDown(_data), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();;
            }
        }
        public ActionResult GetCOADropDowns(SupplierInvoice_Model _data, string COA_Action)
        {
            try
            {
                //data.BatchGid = Batchid;
                ViewBag.GlName = "123456";
                _data._Action = COA_Action;
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                return Json(serviceObj.GetCOADropDown(_data), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();;
            }
        }
        public ActionResult Save_Update_COA(SupplierInvoice_Model _data, Int64 COA_Expense_Gid)
        {
            List<SupplierInvoice_Model> lstInv = new List<SupplierInvoice_Model>();
            try
            {
                _data.Expenseid = COA_Expense_Gid;
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                dt = serviceObj.SaveCOA(_data);
                return Json(JsonConvert.SerializeObject(dt));
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();;
            }
        }
        public ActionResult Remove_COA(Int64 COA_Gid)
        {
            string Result = string.Empty;
            try
            {
                dt = serviceObj.Remove_COA(COA_Gid);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetPaymodedetails
        public ActionResult GetPaymodeDetails([DataSourceRequest]DataSourceRequest request, SupplierInvoice_Model _data)
        {
            try
            {
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                return Json(serviceObj.GetPaymodeDetails(_data).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return Json(serviceObj.GetCOADropDown(_data).ToDataSourceResult(request));
            }
        }
        #endregion

        #region PaymentDeletion
        public ActionResult Delete_Invoicecredit(SupplierInvoice_Model _data)
        {
            try
            {
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                dt = serviceObj.DeleteInvoicepayment(_data);
                return Json(JsonConvert.SerializeObject(dt));
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                _data.Message = Convert.ToString(ex.Message);
                _data.Clear = "0";
                return Json(new { _data.Message, _data.Clear }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region TDS
        public ActionResult GetTaxDetails(SupplierInvoice_Model _data)
        {
            try
            {
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                dt = serviceObj.GetTaxDetails(_data);
                return Json(JsonConvert.SerializeObject(dt));
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                _data.Message = Convert.ToString(ex.Message);
                _data.Clear = "0";
                return Json(new { _data.Message, _data.Clear }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Delete_InvoiceTDS(SupplierInvoice_Model _data)
        {
            List<SupplierInvoice_Model> lstInv = new List<SupplierInvoice_Model>();
            try
            {
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                dt = serviceObj.DeleteInvoiceTDS(_data);
                return Json(JsonConvert.SerializeObject(dt));
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();;
            }
        }
        #endregion

        #region POMapping Details
        public ActionResult SetPODetails(SupplierInvoice_Model _data)
        {
            try
            {
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                dt = serviceObj.SetPODetails(_data);
                return Json(JsonConvert.SerializeObject(dt));
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();;
            }
        }

        public ActionResult PoMappingDeletion(SupplierInvoice_Model _data)
        {
            try
            {
                _data.Loginid = Convert.ToInt64(Session["Employee_Gid"]);
                dt = serviceObj.PoMappingDeletion(_data);
                return Json(JsonConvert.SerializeObject(dt));
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return View();;
            }
        }
        #endregion

    }
}