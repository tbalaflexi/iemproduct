﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using DataAccessHandler.Models.EOW;
using EOW.Service;
using Newtonsoft.Json;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace EOW.Controllers
{
    public class AdvanceRequisitionController : Controller
    {
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(AdvanceRequisitionController));
        DataTable dt = new DataTable();
        AdvanceRequisition_Service objService = new AdvanceRequisition_Service();
        AdvanceRequisition_Model objModel = new AdvanceRequisition_Model();
        string Result = string.Empty;

        // GET: AdvanceRequisition
        public ActionResult AdvanceRequisition(Int64 BatchGid = 0, Int64 EcfGID = 0, string PageStatus = "Draft", string Display_Status = "Draft")
        {
            if (PageStatus.ToLower() == "draft")
            {
                EcfGID = 0;
                ViewBag.IsApprover = false;
            }
            else
            {
                ViewBag.IsApprover = true;
            }
            if (Session["Employee_Gid"] != null)
            {
                objModel = objService.Get_AdvaneRquisition_BatchDetails(BatchGid, EcfGID);
                if (BatchGid == 0 && EcfGID == 0)
                {
                    objModel.Employee_Name = Session["Employee_Code"].ToString() + "-" + Session["Employee_Name"].ToString();
                    objModel.Employee_Gid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                }
                objModel.PageStatus = PageStatus;
                objModel.Display_Batch_Status = Display_Status;
            }
            else
            {
                return RedirectToAction("Login", "../Login");
            }
            return View(objModel);
        }

        #region Common method for bindings dropdown lists.
        public JsonResult Get_dropdownlist_records(string Master, string Master_Code, Int64 Master_Gid = 0)
        {
            List<AdvanceRequisition_Model> _lst = new List<AdvanceRequisition_Model>();
            try
            {
                _lst = objService.Get_dropdownlist_records(Master, Master_Code, Master_Gid);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(_lst, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Get_GL_Code(string Master, Int64 Master_Gid)
        {
            string glcode = string.Empty;
            try
            {
                glcode = objService.Get_GL_Code(Master, Master_Gid);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(glcode, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region methods for create/update advance requisition.
        public ActionResult Save_AdvanceRequisition(AdvanceRequisition_Model _objModel, Int64 BatchGid, Int64 RaiserGid, Int64 TypeGid, Int64 SubtypeGid, Int64 SupplierGid, string SupplierEmployeeType)
        {
            try
            {
                _objModel.Batch_Gid = BatchGid;
                _objModel.Employee_Gid = RaiserGid;
                _objModel.Type_Gid = TypeGid;
                _objModel.SubType_Gid = SubtypeGid;
                _objModel.Supplier_Gid = SupplierGid;
                _objModel.Sup_Emp_Type = SupplierEmployeeType;
                _objModel.Logged_UserGid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = objService.Save_AdvanceRequisition(_objModel);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update_AdvanceRequisition(AdvanceRequisition_Model _objModel, Int64 BatchGid, Int64 RaiserGid, Int64 TypeGid, Int64 SubtypeGid, Int64 SupplierGid, string SupplierEmployeeType)
        {
            try
            {
                _objModel.Batch_Gid = BatchGid;
                _objModel.Employee_Gid = RaiserGid;
                _objModel.Type_Gid = TypeGid;
                _objModel.SubType_Gid = SubtypeGid;
                _objModel.Supplier_Gid = SupplierGid;
                _objModel.Sup_Emp_Type = SupplierEmployeeType;
                _objModel.Logged_UserGid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = objService.Update_AdvanceRequisition(_objModel);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region common method for fetching grid records.
        public ActionResult Read_ARF([DataSourceRequest]DataSourceRequest request, string GridName, Int64 BatchGid, Int64 EcfGid)
        {
            try
            {
                return Json(objService.Get_ARF_records(GridName, BatchGid, EcfGid).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return View();
        }
        #endregion
    }
}