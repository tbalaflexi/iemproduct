﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using DataAccessHandler.Models.EOW;
using EOW.Service;
using Newtonsoft.Json;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace EOW.Controllers
{
    public class ClaimController : Controller
    {

        #region Public Objects.
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(ClaimController));
        Claim_Service objService = new Claim_Service();
        Claim_Model objModel = new Claim_Model();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        #endregion

        [HttpPost]
        public ActionResult ClaimPost(Int64? BatchGID, Int64? EcfGID, string ClaimStatus = "Draft", string Display_Status = "Draft")
        {
            return RedirectToAction("Claim", new { BatchGID = BatchGID, EcfGID = EcfGID, ClaimStatus = ClaimStatus, Display_Status = Display_Status });
           // return RedirectToAction("Claim", new { BatchGID = BatchGID, EcfGID = EcfGID, ClaimStatus = ClaimStatus, Display_Status = Display_Status });
        }

        #region Claim Method.
        [HttpGet]
        public ActionResult Claim(Int64? BatchGID, Int64? EcfGID, string ClaimStatus = "Draft", string Display_Status = "Draft")
        {
            try
            {
                Int64 Batch_Gid; Int64 Ecf_Gid;
                if (ClaimStatus == "Draft")
                {
                    if (BatchGID == null)
                    {
                        Batch_Gid = 0;
                    }
                    else
                    {
                        Batch_Gid = Convert.ToInt64(BatchGID);
                    }
                    Ecf_Gid = 0;
                    ViewBag.IsApprover = false;
                }
                else
                {
                    if (BatchGID == null)
                    {
                        Batch_Gid = 0;
                    }
                    else
                    {
                        Batch_Gid = Convert.ToInt64(BatchGID);
                    }
                    if (EcfGID == null)
                    {
                        Ecf_Gid = 0;
                    }
                    else
                    {
                        Ecf_Gid = Convert.ToInt64(EcfGID);
                    }
                    ViewBag.IsApprover = true;
                }

                if (Session["Employee_Gid"] != null)
                {
                    objModel = objService.Get_BatchDetails(Batch_Gid, Ecf_Gid);
                    objModel.HDR_Status = ClaimStatus;
                    objModel.CLM_Create_Mode = Session["Employee_Role"].ToString();
                    objModel.Display_Batch_Status = Display_Status;
                    if (Batch_Gid == 0 && Ecf_Gid == 0)
                    {
                        objModel.HDR_Raiser = Session["Employee_Code"].ToString() + "-" + Session["Employee_Name"].ToString();
                        objModel.HDR_Raiser_Gid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                    }
                }
                else
                {
                    return RedirectToAction("Login", "../Login");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return View(objModel);
        }
        #endregion

        #region Common methods for dropdown bindings.
        public JsonResult Get_DropDown_List(string Master, string EmployeeName, string Master_Code = "", Int64 Master_Gid = 0)
        {
            List<Claim_Model> lst = new List<Claim_Model>();
            try
            {
                //lst = objService.Get_DropDown_List(Master, Master_Code, Master_Gid);
                lst = objService.Get_DropDown_List(Master, Master_Code, Master_Gid, EmployeeName);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            // return Json(lst, JsonRequestBehavior.AllowGet);
            var jsonResult = Json(lst, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public JsonResult Get_GL_Code(string Master, Int64 Master_Gid)
        {
            string glcode = string.Empty;

            try
            {
                glcode = objService.Get_GL_Code(Master, Master_Gid);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(glcode, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region CRUD Methods for Claim/ECF Grid.
        public ActionResult Read_Claim([DataSourceRequest]DataSourceRequest request, string GirdName, Int64 BatchGid, Int64 EcfGid)
        {
            try
            {
                return Json(objService.Read_Claim(GirdName, BatchGid, EcfGid).ToDataSourceResult(request));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return View();
        }

        public ActionResult Save_Claim(Claim_Model objModel, Int64 BatchGid, Int64 RaiserGid, Int64 TypeGid, Int64 SubtypeGid, string Employee_Ids, string Employee_Role, Int64 EcfRaiser)
        {
            string Result = string.Empty;
            try
            {
                objModel.HDR_Batch_Gid = BatchGid;
                objModel.HDR_Raiser_Gid = RaiserGid;
                objModel.CLM_Emp_Gid = EcfRaiser;
                objModel.HDR_Type_Gid = TypeGid;
                objModel.HDR_SubType_Gid = SubtypeGid;
                if (Employee_Role.ToString().Equals("P"))
                {
                    objModel.Logged_UserGid = Convert.ToInt64(Session["Proxy_InsertBy"].ToString());
                }
                else
                {
                    objModel.Logged_UserGid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                }
                objModel.CLM_Create_Mode = Employee_Role;
                objModel.HDR_EmployeeIds = Employee_Ids;
                dt = objService.Save_Claim(objModel);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update_Claim(Claim_Model objModel, Int64 BatchGid, Int64 RaiserGid, Int64 TypeGid, Int64 SubtypeGid, string Employee_Ids, string Employee_Role, Int64 EcfRaiser)
        {
            string Result = string.Empty;
            try
            {
                objModel.HDR_Batch_Gid = BatchGid;
                objModel.Logged_UserGid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                objModel.HDR_EmployeeIds = Employee_Ids;
                objModel.CLM_Emp_Gid = EcfRaiser;
                dt = objService.Update_Claim(objModel);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Common method for deleting claim details.
        [HttpPost]
        public ActionResult Delete_Claim_Datails(Int64 GridGid, Int64 Batch_Gid, string GridName)
        {
            string Result = string.Empty;
            try
            {
                objModel.Common_Gridgid = GridGid;
                objModel.HDR_Batch_Gid = Batch_Gid;
                objModel.Common_Gridname = GridName;
                objModel.Logged_UserGid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = objService.Delete_ClaimDetails(objModel);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region CRUD Methods for Expense Gird.
        public ActionResult Save_Expense(Claim_Model objModel)
        {
            string Result = string.Empty;
            try
            {
                string LoginMode = Session["Employee_Role"].ToString();
                if (LoginMode.ToString().Equals("P"))
                {
                    objModel.Logged_UserGid = Convert.ToInt64(Session["Proxy_InsertBy"].ToString());
                }
                else
                {
                    objModel.Logged_UserGid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                }

                dt = objService.Save_Expense(objModel);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update_Expense(Claim_Model objModel)
        {
            string Result = string.Empty;
            try
            {
                objModel.Logged_UserGid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = objService.Update_Expense(objModel);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Update method for payment grid.
        public ActionResult Update_Payment(Claim_Model objModel /*, string Paymode, string Glcode*/)
        {
            string Result = string.Empty;
            try
            {
                objModel.Logged_UserGid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                //objModel.Pay_paymodemode = Paymode;
                //objModel.Pay_glcode = Glcode;
                dt = objService.Update_Payment(objModel);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Update Method for Remarks grid.
        public ActionResult Update_Remarks(Claim_Model objModel)
        {
            string Result = string.Empty;
            try
            {
                objModel.Logged_UserGid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = objService.Update_Remarks(objModel);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Save/Update Method for Travel/Boarding/Conveyance.
        public ActionResult Update_TravelBoardingConveyance_Details(Claim_Model objModel, Int64 Debitelinegid, Int64 Ecfgid, Int16 DetailTypeId)
        {
            string Result = string.Empty;
            try
            {
                string LoginMode = Session["Employee_Role"].ToString();
                if (LoginMode.ToString().Equals("P"))
                {
                    objModel.Logged_UserGid = Convert.ToInt64(Session["Proxy_InsertBy"].ToString());
                }
                else
                {
                    objModel.Logged_UserGid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                }
                objModel.Det_debilnegid = Debitelinegid;
                objModel.Det_ecfgid = Ecfgid;
                objModel.Det_gridtypegid = DetailTypeId;
                dt = objService.Update_TravelBoardingConveyance_Details(objModel);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region method for Get/Updating tax details.
        [HttpPost]
        public ActionResult Get_TaxDetails(Claim_Model objModel)
        {
            string Result = string.Empty;
            try
            {
                dt = objService.Get_TaxDetails(objModel);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Read_Taxable([DataSourceRequest]DataSourceRequest request, string GirdName, Int64 BatchGid, string DebitLineGid)
        {
            Claim_Model _obj = new Claim_Model();
            try
            {
                _obj.Hidden_taxtype = GirdName;
                _obj.Hidden_ecfgid = BatchGid;
                _obj.Hidden_debitlinegids = DebitLineGid;
                return Json(objService.Read_TaxDetails(_obj).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return View();
        }

        [HttpPost]
        public ActionResult Update_TaxDetails(Claim_Model objModel)
        {
            string Result = string.Empty;
            try
            {
                objModel.Logged_UserGid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                dt = objService.Update_TaxDetails(objModel);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Claim action Draft/Submit.
        public ActionResult Claim_DraftSubmit(Claim_Model objModel)
        {
            string Result = string.Empty;
            try
            {
                objModel.Logged_UserGid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                objModel.CLM_Create_Mode = Session["Employee_Role"].ToString();
                dt = objService.Claim_DraftSubmit(objModel);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Show attribute Partial view.
        [HttpPost]
        public ActionResult Show_APCheckList_PartialView(Int64 SubType_Gid)
        {
            try
            {
                ViewBag.APChecklist = objService.Get_APCheckList(SubType_Gid);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return PartialView("APCheckList_PartialView", ViewBag.APChecklist);
        }
        #endregion

        #region Method for updating checklist reasons,
        [HttpPost]
        public JsonResult Update_Checklist(Int64 ecf_gid, Int64 checklist_gid, string checklist_reason, Int16 checklist_status)
        {
            string result = string.Empty;
            APCheckerEntities _objapmodel = new APCheckerEntities();
            try
            {
                _objapmodel.logged_user_gid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                _objapmodel.CheckList_ecf_gid = ecf_gid;
                _objapmodel.CheckList_Gid = checklist_gid;
                _objapmodel.CheckList_Reason = checklist_reason;
                _objapmodel.CheckList_status = Convert.ToString(checklist_status);
                result = objService.Update_Checklist(_objapmodel);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult APChecklist_Submit(APCheckerEntities APobjModel)
        {
            string Result = string.Empty;
            try
            {
                APobjModel.logged_user_gid = Convert.ToInt64(Session["Employee_Gid"].ToString());
                if (APobjModel.gldate == "")
                {
                    APobjModel.gldate = null;
                }
                dt = objService.APChecklist_Submit(APobjModel);
                Result = JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Get_Selected_Checklists(Claim_Model modelObj_claim)
        {
            string Result = string.Empty; string Result1 = string.Empty;
            try
            {
                ds = objService.Get_Selected_Checklists(modelObj_claim);
                if (ds.Tables.Count > 0)
                {
                    Result = JsonConvert.SerializeObject(ds.Tables[0]);
                    Result1 = JsonConvert.SerializeObject(ds.Tables[1]);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(new { Result, Result1 }, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}