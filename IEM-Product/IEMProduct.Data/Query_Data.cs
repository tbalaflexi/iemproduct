﻿using DataAccessHandler.Models.IEMProduct;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMProduct.Data
{
   public class Query_Data
    {
        Query_Model ModelObject = new Query_Model();                                   //Query model object
        List<Query_Model> ModelList = new List<Query_Model>();                         //Query  list model object
        DataTable dt = new DataTable();
        Common ObjCmn = new Common();                                                              //list model object,, Int64 User_GID

        // Query Read
        public List<Query_Model> ReadRecord(Query_Model ModelObject)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                //ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, User_GID, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.dept, ModelObject.deptid, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.status, ModelObject.statusid, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.fromdate, ModelObject.fromdate, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.todate, ModelObject.todate, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ecfno, ModelObject.ecf_no, DbType.String));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpGetquery, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
                foreach (DataRow dr in dt.Rows)
                {
                    ModelList.Add
                        (
                        new Query_Model
                        {
                           // ecfid = (dr["ecf_gid"].ToString()),
                           id = Convert.ToInt64(dr["ecf_gid"].ToString()),
                           ecf_no = dr["ecfno"].ToString(),
                          // deptid = Convert.ToInt64(dr["deptid"].ToString()),
                          // deptname = dr["deptname"].ToString(),
                            
                          //  statusid = Convert.ToInt64(dr["deptname"].ToString()),
                           ClaimType = dr["ClaimType"].ToString(),
                           //date = dr["ecf_date"].ToString(),
                           date = Convert.ToDateTime(dr["ecf_date"].ToString()),
                           ecf_amount = (dr["ecf_amount"].ToString()),
                           Raiser_Gid = Convert.ToInt64(dr["Raiser_Gid"].ToString()),
                            // date = dr["ecf_date"].ToString(),
                           raiser = dr["Raiser"].ToString(),
                           suppliername = dr["ECF Supplier/Employee"].ToString(),

                           ecf_status = dr["ecfstatus_name"].ToString(),
                            // Action = dr["Action"].ToString(),
                            //  SupplierEmployee_Type = dr["ecf_supplier_employee"].ToString(),
                           Batch_Gid = Convert.ToInt64(dr["ecf_batch_gid"].ToString()),
                            // Ecf_Gid = Convert.ToInt64(dr["ecf_gid"].ToString()),

                            Approver_Status = dr["Approval"].ToString(),
                        });
                }
                return ModelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       

        // dropdown values  Department,status
        public DataTable GetDepartment(string parentcode)
        {

            ObjCmn.parameters = new List<IDbDataParameter>();
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ParentCode, 50, parentcode, DbType.String));
            dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpGetqcDropdown, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            return dt;
        }

       // drop down values binding in status
        public DataTable dropdownvalues(string flag)
        {

            ObjCmn.parameters = new List<IDbDataParameter>();
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.flag, 50, flag, DbType.String));
            dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpNonqcDropdownValues, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            return dt;
        }
    }
}
