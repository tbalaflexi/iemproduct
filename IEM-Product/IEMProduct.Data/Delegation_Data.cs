﻿using DataAccessHandler.Models.IEMProduct;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMProduct.Data
{
   public class Delegation_Data
    {
        Delegation_Model ModelObject = new Delegation_Model();                                   //Delegation model object
        List<Delegation_Model> ModelList = new List<Delegation_Model>();                         //Delegation  list model object
        DataTable dt = new DataTable();
        Common ObjCmn = new Common();                                                              //list model object

        //Delegation Master Read

        public List<Delegation_Model> ReadRecord(Int64 User_Gid)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, User_Gid, DbType.Int64));   
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpGetDelegate, CommandType.StoredProcedure);
                foreach (DataRow dr in dt.Rows)
                {
                    ModelList.Add
                        (
                        new Delegation_Model
                        {
                            delegate_gid = Convert.ToInt32(dr["delegate_gid"].ToString()),
                             empid =Convert.ToInt32(dr["employee_gid"].ToString()), 
                             empname = dr["EmpCodeEmpName"].ToString(),
                             delegate_delmat_flag = dr["delegate_delmat_flag"].ToString(),
                            delegate_delmattypeid = Convert.ToInt32(dr["delegate_delmattypeid"].ToString()),
                             delegate_delmattype = dr["delmattypename"].ToString(),
                            delegate_active = dr["delegate_active"].ToString(),
                            delegate_period_from = dr["delegate_period_from"].ToString(),
                            delegate_period_to = dr["delegate_period_to"].ToString(),
                            delegate_workflow_Role = (dr["delegate_workflow_Rolegid"].ToString()),
                            Rolename = dr["rolename"].ToString(),
                            Selected_supervisor_gid = Convert.ToInt32(dr["employee_gid"].ToString()),

                        });
                }
                return ModelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Delegation Save

        public DataTable SaveRecord(Delegation_Model ModelObject, Int64 User_Gid)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Create, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegate_Id, 0, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegate_By, 50, User_Gid, DbType.String)); 
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegate_To, 50, ModelObject.empid, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegate_delmatflag, 50, ModelObject.delegate_delmat_flag, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegate_delmattype, 50, ModelObject.delegate_delmattypeid, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegate_Active, 50, ModelObject.delegate_active, DbType.String));
                if (ModelObject.delegate_period_from != null)
                {
                    if (ModelObject.delegate_period_from.Length > 19)
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegateperiodfrom, DateTime.Parse(ModelObject.delegate_period_from.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegateperiodfrom, DateTime.ParseExact(ModelObject.delegate_period_from.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegateperiodfrom, ModelObject.delegate_period_from, DbType.String));
                }


                if (ModelObject.delegate_period_to != null)
                {
                    if (ModelObject.delegate_period_to.Length > 19)
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegateperiodto, DateTime.Parse(ModelObject.delegate_period_to.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegateperiodto, DateTime.ParseExact(ModelObject.delegate_period_to.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegateperiodto, ModelObject.delegate_period_to, DbType.String));
                }


                //ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegateperiodfrom, 50, ModelObject.delegate_period_from, DbType.String));
              //  ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegateperiodto, 50, ModelObject.delegate_period_to, DbType.String));

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.delegate_role, 50, ModelObject.delegate_workflow_Role, DbType.String));
             //  ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, User_Gid, DbType.String));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpDmlDelegate, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

       //Delegation Update

        public DataTable UpdateRecord(Delegation_Model ModelObject,Int64 User_Gid)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Create, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegate_Id, ModelObject.delegate_gid, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegate_By, 50,User_Gid, DbType.String)); 
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegate_To, 50, ModelObject.empid, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegate_delmatflag, 50, ModelObject.delegate_delmat_flag, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegate_delmattype, 50, ModelObject.delegate_delmattypeid, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegate_Active, 50, ModelObject.delegate_active, DbType.String));
                if (ModelObject.delegate_period_from != null)
                {
                    if (ModelObject.delegate_period_from.Length > 19)
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegateperiodfrom, DateTime.Parse(ModelObject.delegate_period_from.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegateperiodfrom, DateTime.ParseExact(ModelObject.delegate_period_from.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegateperiodfrom, ModelObject.delegate_period_from, DbType.String));
                }


                if (ModelObject.delegate_period_to != null)
                {
                    if (ModelObject.delegate_period_to.Length > 19)
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegateperiodto, DateTime.Parse(ModelObject.delegate_period_to.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegateperiodto, DateTime.ParseExact(ModelObject.delegate_period_to.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegateperiodto, ModelObject.delegate_period_to, DbType.String));
                }


                //ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegateperiodfrom, 50, ModelObject.delegate_period_from, DbType.String));
                //  ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegateperiodto, 50, ModelObject.delegate_period_to, DbType.String));

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.delegate_role, 50, ModelObject.delegate_workflow_Role, DbType.String));
                //ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
                //cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.UserGid, ModelObject.Loginid, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, User_Gid, DbType.String));
              
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpDmlDelegate, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        //Delegation Delete 
        public DataTable DeleteRecord(Delegation_Model ModelObject, Int64 User_Gid)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Delete, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegate_Id, ModelObject.delegate_gid, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegate_By, 50, 0, DbType.String)); 
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegate_To, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegate_delmatflag, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegate_delmattype, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegate_Active, 50,0, DbType.String));


                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegateperiodfrom, 50, "2019-01-01", DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegateperiodto, 50, "2019-01-01", DbType.String));

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.delegate_role, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, User_Gid, DbType.String));
              
                // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
                //cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.UserGid, ModelObject.Loginid, DbType.String));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpDmlDelegate, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

       // dropdown values delmat type,role
        public DataTable Getdropdown(string parentcode)
        {

            ObjCmn.parameters = new List<IDbDataParameter>();
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ParentCode, 50, parentcode, DbType.String));
            dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpGetqcDropdown, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            return dt;
        }
       //non QC master dropdown values empcode,empname
        public DataTable dropdownvalues(string flag, string EmployeeName)
        {

            ObjCmn.parameters = new List<IDbDataParameter>();
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.flag, 50, flag, DbType.String));
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.p_Employee, EmployeeName, DbType.String));
            dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpNonqcDropdownValues, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            return dt;
        }

       //
        public DataTable GetEmployee_ByCodeorName(string EmployeeName, string flag, string Master)
        {

            ObjCmn.parameters = new List<IDbDataParameter>();
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.p_Master, Master, DbType.String));
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.p_MasterGid, 0, DbType.String));
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.p_Employee, EmployeeName, DbType.String));
            dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SP_EOW_Claim_GetDropDownList, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            return dt;
        }

    }
}
