﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using DataAccessHandler;

namespace IEMProduct.Data
{
    public class Common
    {
        #region Connection String and Parameter Values
        public DBManager dbManager = new DBManager("ConnectionString");
        public List<IDbDataParameter> parameters;
        #endregion

        #region Operation Type(CURD)
        public readonly string Create = "C";
        public readonly string Read = "R";
        public readonly string Update = "U";
        public readonly string Delete = "D";
        #endregion

        #region Loged User Details
        public readonly string LoggedUser = "Flexi";
        public readonly string LoginUserId = "In_LoginUser_GID";
        #endregion

        #region Strored Procedure CURD For QC MAster Screen
        public string strSP = "";
        public readonly string SPGetqc = "SP_Get_QCDetails";
        public readonly string SpGetqcDetails = "SP_Get_QCMasterDetails";
        public readonly string SpDmlqcMaster = "SP_DML_QCMaster";
        public readonly string SpParentDropdown = "SP_Get_ParentCode";
        public readonly string SpDepentDropdown = "SP_Get_DependCode";
        #endregion

        #region Strored Procedure CURD For Non QC MAster Screen Transaction Master
        public readonly string SpGetTransaction = "SP_Get_Transaction";
        public readonly string SpDmlTransaction = "SP_DML_Transaction";
        public readonly string SpGetqcDropdown = "SP_Fetch_QCData";
        public readonly string SpGetqcValues = "SP_GetDropdownValues";
        public readonly string SpNonqcDropdownValues = "Sp_NonQCDropDownValues"; 
        #endregion

        #region Strored Procedure CURD For Non QC MAster Screen PayBank Master
        public readonly string SpGetPaybank = "SP_Get_PayBank";
        public readonly string SpDmlPayBank = "SP_DML_PayBank";
        #endregion

        #region Strored Procedure CURD For Non QC MAster Screen  Attachment Master
        public readonly string SpGetAttachment = "SP_Get_Attachment";
        public readonly string SpDmlAttachment = "SP_DML_Attachement";
        //download  file
        public readonly string SPDownloadfile = "SP_Get_Form_Info";
        public readonly string SpGetAttachments = "SP_Get_BatchWise_Attachment";

        public readonly string p_module_flag = "In_module";
        public readonly string p_batch_gid = "In_batch_gid";
        public readonly string p_ecf_gid = "In_ecf_gid";
        public readonly string p_debitinvoice_flag = "In_debitinvoice_flag";
        #endregion

        #region Strored Procedure CURD For Non QC MAster Screen  Sub Category Master
        public readonly string SpGetSubCategory = "SP_Get_SubCategory";
        public readonly string SpDmlSubCategory = "SP_DML_SubCategory";
        #endregion

        #region Strored Procedure For Workbench
        public readonly string SpGetWorkbench = "SP_Get_Workbench";
        #endregion

        #region Stored Procedure Params For QC MAster Screen.
        public readonly string MasterId = "In_masters_gid";
        //public readonly string MasterTranGid = "In_MasterTran_Gid";
        public readonly string MasterCode = "In_masters_code";
        public readonly string ParentCode = "In_masters_parentcode";
        public readonly string DependCode = "In_masters_dependcode";
        public readonly string MasterDesc = "In_masters_description";
        public readonly string flag = "In_flag";
        public readonly string Action = "In_Action";


        // transaction Master
        public readonly string PayBankGId = "IN_paybank_gid";
        public readonly string PayBankQcBank = "IN_paybank_QCbank";
        public readonly string PayBankBankCode = "IN_paybank_bank_code";
        public readonly string PayBankBankName = "IN_paybank_bank_name";
        public readonly string PayBankAccNo = "IN_paybank_acc_no";
        public readonly string PayBankIFSC = "IN_paybank_ifsc_code";
        public readonly string PayBankBranch = "IN_paybank_branch_name";
        public readonly string PayBAnkGlNo = "IN_gl_no";
        public readonly string PayBankPeriodFrom = "IN_paybank_period_from";
        public readonly string PayBankPeriodTo = "IN_paybank_period_to";
        public readonly string PayBankMemoFileName = "IN_paybank_memo_filename";
        public readonly string PayBankOnlineFileName = "IN_paybank_online_filename";
        public readonly string PAyBankBAnkAddress = "IN_paybank_bank_address";
        public readonly string PayBankDefault = "IN_paybank_default";
        public readonly string PayBankCHQDefault = "IN_paybank_CHQ_default";
        public readonly string PAyBankEFTDefault = "IN_paybank_EFT_default";
        public readonly string PayBankERADefault = "IN_paybank_ERA_default";
        public readonly string PayBankDDDefault = "IN_paybank_DD_default";

        //pay bank master

        public readonly string TranGId = "In_Tran_Gid";
        public readonly string TranId = "In_TranGid";
        public readonly string TranType = "In_Tran_Type";
        public readonly string TranSubType = "In_Tran_SubType";
        public readonly string TranAttachement = "In_Tran_Attachement";
        public readonly string TranPhysicalRecepting = "In_Tran_PhysicalReceipting";

        //attachment
        public readonly string AttachId = "In_attachment_gid";
        public readonly string AttachRefflag = "In_attachment_ref_flag";
        public readonly string AttachRefId = "In_attachment_ref_gid";
        public readonly string AttachFilename = "In_attachment_filename";
        public readonly string AttachTypeId = "In_attachment_attachmenttype_gid";
        public readonly string AttachDesc = "In_attachment_desc";
        public readonly string AttachDocname = "In_attachment_docname";
        public readonly string AttachInvoiceId = "In_attachment_invoice_gid";
        public readonly string AttachmentModuleFlag = "In_attachment_module_flag";
        public readonly string AttachDebitLineId = "In_attachment_debitline_gid";

        //Sub Category
        public readonly string SubCatId = "In_expsubcat_gid";
        public readonly string ExpCatId = "In_expsubcat_expcat_masters_gid";
        public readonly string SubCatCode = "In_expsubcat_code";
        public readonly string SubCatName = "In_expsubcat_name";


        #endregion

        #region Strored Procedure CURD For  WorkFlow Screen.
        public readonly string SpGetWorkFlowDropDown = "SP_Get_Workflow_SummaryList";
        public readonly string Spsetworkflowheader = "SP_Set_WorkFlowHeaders";
        public readonly string SpDMLWorkFlowMaster = "SP_DML_WorkFlow";
        public readonly string SpGetAttribute = "SP_Get_Attributes";
        public readonly string SpGetAttribute_ForWorkFlow = "SP_Get_Attributes_ForWorkFlow";
        #endregion

        #region Stored Procedure Params For WorkFlow Screen.
        public readonly string Master = "In_Master";
        public readonly string Parent_Code = "In_Depend_Code";
        public readonly string WorkFlow_TypeCode = "In_TypeCode";
        public readonly string WorkFlow_TypeGid = "In_Type_Gid";
        public readonly string WorkFlow_SubTypeCode = "In_SubTypeCode";
        public readonly string WorkFlow_Parent = "In_AttributeParent";
        public readonly string WorkFlow_Child = "In_AttributeChild";
        public readonly string WorkFlow_Gid = "In_WFAction_Gid";
        public readonly string WorkFlow_RoleFrom = "In_RoleFrom";
        public readonly string WorkFlow_Action = "In_ActionCode";
        public readonly string WorkFlow_Status = "In_Status";
        public readonly string WorkFlow_RoleTo = "In_RoleTo";
        public readonly string WorkFlow_TimeOut = "In_TmeOut";
        public readonly string WorkFlow_TimeOut_RoleTo = "In_TmeOut_RoleTo";
        public readonly string WorkFlow_Additonal = "In_Additional";
        public readonly string WorkFlowHeader_Gid = "In_WorkFlow_Id";
        #endregion

        #region Stored Procedure for Login.
        public readonly string SpCheckUserCredential = "SP_CheckUserCredential";
        #endregion

        #region Stored Procedure Parameter for Login.
        public readonly string Login_UserCode = "p_UserCode";
        public readonly string Login_UserPassword = "p_Password";
        public readonly string Login_Mode = "p_LoginMode";
        public readonly string Login_ProxyFrom = "p_ProxyFrom";
        #endregion

        //Ramya
        #region SP CURD for Delmat & SlabRange Screen
        public readonly string SpDMLDelmat_SlabRange = "SP_DML_Delmat_SlabRange";
        public readonly string SpGet_Delmat_SlabRange = "SP_Get_Delmat_SlabRange";
        public readonly string SpGet_DelmatName = "SP_Get_DelmatName";

        public readonly string SpGet_Delmatheader = "SP_Get_Delmat_ByDelmatGid";
        public readonly string SpGetDelmat = "SP_Get_Delmat";
        #endregion

        #region Stored Procedure Params For  Delmat & SlabRange Screen.
        public readonly string Slabrange_Gid = "In_SlagRange_gid";
        public readonly string Slabrange_Name = "In_SlabRange_Name";
        public readonly string Slabrange_From = "In_SlabRange_From";
        public readonly string Slabrange_To = "In_SlabRange_To";
        #endregion

        #region SP CURD for Delmat Exception Screen
        public readonly string SpGet_Delmat_Exception = "SP_Get_Delmat_Exception";
        public readonly string SpGet_Delmat_Employee = "SP_Get_Employee";
        public readonly string SpDMLDelmat_Exception = "SP_DML_Delmat_Exception";
        #endregion

        #region Stored Procedure Params For  Delmat Exception Screen.
        public readonly string Delmat_Gid = "In_delmat_gid";
        public readonly string delmatexception_gid = "In_DelmatException_gid";
        public readonly string delmatexception_delmat_gid = "In_delmatexception_delmat_gid";
        public readonly string delmatexception_to = "In_delmatexception_to";
        public readonly string delmatexception_title = "In_delmatexception_title";
        public readonly string delmatexception_Limit = "In_delmatexception_limit";
        #endregion

        #region Stored Procedure for CURD for Delmat Flow Grid
        public readonly string SpDMLDelmat_DelmatFlow = "SP_DML_Delmat_DelmatFlow";
        public readonly string SpGet_Delmat_DelmatFlow = "SP_Get_Delmat_DelmatFlow";
        public readonly string SpGet_Delmat = "SP_DML_Delmat";
        public readonly string SpDMLDelmatsummary = "SP_DML_SummaryDelmat";
        public readonly string SP_Set_Delmat_MatrixLogic = "SP_Set_Delmat_MatrixLogic";
        #endregion

        #region Stored Procedure Params for Delmat Flow Grid
        public readonly string DelmatFlow_Gid = "In_delmatflow_gid";
        public readonly string delmatflow_delmat_gid = "In_delmatflow_delmat_gid";
        public readonly string DelmatFlow_Order = "In_delmatflow_order";
        public readonly string DelmatFlow_Title = "In_delmatflow_title_value";
        public readonly string DelmatFlow_Title_Ref_Gid = "In_delmatflow_title_ref_gid";
        public readonly string DelmatFlow_SlabRange_Gid = "In_delmatflow_SlabRange_Gid";
        public readonly string DelmatFlow_Limit = "In_DelmatFlow_Limit";

        public readonly string delmat_name = "In_delmat_name";
        public readonly string delmat_type_gid = "In_delmat_type_gid";
        public readonly string delmatsubtype_subtype_gids = "In_delmatsubtype_subtype_gids";
        public readonly string delmatattribute_attributechild_gids = "In_delmatattribute_attributechild_gids";
        public readonly string delmatdept_dept_gids = "In_delmatdept_dept_gids";
        public readonly string delmat_Version = "In_delmat_Version";
        public readonly string delmat_Cloned = "In_delmat_Cloned";
        public readonly string delmat_Cloned_delmat_gid = "In_delmat_Cloned_delmat_gid";
        public readonly string delmat_active = "In_delmat_active";
        #endregion

        #region Audit Trail
        public readonly string ECF_Gid = "In_EcfId";
        public readonly string SP_Get_eow_AuditTrail = "SP_Get_eow_AuditTrail";
        #endregion

        #region BulkUpload
        public readonly string Master_code = "In_MasterCode";
        public readonly string Parent_code = "In_ParentCode";
        public readonly string Dependant_Code = "In_DependantCode";
        public readonly string Dependant_ParentCode = "In_Dependant_ParentCode";

        public readonly string SP_eow_ValidateExcelMasterCode = "SP_eow_ValidateExcelMasterCode";
        #endregion

        #region ProxyDetail
        public readonly string userId = "In_UserId";
        public readonly string proxyID = "In_proxyID";
        public readonly string proxyFromId = "In_proxyFromId";
        public readonly string proxyToId = "In_proxyToId";
        public readonly string proxyPeriodFrom = "In_proxyPeriodFrom";
        public readonly string proxyPeriodTo = "In_proxyPeriodTo";
        public readonly string proxyActive = "In_proxyActive";

        #endregion

        #region Storedprocedures for Proxy Master
        public readonly string SpGetProxyDetails = "SP_Get_ProxyBy_Id";
        public readonly string SaveUpdateProxyDetails = "Save_Update_ProxyDetails";
        public readonly string RemoveProxyDetails = "SP_Delete_ProxyDetails";
        #endregion

        #region Delegate
        public readonly string Delegate_Id = "In_delegate_gid";
        public readonly string Delegate_By = "In_delegate_by";
        public readonly string Delegate_To = "In_delegate_to";
        public readonly string Delegate_delmatflag = "In_delegate_delmat_flag";
        public readonly string Delegate_Active = "In_delegate_active";
        public readonly string Delegateperiodfrom = "In_delegate_period_from";
        public readonly string Delegateperiodto = "In_delegate_period_to";
        public readonly string Delegate_Remark = "In_delegate_remark";
        public readonly string Delegate_delmattype = "In_delegate_delmattype";
        public readonly string delegate_role = "In_delegate_workflow_Rolegid";



        public readonly string SpGetDelegate = "SP_Get_Delegate";
        public readonly string SpDmlDelegate = "SP_DML_Delegate";
        #endregion

        #region stored procedure for Transaction Cancellation
        public readonly string Spgettransactioncancellation = "Sp_Get_transactioncancellation";
        #endregion

        #region Workbeck parameters

        public readonly string loginID = "In_LoginId";
        public readonly string actionType = "In_ActionType";

        #endregion

        #region Storedprocedures for Workbench Master
        public readonly string SpGetWorkBenchHistory = "Sp_GetWorkbench_History";

        #endregion

        #region Storedprocedures for Query
        public readonly string SpGetquery = "Sp_Fetch_ECfQuery";

        public readonly string fromdate = "In_fromdt";
        public readonly string todate = "In_todate";
        public readonly string dept = "In_Deptid";
        public readonly string status = "In_Statusid";
        public readonly string ecfno = "In_ecfno";
        #endregion

        #region Proxy Login Ramya
        public readonly string sp_Get_Proxy = "pr_iem_Proxy";
        #endregion

        #region Checklist
        public readonly string checklistgid = "In_checklist_gid";
        public readonly string checklisttypeid = "In_checklist_type_gid";
        public readonly string checklistsubtypeid = "In_checklist_subtype_gid";
        public readonly string checkliststatus = "In_checklist_isactive";
        public readonly string checklistdisplayorder = "In_checklist_displayorder";
        public readonly string checklistitem = "In_checklist_item";
        public readonly string checklistreason = "In_checklist_reason";

        public readonly string Spdmlchecklist = "SP_DML_checklist";
        public readonly string SpGetchecklist = "sp_Get_Checklist";
        #endregion

        #region FundUpdate
        public readonly string Spgetfundupdate = "Sp_Get_FundUpdate";
        public readonly string Spdmlfundupdate = "SP_DML_FundUpdate";

        public readonly string fundgid = "In_fundupdate_gid";
        public readonly string fundbankid = "In_fundupdate_paybankname_id";
        public readonly string funddate = "In_fundupdate_date";
        public readonly string fundopeningbalance = "In_fundupdate_openingbal";
        public readonly string fundinflow = "In_fundupdate_inflow";
        public readonly string fundavailable = "In_fundupdate_fundavailable";

        #endregion

        #region StoredProcedure For Profile
        public readonly string SPGetEmployeeDetails = "SP_IEM_Get_EmployeeDetails";
        public readonly string SPGetEmployeeHirackey = "SP_IEM_Get_EmployeeHirackey";
        #endregion

        #region Profile Parameters
        public readonly string EMPGid = "In_Emp_Gid";
        #endregion

        #region RoleVsEmployee
        public readonly string SP_Get_RoleVsEmployee = "SP_Get_RoleVsEmployee";
        public readonly string SP_Get_EmployeeByRole = "SP_Get_EmployeeByRole";
        public readonly string SP_DML_EmployeeByRole = "SP_DML_EmployeeByRole";
        public readonly string p_roleemployee_gid = "In_roleemployee_gid";
        public readonly string p_Role = "In_Role";
        public readonly string p_Role_Gid = "In_Role_Gid";
        public readonly string p_Employee_Gid = "In_Employee_Gid";
        public readonly string p_RoleEmployee_Gid = "In_roleemployee_gid";
        public readonly string SP_EOW_Claim_GetDropDownList = "SP_EOW_Claim_GetDropDownList";
        public readonly string p_Master = "In_Master";
        public readonly string p_MasterGid = "In_MasterGid";
        public readonly string p_Employee = "In_Employee";
        public readonly string SP_Get_RoleIdByEmployee = "SP_Get_RoleIdByEmployee";
        public readonly string p_RoleGids = "In_RoleGids";
        public readonly string SP_DML_RoleToEmployee = "SP_DML_RoleToEmployee";
        #endregion

        #region Employee Master
        public readonly string EmpGid = "In_employee_gid";
        public readonly string EmpCode = "In_employee_code";
        public readonly string EmpPassword = "In_employee_password";
        public readonly string EmpName = "In_employee_name";
        public readonly string EmpGender = "In_employee_gender";
        public readonly string EmpDob = "In_employee_dob";
        public readonly string EmpDoj = "In_employee_doj";
        public readonly string EmpDesignationGid = "In_employee_iem_designation_gid";
        public readonly string EmpGradeGid = "In_employee_grade_gid";
        public readonly string EmpDeptGid = "In_employee_dept_gid";
        public readonly string EmpAddress1 = "In_employee_addr1";
        public readonly string EmpCityGid = "In_employee_city_gid";
        public readonly string EmpPinCode = "In_employee_pincode";
        public readonly string EmpOfficeEmailid = "In_employee_office_email";
        public readonly string EmpPersonalEmailid = "In_employee_personal_email";
        public readonly string EmpMobileNo = "In_employee_mobile_no";
        public readonly string EmpBranchgid = "In_employee_branch_gid";
        public readonly string EmpEraAccNo = "In_employee_era_acc_no";
        public readonly string EmpEraBankid = "In_employee_era_bank_gid";
        public readonly string EmpCardNo = "In_employee_card_no";
        public readonly string EmpFcGid = "In_employee_FC_gid";
        public readonly string EmpCCGid = "In_employee_CC_gid";
        public readonly string EmpProductGid = "In_employee_product_gid";
        public readonly string EmpFunctionName = "In_employee_functionname";
        public readonly string EmpSupervisorCode = "In_employee_supervisor_code";
        public readonly string EmpAdd2 = "In_employee_addr2";
        public readonly string EmpPermanantAddress = "In_employee_permanent_addr";
        public readonly string EmpContactNo = "In_employee_contact_no";
        public readonly string EmpempBranchgid = "In_employee_employeebranch_gid";
        public readonly string EmpEraBankCode = "In_employee_era_bank_code";
        public readonly string EmpEraBranchGid = "In_employee_era_Branch_Gid";
        public readonly string EmpFcccGid = "In_employee_fccc_gid";
        public readonly string EmpDor = "In_employee_dor";
        public readonly string EmpStatus = "In_employee_status";
        public readonly string EmpLastmodificationon = "In_HRIS_LASTMODIFIEDON";
        public readonly string EmpLastworkingDate = "In_employee_lastworkingdate";
        public readonly string EmpEffectiveDate = "In_employee_effectivedate";
        public readonly string EmpBankAccountType = "In_employee_bankaccounttype";
        public readonly string EmpSupervisorGid = "In_employee_supervisor_gid";
        public readonly string EmpPhysicalBranchGid = "In_employee_physical_branch_gid";
        public readonly string EmpAccountNo = "In_employee_era_acc_no";


        public readonly string Sp_Dml_EmployeeMaster = "Sp_DML_EmployeeMaster";

        #endregion

        #region FlowAssignment
        public readonly string userID = "In_UserID";
        public readonly string AssignType = "In_AssignTypeID";
        public readonly string AssignRefType = "In_RefTypeID";
        public readonly string FlowactionType = "In_ActionType";
        public readonly string ecfSelectedList = "In_EcfSelectedList";
        public readonly string assignToID = "In_FlowAssignToID";
        public readonly string remark = "In_FlowAssignRemarks";
        public readonly string TypeID = "In_TypeID";
        public readonly string findSearch = "In_FindSearch";

        public readonly string SPReadFlowAssignment = "SP_Read_FlowAssignList";
        public readonly string SPSetFlowAssignmentDetails = "Sp_Set_FlowAssignmentDetails";
        public readonly string SpGetFlowAssignList = "Sp_Get_FlowAssignmentDetails";
        public readonly string SpGetAssignmentRefNoUserList = "SP_Get_AssignmentRefNoUserList";

        #endregion

        #region Access Privileges Stored Procedure's Name's
        public readonly string SP_Get_AccessPrivilegesDetails = "SP_Get_AccessPrivileges_Details";
        public readonly string SP_Set_AccessPrivileges = "SP_Set_AccessPrivileges";
        #endregion

        #region Access Privileges Stored Procedure's Parameter's Name
        public readonly string Role_GID = "In_Role_GID";
        public readonly string Menu_Checked = "In_menuchecked";
        public readonly string Menu_NotChecked = "In_menunotchecked";
        #endregion

        #region EMP Master Upload Stored Procedure
        public readonly string SP_Get_EmpUploadDetails = "SP_IEM_Get_Tmp_EmpUpload";
        public readonly string SP_Get_FileCount = "SP_Get_FileCount";
        public readonly string SP_DML_File = "SP_DML_File";
        public readonly string SP_Delete_Temp_EmpUpload = "SP_Tmp_Delete_EmpUpload";
        public readonly string SP_DML_Tmp_EmpUpload = "SP_Set_Tmp_EmpMasterUpload";
        public readonly string SP_DML_EmpUploadDetails = "SP_IEM_DML_EmployeeUpload";
        #endregion

        #region EMP Master Upload Procedure Parameter's
        public readonly string P_File_Gid = "In_File_Gid";
        public readonly string P_FileName = "In_File_Name";
        public readonly string P_FileType = "In_File_type";

        public readonly string P_EmpPassword = "In_Emp_Password";

        public readonly string P_EmpCode = "In_employee_code";
        public readonly string P_EmpName = "In_employee_name";
        public readonly string P_EmpGender = "In_employee_gender";
        public readonly string P_EmpDob = "In_employee_dob";
        public readonly string P_EmpDoj = "In_employee_doj";
        public readonly string P_DesignationCode = "In_designation_code";
        public readonly string P_Gradecode = "In_grade_code";
        public readonly string P_DeptCode = "In_dept_code";
        public readonly string P_Emp_Branch_Code = "In_emp_branch_code";
        public readonly string P_FC_Code = "In_FC_Code";
        public readonly string P_CC_Code = "In_CC_Code";
        public readonly string P_Product_Code = "In_product_code";
        public readonly string P_Addr1 = "In_addr1";
        public readonly string P_Addr2 = "In_addr2";
        public readonly string P_City_Code = "In_city_code";
        public readonly string P_Pincode = "In_pincode";
        public readonly string P_Personal_Email = "In_personal_email";
        public readonly string P_Office_Email = "In_office_email";
        public readonly string P_Mobile_No = "In_mobile_no";
        public readonly string P_Bank_Code = "In_Bank_Code";
        public readonly string P_Bank_Branch_Code = "In_bank_branch_code";
        public readonly string P_AccNo = "In_Acc_No";
        public readonly string P_AccType = "In_Acc_Type";
        public readonly string P_CardNo = "In_Card_No";
        public readonly string P_Emp_Permanent_Addr = "In_employee_permanent_addr";
        public readonly string P_Supervisor_Code = "In_supervisor_code";
        public readonly string P_Emp_Dor = "In_employee_dor";
        public readonly string P_LastWorkingDate = "In_last_working_date";
        public readonly string P_EffictiveDate = "In_effictive_date";

        #endregion

        #region Mail Template Stored Procedure Names
        public readonly string Sp_Get_Mailtemplate_details = "SP_Get_MailTemplateDetails";
        public readonly string Sp_Set_Mailtemplate = "SP_Set_MailTemplate";
        #endregion

        #region Mail Template Stored Procedure parameters
        public readonly string P_DependId = "In_Depend_Gid";
        public readonly string P_TemplateId = "In_template_id";
        public readonly string P_TypeId = "In_type_id";
        public readonly string P_SubTypeIds = "In_subtype_ids";
        public readonly string P_StatusIds = "In_status_ids";
        public readonly string P_ToroleIds = "In_torole_ids";
        public readonly string P_CCroleIds = "In_ccrole_ids";
        public readonly string P_SupplierFlag = "In_supplier_flag";
        public readonly string P_Subject = "In_subject";
        public readonly string P_Template = "In_template";
        public readonly string P_Attachment = "In_attachment";
        public readonly string P_Fields = "In_mailfields";
        public readonly string P_Content = "In_mailcontent";
        public readonly string P_Signature = "In_signature";
        public readonly string P_additionalcc = "In_additionalcc";
        #endregion

        #region Period Close Stored Procedure
        public readonly string Sp_dml_periodclose = "Sp_Dml_PeriodClose";
        public readonly string Sp_get_periodclose = "Sp_Get_PeriodClose";

        #endregion

        #region Period Close Parameters
        public readonly string periodclosegid = "In_PeriodClose_Gid";
        public readonly string periodclosefromdate = "In_PeriodClose_FromDate";
        public readonly string periodclosetodate = "In_PeriodClose_ToDate";
        public readonly string periodclosestatus = "In_PeriodClose_Status";
        #endregion

        #region Dashboard
        public readonly string dashboardUserID = "In_LoginUser_GID";
        public readonly string grpDateFrom = "In_DateFrom";
        public readonly string grpDateTo = "In_DateTo";
        public readonly string dashLoginRole = "In_LoginRole";

        public readonly string SPGetDashboardCountList = "SP_Dashboard_Count";
        public readonly string SPDashboardGraphList = "SP_Dashboard_Graph";
        #endregion


    }
}
