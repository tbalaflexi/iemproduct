﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;

namespace DBHelper
{
    public class MasterModel
    {
        public Int64 MasterGid { get; set; }
        public Int64 MasterTranGid { get; set; }
        public string MasterCode { get; set; }
        public string MasterDesc { get; set; }
        [Display(Name = "Parent Code")]
        [Required(ErrorMessage = "Please select a Parent Code")]
        public Int64 ParentCodeGid { get; set; }
        [UIHint("GetParentCodeDropdown")]
        public string ParentCode { get; set; }
        public Int64 DependCode_Gid { get; set; }
        [UIHint("GetDependCodeDropdown")]
        public string DependCode { get; set; }
        public string InsertedBy { get; set; }
        public string InsertedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string deletedby { get; set; }
        public string code { get; set; }
        public string result { get; set; }
        public string Description { get; set; }
        public string testt { get; set; }
    }
}
