﻿using DBHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMProduct.Data
{
   public  class PayBank_Data
    {

       PayBank_Model ModelObject = new PayBank_Model();

       List<PayBank_Model> ModelList = new List<PayBank_Model>();                         //  list model object
       DataTable dt = new DataTable();
       Common ObjCmn = new Common();  

       //Pay Bank Read
       public List<PayBank_Model> ReadRecord()
       {
           try
           {
               ObjCmn.parameters = new List<IDbDataParameter>();
               dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpGetPaybank, CommandType.StoredProcedure);
               foreach (DataRow dr in dt.Rows)
               {
                   ModelList.Add
                       (
                       new PayBank_Model
                       {
                           paybank_gid = Convert.ToInt32(dr["paybank_gid"].ToString()),
                           paybank_QCbank = dr["paybank_QCbank"].ToString(),                           
                         //  Paybankcode_Depend = dr["Paybankcode_Depend"].ToString(),
                         //  paybank_bank_code = dr["paybank_bank_code"].ToString(),

                           //Depend_Code = dr["Depend_Code"].ToString(),
                           Depend_Code = Convert.ToInt32(dr["Depend_Code"].ToString()),
                           paybank_bank_name = dr["paybank_bank_name"].ToString(),
                           paybank_acc_no = dr["paybank_acc_no"].ToString(),
                         //  paybankifscid = dr["paybankifscid"].ToString(),
                           paybank_ifsc_code=dr["paybank_ifsc_code"].ToString (),
                        //   paybankbranchid = dr["paybankbranchid"].ToString(),
                           paybank_branch_name = dr["paybank_branch_name"].ToString(),
                           paybankglid = Convert.ToInt32(dr["paybankglid"].ToString()),
                           paybank_gl_no = dr["paybank_gl_no"].ToString(),

                           paybank_period_from = dr["paybank_period_from"].ToString(),
                           paybank_period_to = dr["paybank_period_to"].ToString(),
                           paybank_memo_filename = dr["paybank_memo_filename"].ToString(),
                           paybank_online_filename = dr["paybank_online_filename"].ToString(),
                           paybank_bank_address = dr["paybank_bank_address"].ToString(),
                           paybank_CHQ_default = dr["paybank_CHQ_default"].ToString(),
                           paybank_EFT_default = dr["paybank_EFT_default"].ToString(),
                           paybank_ERA_default = dr["paybank_ERA_default"].ToString(),
                           paybank_DD_default = dr["paybank_DD_default"].ToString(),
                         
                           paybank_default = dr["paybank_default"].ToString(),
                           //Depend_Code = dr["paybank_bank_name"].ToString(),
                          // paybank_Depend_Code = dr["paybank_bank_code"].ToString(),
                          // paybank_default = dr["paybank_default"].ToString(),
                         
                        
                         
                       });
               }
               return ModelList;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }


       //Pay Bank Save

       public DataTable SaveRecord(PayBank_Model ModelObject)
       {
           try
           {
               ObjCmn.parameters = new List<IDbDataParameter>();
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Create, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankGId, 0, DbType.Int32));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankQcBank, 50, ModelObject.paybank_QCbank, DbType.String));

               //ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankBankCode, 50, ModelObject.Depend_Code, DbType.String));
               //ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankBankName, 50, ModelObject.paybank_bank_name, DbType.String));


               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankBankCode, 50, ModelObject.Depend_Code, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankBankName, 50, ModelObject.paybank_bank_name, DbType.String));
               
               
               
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankAccNo, 50, ModelObject.paybank_acc_no, DbType.String));
               // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankIFSC, 50, ModelObject.paybankifscid, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankIFSC, 50, ModelObject.paybank_ifsc_code, DbType.String));
               //ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankBranch, 50, ModelObject.paybankbranchid, DbType.String));
              ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankBranch, 50, ModelObject.paybank_branch_name, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBAnkGlNo, 50, ModelObject.paybankglid, DbType.String));

               
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankPeriodFrom, 50, ModelObject.paybank_period_from, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankPeriodTo, 50, ModelObject.paybank_period_to, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankMemoFileName, 50, ModelObject.paybank_memo_filename, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankOnlineFileName, 50, ModelObject.paybank_online_filename, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PAyBankBAnkAddress, 50, ModelObject.paybank_bank_address, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankDefault, 50, ModelObject.paybank_default, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankCHQDefault, 50, ModelObject.paybank_CHQ_default, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PAyBankEFTDefault, 50, ModelObject.paybank_EFT_default, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankERADefault, 50, ModelObject.paybank_ERA_default, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankDDDefault, 50, ModelObject.paybank_DD_default, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
               dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpDmlPayBank, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return dt;
       }

       // Pay Bank Update
        
       public DataTable UpdateRecord(PayBank_Model ModelObject)
       {
           try
           {
               ObjCmn.parameters = new List<IDbDataParameter>();
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Create, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankGId, ModelObject.paybank_gid, DbType.Int32));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankQcBank, 50, ModelObject.paybank_QCbank, DbType.String));

               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankBankCode, 50, ModelObject.Depend_Code, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankBankName, 50, ModelObject.paybank_bank_name, DbType.String));

              // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankBankCode, 50, ModelObject.paybank_bank_code, DbType.String));
              // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankBankName, 50, ModelObject.Depend_Code, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankAccNo, 50, ModelObject.paybank_acc_no, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankIFSC, 50, ModelObject.paybank_ifsc_code, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankBranch, 50, ModelObject.paybank_branch_name, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBAnkGlNo, 50, ModelObject.paybankglid, DbType.String));
               
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankPeriodFrom, 50, ModelObject.paybank_period_from,DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankPeriodTo, 50, ModelObject.paybank_period_to, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankMemoFileName, 50, ModelObject.paybank_memo_filename, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankOnlineFileName, 50, ModelObject.paybank_online_filename, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PAyBankBAnkAddress, 50, ModelObject.paybank_bank_address, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankDefault, 50, ModelObject.paybank_default, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankCHQDefault, 50, ModelObject.paybank_CHQ_default, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PAyBankEFTDefault, 50, ModelObject.paybank_EFT_default, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankERADefault, 50, ModelObject.paybank_ERA_default, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankDDDefault, 50, ModelObject.paybank_DD_default, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
               dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpDmlPayBank, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
       
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return dt;
       }


       //Pay Bank Delet

       public DataTable DeleteRecord(PayBank_Model ModelObject)
       {
           try
           {
               ObjCmn.parameters = new List<IDbDataParameter>();
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Delete, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankGId, ModelObject.paybank_gid, DbType.Int32));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankQcBank, 50, 0, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankBankCode, 50,0, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankBankName, 50, 0, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankAccNo, 50, 0, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankIFSC, 50, 0, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankBranch, 50, 0, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBAnkGlNo, 50, 0, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankPeriodFrom, 50,  "2019-01-01", DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankPeriodTo, 50, "2019-01-01", DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankMemoFileName, 50, 0, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankOnlineFileName, 50, 0, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PAyBankBAnkAddress, 50, 0, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankDefault, 50,0, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankCHQDefault, 50, 0, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PAyBankEFTDefault, 50, 0, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankERADefault, 50, 0, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBankDDDefault, 50, 0, DbType.String));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
               dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpDmlPayBank, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());

           }
           catch (Exception ex)
           {
               throw ex;
           }
           return dt;
       }

       //common qc data


       public DataTable Getdropdown(string parentcode)
       {

           ObjCmn.parameters = new List<IDbDataParameter>();
           ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ParentCode, 50, parentcode, DbType.String));
           dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpGetqcDropdown, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
           return dt;
       }



      // get values
       //get tran sub Type on change

       public DataTable GetbankValues(string paybank_Depend_Code, string parentcode)
       {
           ObjCmn.parameters = new List<IDbDataParameter>();
           ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranId, 50, paybank_Depend_Code, DbType.String));
           ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ParentCode, 50, parentcode, DbType.String));
           dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpGetqcValues, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
           return dt;
       }


    }
}
