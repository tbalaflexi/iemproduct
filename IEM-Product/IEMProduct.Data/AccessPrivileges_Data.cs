﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DBHelper;

namespace IEMProduct.Data
{
    public class AccessPrivileges_Data
    {
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        Common cmnObj = new Common();

        public DataTable AccessPrivileges_GridDatas(Int64 LoginUser_ID, string Action, Int64 Role_GID)
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Action, Action, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.LoginUserId, LoginUser_ID, DbType.Int64));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Role_GID, Role_GID, DbType.Int64));
                dt = cmnObj.dbManager.GetDataTable(cmnObj.SP_Get_AccessPrivilegesDetails, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable Update_MenueRights(Int64 Role_Gid, string menuchecked, string menunotchecked, Int64 LoggedUser_ID)
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Role_GID, Role_Gid, DbType.Int64));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Menu_Checked, menuchecked, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Menu_NotChecked, menunotchecked, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.LoginUserId, LoggedUser_ID, DbType.Int64));
                dt = cmnObj.dbManager.GetDataTable(cmnObj.SP_Set_AccessPrivileges, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
    }
}
