﻿using DataAccessHandler.Models.IEMProduct;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMProduct.Data
{
  public  class CheckList_Data
    {
        Checklist_Model ModelObject = new Checklist_Model();                                   //Checklist model object
        List<Checklist_Model> ModelList = new List<Checklist_Model>();                         //Checklist  list model object
        DataTable dt = new DataTable();
        Common ObjCmn = new Common();                                                              //list model object



        //Checklist Master Read

        public List<Checklist_Model> ReadRecord()
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpGetchecklist, CommandType.StoredProcedure);
                foreach (DataRow dr in dt.Rows)
                {
                    ModelList.Add
                        (
                        new Checklist_Model
                        {
                            checklist_gid = Convert.ToInt32(dr["checklist_gid"].ToString()),
                            checklist_item = dr["checklist_item"].ToString(),
                            checklist_reason = dr["checklist_reason"].ToString(),
                            workorder = Convert.ToInt64(dr["checklist_displayorder"].ToString()),

                            status = dr["checklist_isactive"].ToString(),
                           
                            TranId = Convert.ToInt64(dr["checklist_type_gid"].ToString()),
                            Tran_Type = dr["typename"].ToString(),
                            TransubId = Convert.ToInt64(dr["checklist_subtype_gid"].ToString()),
                            Tran_SubType = dr["subtypename"].ToString(),
                          

                        });
                }
                return ModelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Checklist Save

        public DataTable SaveRecord(Checklist_Model ModelObject)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Create, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.checklistgid, 0, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.checklisttypeid, 50, ModelObject.TranId, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.checklistsubtypeid, 50, ModelObject.TransubId, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.checkliststatus, 50, ModelObject.status, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.checklistdisplayorder, 50, ModelObject.workorder, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.checklistitem, 50, ModelObject.checklist_item, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.checklistreason, 50, ModelObject.checklist_reason, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.Spdmlchecklist, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        // Checklist update

        public DataTable UpdateRecord(Checklist_Model ModelObject)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Create, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.checklistgid, 0, ModelObject.checklist_gid, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.checklisttypeid, 50, ModelObject.TranId, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.checklistsubtypeid, 50, ModelObject.TransubId, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.checkliststatus, 50, ModelObject.status, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.checklistdisplayorder, 50, ModelObject.workorder, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.checklistitem, 50, ModelObject.checklist_item, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.checklistreason, 50, ModelObject.checklist_reason, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.Spdmlchecklist, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        // Checklist delete

        public DataTable DeleteRecord(Checklist_Model ModelObject)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Delete, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.checklistgid, 0, ModelObject.checklist_gid, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.checklisttypeid, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.checklistsubtypeid, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.checkliststatus, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.checklistdisplayorder, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.checklistitem, 50, "", DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.checklistreason, 50, "", DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.Spdmlchecklist, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        //Common Dropdown   Type And   Sub Type of QC Master

        public DataTable Getdropdown(string parentcode)
        {

            ObjCmn.parameters = new List<IDbDataParameter>();
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ParentCode, 50, parentcode, DbType.String));
            dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpGetqcDropdown, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            return dt;
        }




        //get   sub Type on change

        public DataTable GetValues(string TranId, string parentcode)
        {
            ObjCmn.parameters = new List<IDbDataParameter>();
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranId, 50, TranId, DbType.String));
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ParentCode, 50, parentcode, DbType.String));
            dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpGetqcValues, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            return dt;
        }


        //Common Dropdown non qc master

        public DataTable dropdownvalues(string flag)
        {

            ObjCmn.parameters = new List<IDbDataParameter>();
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.flag, 50, flag, DbType.String));
            dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpNonqcDropdownValues, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            return dt;
        }
    }
}
