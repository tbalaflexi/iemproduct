﻿using DataAccessHandler.Models.IEMProduct;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMProduct.Data
{
    public class FundUpdate_Data
    {
        FundUpdate_Model ModelObject = new FundUpdate_Model();                                   //Checklist model object
        List<FundUpdate_Model> ModelList = new List<FundUpdate_Model>();                         //Checklist  list model object
        DataTable dt = new DataTable();
        Common ObjCmn = new Common();                                                              //list model object



        //Checklist Master Read

        public List<FundUpdate_Model> ReadRecord()
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.Spgetfundupdate, CommandType.StoredProcedure);
                foreach (DataRow dr in dt.Rows)
                {
                    ModelList.Add
                        (
                        new FundUpdate_Model
                        {
                              fundupdate_gid = Convert.ToInt32(dr["fundupdate_gid"].ToString()),
                          
                            fundupdate_paybankname_id = Convert.ToInt64(dr["fundupdate_paybankname_id"].ToString()),
                              bankname = dr["bankname"].ToString(),
                            //  fundupdate_paybankname_id = (dr["fundupdate_paybankname_id"].ToString()),

                               fundupdate_date = dr["fundupdate_date"].ToString(),

                              fundupdate_openingbal = Convert.ToInt64(dr["fundupdate_openingbal"].ToString()),
                             fundupdate_inflow = Convert.ToInt64(dr["fundupdate_inflow"].ToString()),
                              fundupdate_fundavailable = Convert.ToInt64(dr["fundupdate_fundavailable"].ToString()),
                              
                             
                              accountNo = dr["paybank_acc_no"].ToString(),     


                        });
                }
                return ModelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Checklist Save

        public DataTable SaveRecord(FundUpdate_Model ModelObject)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Create, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.fundgid, 0, DbType.Int32));

                if (ModelObject.fundupdate_date != null)
                {
                    if (ModelObject.fundupdate_date.Length > 19)
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.funddate, DateTime.Parse(ModelObject.fundupdate_date.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.funddate, DateTime.ParseExact(ModelObject.fundupdate_date.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.funddate, ModelObject.fundupdate_date, DbType.String));
                }

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.fundbankid, 50, ModelObject.fundupdate_paybankname_id, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.fundopeningbalance, 50, ModelObject.fundupdate_openingbal, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.fundinflow, 50, ModelObject.fundupdate_inflow, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.fundavailable, 50, ModelObject.fundupdate_fundavailable, DbType.String));




               // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.funddate, 50, ModelObject.fundupdate_date, DbType.String));
                //ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.checklistreason, 50, ModelObject.checklist_reason, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.Spdmlfundupdate, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        // Checklist update

        public DataTable UpdateRecord(FundUpdate_Model ModelObject)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Update, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.fundgid, 0, ModelObject.fundupdate_gid, DbType.Int32));
                if (ModelObject.fundupdate_date != null)
                {
                    if (ModelObject.fundupdate_date.Length > 19)
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.funddate, DateTime.Parse(ModelObject.fundupdate_date.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.funddate, DateTime.ParseExact(ModelObject.fundupdate_date.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.funddate, ModelObject.fundupdate_date, DbType.String));
                }
                           ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.fundbankid, 50, ModelObject.fundupdate_paybankname_id, DbType.String));
                  ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.fundopeningbalance, 50, ModelObject.fundupdate_openingbal, DbType.String));
                  ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.fundinflow, 50, ModelObject.fundupdate_inflow, DbType.String));
                  ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.fundavailable, 50, ModelObject.fundupdate_fundavailable, DbType.String));

                 



                  //ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.funddate, 50, ModelObject.fundupdate_date, DbType.String));
                 // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.checklistreason, 50, ModelObject.checklist_reason, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.Spdmlfundupdate, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        // Checklist delete

        public DataTable DeleteRecord(FundUpdate_Model ModelObject)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Delete, DbType.String));
              ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.fundgid, 0, ModelObject.fundupdate_gid, DbType.Int32));
              ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.fundbankid, 50, 0, DbType.String));
              ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.fundopeningbalance, 50, 0, DbType.String));
              ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.fundinflow, 50, 0, DbType.String));
              ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.fundavailable, 50, 0, DbType.String));

              

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.funddate, 50,"2019-01-01", DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.Spdmlfundupdate, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        //Common Dropdown   Type And   Sub Type of QC Master

        public DataTable dropdownvalues(string flag)
        {

            ObjCmn.parameters = new List<IDbDataParameter>();
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.flag, 50, flag, DbType.String));
            dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpNonqcDropdownValues, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            return dt;
        }

        //onchange values
        public DataTable GetValues(string TranId, string parentcode)
        {
            ObjCmn.parameters = new List<IDbDataParameter>();
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranId, 50, TranId, DbType.String));
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ParentCode, 50, parentcode, DbType.String));
            dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpGetqcValues, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            return dt;
        }


    }
}
