﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccessHandler.Models.IEMProduct;

namespace IEMProduct.Data
{
    public class MailTemplate_Data
    {
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        Common cmnObj = new Common();

        public DataTable Get_MailTemplate_Details(string Master, Int64 Depend_Id, Int64 LoginUser_ID)
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Action, Master, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.P_DependId, Depend_Id, DbType.Int64));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.LoginUserId, LoginUser_ID, DbType.Int64));
                dt = cmnObj.dbManager.GetDataTable(cmnObj.Sp_Get_Mailtemplate_details, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable Save_MailTemplate(MailTemplate_Model objMail)
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.P_TemplateId, objMail.mailtemplate_id, DbType.Int64));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.P_TypeId, objMail.mailtemplate_type_id, DbType.Int64));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.P_SubTypeIds, objMail.subtype_ids, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.P_StatusIds, objMail.status_ids, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.P_ToroleIds, objMail.torole_ids, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.P_CCroleIds, objMail.ccrole_ids, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.P_SupplierFlag, objMail.supplier_flag, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.P_Subject, objMail.mailtemplate_subject, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.P_Template, objMail.mailtemplate_template, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.P_Attachment, objMail.mailtemplate_attachment, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.P_Fields, objMail.mailfields_ids, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.P_Content, objMail.mailtemplate_content, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.P_Signature, objMail.mailtemplate_signature, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.P_additionalcc, objMail.mailtemplate_additionalcc, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.LoginUserId, objMail.logged_user_id, DbType.Int64));
                dt = cmnObj.dbManager.GetDataTable(cmnObj.Sp_Set_Mailtemplate, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
    }
}
