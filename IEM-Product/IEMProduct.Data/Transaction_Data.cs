﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBHelper;
//using DataAccessHandler.Models.IEMProduct;
using System.Data;

namespace IEMProduct.Data
{
    public class Transaction_Data
    {

        Transaction_Model ModelObject = new Transaction_Model();                                   //Transaction model object
        List<Transaction_Model> ModelList = new List<Transaction_Model>();                         //Transaction  list model object
        DataTable dt = new DataTable();
        Common ObjCmn = new Common();                                                              //list model object

        //Transaction Master Read

        public List<Transaction_Model> ReadRecord()
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpGetTransaction, CommandType.StoredProcedure);
                foreach (DataRow dr in dt.Rows)
                {
                    ModelList.Add
                        (
                        new Transaction_Model
                        {
                            TranGId = Convert.ToInt32(dr["Tran_Gid"].ToString()),
                          // TranId = dr["Tran_Type_Gid"].ToString(),
                           TranId = Convert.ToInt64(dr["Tran_Type_Gid"].ToString()),
                            Tran_Type = dr["Tran_Type"].ToString(),
                           // TransubId = dr["Tran_SubType_Gid"].ToString(),
                            TransubId = Convert.ToInt64(dr["Tran_SubType_Gid"].ToString()),
                            Tran_SubType = dr["Tran_SubType"].ToString(),
                            Tran_Attachement = dr["Tran_Attachement"].ToString(),
                            Tran_PhysicalReceipting = dr["Tran_PhysicalReceipting"].ToString(),

                        });
                }
                return ModelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Transaction Save

        public DataTable SaveRecord(Transaction_Model ModelObject)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Create, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranGId, 0, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranType, 50, ModelObject.TranId, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranSubType, 50, ModelObject.TransubId, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranAttachement, 50, ModelObject.Tran_Attachement, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranPhysicalRecepting, 50, ModelObject.Tran_PhysicalReceipting, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpDmlTransaction, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        // Transaction update

        public DataTable UpdateRecord(Transaction_Model ModelObject)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Create, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranGId, 0, ModelObject.TranGId, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranType, 50, ModelObject.TranId, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranSubType, 50, ModelObject.TransubId, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranAttachement, 50, ModelObject.Tran_Attachement, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranPhysicalRecepting, 50, ModelObject.Tran_PhysicalReceipting, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpDmlTransaction, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        // Transaction delete

        public DataTable DeleteRecord(Transaction_Model ModelObject)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Delete, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranGId, 0, ModelObject.TranGId, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranType, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranSubType, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranAttachement, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranPhysicalRecepting, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpDmlTransaction, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        //Common Dropdown Tran Type And Tran Sub Type of QC Master

        public DataTable Getdropdown(string parentcode)
        {

            ObjCmn.parameters = new List<IDbDataParameter>();
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ParentCode, 50, parentcode, DbType.String));
            dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpGetqcDropdown, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            return dt;
        }




        //get tran sub Type on change

        public DataTable GetValues(string TranId, string parentcode)
        {
            ObjCmn.parameters = new List<IDbDataParameter>();
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranId, 50, TranId, DbType.String));
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ParentCode, 50, parentcode, DbType.String));
            dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpGetqcValues, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            return dt;
        }


        //Common Dropdown non qc master

        public DataTable dropdownvalues(string flag)
        {

            ObjCmn.parameters = new List<IDbDataParameter>();
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.flag, 50, flag, DbType.String));
            dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpNonqcDropdownValues, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            return dt;
        }

    }
}
