﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DataAccessHandler;
using DBHelper;
using MySql.Data.MySqlClient;
using System.Configuration;



namespace IEMProduct.Data
{
    public class QCMasterData
    {
        List<MasterModel> QcMasterList = new List<MasterModel>();     // model list object
        MasterModel QcModelObject = new MasterModel();                // model object
        DataTable dt = new DataTable();
        Common ObjCmn = new Common();

        //QC Master Read
        public List<MasterModel> ReadRecord()
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SPGetqc, CommandType.StoredProcedure);
                foreach (DataRow dr in dt.Rows)
                {
                    QcMasterList.Add
                        (
                        new MasterModel
                        {
                            MasterCode = dr["Master_Code"].ToString(),
                            MasterDesc = (dr["Master_Desc"].ToString()),
                        });
                }
                return QcMasterList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //QC Master ReadDetails
        public List<MasterModel> ReadRecordDetails(string getMastercode)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.MasterCode, 50, getMastercode, DbType.String));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpGetqcDetails, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());

                foreach (DataRow dr in dt.Rows)
                {
                    QcMasterList.Add
                        (
                        new MasterModel
                        {
                            MasterGid = Convert.ToInt64(dr["masters_gid"].ToString()),
                            MasterCode = dr["masters_code"].ToString(),
                            ParentCodeGid = Convert.ToInt64(dr["masters_parentcode"]),
                            ParentCode = (dr["paerentname"].ToString()),
                            DependCode_Gid = Convert.ToInt64(dr["masters_dependcode"].ToString()),
                            DependCode = (dr["dependname"].ToString()),
                            MasterDesc = (dr["masters_description"].ToString()),
                        });
                }

                return QcMasterList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Qc Master Read
        public DataTable SaveRecord(MasterModel QcModelObject)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Create, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.MasterId, 0, DbType.Int32));
                // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.MasterTranGid, 0, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.MasterCode, 50, QcModelObject.MasterCode, DbType.String));
                //  ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ParentCode, 50, QcModelObject.ParentCode, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ParentCode, 50, QcModelObject.ParentCodeGid, DbType.String));
                //ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.DependCode, 50, QcModelObject.DependCode, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.DependCode, 50, QcModelObject.DependCode_Gid, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.MasterDesc, 50, QcModelObject.MasterDesc, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranGId, 50, QcModelObject.code, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpDmlqcMaster, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        // Qc Master Update
        public DataTable UpdateRecord(MasterModel QcModelObject)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Create, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.MasterId, 50, QcModelObject.MasterGid, DbType.Int32));
                // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.MasterTranGid, 50, QcModelObject.MasterGid, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.MasterCode, 50, QcModelObject.MasterCode, DbType.String));
                //ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ParentCode, 50, QcModelObject.ParentCode, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ParentCode, 50, QcModelObject.ParentCodeGid, DbType.String));
                //ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.DependCode, 50, QcModelObject.DependCode, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.DependCode, 50, QcModelObject.DependCode_Gid, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranGId, 50, QcModelObject.code, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.MasterDesc, 50, QcModelObject.MasterDesc, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpDmlqcMaster, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        // Qc Master delete
        public DataTable DeleteRecord(MasterModel QcModelObject)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Delete, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.MasterId, QcModelObject.MasterGid, DbType.Int32));
                // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.MasterTranGid, QcModelObject.MasterGid, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.MasterCode, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ParentCode, 50, 0, DbType.String));
                // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ParentCode, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.DependCode, 50, 0, DbType.String));
                //  ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.DependCode, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.MasterDesc, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranGId, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpDmlqcMaster, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        //Parent Code Dropdown
        public DataTable GetParentCode()
        {
            ObjCmn.parameters = new List<IDbDataParameter>();
            dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpParentDropdown, CommandType.StoredProcedure);
            return dt;
        }

        // Depend Code Dropdown
        public DataTable GetDependCode()
        {
            ObjCmn.parameters = new List<IDbDataParameter>();
            dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpDepentDropdown, CommandType.StoredProcedure);
            return dt;
        }

    }
}


