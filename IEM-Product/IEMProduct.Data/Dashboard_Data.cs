﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IEMProduct.Data;
using System.Data;

namespace IEMProduct.Data
{
   public class Dashboard_Data
    {
      
       DataSet ds = new DataSet();
       Common ObjCmn = new Common();

       public DataSet ReadDashboardList(Int64 userId)
       {
           try
           {
               ObjCmn.parameters = new List<IDbDataParameter>();
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.dashboardUserID, 50, userId, DbType.Int32));
               ds = ObjCmn.dbManager.GetDataSet(ObjCmn.SPGetDashboardCountList, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());

               return ds;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

       public DataSet ReadChartList(Int64 User_GID,string dashDateFrom,string dashDateTo)
       {
           try
           {
               ObjCmn.parameters = new List<IDbDataParameter>();
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.dashboardUserID, 50, User_GID, DbType.Int32));
               if (dashDateFrom != null)
               {
                   if (dashDateFrom.Length > 19)
                   {
                       ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.grpDateFrom, DateTime.Parse(dashDateFrom.Substring(4, 12)), DbType.DateTime));
                   }
                   else
                   {
                       ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.grpDateFrom, DateTime.ParseExact(dashDateFrom.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                   }
               }
               else
               {
                   ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.grpDateFrom, dashDateFrom, DbType.String));
               }

               if (dashDateTo != null)
               {
                   if (dashDateTo.Length > 19)
                   {
                       ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.grpDateTo, DateTime.Parse(dashDateTo.Substring(4, 12)), DbType.DateTime));
                   }
                   else
                   {
                       ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.grpDateTo, DateTime.ParseExact(dashDateTo.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                   }
               }
               else
               {
                   ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.grpDateTo, dashDateTo, DbType.String));
               }  
                
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.dashLoginRole, 50, 0, DbType.String));

               ds = ObjCmn.dbManager.GetDataSet(ObjCmn.SPDashboardGraphList, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());

           }
           catch (Exception ex)
           {
               throw ex;
           }
           return ds;
       }


    }
}
