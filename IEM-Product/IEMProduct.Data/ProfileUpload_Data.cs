﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IEMProduct.Data;
using System.Data;
using DataAccessHandler.Models.IEMProduct;
using DBHelper;

namespace IEMProduct.Data
{
    public class ProfileUpload_Data
    {
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        Common cmnParams = new Common();
      

        #region Methods for Get ProfileUploadData List.
        public DataSet Get_ProfileUploadDetailList(ProfileUpload_Model Obj_Model)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                ds = cmnParams.dbManager.GetDataSet(cmnParams.SP_Get_EmpUploadDetails, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Methods for Get ProfileUploadFile List.
        public DataSet Get_ProfileUploadFileCount(ProfileUpload_Model Obj_Model)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_FileName, Obj_Model.POUpload_FileName, DbType.String));
                ds = cmnParams.dbManager.GetDataSet(cmnParams.SP_Get_FileCount, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Methods for Set FileGid
        public DataSet FileInst_Data(ProfileUpload_Model Obj_Model)
        {

            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_FileName, Obj_Model.POUpload_NewFileName, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_FileType, Obj_Model.POUpload_FileType, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.LoginUserId, Obj_Model.LoginId, DbType.String));
                ds = cmnParams.dbManager.GetDataSet(cmnParams.SP_DML_File, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Methods for Get PO Temp Table Column List
        public DataTable DeleteTempTable(ProfileUpload_Model Obj_Model)
        {
            
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();                
                dt = cmnParams.dbManager.GetDataTable(cmnParams.SP_Delete_Temp_EmpUpload, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region Methods for Set Temp Table PO Upload
        public DataTable SetProfileUpload_TempTable(ProfileUpload_Model Obj_Model)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_File_Gid, Obj_Model.File_Gid, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EmpCode, Obj_Model.Emp_Code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EmpName, Obj_Model.Emp_Name, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EmpGender, Obj_Model.Emp_Gender, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EmpDob, Obj_Model.Emp_Dob, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EmpDoj, Obj_Model.Emp_Doj, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_DesignationCode, Obj_Model.Designation_code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Gradecode, Obj_Model.Grade_code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_DeptCode, Obj_Model.Dept_Code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Emp_Branch_Code, Obj_Model.Emp_Branch_Code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_FC_Code, Obj_Model.FC_Code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_CC_Code, Obj_Model.CC_Code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Product_Code, Obj_Model.Product_Code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Addr1, Obj_Model.Addr1, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Addr2, Obj_Model.Addr2, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_City_Code, Obj_Model.City_Code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Pincode, Obj_Model.Pincode, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Personal_Email, Obj_Model.Personal_email, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Office_Email, Obj_Model.Office_Email, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Mobile_No, Obj_Model.Mobil_No, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Bank_Code, Obj_Model.Bank_Code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Bank_Branch_Code, Obj_Model.Bank_Branch_Code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_AccNo, Obj_Model.Acc_No, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_AccType, Obj_Model.Acc_Type, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_CardNo, Obj_Model.Emp_Card_No, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Emp_Permanent_Addr, Obj_Model.Emp_Parmanent_Addr, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Supervisor_Code, Obj_Model.Supervisor_Code, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_Emp_Dor, Obj_Model.Employee_dor, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_LastWorkingDate, Obj_Model.Last_Working_date, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EffictiveDate, Obj_Model.Effictive_date, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.LoginUserId, Obj_Model.LoginId, DbType.String));
                dt = cmnParams.dbManager.GetDataTable(cmnParams.SP_DML_Tmp_EmpUpload, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Medhod for Confirm Temp data to Main Table
        public DataTable SetFileConfirm_Data(ProfileUpload_Model Obj_Model)
        {
            try
            {
                cmnParams.parameters = new List<IDbDataParameter>();
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.P_EmpPassword, Obj_Model.Emp_Password, DbType.String));
                cmnParams.parameters.Add(cmnParams.dbManager.CreateParameter(cmnParams.LoginUserId, Obj_Model.LoginId, DbType.String));
                dt = cmnParams.dbManager.GetDataTable(cmnParams.SP_DML_EmpUploadDetails, CommandType.StoredProcedure, cmnParams.parameters.ToArray());
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion
    }
}
