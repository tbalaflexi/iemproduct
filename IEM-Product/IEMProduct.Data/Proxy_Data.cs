﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IEMProduct.Data;
using System.Data;
using DataAccessHandler.Models.IEMProduct;

namespace IEMProduct.Data
{
    public class Proxy_Data
    {

        Proxy_Model ModelObject = new Proxy_Model();
        List<Proxy_Model> ModelList = new List<Proxy_Model>();
        DataTable dt = new DataTable();
        Common ObjCmn = new Common();
        DataSet ds = new DataSet();


        public DataSet ReadRecord(Int64 userId)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.userId, 50, userId, DbType.Int32));
                ds = ObjCmn.dbManager.GetDataSet(ObjCmn.SpGetProxyDetails, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
               
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public DataTable Save_Update_ProxyDetails(Proxy_Model modelObj)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, modelObj.Action, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.proxyID, modelObj.proxyGid, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.proxyFromId, modelObj.proxyFromId, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.proxyToId, modelObj.proxyToId, DbType.Int64));
                if (modelObj.proxyPeriodFrom != null)
                {
                    if (modelObj.proxyPeriodFrom.Length > 19)
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.proxyPeriodFrom, DateTime.Parse(modelObj.proxyPeriodFrom.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.proxyPeriodFrom, DateTime.ParseExact(modelObj.proxyPeriodFrom.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.proxyPeriodFrom, modelObj.proxyPeriodFrom, DbType.String));
                }

                if (modelObj.proxyPeriodTo != null)
                {
                    if (modelObj.proxyPeriodTo.Length > 19)
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.proxyPeriodTo, DateTime.Parse(modelObj.proxyPeriodTo.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.proxyPeriodTo, DateTime.ParseExact(modelObj.proxyPeriodTo.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.proxyPeriodTo, modelObj.proxyPeriodTo, DbType.String));
                }

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.proxyActive, modelObj.proxyActiveId, DbType.String));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SaveUpdateProxyDetails, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }


          public DataTable Remove_ProxyDetails(Proxy_Model modelObj)
            {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, modelObj.Action, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.proxyID, modelObj.proxyGid, DbType.Int64));

                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.RemoveProxyDetails, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
            }
        //
          public DataTable GetEmployee_ByCodeorName(string EmployeeName, string flag, string Master)
          {

              ObjCmn.parameters = new List<IDbDataParameter>();
              ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.p_Master, Master, DbType.String));
              ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.p_MasterGid, 0, DbType.String));
              ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.p_Employee, EmployeeName, DbType.String));
              dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SP_EOW_Claim_GetDropDownList, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
              return dt;
          }

    }
}
