﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DBHelper;
namespace IEMProduct.Data
{

    public class WorkFlow_Data
    {
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        Common cmnObj = new Common();
        public DataTable Get_DropDownLists(string Master, string Depend_Code, string TypeCode, string SubTypeCode, string AttributeParent, string AttributeChild)
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Master, Master, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Parent_Code, Depend_Code, DbType.String));

                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_TypeCode, TypeCode, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_SubTypeCode, SubTypeCode, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_Parent, AttributeParent, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_Child, AttributeChild, DbType.String));

                dt = cmnObj.dbManager.GetDataTable(cmnObj.SpGetWorkFlowDropDown, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataSet Get_Attributes(Int64 Type_Gid)
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_TypeGid, Type_Gid, DbType.String));
                ds = cmnObj.dbManager.GetDataSet(cmnObj.SpGetAttribute, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        #region Get Attributes for workflow
        public DataSet Get_Attributes_ForWorkFlow(string Type_Code, Int64 Logged_UserGid)
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_TypeCode, Type_Code, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.LoginUserId, Logged_UserGid, DbType.Int64));
                ds = cmnObj.dbManager.GetDataSet(cmnObj.SpGetAttribute_ForWorkFlow, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        #endregion 

        public DataTable UpdateHeaderDetails(WorkFlow_Model modelObj)
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Action, modelObj.workflow_action, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlowHeader_Gid, modelObj.workflow_id, DbType.Int64));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_TypeCode, modelObj.workflow_typecode, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_SubTypeCode, modelObj.workflow_subtypecode, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_Child, modelObj.workflow_attrib_childid, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.LoginUserId, modelObj.Logged_UserGID, DbType.Int64));
                dt = cmnObj.dbManager.GetDataTable(cmnObj.Spsetworkflowheader, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable Save_WorkFlow(WorkFlow_Model modelObj)
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Action, cmnObj.Create, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlowHeader_Gid, modelObj.workflow_id, DbType.Int64));
                //cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_TypeCode, modelObj.HDN_Type, DbType.String));
                //cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_SubTypeCode, modelObj.HDN_SubType, DbType.String));
                //cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_Parent, modelObj.WF_Parent, DbType.String));
                //cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_Child, modelObj.WF_Child, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_Gid, modelObj.WorkFlow_Gid, DbType.Int64));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_RoleFrom, modelObj.RoleFrom_Code, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_Action, modelObj.Action_Code, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_Status, modelObj.Status_Code, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_RoleTo, modelObj.RoleTo_Code, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_TimeOut, modelObj.TimeOut, DbType.Int64));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_TimeOut_RoleTo, modelObj.TimeOut_RoleTo_Code, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_Additonal, modelObj.Additional, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.LoginUserId, modelObj.Logged_UserGID, DbType.Int64));
                dt = cmnObj.dbManager.GetDataTable(cmnObj.SpDMLWorkFlowMaster, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable Update_WorkFlow(WorkFlow_Model modelObj)
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Action, cmnObj.Update, DbType.String)); 
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlowHeader_Gid, modelObj.workflow_id, DbType.Int64));
                //cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_TypeCode, modelObj.HDN_Type, DbType.String));
                //cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_SubTypeCode, modelObj.HDN_SubType, DbType.String));
                //cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_Parent, modelObj.WF_Parent, DbType.String));
                //cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_Child, modelObj.WF_Child, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_Gid, modelObj.WorkFlow_Gid, DbType.Int64));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_RoleFrom, modelObj.RoleFrom_Code, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_Action, modelObj.Action_Code, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_Status, modelObj.Status_Code, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_RoleTo, modelObj.RoleTo_Code, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_TimeOut, modelObj.TimeOut, DbType.Int64));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_TimeOut_RoleTo, modelObj.TimeOut_RoleTo_Code, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_Additonal, modelObj.Additional, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.LoginUserId, modelObj.Logged_UserGID, DbType.Int64));
                dt = cmnObj.dbManager.GetDataTable(cmnObj.SpDMLWorkFlowMaster, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable Remove_WorkFlow(Int64 Master_Gid, Int64 Logged_UserGID)
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Action, cmnObj.Delete, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlowHeader_Gid, "0", DbType.Int64));
                //cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_TypeCode, modelObj.HDN_Type, DbType.String));
                //cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_SubTypeCode, modelObj.HDN_SubType, DbType.String));
                //cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_Parent, modelObj.WF_Parent, DbType.String));
                //cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_Child, modelObj.WF_Child, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_Gid, Master_Gid, DbType.Int64));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_RoleFrom, "", DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_Action, "", DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_Status, "", DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_RoleTo, "", DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_TimeOut, "0", DbType.Int64));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_TimeOut_RoleTo, "", DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.WorkFlow_Additonal, "", DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.LoginUserId, Logged_UserGID, DbType.Int64));
                dt = cmnObj.dbManager.GetDataTable(cmnObj.SpDMLWorkFlowMaster, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
    }
}
