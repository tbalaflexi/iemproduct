﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccessHandler.Models.IEMProduct;
namespace IEMProduct.Data
{
    public class RoleEmployee_Data
    {
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        Common cmnObj = new Common();
        //RoleEmployee_Model ObjModel = new RoleEmployee_Model();
        List<RoleEmployee_Model> ModelList = new List<RoleEmployee_Model>();
        public DataTable ReadRole()
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                dt = cmnObj.dbManager.GetDataTable(cmnObj.SP_Get_RoleVsEmployee, CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable ReadEmployeeByRole(RoleEmployee_Model ObjModel)
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.p_Role, ObjModel.Role, DbType.String));
                dt = cmnObj.dbManager.GetDataTable(cmnObj.SP_Get_EmployeeByRole, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable Remove_EmployeeRole(Int64 RoleEmployee_Gid, Int64 LoginUserId)
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Action, cmnObj.Delete, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.p_RoleEmployee_Gid, RoleEmployee_Gid, DbType.Int64));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.p_Role_Gid, 0, DbType.Int64));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.p_Employee_Gid, 0, DbType.Int64));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.LoginUserId, LoginUserId, DbType.Int64));
                dt = cmnObj.dbManager.GetDataTable(cmnObj.SP_DML_EmployeeByRole, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable Save_EmployeeRole(RoleEmployee_Model ObjModel, Int64 LoginUserId)
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Action, cmnObj.Create, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.p_roleemployee_gid, ObjModel.roleemployee_gid, DbType.Int64));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.p_Role_Gid, ObjModel.Role_Gid, DbType.Int64));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.p_Employee_Gid, ObjModel.Employee_Gid, DbType.Int64));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.LoginUserId, LoginUserId, DbType.Int64));
                dt = cmnObj.dbManager.GetDataTable(cmnObj.SP_DML_EmployeeByRole, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable Get_Employee_List(string EmployeeName)
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.p_Master, "Employee_Search", DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.p_MasterGid, 0, DbType.Int64));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.p_Employee, EmployeeName, DbType.String));
                dt = cmnObj.dbManager.GetDataTable(cmnObj.SP_EOW_Claim_GetDropDownList, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable Get_ALLEmployee_List(string Master)
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.p_Master, Master, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.p_MasterGid, 0, DbType.Int64));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.p_Employee, "", DbType.String));
                dt = cmnObj.dbManager.GetDataTable(cmnObj.SP_EOW_Claim_GetDropDownList, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable ReadRoleByEmployee(RoleEmployee_Model ObjModel)
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.p_Employee_Gid, ObjModel.Employee_Gid, DbType.String));
                dt = cmnObj.dbManager.GetDataTable(cmnObj.SP_Get_RoleIdByEmployee, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable Save_RoleToEmployee(RoleEmployee_Model ObjModel, Int64 LoginUserId)
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Action, cmnObj.Create, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.p_roleemployee_gid, ObjModel.roleemployee_gid, DbType.Int64));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.p_RoleGids, ObjModel.RoleIds, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.p_Employee_Gid, ObjModel.Employee_Gid, DbType.Int64));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.LoginUserId, LoginUserId, DbType.Int64));
                dt = cmnObj.dbManager.GetDataTable(cmnObj.SP_DML_RoleToEmployee, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
    }
}
