﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace IEMProduct.Data
{
    public class Login_Data
    {
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        Common cmnObj = new Common();

        public DataTable CheckUserCredential(string usercode, string userpassword, string LoginMode, string ProxyFrom)
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Login_UserCode, usercode, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Login_UserPassword, userpassword, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Login_Mode, LoginMode, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Login_ProxyFrom, ProxyFrom, DbType.String));
                dt = cmnObj.dbManager.GetDataTable(cmnObj.SpCheckUserCredential, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable Get_ProxyFrom(string UserName)
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Login_UserCode, UserName, DbType.String));
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Action, "GetProxyFrom", DbType.String));
                dt = cmnObj.dbManager.GetDataTable(cmnObj.sp_Get_Proxy, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
