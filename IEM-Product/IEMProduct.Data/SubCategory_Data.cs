﻿using DBHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMProduct.Data
{
    public class SubCategory_Data
    {
        SubCategory_Model ModelObject = new SubCategory_Model();
        List<SubCategory_Model> ModelList = new List<SubCategory_Model>();                         //  Sub Category list model object
        DataTable dt = new DataTable();
        Common ObjCmn = new Common();

        // Sub Category Read
        public List<SubCategory_Model> ReadRecord()
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpGetSubCategory, CommandType.StoredProcedure);
                foreach (DataRow dr in dt.Rows)
                {
                    ModelList.Add
                        (
                        new SubCategory_Model
                        {
                            expsubcat_gid = Convert.ToInt32(dr["expsubcat_gid"].ToString()),
                            expsubcat_expcat_masters_gid = Convert.ToInt64(dr["expsubcat_expcat_masters_gid"].ToString()),
                            expcategory = dr["expcategory"].ToString(),
                            NatureOfExpenseid = Convert.ToInt32(dr["NatureOfExpenseid"].ToString()),
                            NatureOfExpense = dr["natureofexp"].ToString(),
                            paybankglid = Convert.ToInt64(dr["expsubcat_gl_masters_gid"].ToString()),
                            paybank_gl_no = dr["glno"].ToString(),
                            expsubcat_name = dr["expsubcat_name"].ToString(),
                            expsubcat_code = dr["expsubcat_code"].ToString(),

                        });
                }
                return ModelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Sub Category Save

        public DataTable SaveRecord(SubCategory_Model ModelObject)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Create, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.SubCatId, 0, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ExpCatId, 50, ModelObject.expsubcat_expcat_masters_gid, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.SubCatCode, 50, ModelObject.expsubcat_code, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.SubCatName, 50, ModelObject.expsubcat_name, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBAnkGlNo, 50, ModelObject.paybankglid, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpDmlSubCategory, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        //Sub Category Update

        public DataTable UpdateRecord(SubCategory_Model ModelObject)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Create, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.SubCatId, ModelObject.expsubcat_gid, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ExpCatId, 50, ModelObject.expsubcat_expcat_masters_gid, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.SubCatCode, 50, ModelObject.expsubcat_code, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.SubCatName, 50, ModelObject.expsubcat_name, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBAnkGlNo, 50, ModelObject.paybankglid, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpDmlSubCategory, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        //Sub Category Delete
        public DataTable DeleteRecord(SubCategory_Model ModelObject)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Delete, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.SubCatId, ModelObject.expsubcat_gid, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ExpCatId, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.SubCatCode, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.SubCatName, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.PayBAnkGlNo, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, 1, DbType.Int32));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpDmlSubCategory, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        //dropdown binding
        public DataTable Getdropdown(string parentcode)
        {

            ObjCmn.parameters = new List<IDbDataParameter>();
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ParentCode, 50, parentcode, DbType.String));
            dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpGetqcDropdown, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            return dt;
        }




        //get values
        public DataTable GetValues(string TranId, string parentcode)
        {
            ObjCmn.parameters = new List<IDbDataParameter>();
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranId, 50, TranId, DbType.String));
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ParentCode, 50, parentcode, DbType.String));
            dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpGetqcValues, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            return dt;
        }


    }
}
