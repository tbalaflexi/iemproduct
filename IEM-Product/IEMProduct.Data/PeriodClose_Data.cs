﻿using DataAccessHandler.Models.IEMProduct;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMProduct.Data
{
    public class PeriodClose_Data
    {
        PeriodClose_Model ModelObject = new PeriodClose_Model();                                   //PeriodClose model object
        List<PeriodClose_Model> ModelList = new List<PeriodClose_Model>();                         //PeriodClose  list model object
        DataTable dt = new DataTable();
        Common ObjCmn = new Common();                                                              //list model object

        //PeriodClose Master Read

        public List<PeriodClose_Model> ReadRecord(Int64 User_Gid)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, User_Gid, DbType.Int64));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.Sp_get_periodclose, CommandType.StoredProcedure);
                foreach (DataRow dr in dt.Rows)
                {
                    ModelList.Add
                        (
                        new PeriodClose_Model
                        {
                            PeriodCloseGid = Convert.ToInt32(dr["PeriodClose_Gid"].ToString()),
                            PeriodCloseFromDate = dr["PeriodClose_FromDate"].ToString(),
                            PeriodCloseToDate = dr["PeriodClose_ToDate"].ToString(),
                            PeriodCloseStatus = dr["PeriodClose_Status"].ToString(), 
                            
                        });
                }
                return ModelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // PeriodClose Save

        public DataTable SaveRecord(PeriodClose_Model ModelObject, Int64 User_Gid)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Create, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.periodclosegid, 0, DbType.Int32));
               // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TranGId, 0, DbType.Int32));
              //  ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegate_To, 50, ModelObject.PeriodCloseFromDate, DbType.String));
              //  ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Delegate_delmatflag, 50, ModelObject.PeriodCloseToDate, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.periodclosestatus, 50, ModelObject.PeriodCloseStatus, DbType.String));

                if (ModelObject.PeriodCloseFromDate != null)
                {
                    if (ModelObject.PeriodCloseFromDate.Length > 19)
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.periodclosefromdate, DateTime.Parse(ModelObject.PeriodCloseFromDate.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.periodclosefromdate, DateTime.ParseExact(ModelObject.PeriodCloseFromDate.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.periodclosefromdate, ModelObject.PeriodCloseFromDate, DbType.String));
                }


                if (ModelObject.PeriodCloseToDate != null)
                {
                    if (ModelObject.PeriodCloseToDate.Length > 19)
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.periodclosetodate, DateTime.Parse(ModelObject.PeriodCloseToDate.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.periodclosetodate, DateTime.ParseExact(ModelObject.PeriodCloseToDate.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.periodclosetodate, ModelObject.PeriodCloseToDate, DbType.String));
                }

                  
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, User_Gid, DbType.String));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.Sp_dml_periodclose, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        //PeriodClose Update

        public DataTable UpdateRecord(PeriodClose_Model ModelObject, Int64 User_Gid)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Create, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.periodclosegid, ModelObject.PeriodCloseGid, DbType.Int32));

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.periodclosestatus, 50, ModelObject.PeriodCloseStatus, DbType.String));
                if (ModelObject.PeriodCloseFromDate != null)
                {
                    if (ModelObject.PeriodCloseFromDate.Length > 19)
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.periodclosefromdate, DateTime.Parse(ModelObject.PeriodCloseFromDate.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.periodclosefromdate, DateTime.ParseExact(ModelObject.PeriodCloseFromDate.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.periodclosefromdate, ModelObject.PeriodCloseFromDate, DbType.String));
                }


                if (ModelObject.PeriodCloseToDate != null)
                {
                    if (ModelObject.PeriodCloseToDate.Length > 19)
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.periodclosetodate, DateTime.Parse(ModelObject.PeriodCloseToDate.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.periodclosetodate, DateTime.ParseExact(ModelObject.PeriodCloseToDate.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.periodclosetodate, ModelObject.PeriodCloseToDate, DbType.String));
                }
                 
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, User_Gid, DbType.String));

                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.Sp_dml_periodclose, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        //non qc dropdown values
        public DataTable dropdownvalues(string flag)
        {

            ObjCmn.parameters = new List<IDbDataParameter>();
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.flag, 50, flag, DbType.String));
            dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpNonqcDropdownValues, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            return dt;
        }
    }
}
