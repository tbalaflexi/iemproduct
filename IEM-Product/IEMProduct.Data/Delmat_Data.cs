﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using DataAccessHandler.Models.IEMProduct;

namespace IEMProduct.Data
{
   public class Delmat_Data
    {
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        Common cmnObj = new Common();
        Delmat_Model ObjModel = new Delmat_Model();
        List<Delmat_Model> ModelList = new List<Delmat_Model>();
                                                                   //list model object



       //delmat read

        public DataTable ReadRecord( )
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                //cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Delmat_Gid, 50, getMastercode, DbType.String));
          
                dt = cmnObj.dbManager.GetDataTable(cmnObj.SpGetDelmat, CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable LoadSlabRange()
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                //cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Slabrange_Gid, data.Slabrange_Gid, DbType.String));
                //cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Slabrange_Name, data.Slabrange_Name, DbType.String));

                //cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Slabrange_From, data.Slabrange_From, DbType.String));
                //cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Slabrange_To, data.Slabrange_To, DbType.String)); 
                dt = cmnObj.dbManager.GetDataTable(cmnObj.SpGet_Delmat_SlabRange, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable LoadException(Delmat_Model data)
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Delmat_Gid, data.Delmat_Gid, DbType.Int64));
                dt = cmnObj.dbManager.GetDataTable(cmnObj.SpGet_Delmat_Exception, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetEmployee()
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                dt = cmnObj.dbManager.GetDataTable(cmnObj.SpGet_Delmat_Employee, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetDelmatName(Delmat_Model data)
        {
            try
            {
                cmnObj.parameters = new List<IDbDataParameter>();
                dt = cmnObj.dbManager.GetDataTable(cmnObj.SpGet_DelmatName, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
       public DataTable Save_Delmat_SlabRange(Delmat_Model modelObj)
       {
           try
           {
               cmnObj.parameters = new List<IDbDataParameter>();
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Action, cmnObj.Create, DbType.String));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Slabrange_Name, modelObj.Slabrange_Name, DbType.String));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Slabrange_From, modelObj.Slabrange_From, DbType.Decimal));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Slabrange_To, modelObj.Slabrange_To, DbType.Decimal));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Slabrange_Gid, modelObj.Slabrange_Gid, DbType.Int64));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.LoginUserId, 1, DbType.Int64));
               dt = cmnObj.dbManager.GetDataTable(cmnObj.SpDMLDelmat_SlabRange, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return dt;
       }
       public DataTable Save_Update_Delmat_Exception(Delmat_Model modelObj)
       {
           try
           {
               cmnObj.parameters = new List<IDbDataParameter>();
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Action, cmnObj.Create, DbType.String));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmatexception_gid, modelObj.delmatexception_gid, DbType.Int64));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmatexception_delmat_gid, modelObj.delmatexception_delmat_gid, DbType.Int64));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmatexception_to, modelObj.delmatexception_to, DbType.Int64));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmatexception_title, modelObj.delmatexception_title, DbType.String));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmatexception_Limit, modelObj.delmatexception_Limit, DbType.Decimal));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.LoginUserId, 1, DbType.Int64));
               dt = cmnObj.dbManager.GetDataTable(cmnObj.SpDMLDelmat_Exception, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return dt;
       }

       public DataTable Remove_Exception(Int64 Delmat_Exeption_Gid)
       {
           try
           {
               cmnObj.parameters = new List<IDbDataParameter>();
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Action, cmnObj.Delete, DbType.String));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmatexception_gid, Delmat_Exeption_Gid, DbType.Int64));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmatexception_delmat_gid,0, DbType.Int64));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmatexception_to, 0, DbType.Int64));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmatexception_title, "", DbType.String));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmatexception_Limit, 0, DbType.Decimal));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.LoginUserId, 1, DbType.Int64));
               dt = cmnObj.dbManager.GetDataTable(cmnObj.SpDMLDelmat_Exception, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return dt;
       }

       public DataTable Remove_SlabRange(Int64 SlabRange_Gid)
       {
           try
           {
               cmnObj.parameters = new List<IDbDataParameter>();
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Action, cmnObj.Delete, DbType.String));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Slabrange_Name, "", DbType.String));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Slabrange_From, 0, DbType.Decimal));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Slabrange_To, 0, DbType.Decimal));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Slabrange_Gid, SlabRange_Gid, DbType.Int64));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.LoginUserId, 1, DbType.Int64));
               dt = cmnObj.dbManager.GetDataTable(cmnObj.SpDMLDelmat_SlabRange, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return dt;
       }

       public DataTable Save_Update_DelmatFlow(Delmat_Model modelObj)
       {
           try
           {
               cmnObj.parameters = new List<IDbDataParameter>();
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Action, cmnObj.Create, DbType.String));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.DelmatFlow_Gid, modelObj.DelmatFlow_Gid, DbType.Int64));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmatflow_delmat_gid, modelObj.Delmat_Gid, DbType.Int64));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.DelmatFlow_Order, modelObj.DelmatFlow_Order, DbType.Int32));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.DelmatFlow_Title, modelObj.Title_Code, DbType.String));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.DelmatFlow_Title_Ref_Gid, modelObj.Title_Ref_Gid, DbType.Int64));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.DelmatFlow_SlabRange_Gid, modelObj.Slabrange_Gid, DbType.Int64));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.LoginUserId, 1, DbType.Int64));
               dt = cmnObj.dbManager.GetDataTable(cmnObj.SpDMLDelmat_DelmatFlow, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return dt;
       }
       public DataTable Remove_DelmatFlow(Int64 DelmatFlow_Gid)
       {
           try
           {
               cmnObj.parameters = new List<IDbDataParameter>();
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Action, cmnObj.Delete, DbType.String));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.DelmatFlow_Gid, DelmatFlow_Gid, DbType.Int64));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmatflow_delmat_gid,0, DbType.Int64));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.DelmatFlow_Order, 0, DbType.Int32));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.DelmatFlow_Title, "", DbType.String));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.DelmatFlow_Title_Ref_Gid, 0, DbType.Int64));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.DelmatFlow_SlabRange_Gid, 0, DbType.Int32));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.LoginUserId, 1, DbType.Int64));
               dt = cmnObj.dbManager.GetDataTable(cmnObj.SpDMLDelmat_DelmatFlow, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return dt;
       }

       // delete delmat
       public DataTable Remove_Delmat(Int64 Delmat_Gid)
       {
           try
           {
               cmnObj.parameters = new List<IDbDataParameter>();
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Action, cmnObj.Delete, DbType.String));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Delmat_Gid, Delmat_Gid, DbType.Int64));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmat_name, 0, DbType.String));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmat_Version, 0, DbType.Int64));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmat_active, 0, DbType.String));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.LoginUserId, 1, DbType.Int64));
               dt = cmnObj.dbManager.GetDataTable(cmnObj.SpDMLDelmatsummary, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return dt;
       }

       public DataTable Read_DelmatFlow(Int64 Delmat_Gid)
       {
           try
           {
               cmnObj.parameters = new List<IDbDataParameter>();
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Delmat_Gid, Delmat_Gid, DbType.Int64));
               dt = cmnObj.dbManager.GetDataTable(cmnObj.SpGet_Delmat_DelmatFlow, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return dt;
       }
       public DataTable Save_Update_Delmat(Delmat_Model ObjModel)
       {
           try
           {
               cmnObj.parameters = new List<IDbDataParameter>();
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Action, cmnObj.Create, DbType.String));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Delmat_Gid,ObjModel.DelmatHeader_gid, DbType.Int64));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmat_name, ObjModel.Delmat_Name, DbType.String));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmat_type_gid, ObjModel.Type_gid, DbType.Int64));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmatsubtype_subtype_gids, ObjModel.SubType_gids, DbType.String));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmatattribute_attributechild_gids, ObjModel.AttributeChilds, DbType.String));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmatdept_dept_gids, ObjModel.Dept_Gids, DbType.String));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmat_Cloned, ObjModel.IsCloned, DbType.String));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmat_Version, ObjModel.Version, DbType.Decimal));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmat_Cloned_delmat_gid, ObjModel.ExistingDelmat_Gid, DbType.Int64));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.delmat_active, ObjModel.IsActive, DbType.String));
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.LoginUserId, 1, DbType.Int64));
               dt = cmnObj.dbManager.GetDataTable(cmnObj.SpGet_Delmat, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return dt;
       }


       //Read Header
       public DataTable Read_DelmatHeader(Int64 Delmat_Gid)
       {
           try
           {
               cmnObj.parameters = new List<IDbDataParameter>();
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Delmat_Gid, Delmat_Gid, DbType.Int64));
               dt = cmnObj.dbManager.GetDataTable(cmnObj.SpGet_Delmatheader, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return dt;
       }

       public DataTable Save_DelmatMatrix(Int64 Delmat_Gid)
       {
           try
           {
               cmnObj.parameters = new List<IDbDataParameter>();
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Delmat_Gid, Delmat_Gid, DbType.Int64));
               dt = cmnObj.dbManager.GetDataTable(cmnObj.SP_Set_Delmat_MatrixLogic, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return dt;
       }
       
 
    }
}
