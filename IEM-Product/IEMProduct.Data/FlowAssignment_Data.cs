﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccessHandler.Models.IEMProduct;
namespace IEMProduct.Data
{
   public class FlowAssignment_Data
    {
       
        FlowAssignment_Model lstmdl = new FlowAssignment_Model();
        List<FlowAssignment_Model> lstData = new List<FlowAssignment_Model>();
        Common ObjCmn = new Common();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();

       public DataTable ReadFlowAssignment(Int64 User_GID,Int64 assignType,string assignRefUserType)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.userID, 50, User_GID, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AssignType, 50, assignType, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AssignRefType, 50, assignRefUserType,DbType.String));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SPReadFlowAssignment, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
 
            }
           catch(Exception ex)
            {
                throw ex;
            }
            return dt;
        }


    public DataTable Save_FlowAssignmentList(FlowAssignment_Model modelObj)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.userID, 50, modelObj.loginUserID, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.FlowactionType, modelObj.actionType, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ecfSelectedList, modelObj.assignSelectedID, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.assignToID, modelObj.assignToID, DbType.Int64));

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.remark, modelObj.assignRemarks, DbType.String));              

                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SPSetFlowAssignmentDetails, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
          


       

       //public DataTable Remove_FlowAssignmentfromlist(FlowAssignment_Model modelObj)
       //{
       //    try
       //    {
       //        ObjCmn.parameters = new List<IDbDataParameter>();
       //        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, modelObj.Action, DbType.String));
       //        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.proxyID, modelObj.proxyGid, DbType.Int64));

       //        dt = ObjCmn.dbManager.GetDataTable(ObjCmn.RemoveProxyDetails, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
       //    }
       //    catch (Exception ex)
       //    {
       //        throw ex;
       //    }
       //    return dt;
       //}

       #region Common methods fro Dropdowns binndings.
        public DataSet GetAssignmentTypeList(string EmployeeName)
       {
           try
           {
               ObjCmn.parameters = new List<IDbDataParameter>();
              // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.userId, 50, userId, DbType.Int32));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.findSearch, EmployeeName, DbType.String));
               ds = ObjCmn.dbManager.GetDataSet(ObjCmn.SpGetFlowAssignList, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());

               //return ds;
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return ds;
       }
        public DataTable GetAssignmentRefNoUserList(int assignTypeID, string FindSearch)
       {
           try
           {
               ObjCmn.parameters = new List<IDbDataParameter>();
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.TypeID, assignTypeID,DbType.Int32));
               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.findSearch, FindSearch, DbType.String));
               dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpGetAssignmentRefNoUserList, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());

               //return ds;
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return dt;
       }

       
       #endregion

    }
}
 
