﻿using DBHelper;
//using DataAccessHandler.Models.IEMProduct;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace IEMProduct.Data
{
    public class Attachment_Data
    {
        Attachment_Model ModelObject = new Attachment_Model();                                   //Attachment model object
        List<Attachment_Model> ModelList = new List<Attachment_Model>();                         //Attachment  list model object
        DataTable dt = new DataTable();
        Common ObjCmn = new Common();                                                              //list model object

        //Attachment Read

        public List<Attachment_Model> ReadRecord(Attachment_Model ModelObject)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachRefflag, 50, ModelObject.attachment_ref_flag, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachRefId, 50, ModelObject.attachment_ref_gid, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachmentModuleFlag, 50, ModelObject.attachment_module_flag, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachInvoiceId, 50, ModelObject.attachment_invoice_gid, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachDebitLineId, 50, ModelObject.debitline_gid, DbType.Int64));
                
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpGetAttachment, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
                foreach (DataRow dr in dt.Rows)
                {
                    ModelList.Add
                        (
                        new Attachment_Model
                        {
                            attachment_gid = Convert.ToInt64(dr["attachment_gid"].ToString()),
                            attachment_filename = dr["attachment_filename"].ToString(),//Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Content/UploadedFiles"), Path.GetFileName(Convert.ToInt64(dr["attachment_gid"].ToString()) + "_" + dr["attachment_filename"].ToString())),
                            attachment_attachmenttype_gid = Convert.ToInt64(dr["attachment_attachmenttype_gid"].ToString()),
                            docgrpname = dr["docgrpname"].ToString(),
                            attachment_desc = dr["attachment_desc"].ToString(),
                            attachment_inserted_by = dr["attachment_inserted_by"].ToString(),
                            attachment_inserted_date = dr["attachment_inserted_date"].ToString(),

                        });
                }
                return ModelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Attachment Save

        public DataTable SaveRecord(Attachment_Model ModelObject)
        {


            try
            {

                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Create, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachId, 0, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachRefflag, 50, ModelObject.attachment_ref_flag, DbType.Int16));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachRefId, 50, ModelObject.attachment_ref_gid, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachFilename, 50, ModelObject.attachment_filename, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachTypeId, 50, ModelObject.attachment_attachmenttype_gid, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachDesc, 50, ModelObject.attachment_desc, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachDocname, 50, ModelObject.attachment_docname, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachInvoiceId, 50, ModelObject.attachment_invoice_gid, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachmentModuleFlag, 50, ModelObject.attachment_module_flag, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, ModelObject.Logger_UserId, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachDebitLineId, 50, ModelObject.debitline_gid, DbType.Int64));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpDmlAttachment, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        //Attachment Delete

        public DataTable DeleteRecord(Attachment_Model ModelObject)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Delete, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachId, ModelObject.attachment_gid, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachRefflag, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachRefId, 50, 0, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachFilename, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachTypeId, 50, 0, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachDesc, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachDocname, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachInvoiceId, 50, 0, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachmentModuleFlag, 50, ModelObject.attachment_module_flag, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, ModelObject.Logger_UserId, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachDebitLineId, 50, ModelObject.debitline_gid, DbType.Int64));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpDmlAttachment, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        //dropdown binding


        public DataTable dropdownvalues(string parentcode)
        {

            ObjCmn.parameters = new List<IDbDataParameter>();
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ParentCode, 50, parentcode, DbType.String));
            dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpGetqcDropdown, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            return dt;
        }

        #region get document informations.
        public DataTable Get_Document_Info(Int64 attachment_gid)
        {

            Dictionary<string, Object> values = new Dictionary<string, object>();
            try
            {

                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.AttachId, 50, attachment_gid, DbType.String));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SPDownloadfile, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region Audit Trail
        public DataTable Read_AuditTrail(Int64 ECF_Gid)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ECF_Gid, ECF_Gid, DbType.Int64));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SP_Get_eow_AuditTrail, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion
        public DataTable GetAttachments(Attachment_Model _objmodel)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.p_module_flag, _objmodel.attach_module, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.p_batch_gid, _objmodel.attach_batch_gid, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.p_ecf_gid, _objmodel.attach_ecf_gid, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.p_debitinvoice_flag, _objmodel.attach_debitinvoice_flag, DbType.String));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpGetAttachments, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

    }
}
