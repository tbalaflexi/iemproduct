﻿using DBHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMProduct.Data
{
  public  class Workbench_Data
    {

        Workbench_Model ModelObject = new Workbench_Model();  //Workbench model object
        DataSet ds = new DataSet();
        List<Workbench_Model> ModelList = new List<Workbench_Model>();                         //Workbench  list model object
        DataTable dt = new DataTable();
        Common ObjCmn = new Common();                                                              //list model object

        //Workbench Read
        public List<Workbench_Model> ReadRecord(Int64 Raiser_Gid, Int64 Login_Gid, string LoginMode)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, Raiser_Gid, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.loginID, Login_Gid, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Login_Mode, LoginMode, DbType.String)); 
                ds = ObjCmn.dbManager.GetDataSet(ObjCmn.SpGetWorkbench, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    ModelList.Add
                        (
                        new Workbench_Model
                        {
                            id = Convert.ToInt64(dr["ecf_gid"].ToString()),
                            ecf_no = dr["ecf_no"].ToString(),
                            ecf_amount = (dr["ecf_amount"].ToString()),
                            ecf_date = dr["ecf_date"].ToString(),
                            employee_name = dr["Raiser"].ToString(),
                            supplierheader_name = dr["ECF Supplier/Employee"].ToString(),
                            ClaimType = dr["ClaimType"].ToString(),
                            ecf_status = dr["ecfstatus_name"].ToString(),
                           // Action = dr["Action"].ToString(),
                            SupplierEmployee_Type = dr["ecf_supplier_employee"].ToString(),
                            Batch_Gid = Convert.ToInt64(dr["ecf_batch_gid"].ToString()),
                            Ecf_Gid = Convert.ToInt64(dr["ecf_gid"].ToString()),
                            Raiser_Gid = Convert.ToInt64(dr["Raiser_Gid"].ToString()),
                            Approver_Status = dr["Approval"].ToString(),
                            ecfDescription = dr["ecf_description"].ToString(),
                            displaying_status = dr["Batch_Status"].ToString()
                        });
                }
                return ModelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public List<Workbench_Model> ReadRecordApprover(Int64 User_GId, Int64 Login_Gid, string LoginMode)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, User_GId, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.loginID, Login_Gid, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Login_Mode, LoginMode, DbType.String)); 
                ds = ObjCmn.dbManager.GetDataSet(ObjCmn.SpGetWorkbench, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
                dt = ds.Tables[1];
                foreach (DataRow dr in dt.Rows)
                {
                    ModelList.Add
                        (
                        new Workbench_Model
                        {
                            id = Convert.ToInt64(dr["queue_ref_gid"].ToString()),
                            ecf_no = dr["doc_no"].ToString(),
                            ecf_amount = (dr["doc_amount"].ToString()),
                            ecf_date = dr["doc_date"].ToString(),
                            employee_name = dr["doc_raiser_name"].ToString(),
                            supplierheader_name = dr["doc_supplier_employee"].ToString(),
                            ClaimType = dr["doc_cliam_type"].ToString(),
                            ecf_status = dr["docstatus_name"].ToString(),
                            // Action = dr["Action"].ToString(),
                            SupplierEmployee_Type = dr["doc_supplier_employee_type"].ToString(),
                            Batch_Gid = Convert.ToInt64(dr["doc_batch_gid"].ToString()),
                            Ecf_Gid = Convert.ToInt64(dr["queue_ref_gid"].ToString()),
                            Raiser_Gid = Convert.ToInt64(dr["doc_raiser_gid"].ToString()),
                            Approver_Status = dr["doc_approval"].ToString(),
                            ecfDescription = dr["doc_description"].ToString(),
                            displaying_status = dr["doc_batch_status"].ToString()
                        });
                }
                return ModelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public List<Workbench_Model> ReadActionHistory(Workbench_Model obj)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.loginID, obj.Login_UserID, DbType.Int64));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.actionType,50, obj.Action, DbType.String));
                ds = ObjCmn.dbManager.GetDataSet(ObjCmn.SpGetWorkBenchHistory, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    ModelList.Add
                        (
                        new Workbench_Model
                        {
                            id = Convert.ToInt64(dr["ecf_gid"].ToString()),
                            ecf_no = dr["ecf_no"].ToString(),
                            ecf_amount = (dr["ecf_amount"].ToString()),
                            ecf_date = dr["ecf_date"].ToString(),
                            employee_name = dr["Raiser"].ToString(),
                            supplierheader_name = dr["ECF Supplier/Employee"].ToString(),
                            ClaimType = dr["ClaimType"].ToString(),
                            ecf_status = dr["ecfstatus_name"].ToString(),
                            // Action = dr["Action"].ToString(),
                            SupplierEmployee_Type = dr["ecf_supplier_employee"].ToString(),
                            Batch_Gid = Convert.ToInt64(dr["ecf_batch_gid"].ToString()),
                            Ecf_Gid = Convert.ToInt64(dr["ecf_gid"].ToString()),
                            Raiser_Gid = Convert.ToInt64(dr["Raiser_Gid"].ToString()),
                            Approver_Status = dr["Approval"].ToString(),
                            ecfDescription = dr["ecf_description"].ToString(),
                        });
                }
                return ModelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
