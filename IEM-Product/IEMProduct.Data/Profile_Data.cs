﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IEMProduct.Data;
using System.Data;
using DataAccessHandler.Models.IEMProduct;
using DBHelper;

namespace IEMProduct.Data
{ 

    public class Profile_Data
    {
        Profile_Model ModelObject = new Profile_Model();
        List<Profile_Model> ModelList = new List<Profile_Model>();
        DataTable dt = new DataTable();
        Common ObjCmn = new Common();
        DataSet ds = new DataSet();


        public DataTable  GetEmpDetails(Profile_Model ModelObject)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, ModelObject.Action, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EMPGid , ModelObject.Emp_Gid, DbType.Int64));              
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SPGetEmployeeDetails, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());

               return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetEmpDetailsHirackey(Profile_Model ModelObject)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();               
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EMPGid, ModelObject.Emp_Gid, DbType.Int64));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SPGetEmployeeHirackey, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //DRopdown Binding
        public DataTable Getdropdownvalues(string flag)
        {

            ObjCmn.parameters = new List<IDbDataParameter>();
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.ParentCode, 50, flag, DbType.String));
            dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpGetqcDropdown, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            return dt;
        }

        //Employee non qc Master:
        public DataTable dropdownvalues(string flag)
        {

            ObjCmn.parameters = new List<IDbDataParameter>();
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.flag, 50, flag, DbType.String));
            dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SpNonqcDropdownValues, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            return dt;
        }
        public DataTable GetEmployee_ByCodeorName(string EmployeeName, string flag,string Master)
        {

            ObjCmn.parameters = new List<IDbDataParameter>();
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.p_Master, Master, DbType.String));
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.p_MasterGid, 0, DbType.String));
            ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.p_Employee, EmployeeName, DbType.String));
            dt = ObjCmn.dbManager.GetDataTable(ObjCmn.SP_EOW_Claim_GetDropDownList, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            return dt;
        }
        //Employeee master insert
        public DataTable SaveRecord(Profile_Model ModelObject, Int64 User_GID)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Create, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpGid, 0, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpCode, 50, ModelObject.Emp_Code, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpPassword, 50, ModelObject.password, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpName, 50, ModelObject.Emp_Name, DbType.String));

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpGender, 50, ModelObject.Gender, DbType.String));
                //  ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDob, 50, ModelObject.DOB, DbType.String));

                if (ModelObject.DOB != null)
                {
                    if (ModelObject.DOB.Length > 19)
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDob, DateTime.Parse(ModelObject.DOB.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDob, DateTime.ParseExact(ModelObject.DOB.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDob, ModelObject.DOB, DbType.String));
                }

                //  ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDoj, 50, ModelObject.DOJ, DbType.String));

                if (ModelObject.DOJ != null)
                {
                    if (ModelObject.DOJ.Length > 19)
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDoj, DateTime.Parse(ModelObject.DOJ.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDoj, DateTime.ParseExact(ModelObject.DOJ.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDoj, ModelObject.DOJ, DbType.String));
                }

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDesignationGid, 50, ModelObject.Designationid, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpGradeGid, 50, ModelObject.Gradeid, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDeptGid, 50, ModelObject.Departmentid, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpAddress1, 50, ModelObject.Emp_Addr1, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpCityGid, 50, ModelObject.cityid, DbType.String));

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpPinCode, 50, ModelObject.Pincode, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpOfficeEmailid, 50, ModelObject.Office_Email, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpPersonalEmailid, 50, ModelObject.Personal_Email, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpMobileNo, 50, ModelObject.mobileno, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpBranchgid, 50, ModelObject.branchid, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpAccountNo, 50, ModelObject.AccountNo, DbType.String));

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpEraBankid, 50, ModelObject.Bankid, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpCardNo, 50, ModelObject.Card_No, DbType.String));

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpFcGid, 50, ModelObject.Fcid, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpCCGid, 50, ModelObject.CCid, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpProductGid, 50, ModelObject.Productid, DbType.String));

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpAdd2, 50, ModelObject.Emp_Addr2, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpPermanantAddress, 50, ModelObject.Address, DbType.String));

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpEraBranchGid, 50, ModelObject.empbranch_id, DbType.String));
                //if (ModelObject.employee_dor != null)
                //{
                //    if (ModelObject.employee_dor.Length > 19)
                //    {
                //        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDor, DateTime.Parse(ModelObject.employee_dor.Substring(4, 12)), DbType.DateTime));
                //    }
                //    else
                //    {
                //        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDor, DateTime.ParseExact(ModelObject.employee_dor.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                //    }
                //}
                //else
                //{
                //    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDor, ModelObject.employee_dor, DbType.String));
                //}



                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDor, 50, ModelObject.employee_dor, DbType.String));


                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpStatus, 50, ModelObject.emp_status, DbType.String));



                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpLastmodificationon, 50, ModelObject.HRIS_LASTMODIFIEDON, DbType.String));


                //if (ModelObject.HRIS_LASTMODIFIEDON != null)
                //{
                //    if (ModelObject.HRIS_LASTMODIFIEDON.Length > 19)
                //    {
                //        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpLastmodificationon, DateTime.Parse(ModelObject.HRIS_LASTMODIFIEDON.Substring(4, 12)), DbType.DateTime));
                //    }
                //    else
                //    {
                //        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpLastmodificationon, DateTime.ParseExact(ModelObject.HRIS_LASTMODIFIEDON.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                //    }
                //}
                //else
                //{
                //    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpLastmodificationon, ModelObject.HRIS_LASTMODIFIEDON, DbType.String));
                //}

               ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpLastworkingDate, 50, ModelObject.employee_lastworkingdate, DbType.String));

                //if (ModelObject.employee_lastworkingdate != null)
                //{
                //    if (ModelObject.employee_lastworkingdate.Length > 19)
                //    {
                //        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpLastworkingDate, DateTime.Parse(ModelObject.employee_lastworkingdate.Substring(4, 12)), DbType.DateTime));
                //    }
                //    else
                //    {
                //        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpLastworkingDate, DateTime.ParseExact(ModelObject.employee_lastworkingdate.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                //    }
                //}
                //else
                //{
                //    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpLastworkingDate, ModelObject.employee_lastworkingdate, DbType.String));
                //}

                 ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpEffectiveDate, 50, ModelObject.employee_effectivedate, DbType.String));

                //if (ModelObject.employee_effectivedate != null)
                //{
                //    if (ModelObject.employee_effectivedate.Length > 19)
                //    {
                //        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpEffectiveDate, DateTime.Parse(ModelObject.employee_effectivedate.Substring(4, 12)), DbType.DateTime));
                //    }
                //    else
                //    {
                //        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpEffectiveDate, DateTime.ParseExact(ModelObject.employee_effectivedate.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                //    }
                //}
                //else
                //{
                //    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpEffectiveDate, ModelObject.employee_effectivedate, DbType.String));
                //}

              //  ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpLastmodificationon, 50, ModelObject.HRIS_LASTMODIFIEDON, DbType.String));



                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpBankAccountType, 50, ModelObject.employee_bankaccounttype, DbType.String));

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpSupervisorGid, 50, ModelObject.employee_supervisor_gid, DbType.String));

                // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpCardNo, 50, ModelObject.Card_No, DbType.String));

                // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpFunctionName, 50, ModelObject.Function_Name, DbType.String));
                // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpempBranchgid, 50, ModelObject.employee_employeebranch_gid, DbType.String));






                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, User_GID, DbType.Int32));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.Sp_Dml_EmployeeMaster, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        //Employee Master Updates
        public DataTable UpdateRecord(Profile_Model ModelObject, Int64 User_GID)
        {
            try
            {
                ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Create, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpGid, ModelObject.Emp_Gid, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpCode, 50, ModelObject.Emp_Code, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpPassword, 50, ModelObject.password, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpName, 50, ModelObject.Emp_Name, DbType.String));

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpGender, 50, ModelObject.Gender, DbType.String));
                //  ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDob, 50, ModelObject.DOB, DbType.String));

                if (ModelObject.DOB != null)
                {
                    if (ModelObject.DOB.Length > 19)
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDob, DateTime.Parse(ModelObject.DOB.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDob, DateTime.ParseExact(ModelObject.DOB.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDob, ModelObject.DOB, DbType.String));
                }

                //  ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDoj, 50, ModelObject.DOJ, DbType.String));

                if (ModelObject.DOJ != null)
                {
                    if (ModelObject.DOJ.Length > 19)
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDoj, DateTime.Parse(ModelObject.DOJ.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDoj, DateTime.ParseExact(ModelObject.DOJ.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDoj, ModelObject.DOJ, DbType.String));
                }

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDesignationGid, 50, ModelObject.Designationid, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpGradeGid, 50, ModelObject.Gradeid, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDeptGid, 50, ModelObject.Departmentid, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpAddress1, 50, ModelObject.Emp_Addr1, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpCityGid, 50, ModelObject.cityid, DbType.String));

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpPinCode, 50, ModelObject.Pincode, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpOfficeEmailid, 50, ModelObject.Office_Email, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpPersonalEmailid, 50, ModelObject.Personal_Email, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpMobileNo, 50, ModelObject.mobileno, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpBranchgid, 50, ModelObject.branchid, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpAccountNo, 50, ModelObject.AccountNo, DbType.String));

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpEraBankid, 50, ModelObject.Bankid, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpCardNo, 50, ModelObject.Card_No, DbType.String));
               
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpFcGid, 50, ModelObject.Fcid, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpCCGid, 50, ModelObject.CCid, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpProductGid, 50, ModelObject.Productid, DbType.String));

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpAdd2, 50, ModelObject.Emp_Addr2, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpPermanantAddress, 50, ModelObject.Address, DbType.String));

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpEraBranchGid, 50, ModelObject.empbranch_id, DbType.String));
                if (ModelObject.employee_dor != null || ModelObject.employee_dor == "0")
                {
                    if (ModelObject.employee_dor.Length > 19)
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDor, DateTime.Parse(ModelObject.employee_dor.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDor, DateTime.ParseExact(ModelObject.employee_dor.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDor, ModelObject.employee_dor, DbType.String));
                }

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpStatus, 50, ModelObject.emp_status, DbType.String));

                if (ModelObject.HRIS_LASTMODIFIEDON != null)
                {
                    if (ModelObject.HRIS_LASTMODIFIEDON.Length > 19)
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpLastmodificationon, DateTime.Parse(ModelObject.HRIS_LASTMODIFIEDON.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpLastmodificationon, DateTime.ParseExact(ModelObject.HRIS_LASTMODIFIEDON.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpLastmodificationon, ModelObject.HRIS_LASTMODIFIEDON, DbType.String));
                }

                //ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpLastworkingDate, 50, ModelObject.employee_lastworkingdate, DbType.String));

                if (ModelObject.employee_lastworkingdate != null)
                {
                    if (ModelObject.employee_lastworkingdate.Length > 19)
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpLastworkingDate, DateTime.Parse(ModelObject.employee_lastworkingdate.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpLastworkingDate, DateTime.ParseExact(ModelObject.employee_lastworkingdate.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpLastworkingDate, ModelObject.employee_lastworkingdate, DbType.String));
                }

                //   ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpEffectiveDate, 50, ModelObject.employee_effectivedate, DbType.String));

                if (ModelObject.employee_effectivedate != null)
                {
                    if (ModelObject.employee_effectivedate.Length > 19)
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpEffectiveDate, DateTime.Parse(ModelObject.employee_effectivedate.Substring(4, 12)), DbType.DateTime));
                    }
                    else
                    {
                        ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpEffectiveDate, DateTime.ParseExact(ModelObject.employee_effectivedate.Substring(0, 10), "dd-MM-yyyy", null), DbType.DateTime));
                    }
                }
                else
                {
                    ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpEffectiveDate, ModelObject.employee_effectivedate, DbType.String));
                }

                //ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpLastmodificationon, 50, ModelObject.HRIS_LASTMODIFIEDON, DbType.String));



                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpBankAccountType, 50, ModelObject.employee_bankaccounttype, DbType.String));


                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpSupervisorGid, 50, ModelObject.employee_supervisor_gid, DbType.String));

                // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpCardNo, 50, ModelObject.Card_No, DbType.String));

                // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpFunctionName, 50, ModelObject.Function_Name, DbType.String));
                // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpempBranchgid, 50, ModelObject.employee_employeebranch_gid, DbType.String));





                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, User_GID, DbType.Int32));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.Sp_Dml_EmployeeMaster, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable DeleteRecord(Profile_Model ModelObject, Int64 User_Gid)
        {
            try
            {
             ObjCmn.parameters = new List<IDbDataParameter>();
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.Action, 50, ObjCmn.Delete, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpGid, ModelObject.Emp_Gid, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpCode, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpName, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpGender, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpEraBankid, 50, 0, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpBranchgid, 50, 0, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDob, 50, "2019-01-01", DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDoj, 50, "2019-01-01", DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDesignationGid, 50, 0, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpGradeGid, 50, 0, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDeptGid, 50, 0, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpFcGid, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpCCGid, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpProductGid, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpAddress1, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpAdd2, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpPermanantAddress, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpPersonalEmailid, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpOfficeEmailid, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpSupervisorGid, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpStatus, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpAccountNo, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpEraBranchGid, 50, 0, DbType.Int32));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpCardNo, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpCityGid, 50, 0, DbType.String));
               // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpFunctionName, 50, 0, DbType.String));
               // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpempBranchgid, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpMobileNo, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpPassword, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpPinCode, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpBankAccountType, 50, 0, DbType.String));
               // ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDor, 50, 0, DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpDor, 50, "2019-01-01", DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpLastworkingDate, 50, "2019-01-01", DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpEffectiveDate, 50, "2019-01-01", DbType.String));
                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.EmpLastmodificationon, 50, "2019-01-01", DbType.String));

                ObjCmn.parameters.Add(ObjCmn.dbManager.CreateParameter(ObjCmn.LoginUserId, 50, User_Gid, DbType.Int32));
                dt = ObjCmn.dbManager.GetDataTable(ObjCmn.Sp_Dml_EmployeeMaster, CommandType.StoredProcedure, ObjCmn.parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
    }
}
