﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using DataAccessHandler.Models.IEMProduct;
using DBHelper;
namespace IEMProduct.Data
{
   public class BulkUpload_Data
    {
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        Common cmnObj = new Common();
        BulkUpload_Model ObjModel =new BulkUpload_Model();

       public DataTable ValidateExcelMasterCode(string Master_Code,string Dependant_Code,string Action)
       {
           try
           {
               cmnObj.parameters = new List<IDbDataParameter>();
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Master_code, Master_Code, DbType.String)); 
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Dependant_Code, Dependant_Code, DbType.String)); 
               cmnObj.parameters.Add(cmnObj.dbManager.CreateParameter(cmnObj.Action, Action, DbType.String));
               dt = cmnObj.dbManager.GetDataTable(cmnObj.SP_eow_ValidateExcelMasterCode, CommandType.StoredProcedure, cmnObj.parameters.ToArray());
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return dt;
       }
    }
}
