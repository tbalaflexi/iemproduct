﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessHandler.Models.FlexiBuy;
using FlexiBuy.Data;
using System.Data;

namespace FlexiBuy.Service
{
    public class POUpload_Service
    {
        #region Declarations
        POUpload_Data objData = new POUpload_Data();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        List<POUpload_Model> lst = new List<POUpload_Model>();
        #endregion

        public List<POUpload_Model> Get_POUPloadDetailList(POUpload_Model Obj_Model)
        {

            try
            {
                Int64 j = 0;
                ds = objData.Get_POUploadDetailList(Obj_Model);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        POUpload_Model _obj = new POUpload_Model();
                         j++;
                        _obj.SlNo = j;
                        _obj.poupload_gid = Convert.ToInt64(dt.Rows[i]["poupload_gid"].ToString());
                        _obj.File_Gid = Convert.ToInt64(dt.Rows[i]["File_Gid"].ToString());
                        _obj.Poheader_pono = dt.Rows[i]["Poheader_pono"].ToString();

                        _obj.poheader_desc = dt.Rows[i]["poheader_desc"].ToString();
                        _obj.poheader_period_from = dt.Rows[i]["poheader_period_from"].ToString();
                        _obj.poheader_period_to = dt.Rows[i]["poheader_period_to"].ToString();
                        _obj.poheader_vendor_code = dt.Rows[i]["poheader_vendor_code"].ToString();
                        _obj.poheader_vendor_contact_code = dt.Rows[i]["poheader_vendor_contact_code"].ToString();
                        _obj.poheader_type = dt.Rows[i]["poheader_type"].ToString();
                        _obj.poheader_request = dt.Rows[i]["poheader_request"].ToString();
                        _obj.poheader_raiser_code = dt.Rows[i]["poheader_raiser_code"].ToString();
                        _obj.podts_item_desc = dt.Rows[i]["podts_item_desc"].ToString();
                        _obj.podts_item_Qty = dt.Rows[i]["podts_item_Qty"].ToString();
                        _obj.podts_unit_price = dt.Rows[i]["podts_unit_price"].ToString();
                        _obj.podts_item_total = dt.Rows[i]["podts_item_total"].ToString();
                        _obj.podts_service_code = dt.Rows[i]["podts_service_code"].ToString();
                        _obj.podts_uom_code = dt.Rows[i]["podts_uom_code"].ToString();
                        _obj.podts_shipment_type = dt.Rows[i]["podts_shipment_type"].ToString();
                        _obj.podts_shipment_branch_code = dt.Rows[i]["podts_shipment_branch_code"].ToString();
                        _obj.podts_shipment_emp_code = dt.Rows[i]["podts_shipment_emp_code"].ToString();
                        _obj.grninwrdheader_dcno = dt.Rows[i]["grninwrdheader_dcno"].ToString();
                        _obj.grninwrdheader_invoiceno = dt.Rows[i]["grninwrdheader_invoiceno"].ToString();
                        _obj.grn_inward_qty = dt.Rows[i]["grn_inward_qty"].ToString();
                        _obj.asset_sl_no = dt.Rows[i]["asset_sl_no"].ToString();
                        _obj.grn_putuseddate = dt.Rows[i]["grn_putuseddate"].ToString();
                        _obj.grn_mft_name = dt.Rows[i]["grn_mft_name"].ToString();
                        _obj.podts_gst_applicable = dt.Rows[i]["podts_gst_applicable"].ToString();
                        _obj.HSN_Code = dt.Rows[i]["HSN_Code"].ToString();
                        _obj.podts_tax_amount = dt.Rows[i]["podts_tax_amount"].ToString();
                        _obj.provider_loc = dt.Rows[i]["provider_loc"].ToString();
                        _obj.receiver_loc = dt.Rows[i]["receiver_loc"].ToString();
                        _obj.cwiprelease_branch_code = dt.Rows[i]["cwiprelease_branch_code"].ToString();
                        _obj.cwiprelease_emp_code = dt.Rows[i]["cwiprelease_emp_code"].ToString();
                        _obj.poupload_status = dt.Rows[i]["poupload_status"].ToString();
                        _obj.poupload_remarks = dt.Rows[i]["poupload_remarks"].ToString();
                        _obj.poupload_move_flag = dt.Rows[i]["poupload_move_flag"].ToString();
                        _obj.poupload_valid_flag = dt.Rows[i]["poupload_valid_flag"].ToString();
                        lst.Add(_obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }

        public Int64 Get_FileCount(POUpload_Model Obj_Model)
        {
            Int64 FileCount = 0;
            try
            {

                ds = objData.Get_POUploadFileCount(Obj_Model);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        FileCount = Convert.ToInt64(dt.Rows[i]["FileCount"]);

                    }
                }

                return FileCount;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public Int64 FileInst_Data (POUpload_Model Obj_Model)
        {
            Int64 FileGid = 0;
            try
            {
                ds = objData.FileInst_Data(Obj_Model);

                if (ds.Tables.Count>0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count;i++ )
                    {
                        FileGid = Convert.ToInt64(dt.Rows[i]["File_gid"]);
                    }
                }

                return FileGid;
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }

        public DataTable SaveTempPOUploadData(POUpload_Model Obj_Model)
        {
            try
            {
                dt = objData.SetPOUpload_TempTable(Obj_Model);

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Int64 TruncateTempData(POUpload_Model Obj_Model)
        {
            Int64 Result = 0;
            try
            {
               dt = objData.GetColumnList(Obj_Model);
                if (dt.Rows .Count > 0)
                {
                    Result = 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Result;

        }
        public DataTable SetFileConfirm_Data(POUpload_Model Obj_Model)
        {
            try
            {
                dt = objData.SetFileConfirm_Data(Obj_Model);

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
