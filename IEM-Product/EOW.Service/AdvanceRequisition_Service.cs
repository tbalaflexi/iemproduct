﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccessHandler.Models.EOW;
using EOW.Data;

namespace EOW.Service
{
    public class AdvanceRequisition_Service
    {
        AdvanceRequisition_Data objData = new AdvanceRequisition_Data();
        Claim_Data _objData = new Claim_Data();
        DataTable dt = new DataTable();

        #region Get Batch Details.
        public AdvanceRequisition_Model Get_AdvaneRquisition_BatchDetails(Int64 Batchgid, Int64 Ecfgid)
        {
            AdvanceRequisition_Model _objModel = new AdvanceRequisition_Model();
            try
            {
                dt = _objData.Get_BatchDetails(Batchgid, Ecfgid);
                if (dt.Rows.Count > 0)
                {
                    _objModel.Batch_Gid = Convert.ToInt64(dt.Rows[0]["batch_gid"].ToString());
                    _objModel.Batch_No = dt.Rows[0]["batch_no"].ToString();
                    _objModel.Batch_Amount = Convert.ToDouble(dt.Rows[0]["batch_totalamt"].ToString());
                    _objModel.Type_Gid = Convert.ToInt64(dt.Rows[0]["ecf_doctype_gid"].ToString());
                    _objModel.SubType_Gid = Convert.ToInt64(dt.Rows[0]["ecf_docsubtype_gid"].ToString());
                    _objModel.Employee_Gid = Convert.ToInt64(dt.Rows[0]["raiser_gid"].ToString());
                    _objModel.Employee_Name = dt.Rows[0]["raiser"].ToString();
                    if (Ecfgid != 0)
                    {
                        _objModel.Ecf_Gid = Convert.ToInt64(dt.Rows[0]["ecf_gid"].ToString());
                    }
                    else
                    {
                        _objModel.Ecf_Gid = Ecfgid;
                    }
                    _objModel.Sup_Emp_Type = dt.Rows[0]["ecf_supplier_employee"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _objModel;
        }
        #endregion

        #region Common method for bindings dropdown lists.
        public List<AdvanceRequisition_Model> Get_dropdownlist_records(string Master, string Master_Code, Int64 Master_Gid)
        {
            List<AdvanceRequisition_Model> lst = new List<AdvanceRequisition_Model>();
            try
            {

                #region Fetch QCD & Non-QCD Datas to Datatable.
                if (Master == "Type" || Master == "SubType" || Master == "Supplier" || Master == "AdvanceType")
                {
                    dt = objData.Get_dropdownlist_records(Master, Master_Gid);
                }
                #endregion

                #region Add datatable values to List.
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0].ToString() != "SQLEXCEPTION")
                    {
                        if (Master == "Type")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                AdvanceRequisition_Model _objModel = new AdvanceRequisition_Model();
                                _objModel.Type_Gid = Convert.ToInt64(row["TypeGid"].ToString());
                                _objModel.Type_Name = row["TypeName"].ToString();
                                lst.Add(_objModel);
                            }
                        }
                        if (Master == "SubType")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                AdvanceRequisition_Model _objModel = new AdvanceRequisition_Model();
                                _objModel.SubType_Gid = Convert.ToInt64(row["SubType_Gid"].ToString());
                                _objModel.SubType_Name = row["SubType_Name"].ToString();
                                lst.Add(_objModel);
                            }
                        }
                        if (Master == "Supplier")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                AdvanceRequisition_Model _objModel = new AdvanceRequisition_Model();
                                _objModel.Supplier_Gid = Convert.ToInt64(row["supplierheader_gid"].ToString());
                                _objModel.Supplier_Name = row["supplier"].ToString();
                                lst.Add(_objModel);
                            }
                        }
                        if (Master == "AdvanceType")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                AdvanceRequisition_Model _objModel = new AdvanceRequisition_Model();
                                _objModel.AdvanceType_Gid = Convert.ToInt64(row["advancetype_gid"].ToString());
                                _objModel.AdvanceType_Name = row["advancetype_name"].ToString();
                                lst.Add(_objModel);
                            }
                        }
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }

        public string Get_GL_Code(string Master, Int64 Master_Gid)
        {
            string Glcode = string.Empty;
            try
            {
                dt = objData.Get_dropdownlist_records(Master, Master_Gid);
                if (dt.Rows.Count > 0)
                {
                    Glcode = dt.Rows[0][0].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Glcode;
        }

        #endregion

        #region methods for create/update advance requisition.
        public DataTable Save_AdvanceRequisition(AdvanceRequisition_Model _objModel)
        {
            try
            {
                return objData.Save_AdvanceRequisition(_objModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable Update_AdvanceRequisition(AdvanceRequisition_Model _objModel)
        {
            try
            {
                return objData.Update_AdvanceRequisition(_objModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region common method for fetching grid records.
        public List<AdvanceRequisition_Model> Get_ARF_records(string GridName, Int64 BatchGid, Int64 EcfGid)
        {
            List<AdvanceRequisition_Model> lst = new List<AdvanceRequisition_Model>();
            try
            {
                if (BatchGid > 0)
                {
                    dt = objData.Get_ARF_records(GridName, BatchGid, EcfGid);
                }
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0].ToString() != "SQLEXCEPTION")
                    {
                        if (GridName == "AdvanceRequest")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                AdvanceRequisition_Model _objModel = new AdvanceRequisition_Model();
                                _objModel.Ref_Gid = Convert.ToInt64(row["ecf_gid"].ToString());
                                _objModel.Ref_No = row["ecf_no"].ToString();
                                _objModel.AdvanceType_Gid = Convert.ToInt64(row["advancetype_gid"].ToString());
                                _objModel.AdvanceType_Name = row["advancetype_name"].ToString();
                                _objModel.GL_CodeDesc = row["ecfarf_dr_gl_no"].ToString();
                                if (row["ecfarf_liq_date"].ToString() != "" && row["ecfarf_liq_date"].ToString() != null)
                                {
                                    _objModel.Liquidation_Date = Convert.ToDateTime(row["ecfarf_liq_date"].ToString()).ToString("dd-MM-yyyy");
                                }
                                else
                                {
                                    _objModel.Liquidation_Date = "";
                                }
                                _objModel.Advance_Amount = Convert.ToDouble(row["ecfarf_amount"].ToString());
                                _objModel.Description = row["ecfarf_desc"].ToString();
                                _objModel.Debitline_Gid = Convert.ToInt64(row["debitline_gid"].ToString());
                                _objModel.Invoice_Gid = Convert.ToInt64(row["invoice_gid"].ToString());
                                _objModel.Emp_Supl_Code = row["SupplierEmployee"].ToString();
                                _objModel.Supplier_Gid = Convert.ToInt64(row["ecf_supplier_gid"].ToString());
                                _objModel.Supplier_Name = row["supplierheader_name"].ToString();
                                lst.Add(_objModel);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }
        #endregion

    }
}
