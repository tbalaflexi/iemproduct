﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EOW.Data;
using DBHelper;
using DataAccessHandler.Models.EOW;
using DataAccessHandler.Models.IEMProduct;
using SharedData;


namespace EOW.Service
{
    public class SupplierInvoiceService
    {
        #region Declarations
        SupplierInvoiceData dataObj = new SupplierInvoiceData();
        DropDown_Bindings cmnObj = new DropDown_Bindings();
        QCD_Model objQCD = new QCD_Model();
        List<SupplierInvoice_Model> objLst = new List<SupplierInvoice_Model>();
        EOWCommon cmnParams = new EOWCommon();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        #endregion

        public List<SupplierInvoice_Model> Get_DropDownList(SupplierInvoice_Model data)
        {
            List<SupplierInvoice_Model> lst = new List<SupplierInvoice_Model>();
            try
            {
                if ((data.Type_Gid == 0 || data.Type_Gid == null) && (data.SubType_Gid == 0 || data.SubType_Gid == null))
                {
                    if (data.MasterCode == "Type")
                    {
                        objQCD.Master_Code = cmnParams.TypeCode;
                    }
                    else if (data.MasterCode == "SubType")
                    {
                        objQCD.Master_Code = cmnParams.SubTypeCode;
                    }

                }

                if (data.Type_Gid > 0 || data.SubType_Gid > 0)
                {
                    objQCD.Dependent_Gid = data.Type_Gid;
                    objQCD.Parent_Gid = data.SubType_Gid;
                }
                if (data.MasterCode == "Tax")
                {
                    objQCD.Master_Code = cmnParams.TaxCode;
                    objQCD.Values = data.Values;
                }
                else if (data.MasterCode == "SubTax")
                {
                    objQCD.Master_Code = cmnParams.SubTaxCode;
                    objQCD.Values = data.Values;
                }
                if (data.SubTaxid > 0 || data.Taxid > 0)
                {

                    objQCD.Parent_Gid = data.SubTaxid;
                    objQCD.Dependent_Gid = data.Taxid;
                    objQCD.Values = data.Values;
                }
                dt = cmnObj.DropDownValues(objQCD);


                if (dt.Rows.Count > 0)
                {

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (data.MasterCode == "Type" || data.MasterCode == "SubType")
                        {
                            if ((data.Type_Gid == 0 || data.Type_Gid == null) && (objQCD.Master_Code == cmnParams.TypeCode))
                            {
                                SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                                Obj.Type_Gid = Convert.ToInt64(dt.Rows[i][0]);
                                Obj.Type_Code = Convert.ToString(dt.Rows[i][1]);
                                Obj.Type_Name = Convert.ToString(dt.Rows[i][2]);
                                lst.Add(Obj);
                            }

                            else if (data.Type_Gid > 0)
                            {
                                SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                                Obj.SubType_Gid = Convert.ToInt64(dt.Rows[i][0]);
                                Obj.SubType_Code = Convert.ToString(dt.Rows[i][1]);
                                Obj.SubType_Name = Convert.ToString(dt.Rows[i][2]);
                                lst.Add(Obj);
                            }
                        }
                        else if (data.MasterCode == "Tax" || data.MasterCode == "SubTax")
                        {
                            if ((data.Taxid == 0 || data.Taxid == null) && (objQCD.Master_Code == cmnParams.TaxCode))
                            {
                                SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                                Obj.Taxid = Convert.ToInt64(dt.Rows[i][0]);
                                Obj.TaxCode = Convert.ToString(dt.Rows[i][1]);
                                Obj.TaxName = Convert.ToString(dt.Rows[i][2]);
                                lst.Add(Obj);
                            }

                            else if (data.Taxid > 0)
                            {
                                SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                                Obj.SubTaxid = Convert.ToInt64(dt.Rows[i][0]);
                                Obj.SubTaxCode = Convert.ToString(dt.Rows[i][1]);
                                Obj.SubTaxName = Convert.ToString(dt.Rows[i][2]);
                                lst.Add(Obj);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }

        public DataTable SaveInvoice(SupplierInvoice_Model data)
        {
            try
            {
                if (data.GridType == "Invoice")
                {
                    data._Action = cmnParams.Insert;
                    dt = dataObj.SaveInvoice(data);
                }
                else if (data.GridType == "Expense")
                {
                    data._Action = cmnParams.Insert;
                    dt = dataObj.SaveExpense(data);
                }
                else if (data.GridType == "Credit")
                {
                    data._Action = cmnParams.Insert;
                    dt = dataObj.SaveCredit(data);
                }
                else if (data.GridType == "Tax")
                {
                    data._Action = cmnParams.Insert;
                    dt = dataObj.SaveTax(data);
                }

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public DataTable UpdateInvoice(SupplierInvoice_Model data)
        {
            try
            {
                if (data.GridType == "Invoice")
                {
                    data._Action = cmnParams.Update;
                    dt = dataObj.SaveInvoice(data);
                }
                else if (data.GridType == "Expense")
                {
                    data._Action = cmnParams.Update;
                    dt = dataObj.SaveExpense(data);
                }
                else if (data.GridType == "Credit")
                {
                    data._Action = cmnParams.Update;
                    dt = dataObj.SaveCredit(data);
                }
                else if (data.GridType == "Remarks")
                {
                    data._Action = cmnParams.Update;
                    dt = dataObj.SaveRemarks(data);
                }
                else if (data.GridType == "Tax")
                {
                    data._Action = cmnParams.Update;
                    dt = dataObj.SaveTax(data);
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #region Supplier Invoice Deletions
        public DataTable DeleteInvoice(SupplierInvoice_Model data)
        {
            try
            {
                data._Action = cmnParams.Delete;
                dt = dataObj.SaveInvoice(data);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataTable DeleteInvoiceExp(SupplierInvoice_Model data)
        {
            try
            {
                data._Action = cmnParams.Delete;
                dt = dataObj.SaveExpense(data);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataTable DeleteInvoicepayment(SupplierInvoice_Model data)
        {
            try
            {
                data._Action = cmnParams.Delete;
                dt = dataObj.SaveCredit(data);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataTable DeleteInvoiceTDS(SupplierInvoice_Model data)
        {
            try
            {
                data._Action = cmnParams.Delete;
                dt = dataObj.SaveTax(data);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region Get Raiser
        public List<SupplierInvoice_Model> GetRaiser(SupplierInvoice_Model data)
        {
            try
            {
                ds = dataObj.GetRaiser(data);
                if (ds.Tables.Count > 1)
                {
                    dt = ds.Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                            Obj.Raiserid = Convert.ToInt64(dt.Rows[i]["employeeid"]);
                            Obj.RaiserName = Convert.ToString(dt.Rows[i]["employeename"]);
                            objLst.Add(Obj);
                        }
                    }

                }
                else
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.Message = Convert.ToString(dt.Rows[i]["Message"]);
                        Obj.Clear = Convert.ToString(dt.Rows[i]["Clear"]);
                        objLst.Add(Obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objLst;
        }
        public List<SupplierInvoice_Model> Get_Raiser_DropDown_List(string MasterName, Int64 Master_Gid, string EmpName)
        {
            try
            {
                dt = dataObj.Get_Raiser_DropDown_List(MasterName, Master_Gid, EmpName);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.Raiserid = Convert.ToInt64(dt.Rows[i]["employeeid"]);
                        Obj.RaiserName = Convert.ToString(dt.Rows[i]["employeename"]);
                        objLst.Add(Obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objLst;
        }
        #endregion

        #region Get Supplier
        public List<SupplierInvoice_Model> GetSupplier(SupplierInvoice_Model data)
        {
            try
            {
                ds = dataObj.GetSupplier(data);
                if (ds.Tables.Count > 1)
                {
                    dt = ds.Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                            Obj.Supplierid = Convert.ToInt64(dt.Rows[i]["Supplierid"]);
                            Obj.SupplierName = Convert.ToString(dt.Rows[i]["SupplierName"]);
                            objLst.Add(Obj);
                        }
                    }

                }
                else
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.Message = Convert.ToString(dt.Rows[i]["Message"]);
                        Obj.Clear = Convert.ToString(dt.Rows[i]["Clear"]);
                        objLst.Add(Obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objLst;
        }
        #endregion

        public List<SupplierInvoice_Model> GetInvoices(SupplierInvoice_Model data)
        {
            List<SupplierInvoice_Model> lstInv = new List<SupplierInvoice_Model>();
            try
            {
                ds = dataObj.GetInvoices(data);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        if (data.GridType == "Invoice")
                        {
                            Obj.Refid = Convert.ToInt64(dt.Rows[i]["ecf_gid"]);
                            Obj.RefrenceNo = Convert.ToString(dt.Rows[i]["ecf_no"]);
                            Obj.SupplierName = Convert.ToString(dt.Rows[i]["SupplierName"]);
                            Obj.Beneficiary = Convert.ToString(dt.Rows[i]["subeneficiary"]);
                            Obj.Supplierid = Convert.ToInt64(dt.Rows[i]["Supplierid"]);
                            Obj.Invoiceid = Convert.ToInt64(dt.Rows[i]["invoice_gid"]);
                            Obj.InvoiceNo = Convert.ToString(dt.Rows[i]["invoice_no"]);
                            Obj.InvoiceDate = Convert.ToString(dt.Rows[i]["invoice_date"]);
                            Obj.InvServiceMonth = Convert.ToString(dt.Rows[i]["invoice_service_month"]);
                            Obj.InvoiceAmt = Convert.ToDecimal(dt.Rows[i]["invoice_amount"]);
                            Obj.InvDescription = Convert.ToString(dt.Rows[i]["invoice_desc"]);
                            Obj.ProvLocid = Convert.ToInt64(dt.Rows[i]["Providerid"]);
                            Obj.ProviderLoc = Convert.ToString(dt.Rows[i]["ProviderName"]);
                            Obj.RecvLocid = Convert.ToInt64(dt.Rows[i]["Receiverid"]);
                            Obj.ReceiverLoc = Convert.ToString(dt.Rows[i]["ReceiverName"]);
                            Obj.TaxStatus = Convert.ToString(dt.Rows[i]["Taxstauts"]);
                            Obj.ProviderGstn = Convert.ToString(dt.Rows[i]["invoice_gstin_vendor"]);
                            Obj.ReceiverGstn = Convert.ToString(dt.Rows[i]["invoice_gstin_ficcl"]);
                            Obj.AmortDesc = Convert.ToString(dt.Rows[i]["AmortDesc"]);
                            Obj.AmortFlag = Convert.ToString(dt.Rows[i]["AmortFlag"]);
                            Obj.AmortFrom = Convert.ToString(dt.Rows[i]["ecf_amort_from"]);
                            Obj.AmortTo = Convert.ToString(dt.Rows[i]["ecf_amort_to"]);
                            Obj.RetentionFlag = Convert.ToString(dt.Rows[i]["invoice_retention_flag"]);
                            Obj.RetentionRate = Convert.ToDecimal(dt.Rows[i]["invoice_retention_rate"]);
                            Obj.RetentionAmt = Convert.ToDecimal(dt.Rows[i]["invoice_retention_amount"]);
                            Obj.RetentionException = Convert.ToDecimal(dt.Rows[i]["invoice_retention_exception"]);
                            Obj.UrgentFlag = Convert.ToString(dt.Rows[i]["ecf_urgent_flag"]);
                            Obj.Currencyid = Convert.ToInt64(dt.Rows[i]["ecf_currency_gid"]);
                            Obj.CurrencyName = Convert.ToString(dt.Rows[i]["ecf_currency_code"]);
                            Obj.CurrencyRate = Convert.ToDecimal(dt.Rows[i]["ecf_currency_rate"]);
                            Obj.TypeFlag = Convert.ToString(dt.Rows[i]["ecf_po_type"]);
                            Obj.TdsCount = Convert.ToInt16(dt.Rows[i]["TdsCount"]);
                            Obj.AttachmentCount = Convert.ToInt16(dt.Rows[i]["Attachmentcount"]);
                            Obj.SubType_Gid = Convert.ToInt64(dt.Rows[i]["docsubtype_gid"]);
                            Obj.SubType_Name = Convert.ToString(dt.Rows[i]["docsubtype_name"]);
                            Obj.InvPOCount = Convert.ToInt64(dt.Rows[i]["invoicepoitemcount"]);
                        }
                        else if (data.GridType == "Expense")
                        {
                            Obj.Refid = Convert.ToInt64(dt.Rows[i]["ecf_gid"]);
                            Obj.RefrenceNo = Convert.ToString(dt.Rows[i]["ecf_no"]);
                            Obj.Invoiceid = Convert.ToInt64(dt.Rows[i]["invoice_gid"]);
                            Obj.InvoiceNo = Convert.ToString(dt.Rows[i]["invoice_no"]);
                            Obj.Expenseid = Convert.ToInt64(dt.Rows[i]["debitline_gid"]);
                            Obj.ExpCatid = Convert.ToInt64(dt.Rows[i]["Expcatid"]);
                            Obj.ExpCatname = Convert.ToString(dt.Rows[i]["ExpcatName"]);
                            Obj.AssetCatid = Convert.ToInt64(dt.Rows[i]["Expcatid"]);
                            Obj.AssetCatName = Convert.ToString(dt.Rows[i]["ExpcatName"]);
                            Obj.ExpNatureid = Convert.ToInt64(dt.Rows[i]["Natureexpid"]);
                            Obj.ExpNaturename = Convert.ToString(dt.Rows[i]["Naturedesc"]);
                            Obj.ExpSubcatid = Convert.ToInt64(dt.Rows[i]["Expsubcatid"]);
                            Obj.ExpSubcatname = Convert.ToString(dt.Rows[i]["ExpsubcatName"]);
                            Obj.AssetSubcatid = Convert.ToInt64(dt.Rows[i]["Expsubcatid"]);
                            Obj.AssetSubcatname = Convert.ToString(dt.Rows[i]["ExpsubcatName"]);
                            Obj.Glid = Convert.ToInt64(dt.Rows[i]["expsubcat_gl_masters_gid"]);
                            Obj.GlName = Convert.ToString(dt.Rows[i]["Glno"]);
                            Obj.AssetGlid = Convert.ToInt64(dt.Rows[i]["expsubcat_gl_masters_gid"]);
                            Obj.AssetGlName = Convert.ToString(dt.Rows[i]["Glno"]);
                            Obj.ExpenseAmt = Convert.ToDecimal(dt.Rows[i]["debitline_amount"]);
                            Obj.HsnId = Convert.ToInt64(dt.Rows[i]["Hsnid"]);
                            Obj.HsnName = Convert.ToString(dt.Rows[i]["HsnName"]);
                            Obj.ExpDesc = Convert.ToString(dt.Rows[i]["debitline_desc"]);
                            Obj.Tax = Convert.ToString(dt.Rows[i]["Tax"]);
                            Obj.TaxCount = Convert.ToInt16(dt.Rows[i]["TaxCount"]);
                        }

                        else if (data.GridType == "Credit")
                        {
                            Obj.Refid = Convert.ToInt64(dt.Rows[i]["ecf_gid"]);
                            Obj.RefrenceNo = Convert.ToString(dt.Rows[i]["ecf_no"]);
                            Obj.Invoiceid = Convert.ToInt64(dt.Rows[i]["invoice_gid"]);
                            Obj.InvoiceNo = Convert.ToString(dt.Rows[i]["invoice_no"]);
                            Obj.Creditid = Convert.ToInt64(dt.Rows[i]["creditline_gid"]);
                            Obj.CreditAmt = Convert.ToDecimal(dt.Rows[i]["creditline_amount"]);
                            //Obj.Bankid = Convert.ToInt64(dt.Rows[i]["Bankid"]);
                            //Obj.BankName = Convert.ToString(dt.Rows[i]["BankName"]);
                            Obj.Beneficiary = Convert.ToString(dt.Rows[i]["creditline_beneficiary"]);
                            Obj.IfscCode = Convert.ToString(dt.Rows[i]["creditline_ifsc_code"]);
                            //Obj.Glid = Convert.ToInt64(dt.Rows[i]["Glid"]);
                            //Obj.GlName = Convert.ToString(dt.Rows[i]["creditline_gl_no"]);
                            Obj.AccountNo = Convert.ToString(dt.Rows[i]["creditline_ref_no"]);
                            Obj.Paymodeid = Convert.ToInt64(dt.Rows[i]["paymode_gid"]);
                            Obj.Paymode = Convert.ToString(dt.Rows[i]["paymode_code"]);
                            Obj.CreditDesc = Convert.ToString(dt.Rows[i]["creditline_desc"]);
                            Obj.Bankid = Convert.ToInt64(dt.Rows[i]["bankid"]);
                            Obj.BankName = Convert.ToString(dt.Rows[i]["bankname"]);
                            Obj.HD_Sus_Selected_GL = Convert.ToString(dt.Rows[i]["creditline_ref_no"]);
                        }

                        else if (data.GridType == "Remarks")
                        {
                            Obj.Refid = Convert.ToInt64(dt.Rows[i]["ecf_gid"]);
                            Obj.RefrenceNo = Convert.ToString(dt.Rows[i]["ecf_no"]);
                            Obj.EcfRemark = Convert.ToString(dt.Rows[i]["ecf_remark"]);
                            Obj.Ecfstatus = Convert.ToString(dt.Rows[i]["ecfstatus_name"]);
                        }

                        else if (data.GridType == "Tax")
                        {
                            Obj.Refid = Convert.ToInt64(dt.Rows[i]["ecf_gid"]);
                            Obj.RefrenceNo = Convert.ToString(dt.Rows[i]["ecf_no"]);
                            Obj.Invoiceid = Convert.ToInt64(dt.Rows[i]["invoice_gid"]);
                            Obj.InvoiceNo = Convert.ToString(dt.Rows[i]["invoice_no"]);
                            Obj.InvoiceAmt = Convert.ToDecimal(dt.Rows[i]["invoice_amount"]);
                            Obj.TaxName = Convert.ToString(dt.Rows[i]["Taxtype"]);
                            Obj.Taxid = Convert.ToInt64(dt.Rows[i]["Taxgid"]);
                            Obj.TaxAmt = Convert.ToInt64(dt.Rows[i]["invoicetax_amount"]);
                            Obj.SubTaxName = Convert.ToString(dt.Rows[i]["Taxsubtype"]);
                            Obj.SubTaxid = Convert.ToInt64(dt.Rows[i]["SubTaxgid"]);
                            Obj.TaxableAmt = Convert.ToDecimal(dt.Rows[i]["invoicetax_taxable_amount"]);
                            Obj.TaxAmt = Convert.ToDecimal(dt.Rows[i]["invoicetax_amount"]);
                            Obj.TaxType = Convert.ToString(dt.Rows[i]["invoicetax_taxtype"]);
                            Obj.TaxGl = Convert.ToString(dt.Rows[i]["invoicetax_gl_no"]);
                            Obj.HsnName = Convert.ToString(dt.Rows[i]["hsn"]);
                            Obj.TaxRate = Convert.ToDecimal(dt.Rows[i]["invoicetax_rate"]);
                            Obj.Expenseid = Convert.ToInt64(dt.Rows[i]["debitline_gid"]);
                            Obj.InvTaxid = Convert.ToInt64(dt.Rows[i]["invoicetax_gid"]);
                        }
                        else if (data.GridType == "POHeader")
                        {
                            Obj.Poid = Convert.ToInt64(dt.Rows[i]["poheader_gid"]);
                            Obj.PoNo = Convert.ToString(dt.Rows[i]["poheader_pono"]);
                            Obj.PoDesc = Convert.ToString(dt.Rows[i]["poheader_vendor_note"]);
                            Obj.Pototalamt = Convert.ToDecimal(dt.Rows[i]["Po_Totalamt"]);
                        }
                        else if (data.GridType == "PODetails")
                        {
                            Obj.Poid = Convert.ToInt64(dt.Rows[i]["poheader_gid"]);
                            Obj.Podetid = Convert.ToInt64(dt.Rows[i]["podetails_gid"]);
                            Obj.PoNo = Convert.ToString(dt.Rows[i]["poheader_pono"]);
                            Obj.PoDesc = Convert.ToString(dt.Rows[i]["poheader_vendor_note"]);
                            Obj.Pototalamt = Convert.ToDecimal(dt.Rows[i]["Po_Totalamt"]);
                            Obj.ProdServName = Convert.ToString(dt.Rows[i]["ProdService"]);
                            Obj.PodetOrdQty = Convert.ToDecimal(dt.Rows[i]["OrderedQty"]);
                            Obj.PodetUnit = Convert.ToDecimal(dt.Rows[i]["podetails_unitprice"]);
                            Obj.GrndetRecQty = Convert.ToDecimal(dt.Rows[i]["ReceivedQty"]);
                            Obj.GrnHeadid = Convert.ToInt64(dt.Rows[i]["grninwrdheader_gid"]);
                            Obj.Grndetid = Convert.ToInt64(dt.Rows[i]["grninwrddet_gid"]);
                            Obj.PodetUnitprice = Convert.ToInt64(dt.Rows[i]["unitprice"]);
                            Obj.GrnRefno = Convert.ToString(dt.Rows[i]["grninwrdheader_refno"]);
                            Obj.BalanceQty = Convert.ToDecimal(dt.Rows[i]["balanceqty"]);
                        }
                        else if (data.GridType == "POSummary")
                        {
                            Obj.Poid = Convert.ToInt64(dt.Rows[i]["poheader_gid"]);
                            Obj.Podetid = Convert.ToInt64(dt.Rows[i]["podetails_gid"]);
                            Obj.PoNo = Convert.ToString(dt.Rows[i]["poheader_pono"]);
                            Obj.PoDesc = Convert.ToString(dt.Rows[i]["poheader_vendor_note"]);
                            Obj.Pototalamt = Convert.ToDecimal(dt.Rows[i]["Po_Totalamt"]);
                            Obj.ProdServName = Convert.ToString(dt.Rows[i]["ProdService"]);
                            Obj.PodetOrdQty = Convert.ToDecimal(dt.Rows[i]["OrderedQty"]);
                            Obj.PodetUnit = Convert.ToDecimal(dt.Rows[i]["podetails_unitprice"]);
                            Obj.GrndetRecQty = Convert.ToDecimal(dt.Rows[i]["ReceivedQty"]);
                            Obj.GrnHeadid = Convert.ToInt64(dt.Rows[i]["grninwrdheader_gid"]);
                            Obj.Grndetid = Convert.ToInt64(dt.Rows[i]["grninwrddet_gid"]);
                            Obj.PodetUnitprice = Convert.ToInt64(dt.Rows[i]["unitprice"]);
                            Obj.GrnRefno = Convert.ToString(dt.Rows[i]["grninwrdheader_refno"]);
                            Obj.MappedQty = Convert.ToDecimal(dt.Rows[i]["invoicepoitem_qty"]);
                            Obj.SubTaxid = Convert.ToInt64(dt.Rows[i]["SubTaxgid"]);
                            Obj.SubTaxName = Convert.ToString(dt.Rows[i]["Subtaxname"]);
                            Obj.TaxRate = Convert.ToDecimal(dt.Rows[i]["pogst_Taxrate"]);
                            Obj.TaxAmt = Convert.ToDecimal(dt.Rows[i]["pogst_TaxAmt"]);
                            Obj.TaxableAmt = Convert.ToDecimal(dt.Rows[i]["pogst_TaxableAmt"]);
                            Obj.PodetTotamt = Convert.ToDecimal(dt.Rows[i]["pogst_GstNetAmt"]);
                            Obj.Invoicepoitemid = Convert.ToInt64(dt.Rows[i]["invoicepoitem_gid"]);
                        }

                        lstInv.Add(Obj);
                    }
                }
                //else
                //{
                //    dt = ds.Tables[1];
                //}

                return lstInv;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SupplierInvoice_Model GetBatch(SupplierInvoice_Model data)
        {
            try
            {
                SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                ds = dataObj.GetInvoices(data);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Obj.BatchGid = Convert.ToInt64(dt.Rows[i]["Batch_Gid"]);
                        // Obj.Raiserid = Convert.ToInt64(dt.Rows[i]["Login_gid"]);
                        Obj.Type_Gid = Convert.ToInt64(dt.Rows[i]["ecf_doctype_gid"]);
                        Obj.SubType_Gid = Convert.ToInt64(dt.Rows[i]["ecf_docsubtype_gid"]);
                        Obj.Raiserid = Convert.ToInt64(dt.Rows[i]["ecf_raiser"]);
                        Obj.RaiserName = Convert.ToString(dt.Rows[i]["raiser_name"]);
                        Obj.BatchNo = Convert.ToString(dt.Rows[i]["batch_no"]);
                        Obj.BatchAmt = Convert.ToDouble(dt.Rows[i]["batch_totalamt"]);
                    }
                }

                return Obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Get GSTN Drop Down
        public List<SupplierInvoice_Model> GetGstnLoc(SupplierInvoice_Model data)
        {
            try
            {
                ds = dataObj.GetGstnDropdown(data);
                if (ds.Tables.Count > 1)
                {
                    dt = ds.Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        if (data._Action == "ProviderLoc")
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                                Obj.ProvLocid = Convert.ToInt64(dt.Rows[i]["ProvLocid"]);
                                Obj.ProviderLoc = Convert.ToString(dt.Rows[i]["ProviderLoc"]);
                                objLst.Add(Obj);
                            }
                        }

                        else if (data._Action == "ReceiverLoc")
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                                Obj.RecvLocid = Convert.ToInt64(dt.Rows[i]["RecvLocid"]);
                                Obj.ReceiverLoc = Convert.ToString(dt.Rows[i]["ReceiverLoc"]);
                                objLst.Add(Obj);
                            }
                        }

                        else if (data._Action == "ProviderGSTN")
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                                Obj.ProviderGstn = Convert.ToString(dt.Rows[i]["ProviderGstn"]);
                                objLst.Add(Obj);
                            }
                        }

                        else if (data._Action == "ReceiverGSTN")
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                                Obj.ReceiverGstn = Convert.ToString(dt.Rows[i]["ReceiverGstn"]);
                                objLst.Add(Obj);
                            }
                        }
                        else if (data._Action == "Currency")
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                                Obj.Currencyid = Convert.ToInt64(dt.Rows[i]["Currencyid"]);
                                Obj.CurrencyName = Convert.ToString(dt.Rows[i]["CurrencyName"]);
                                objLst.Add(Obj);
                            }
                        }

                    }

                }
                else
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.Message = Convert.ToString(dt.Rows[i]["Message"]);
                        Obj.Clear = Convert.ToString(dt.Rows[i]["Clear"]);
                        objLst.Add(Obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objLst;
        }
        #endregion

        #region DropDown Bindings For Expense Grid
        public List<SupplierInvoice_Model> GetExpDropDown(SupplierInvoice_Model data)
        {
            try
            {

                if (data._Action == "ExpRefno")
                {
                    ds = dataObj.GetExpDropDown(data);
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.Refid = Convert.ToInt64(dt.Rows[i]["Ecfid"]);
                        Obj.RefrenceNo = Convert.ToString(dt.Rows[i]["EcfNo"]);
                        objLst.Add(Obj);
                    }
                }
                else if (data._Action == "ExpNat")
                {
                    objQCD.Master_Code = cmnParams.ExpNatCode;
                    dt = cmnObj.DropDownValues(objQCD);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.ExpNatureid = Convert.ToInt64(dt.Rows[i][0]);
                        Obj.ExpNaturename = Convert.ToString(dt.Rows[i][2]);
                        objLst.Add(Obj);
                    }
                }
                else if (data._Action == "ExpCat")
                {
                    objQCD.Master_Code = cmnParams.ExpCatCode;
                    objQCD.Dependent_Gid = data.ExpNatureid;
                    dt = cmnObj.DropDownValues(objQCD);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.ExpCatid = Convert.ToInt64(dt.Rows[i][0]);
                        Obj.ExpCatname = Convert.ToString(dt.Rows[i][2]);
                        objLst.Add(Obj);
                    }
                }
                else if (data._Action == "ExpSubcat")
                {
                    ds = dataObj.GetExpDropDown(data);
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.ExpSubcatid = Convert.ToInt64(dt.Rows[i]["ExpSubcatid"]);
                        Obj.ExpSubcatname = Convert.ToString(dt.Rows[i]["ExpSubcatname"]);
                        objLst.Add(Obj);
                    }
                }
                else if (data._Action == "ExpGl")
                {
                    data.MasterCode = cmnParams.GLCode;
                    ds = dataObj.GetExpDropDown(data);
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.Glid = Convert.ToInt64(dt.Rows[i]["Glid"]);
                        Obj.GlName = Convert.ToString(dt.Rows[i]["GlName"]);
                        objLst.Add(Obj);
                    }
                }
                else if (data._Action == "AssetGl")
                {
                    data.MasterCode = cmnParams.GLCode;
                    ds = dataObj.GetExpDropDown(data);
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.Glid = Convert.ToInt64(dt.Rows[i]["Glid"]);
                        Obj.GlName = Convert.ToString(dt.Rows[i]["GlName"]);
                        objLst.Add(Obj);
                    }
                }
                else if (data._Action == "ExpHsn")
                {
                    data.MasterCode = cmnParams.HSNCode;
                    ds = dataObj.GetExpDropDown(data);
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.HsnId = Convert.ToInt64(dt.Rows[i]["HsnId"]);
                        Obj.HsnName = Convert.ToString(dt.Rows[i]["HsnName"]);
                        objLst.Add(Obj);
                    }
                }
                else if (data._Action == "AssetCat")
                {
                    objQCD.Master_Code = cmnParams.AssetCatCode;
                    dt = cmnObj.DropDownValues(objQCD);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.AssetCatid = Convert.ToInt64(dt.Rows[i][0]);
                        Obj.AssetCatName = Convert.ToString(dt.Rows[i][2]);
                        objLst.Add(Obj);
                    }
                }
                else if (data._Action == "AssetSubcat")
                {
                    ds = dataObj.GetExpDropDown(data);
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.AssetSubcatid = Convert.ToInt64(dt.Rows[i]["AssetSubcatid"]);
                        Obj.AssetSubcatname = Convert.ToString(dt.Rows[i]["AssetSubcatname"]);
                        objLst.Add(Obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objLst;
        }
        #endregion

        #region DropDown Bindings For Payment Grid
        public List<SupplierInvoice_Model> GetCrDropDown(SupplierInvoice_Model data)
        {
            try
            {
                if (data._Action == "CreditRefno")
                {
                    ds = dataObj.GetCrDropDown(data);
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.Refid = Convert.ToInt64(dt.Rows[i]["Ecfid"]);
                        Obj.RefrenceNo = Convert.ToString(dt.Rows[i]["EcfNo"]);
                        objLst.Add(Obj);
                    }
                }

                if (data._Action == "PayMode")
                {
                    ds = dataObj.GetCrDropDown(data);
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.Paymodeid = Convert.ToInt64(dt.Rows[i]["Paymodeid"]);
                        Obj.Paymode = Convert.ToString(dt.Rows[i]["Paymodename"]);
                        objLst.Add(Obj);
                    }
                }

                if (data._Action == "PayBank")
                {
                    ds = dataObj.GetCrDropDown(data);
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.Bankid = Convert.ToInt64(dt.Rows[i]["bankid"]);
                        Obj.BankName = Convert.ToString(dt.Rows[i]["bankname"]);
                        objLst.Add(Obj);
                    }
                }

                if (data._Action == "SusGL")
                {
                    ds = dataObj.GetCrDropDown(data);
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.Glid = Convert.ToInt64(dt.Rows[i]["Glid"]);
                        Obj.GlName = Convert.ToString(dt.Rows[i]["Glname"]);
                        objLst.Add(Obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objLst;
        }
        #endregion

        #region Supplier Invoice Submit
        public DataTable ApproveInvoice(SupplierInvoice_Model data)
        {
            try
            {
                dt = dataObj.ApproveInvoice(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable SubmitInvoice(SupplierInvoice_Model data)
        {
            try
            {
                dt = dataObj.SubmitInvoice(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion


        #region DropDown Bindings For COA Grid

        public List<SupplierInvoice_Model> GetCOADropDown(SupplierInvoice_Model data)
        {
            try
            {
                ds = dataObj.GetCOADropDown(data);
                dt = ds.Tables[0];
                if (data._Action == "FC")
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.FC_gid = Convert.ToInt64(dt.Rows[i][0]);
                        Obj.FC_Code = Convert.ToString(dt.Rows[i][1]);
                        objLst.Add(Obj);
                    }
                }
                else if (data._Action == "CC")
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.CC_gid = Convert.ToInt64(dt.Rows[i][0]);
                        Obj.CC_Code = Convert.ToString(dt.Rows[i][1]);
                        objLst.Add(Obj);
                    }
                }
                else if (data._Action == "Product")
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.Product_gid = Convert.ToInt64(dt.Rows[i][0]);
                        Obj.Product_Code = Convert.ToString(dt.Rows[i][1]);
                        objLst.Add(Obj);
                    }
                }
                else if (data._Action == "OU")
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.OU_gid = Convert.ToInt64(dt.Rows[i][0]);
                        Obj.OU_Code = Convert.ToString(dt.Rows[i][1]);
                        objLst.Add(Obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objLst;
        }
        public List<SupplierInvoice_Model> GetReferenceNoDropDown(SupplierInvoice_Model data)
        {
            try
            {
                ds = dataObj.GetReferenceNoDropDown(data);
                dt = ds.Tables[0];

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (data._Action == "ECF")
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.ecf_gid = Convert.ToInt64(dt.Rows[i][0]);
                        Obj.ecf_code = Convert.ToString(dt.Rows[i][1]);
                        objLst.Add(Obj);
                    }
                    else if (data._Action == "GL")
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.GL_Gid = Convert.ToInt64(dt.Rows[i][0]);
                        Obj.GL_No = Convert.ToString(dt.Rows[i][1]);
                        objLst.Add(Obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objLst;
        }
        public List<SupplierInvoice_Model> GetCOA(SupplierInvoice_Model data)
        {
            List<SupplierInvoice_Model> lstInv = new List<SupplierInvoice_Model>();
            try
            {
                ds = dataObj.GetCOA(data);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                        Obj.ecf_gid = Convert.ToInt64(dt.Rows[i]["ecf_gid"]);
                        Obj.Expenseid = Convert.ToInt64(dt.Rows[i]["Expenseid"]);
                        Obj.COAid = Convert.ToInt64(dt.Rows[i]["COAid"]);
                        Obj.GlName = Convert.ToString(dt.Rows[i]["GlName"]);
                        Obj.FC_gid = Convert.ToInt64(dt.Rows[i]["fc_gid"]);
                        Obj.FC_Code = Convert.ToString(dt.Rows[i]["FC_Code"]);
                        Obj.CC_gid = Convert.ToInt64(dt.Rows[i]["cc_gid"]);
                        Obj.CC_Code = Convert.ToString(dt.Rows[i]["CC_Code"]);
                        Obj.Product_gid = Convert.ToInt64(dt.Rows[i]["product_gid"]);
                        Obj.Product_Code = Convert.ToString(dt.Rows[i]["Product_Code"]);
                        Obj.OU_gid = Convert.ToInt64(dt.Rows[i]["ou_gid"]);
                        Obj.OU_Code = Convert.ToString(dt.Rows[i]["OU_Code"]);
                        Obj.COAAmount = Convert.ToDecimal(dt.Rows[i]["COA_Amount"]);
                        lstInv.Add(Obj);
                    }
                }

                return lstInv;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable SaveCOA(SupplierInvoice_Model data)
        {
            try
            {
                string[] FCCode = data.FC_Code.Split('-');
                data.FC_Code = FCCode[0].ToString();
                string[] CCCode = data.CC_Code.Split('-');
                data.CC_Code = CCCode[0].ToString();
                string[] ProductCode = data.Product_Code.Split('-');
                data.Product_Code = ProductCode[0].ToString();
                string[] OUCode = data.OU_Code.Split('-');
                data.OU_Code = OUCode[0].ToString();
                dt = dataObj.SaveCOA(data);

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public DataTable Remove_COA(Int64 COA_Gid)
        {
            try
            {
                return dataObj.Remove_COA(COA_Gid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public List<SupplierInvoice_Model> GetPaymodeDetails(SupplierInvoice_Model _data)
        {
            try
            {
                dt = dataObj.GetPaymodeDetails(_data);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SupplierInvoice_Model Obj = new SupplierInvoice_Model();
                    if (_data._Action == "PPX")
                    {
                        Obj.Refid = Convert.ToInt64(dt.Rows[i]["ecf_gid"]);
                        Obj.RefrenceNo = Convert.ToString(dt.Rows[i]["ecf_no"]);
                        Obj.GlName = Convert.ToString(dt.Rows[i]["advancetype_gl_no"]);
                        Obj.ArfDesc = Convert.ToString(dt.Rows[i]["ecfarf_desc"]);
                        Obj.LiqudatonDate = Convert.ToDateTime(dt.Rows[i]["ecfarf_liq_date"]);
                        Obj.EcfarfAmt = Convert.ToDecimal(dt.Rows[i]["ecfarf_amount"]);
                        Obj.ExceptionAmt = Convert.ToInt64(dt.Rows[i]["ecfarf_exception"]);
                        Obj.Paymodeid = Convert.ToInt64(dt.Rows[i]["paymodeid"]);
                        Obj.Supplierid = Convert.ToInt64(dt.Rows[i]["supplierheader_gid"]);
                    }
                    else if (_data._Action == "SupplierPaymode")
                    {
                        Obj.Beneficiary = Convert.ToString(dt.Rows[i]["payment_beneficiaryname"]);
                        Obj.AccountNo = Convert.ToString(dt.Rows[i]["payment_accountno"]);
                        Obj.IfscCode = Convert.ToString(dt.Rows[i]["ifccode"]);
                        Obj.BranchName = Convert.ToString(dt.Rows[i]["branch"]);
                        Obj.BranchId = Convert.ToInt64(dt.Rows[i]["branchid"]);
                        Obj.BankName = Convert.ToString(dt.Rows[i]["bankname"]);
                        Obj.Supplierid = Convert.ToInt64(dt.Rows[i]["supplierheader_gid"]);
                        Obj.Paymodeid = Convert.ToInt64(dt.Rows[i]["paymodeid"]);
                    }

                    objLst.Add(Obj);
                }
                return objLst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region TDS
        public DataTable GetTaxDetails(SupplierInvoice_Model _data)
        {
            try
            {
                ds = dataObj.GetTaxDetails(_data);
                //if (ds.Tables.Count > 1)
                //{
                dt = ds.Tables[0];
                //}
                //else
                //{
                //    dt = ds.Tables[0];
                //}
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Upload Debitline
        public DataTable Save_UploadDebitline(SupplierInvoice_Model obj, Int64 Loginid, DataTable dt)
        {
            try
            {
                DataTable dtResult = new DataTable();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SupplierInvoice_Model ObjModel = new SupplierInvoice_Model();
                    ObjModel.BatchGid = obj.BatchGid;
                    ObjModel.RefrenceNo = dt.Rows[i]["Reference No"].ToString();
                    ObjModel.ExpNaturename = dt.Rows[i]["Nature of Expenses"].ToString();
                    ObjModel.ExpCatname = dt.Rows[i]["Main Category"].ToString();
                    ObjModel.ExpSubcatname = dt.Rows[i]["Sub Category"].ToString();
                    ObjModel.ExpDesc = dt.Rows[i]["Description"].ToString();
                    /* ObjModel.FC_Code = dt.Rows[i]["Function Code"].ToString();
                     ObjModel.CC_Code = dt.Rows[i]["Cost Code"].ToString();
                     ObjModel.Product_Code = dt.Rows[i]["Product Code"].ToString();
                     ObjModel.OU_Code = dt.Rows[i]["OU Code"].ToString();*/
                    ObjModel.ExpenseAmt = Convert.ToDecimal(dt.Rows[i]["Amount"]);
                    ObjModel.HsnName = dt.Rows[i]["hsncode"].ToString();
                    ObjModel.Loginid = Loginid;
                    dtResult = dataObj.Save_UploadDebitline(ObjModel);
                }
                return dtResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public DataTable Save_UploadInvoice(SupplierInvoice_Model obj, Int64 Loginid, DataTable dt)
        {
            try
            {
                Int64 BatchGid = 0;
                DataTable dtResult = new DataTable();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SupplierInvoice_Model ObjModel = new SupplierInvoice_Model();
                    if (dtResult.Rows.Count > 0)
                    {
                        if (dtResult.Rows[0]["Clear"].ToString().Equals("1"))
                        {
                            BatchGid = Convert.ToInt64(dtResult.Rows[0]["Batchid"]);
                        }
                    }
                    ObjModel.BatchGid = BatchGid;
                    ObjModel.Type_Gid = obj.Type_Gid;
                    ObjModel.SubType_Gid = obj.SubType_Gid;
                    ObjModel.Raiserid = obj.Raiserid;
                    ObjModel.InvoiceNo = dt.Rows[i]["Invoice No"].ToString();
                    ObjModel.SupplierCode = dt.Rows[i]["Supplier Code"].ToString();
                    //ObjModel.SupplierName = dt.Rows[i]["Supplier Name"].ToString();
                    ObjModel.InvoiceDate = Convert.ToString(dt.Rows[i]["Invoice Date"]);
                    ObjModel.ProviderLoc = dt.Rows[i]["Provider Location"].ToString();
                    ObjModel.ReceiverLoc = dt.Rows[i]["Receiver Location"].ToString();
                    ObjModel.InvoiceAmt = Convert.ToDecimal(dt.Rows[i]["Invoice Amount"]);
                    ObjModel.TaxStatus = dt.Rows[i]["Tax Type"].ToString();
                    ObjModel.InvDescription = dt.Rows[i]["Descrition"].ToString();
                    ObjModel.Loginid = Loginid;
                    dtResult = dataObj.Save_UploadInvoice(ObjModel);
                }
                return dtResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataTable Save_UploadCOA(SupplierInvoice_Model data, Int64 Loginid, DataTable dt)
        {
            try
            {
                DataTable dtresult = new DataTable();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SupplierInvoice_Model ObjModel = new SupplierInvoice_Model();

                    ObjModel.BatchGid = data.BatchGid;
                    ObjModel.Type_Gid = data.Type_Gid;
                    ObjModel.SubType_Gid = data.SubType_Gid;
                    ObjModel.RefrenceNo = dt.Rows[i]["Reference No"].ToString();
                    ObjModel.GL_No = dt.Rows[i]["GL Code"].ToString();
                    ObjModel.FC_Code = Convert.ToString(dt.Rows[i]["Function Code"]);
                    ObjModel.CC_Code = dt.Rows[i]["CC Code"].ToString();
                    ObjModel.Product_Code = dt.Rows[i]["Product Code"].ToString();
                    ObjModel.OU_Code = dt.Rows[i]["OU Code"].ToString();
                    ObjModel.COAAmount = Convert.ToDecimal(dt.Rows[i]["Amount"]);
                    ObjModel.Loginid = Loginid;
                    dtresult = dataObj.Save_UploadCOA(ObjModel);
                }
                return dtresult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region GetCurrencyRate
        public DataTable GetCurrencyRate(SupplierInvoice_Model _data)
        {
            try
            {
                ds = dataObj.GetGstnDropdown(_data);
                dt = ds.Tables[0];
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region PO Mapping
        public DataTable SetPODetails(SupplierInvoice_Model _data)
        {
            try
            {
                dt = dataObj.SetPODetails(_data);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable PoMappingDeletion(SupplierInvoice_Model _data)
        {
            try
            {
                _data._Action = "InvoicepoitemDeletion"; _data.Expenseid = _data.Invoicepoitemid;
                dt = dataObj.SaveExpense(_data);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
