﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using SharedData;
using DataAccessHandler.Models.EOW;
using DataAccessHandler.Models.IEMProduct;
using EOW.Data;

namespace EOW.Service
{
    public class Claim_Service
    {
        DropDown_Bindings ObjShared = new DropDown_Bindings();
        //SupplierInvoice_Model objSuplModel = new SupplierInvoice_Model();
        QCD_Model objModelQcd = new QCD_Model();
        Claim_Data objData = new Claim_Data();
        DataTable dt = new DataTable();

        #region Common methods for dropdown bindings.
        public List<Claim_Model> Get_DropDown_List(string Master, string Master_Code, Int64 Master_Gid, string EmpName)
        {
            List<Claim_Model> lst = new List<Claim_Model>();
            try
            {
                #region QCD Parametes.
                if (Master == "NOE" || Master == "CTG" || Master == "GL" || Master == "City")
                {
                    if (Master_Gid != 0)
                    {
                        objModelQcd.Master_Code = "";
                        objModelQcd.Parent_Gid = 0;
                        objModelQcd.Dependent_Gid = Master_Gid;

                    }
                    else
                    {
                        objModelQcd.Master_Code = Master_Code;
                        objModelQcd.Parent_Gid = 0;
                        objModelQcd.Dependent_Gid = 0;
                    }
                }
                #endregion

                #region Fetch QCD & Non-QCD Datas to Datatable.
                if (Master == "Employee" || Master == "ECF" || Master == "SCTG" || Master == "HSN" || Master == "Invoice" ||
                    Master == "Paymode" || Master == "PayECF" || Master == "BoardingType" || Master == "TravelType" || Master == "Paymode_EFT" ||
                    Master == "TravelClass" || Master == "Supplier" || Master == "ProviderLocation" || Master == "ReceiverLocation" ||
                    Master == "Type" || Master == "SubType" || Master == "DetailType" || Master == "ECFRaiser" || Master == "Employee_Search" ||
                    Master == "Employee_ByID" || Master == "All_Employee" || Master == "Raiser_ByID")
                {
                    dt = objData.Get_DropDown_List(Master, Master_Gid, EmpName);
                }
                else
                {
                    //dt = ObjShared.DropDownValues(objSuplModel);
                    dt = ObjShared.DropDownValues(objModelQcd);
                }
                #endregion

                #region Add datatable values to List.
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0].ToString() != "SQLEXCEPTION")
                    {
                        if (Master == "Type")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.HDR_Type_Gid = Convert.ToInt64(row["TypeGid"].ToString());
                                objModel.HDR_Type_Name = row["TypeName"].ToString();
                                lst.Add(objModel);
                            }
                        }
                        else if (Master == "SubType")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.HDR_SubType_Gid = Convert.ToInt64(row["SubType_Gid"].ToString());
                                objModel.HDR_SubType_Name = row["SubType_Name"].ToString();
                                lst.Add(objModel);
                            }
                        }
                        else if (Master == "ECFRaiser")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.CLM_Emp_Gid = Convert.ToInt64(row["employeeid"].ToString());
                                objModel.CLM_Emp_Code = row["employeename"].ToString();
                                lst.Add(objModel);
                            }
                        }
                        else if (Master == "Raiser_ByID")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.CLM_Emp_Gid = Convert.ToInt64(row["employee_gid"].ToString());
                                objModel.CLM_Emp_Code = row["employee_name"].ToString();
                                lst.Add(objModel);
                            }
                        }
                        else if (Master == "Employee" || Master == "Employee_ByID" || Master == "Employee_Search")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.CLM_Add_EmpGid = row["employee_gid"].ToString();
                                objModel.CLM_Add_EmpName = row["employee_name"].ToString();
                                lst.Add(objModel);
                            }
                        }
                       /* else if (Master == "Employee_Search")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.CLM_Add_EmpGid = row["employee_gid"].ToString();
                                objModel.CLM_Add_EmpName = row["employee_name"].ToString();
                                lst.Add(objModel);
                            }
                        }*/
                        else if (Master == "All_Employee")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.CLM_Add_EmpGid = row["employee_gid"].ToString();
                                objModel.CLM_Emp_Code = row["employee_code"].ToString();
                                objModel.CLM_Add_EmpName = row["employee_name"].ToString();
                                lst.Add(objModel);
                            }
                        }
                        else if (Master == "ECF")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.Exp_ecfgid = Convert.ToInt64(row["ecf_gid"].ToString());
                                objModel.Exp_ecfno = row["ecf_no"].ToString();
                                lst.Add(objModel);
                            }
                        }
                        else if (Master == "NOE")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.Exp_natureexpgid = Convert.ToInt64(row["masters_gid"].ToString());
                                objModel.Exp_natureexpname = row["masters_description"].ToString();
                                lst.Add(objModel);
                            }
                        }
                        else if (Master == "CTG")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.Exp_catggid = Convert.ToInt64(row["masters_gid"].ToString());
                                objModel.Exp_catgname = row["masters_description"].ToString();
                                lst.Add(objModel);
                            }
                        }
                        else if (Master == "SCTG")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.Exp_subcatggid = Convert.ToInt64(row["expsubcat_gid"].ToString());
                                objModel.Exp_subcatgname = row["expsubcat_name"].ToString();
                                lst.Add(objModel);
                            }
                        }
                        else if (Master == "HSN")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.Exp_hsngid = Convert.ToInt64(row["masters_gid"].ToString());
                                objModel.Exp_hsnname = row["masters_description"].ToString();
                                lst.Add(objModel);
                            }
                        }
                        else if (Master == "Invoice")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.Exp_invoiceno = row["invoice_no"].ToString();
                                lst.Add(objModel);
                            }
                        }
                        else if (Master == "Paymode" || Master == "Paymode_EFT")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.Pay_paymodemode = row["paymode_code"].ToString();
                                objModel.Pay_paymodename = row["paymode_name"].ToString();
                                lst.Add(objModel);
                            }
                        }
                        else if (Master == "GL")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.Pay_glcode = row["masters_code"].ToString();
                                objModel.Pay_glname = row["masters_description"].ToString();
                                lst.Add(objModel);
                            }
                        }
                        else if (Master == "PayECF")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.Pay_ecfgid = Convert.ToInt64(row["ecf_gid"].ToString());
                                objModel.Pay_ecfno = row["ecf_no"].ToString();
                                lst.Add(objModel);
                            }
                        }
                        else if (Master == "BoardingType" || Master == "TravelType")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.Det_typegid = Convert.ToInt64(row["transport_gid"].ToString());
                                objModel.Det_typename = row["transport_name"].ToString();
                                lst.Add(objModel);
                            }
                        }
                        else if (Master == "City")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.Det_placefrom = row["masters_description"].ToString();
                                objModel.Det_placeto = row["masters_description"].ToString();
                                lst.Add(objModel);
                            }
                        }
                        else if (Master == "TravelClass")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.Det_travelclassgid = Convert.ToInt64(row["transportclass_gid"].ToString());
                                objModel.Det_travelclassname = row["transportclass_name"].ToString();
                                lst.Add(objModel);
                            }
                        }
                        else if (Master == "Supplier")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.Tax_suppliergid = Convert.ToInt64(row["supplierheader_gid"].ToString());
                                objModel.Tax_suppliername = row["supplier"].ToString();
                                lst.Add(objModel);
                            }
                        }
                        else if (Master == "ProviderLocation")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.Tax_providerlocationgid = Convert.ToInt64(row["provider_id"].ToString());
                                objModel.Tax_providerlocationname = row["provider_name"].ToString();
                                lst.Add(objModel);
                            }
                        }
                        else if (Master == "ReceiverLocation")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.Tax_receiverlocationgid = Convert.ToInt64(row["receiver_id"].ToString());
                                objModel.Tax_receiverlocationname = row["receiver_name"].ToString();
                                lst.Add(objModel);
                            }
                        }
                        else if (Master == "DetailType")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.Det_gridtypegid = Convert.ToInt16(row["DetailGid"].ToString());
                                objModel.Det_gridtypename = row["DetailType"].ToString();
                                lst.Add(objModel);
                            }
                        }
                    }
                #endregion

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }

        public string Get_GL_Code(string Master, Int64 Master_Gid)
        {
            string Glcode = string.Empty;
            string EmpName = string.Empty;
            try
            {
                dt = objData.Get_DropDown_List(Master, Master_Gid, EmpName);
                if (dt.Rows.Count > 0)
                {
                    Glcode = dt.Rows[0][0].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Glcode;
        }
        #endregion

        #region CRUD operation Methods for Claim/ECF Grid.
        public List<Claim_Model> Read_Claim(string GridName, Int64 BatchGid, Int64 EcfGid)
        {
            List<Claim_Model> lst = new List<Claim_Model>();
            try
            {
                if (BatchGid > 0)
                {
                    dt = objData.Read_Claim(GridName, BatchGid, EcfGid);
                }
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0].ToString() != "SQLEXCEPTION")
                    {
                        if (GridName == "Claim")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.CLM_Ecf_Gid = Convert.ToInt64(row["ecf_gid"].ToString());
                                objModel.CLM_Ecf_No = row["ecf_no"].ToString();
                                if (row["ecf_date"].ToString() != "" && row["ecf_date"].ToString() != null)
                                {
                                    objModel.CLM_Claim_Date = Convert.ToDateTime(row["ecf_date"].ToString()).ToString("dd-MM-yyyy");
                                }
                                else
                                {
                                    objModel.CLM_Claim_Date = "";
                                }
                                objModel.CLM_Create_Mode = row["ecf_create_mode"].ToString();
                                objModel.CLM_Emp_Gid = Convert.ToInt64(row["ecf_raiser"].ToString());
                                objModel.CLM_Emp_Code = row["raiser_code"].ToString();
                                if (row["ecf_claim_month"].ToString() != "" && row["ecf_claim_month"].ToString() != null)
                                {
                                    objModel.CLM_Claim_Month = Convert.ToDateTime(row["ecf_claim_month"].ToString()).ToString("dd-MM-yyyy");
                                }
                                else
                                {
                                    objModel.CLM_Claim_Date = "";
                                }
                                objModel.CLM_Claim_Amount = Convert.ToDouble(row["ecf_amount"].ToString());
                                objModel.HDR_EmployeeIds = row["Empid"].ToString();
                                //objModel.CLM_Add_EmpGid = row["Empid"].ToString();
                                //objModel.CLM_Add_EmpName = row["person_name"].ToString();
                                objModel.CLM_Claim_Description = row["ecf_description"].ToString();
                                lst.Add(objModel);
                            }
                        }
                        else if (GridName == "Expense")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.Exp_gid = Convert.ToInt64(row["debitline_gid"].ToString());
                                objModel.Exp_ecfgid = Convert.ToInt64(row["invoice_ecf_gid"].ToString());
                                objModel.Exp_ecfno = row["ecf_no"].ToString();
                                objModel.Exp_invoiceno = row["invoice_no"].ToString();
                                objModel.Exp_natureexpgid = Convert.ToInt64(row["debitline_expnature_gid"].ToString());
                                objModel.Exp_natureexpname = row["expnature_name"].ToString();
                                objModel.Exp_catggid = Convert.ToInt64(row["debitline_expcat_gid"].ToString());
                                objModel.Exp_catgname = row["expcatg_name"].ToString();
                                objModel.Exp_subcatggid = Convert.ToInt64(row["debitline_expsubcat_gid"].ToString());
                                objModel.Exp_subcatgname = row["expsubcat_name"].ToString();
                                objModel.Exp_glcode = row["debitline_gl_no"].ToString();
                                objModel.Exp_hsngid = Convert.ToInt64(row["debitline_hsn_gid"].ToString());
                                objModel.Exp_hsnname = row["hsn_name"].ToString();
                                objModel.Exp_debitamount = Convert.ToDouble(row["debitline_amount"].ToString());
                                objModel.Exp_taxtype = row["invoice_gst_charged"].ToString();
                                objModel.Exp_taxtypetext = row["TaxType_Text"].ToString();
                                objModel.Det_gridtypegid = Convert.ToInt16(row["debitline_detail_typeid"].ToString());
                                objModel.Det_gridtypename = row["debitline_detail_typename"].ToString();
                                objModel.AttachmentCount = Convert.ToInt16(row["AttachmentCount"].ToString());
                                objModel.TaxCount = Convert.ToInt16(row["TaxCount"].ToString());
                                objModel.DetailCount = Convert.ToInt16(row["DetailCount"].ToString());
                                lst.Add(objModel);
                            }
                        }
                        else if (GridName == "Payment")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.Pay_gid = Convert.ToInt64(row["creditline_gid"].ToString());
                                objModel.Pay_ecfgid = Convert.ToInt64(row["creditline_ecf_gid"].ToString());
                                objModel.Pay_ecfno = row["ecf_no"].ToString();
                                objModel.Pay_paymodemode = row["creditline_pay_mode"].ToString();
                                objModel.Pay_paymodename = row["paymode_name"].ToString();
                                objModel.Pay_referenceno = row["creditline_ref_no"].ToString();
                                objModel.Pay_beneficiary = row["creditline_beneficiary"].ToString();
                                objModel.Pay_creditamount = Convert.ToDouble(row["creditline_amount"].ToString());
                                if (row["paymentdate"].ToString() != "" && row["paymentdate"].ToString() != null)
                                {
                                    objModel.Pay_paymentdate = Convert.ToDateTime(row["ecf_claim_month"].ToString()).ToString("dd-MM-yyyy");
                                }
                                else
                                {
                                    objModel.Pay_paymentdate = "";
                                }
                                objModel.Pay_paymentstatus = row["paymentstatus"].ToString();
                                objModel.Pay_glcode = row["creditline_gl_no"].ToString();
                                objModel.Pay_glname = row["creditline_gl_name"].ToString();
                                objModel.HDR_Batch_Gid = Convert.ToInt64(row["ecf_gid"].ToString());
                                lst.Add(objModel);
                            }
                        }
                        else if (GridName == "Remarks")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.Rem_gid = Convert.ToInt64(row["ecf_gid"].ToString());
                                objModel.Rem_ecfno = row["ecf_no"].ToString();
                                objModel.Rem_status = row["ecfstatus_name"].ToString();
                                objModel.Rem_remarks = row["ecf_remark"].ToString();
                                lst.Add(objModel);
                            }
                        }
                        else if (GridName == "BoardingLodging")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.Det_gid = Convert.ToInt64(row["ecftravel_gid"].ToString());
                                objModel.Det_ecfgid = Convert.ToInt64(row["ecftravel_ecf_gid"].ToString());
                                objModel.Det_debilnegid = Convert.ToInt64(row["ecftravel_ecfdebitline_gid"].ToString());
                                objModel.Det_type = row["ecftravel_type"].ToString();
                                objModel.Det_typegid = Convert.ToInt64(row["ecftravel_transport_gid"].ToString());
                                objModel.Det_typename = row["transport_name"].ToString();
                                objModel.Det_placefrom = row["ecftravel_city_from"].ToString();
                                if (row["ecftravel_date_from"].ToString() != "" && row["ecftravel_date_from"].ToString() != null)
                                {
                                    objModel.Det_datefrom = Convert.ToDateTime(row["ecftravel_date_from"].ToString()).ToString("dd-MM-yyyy");
                                }
                                else
                                {
                                    objModel.Det_datefrom = "";
                                }
                                if (row["ecftravel_date_to"].ToString() != "" && row["ecftravel_date_to"].ToString() != null)
                                {
                                    objModel.Det_dateto = Convert.ToDateTime(row["ecftravel_date_to"].ToString()).ToString("dd-MM-yyyy");
                                }
                                else
                                {
                                    objModel.Det_dateto = "";
                                }
                                objModel.Det_amount = Convert.ToDouble(row["ecftravel_amount"].ToString());
                                objModel.Det_hotel = row["ecftravel_hotel"].ToString();
                                lst.Add(objModel);
                            }
                        }
                        else if (GridName == "Travel")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.Det_gid = Convert.ToInt64(row["ecftravel_gid"].ToString());
                                objModel.Det_ecfgid = Convert.ToInt64(row["ecftravel_ecf_gid"].ToString());
                                objModel.Det_debilnegid = Convert.ToInt64(row["ecftravel_ecfdebitline_gid"].ToString());
                                objModel.Det_type = row["ecftravel_type"].ToString();
                                objModel.Det_typegid = Convert.ToInt64(row["ecftravel_transport_gid"].ToString());
                                objModel.Det_typename = row["transport_name"].ToString();
                                objModel.Det_travelclassgid = Convert.ToInt64(row["ecftravel_transportclass_gid"].ToString());
                                objModel.Det_travelclassname = row["transportclass_name"].ToString();
                                objModel.Det_placefrom = row["ecftravel_city_from"].ToString();
                                objModel.Det_placeto = row["ecftravel_city_to"].ToString();
                                if (row["ecftravel_date_from"].ToString() != "" && row["ecftravel_date_from"].ToString() != null)
                                {
                                    objModel.Det_datefrom = Convert.ToDateTime(row["ecftravel_date_from"].ToString()).ToString("dd-MM-yyyy");
                                }
                                else
                                {
                                    objModel.Det_datefrom = "";
                                }
                                objModel.Det_amount = Convert.ToDouble(row["ecftravel_amount"].ToString());
                                lst.Add(objModel);
                            }
                        }
                        else if (GridName == "Conveyance")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.Det_gid = Convert.ToInt64(row["ecftravel_gid"].ToString());
                                objModel.Det_ecfgid = Convert.ToInt64(row["ecftravel_ecf_gid"].ToString());
                                objModel.Det_debilnegid = Convert.ToInt64(row["ecftravel_ecfdebitline_gid"].ToString());
                                objModel.Det_type = row["ecftravel_type"].ToString();
                                objModel.Det_typegid = Convert.ToInt64(row["ecftravel_transport_gid"].ToString());
                                objModel.Det_typename = row["transport_name"].ToString();
                                objModel.Det_placefrom = row["ecftravel_city_from"].ToString();
                                objModel.Det_placeto = row["ecftravel_city_to"].ToString();
                                if (row["ecftravel_date_from"].ToString() != "" && row["ecftravel_date_from"].ToString() != null)
                                {
                                    objModel.Det_datefrom = Convert.ToDateTime(row["ecftravel_date_from"].ToString()).ToString("dd-MM-yyyy");
                                }
                                else
                                {
                                    objModel.Det_datefrom = "";
                                }
                                objModel.Det_km = Convert.ToDouble(row["ecftravel_distance"].ToString());
                                objModel.Det_rate = Convert.ToDouble(row["ecftravel_rate"].ToString());
                                objModel.Det_amount = Convert.ToDouble(row["ecftravel_amount"].ToString());
                                lst.Add(objModel);
                            }
                        }
                        else if (GridName == "PPXADJUSTMENT")
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                Claim_Model objModel = new Claim_Model();
                                objModel.ppx_ecfgid = Convert.ToInt64(row["ecf_gid"].ToString());
                                objModel.ppx_ecfno = row["ecf_no"].ToString();
                                objModel.ppx_beneficiary = row["beneficiary"].ToString();
                                objModel.ppx_beneficiaryaccount = row["beneficiaryaccount"].ToString();
                                objModel.ppx_ecfarfgid = Convert.ToInt64(row["ecfarf_gid"].ToString());
                                objModel.ppx_arfamount = Convert.ToDecimal(row["ecfarf_amount"].ToString());
                                objModel.ppx_arfexception = Convert.ToDecimal(row["ecfarf_exception"].ToString());
                                objModel.ppx_arfglno = row["ecfarf_dr_gl_no"].ToString();
                                lst.Add(objModel);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }

        public DataTable Save_Claim(Claim_Model objModel)
        {
            try
            {
                return objData.Save_Claim(objModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Update_Claim(Claim_Model ObjModel)
        {
            try
            {
                return objData.Update_Claim(ObjModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Delete_ClaimDetails(Claim_Model ObjModel)
        {
            try
            {
                return objData.Delete_ClaimDetails(ObjModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region CRUD Methods for Expense Grid.
        public DataTable Save_Expense(Claim_Model objModel)
        {
            try
            {
                return objData.Save_Expense(objModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Update_Expense(Claim_Model objModel)
        {
            try
            {
                return objData.Update_Expense(objModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Update method for payment grid.
        public DataTable Update_Payment(Claim_Model objModel)
        {
            try
            {
                return objData.Update_Payment(objModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Update Method for Remarks grid.
        public DataTable Update_Remarks(Claim_Model objModel)
        {
            try
            {
                return objData.Update_Remarks(objModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Save/Update Method for Travel/Boarding/Conveyance.
        public DataTable Update_TravelBoardingConveyance_Details(Claim_Model objModel)
        {
            try
            {
                return objData.Update_TravelBoardingConveyance_Details(objModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region method for get/updating tax details.
        public DataTable Update_TaxDetails(Claim_Model objModel)
        {
            try
            {
                return objData.Update_TaxDetails(objModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable Get_TaxDetails(Claim_Model objModel)
        {
            try
            {
                return objData.Get_TaxDetails(objModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Claim_Model> Read_TaxDetails(Claim_Model objModel)
        {
            List<Claim_Model> lst = new List<Claim_Model>();
            try
            {
                dt = objData.Get_TaxDetails(objModel);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        Claim_Model _objModel = new Claim_Model();
                        _objModel.Taxable_debitlinegid = Convert.ToInt64(row["debitline_gid"].ToString());
                        _objModel.Taxable_glcodedesc = row["invoicetax_gl_no"].ToString();
                        _objModel.Taxable_hsncode = row["hsncode"].ToString();
                        _objModel.Taxable_hsndesc = row["hsndesc"].ToString();
                        _objModel.Taxable_subtype = row["invoicetax_taxtype"].ToString();
                        _objModel.Taxable_amount = Convert.ToDouble(row["invoicetax_taxable_amount"].ToString());
                        _objModel.Taxable_rate = Convert.ToDouble(row["invoicetax_rate"].ToString());
                        _objModel.Taxable_gsttaxamount = Convert.ToDouble(row["invoicetax_amount"].ToString());
                        lst.Add(_objModel);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }
        #endregion

        #region Claim action Draft/Submit.
        public DataTable Claim_DraftSubmit(Claim_Model objModel)
        {
            try
            {
                return objData.Claim_DraftSubmit(objModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Get Batch Details.
        public Claim_Model Get_BatchDetails(Int64 Batchgid, Int64 Ecfgid)
        {
            Claim_Model _obj = new Claim_Model();
            try
            {
                dt = objData.Get_BatchDetails(Batchgid, Ecfgid);
                if (dt.Rows.Count > 0)
                {
                    _obj.HDR_Batch_Gid = Convert.ToInt64(dt.Rows[0]["batch_gid"].ToString());
                    _obj.HDR_Batch_No = dt.Rows[0]["batch_no"].ToString();
                    _obj.HDR_Batch_Amount = Convert.ToDecimal(dt.Rows[0]["batch_totalamt"].ToString());
                    _obj.HDR_Type_Gid = Convert.ToInt64(dt.Rows[0]["ecf_doctype_gid"].ToString());
                    _obj.HDR_SubType_Gid = Convert.ToInt64(dt.Rows[0]["ecf_docsubtype_gid"].ToString());
                    _obj.HDR_Raiser_Gid = Convert.ToInt64(dt.Rows[0]["raiser_gid"].ToString());
                    _obj.HDR_Raiser = dt.Rows[0]["raiser"].ToString();
                    if (Ecfgid != 0)
                    {
                        _obj.HDR_EcfGId = Convert.ToInt64(dt.Rows[0]["ecf_gid"].ToString());
                    }
                    else
                    {
                        _obj.HDR_EcfGId = Ecfgid;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _obj;
        }
        #endregion

        #region Claim Bulk Upload
        public DataTable Save_Claim_BulkECF(SupplierInvoice_Model obj, Int64 Loginid, DataTable dt)
        {
            try
            {
                Int64 BatchGid = 0;
                DataTable dtResult = new DataTable();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Claim_Model ObjModel = new Claim_Model();
                    if (dtResult.Rows.Count > 0)
                    {
                        if (dtResult.Rows[0]["Clear"].ToString().Equals("1"))
                        {
                            BatchGid = Convert.ToInt64(dtResult.Rows[0]["batch_gid"]);
                        }
                    }
                    ObjModel.HDR_Batch_Gid = BatchGid;
                    ObjModel.HDR_Type_Gid = obj.Type_Gid;
                    ObjModel.HDR_SubType_Gid = obj.SubType_Gid;
                    ObjModel.HDR_Raiser_Code = dt.Rows[i]["Raiser Code"].ToString();
                    ObjModel.CLM_Create_Mode = obj.CLM_Create_Mode;
                    ObjModel.CLM_Emp_Gid = Loginid;
                    ObjModel.CLM_Claim_Date = dt.Rows[i]["Claim Date"].ToString();
                    ObjModel.CLM_Claim_Amount = Convert.ToDouble(dt.Rows[i]["Claim Amount"]);
                    ObjModel.CLM_Claim_Description = dt.Rows[i]["Claim Description"].ToString();
                    ObjModel.Logged_UserGid = Loginid;
                    if (obj.Claimtype.ToString().Equals("Travel"))
                    {
                        ObjModel.HDR_Employee_Codes = dt.Rows[i]["Additional Employee Codes"].ToString();
                    }
                    else
                    {
                        ObjModel.HDR_Employee_Codes = "0";
                    }
                    dtResult = objData.Save_Claim_BulkECF(ObjModel);
                }
                return dtResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public DataTable Save_UploadLocalConveyance(SupplierInvoice_Model data, Int64 Loginid, DataTable dt)
        {
            try
            {
                DataTable dtresult = new DataTable();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Claim_Model ObjModel = new Claim_Model();

                    if (dtresult.Rows.Count > 0)
                    {
                        ObjModel.Exp_gid = Convert.ToInt64(dtresult.Rows[0]["debitline_gid"]);
                    }
                    ObjModel.HDR_Batch_Gid = data.BatchGid;
                    ObjModel.HDR_Type_Gid = data.Type_Gid;
                    ObjModel.HDR_SubType_Gid = data.SubType_Gid;
                    ObjModel.HDR_Raiser_Code = dt.Rows[i]["Employee Code"].ToString();
                    ObjModel.CLM_Ecf_No = dt.Rows[i]["Reference No"].ToString();
                    ObjModel.CLM_Create_Mode = data.CLM_Create_Mode;
                    ObjModel.Det_type = Convert.ToString(dt.Rows[i]["Transport Type"]);
                    ObjModel.Det_datefrom = dt.Rows[i]["Date"].ToString();
                    ObjModel.Det_placefrom = dt.Rows[i]["Location From"].ToString();
                    ObjModel.Det_placeto = dt.Rows[i]["Location To"].ToString();
                    ObjModel.Det_km = Convert.ToDouble(dt.Rows[i]["Km"]);
                    ObjModel.Det_rate = Convert.ToDouble(dt.Rows[i]["Rate"]);
                    ObjModel.Det_amount = Convert.ToDouble(dt.Rows[i]["Amount"]);
                    ObjModel.Logged_UserGid = Loginid;

                    dtresult = objData.Save_Claim_BulkLocalConveyance(ObjModel);
                }
                return dtresult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataTable Save_Bulk_Expense(SupplierInvoice_Model data, Int64 loginid, DataTable dt)
        {
            try
            {
                DataTable dtresult = new DataTable();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Claim_Model ObjModel = new Claim_Model();
                    //ObjModel.BatchGid = obj.BatchGid;
                    ObjModel.CLM_Ecf_No = dt.Rows[i]["Reference No"].ToString();
                    ObjModel.Exp_natureexpname = dt.Rows[i]["Nature of Expenses"].ToString();
                    ObjModel.Exp_catgname = dt.Rows[i]["Main Category"].ToString();
                    ObjModel.Exp_subcatgname = dt.Rows[i]["Sub Category"].ToString();
                    ObjModel.Exp_taxtype = dt.Rows[i]["Tax Type"].ToString();
                    ObjModel.Exp_debitamount = Convert.ToDouble(dt.Rows[i]["Amount"]);
                    ObjModel.Exp_hsnname = dt.Rows[i]["hsncode"].ToString();
                    ObjModel.Logged_UserGid = loginid;
                    dtresult = objData.Save_Bulk_Expense(ObjModel);
                }
                return dtresult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        public List<APCheckerEntities> Get_APCheckList(Int64 SubType_Gid)
        {
            List<APCheckerEntities> lstAP = new List<APCheckerEntities>();
            try
            {
                string EmpName = "";
                dt = objData.Get_DropDown_List("APCheckList", SubType_Gid, EmpName);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        APCheckerEntities objAP = new APCheckerEntities();
                        objAP.CheckList_Gid = Convert.ToInt64(row["checklist_gid"].ToString());
                        objAP.CheckList_Name = row["checklist_item"].ToString();
                        objAP.CheckList_Reason = row["checklist_reason"].ToString();
                        lstAP.Add(objAP);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstAP;
        }

        public string Update_Checklist(APCheckerEntities _objapmodel)
        {
            string result = string.Empty;
            try
            {
                dt = objData.Update_Checklist(_objapmodel);
                result = dt.Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public DataTable APChecklist_Submit(APCheckerEntities APobjModel)
        {
            try
            {
                return objData.APChecklist_Submit(APobjModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet Get_Selected_Checklists(Claim_Model objModel)
        {
            try
            {
                return objData.Get_Selected_Checklists(objModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
