﻿using DataAccessHandler.Models.EOW;
using EOW.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EOW.Service
{
    public class TransactionCancellation_Service
    {

        TransactionCancellation_Model ModelObject = new TransactionCancellation_Model();                                   // TransactionCancellation model object
        List<TransactionCancellation_Model> ModelList = new List<TransactionCancellation_Model>();                         //TransactionCancellation list model object

        TransactionCancellation_Data DataObject = new TransactionCancellation_Data();//transaction cancellation data obj


        // TransactionCancellation read 
        public List<TransactionCancellation_Model> ReadRecord(Int64 User_Gid)
        {
            try
            {
                return DataObject.ReadRecord(User_Gid);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        //Delete Transaction Cancellation
        public DataTable DeleteRecord(string claim)
        {
            try
            {
                return DataObject.DeleteRecord(claim);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
