﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccessHandler.Models.EOW;
using EOW.Data;
namespace EOW.Service
{
    public class PhysicalDespatch_Service
    {
        List<PhysicalDespatch_Model> ModelList = new List<PhysicalDespatch_Model>();
        PhysicalDespatch_Data DataObject = new PhysicalDespatch_Data();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();


        public List<PhysicalDespatch_Model> ReadPhysicalDespatch(Int64 userId)
        {
            List<PhysicalDespatch_Model> lst = new List<PhysicalDespatch_Model>();
            try
            {
                ds = DataObject.ReadPhysicalDespatch(userId);
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        PhysicalDespatch_Model objModel = new PhysicalDespatch_Model();

                        objModel.ecfID = Convert.ToInt64(dr["ecf_gid"].ToString());
                        objModel.ecfNumber = dr["ecf_no"].ToString();
                        objModel.ecfSubType = dr["ClaimType"].ToString();
                        objModel.raiserName = dr["Raiser"].ToString();
                        objModel.supplierName = dr["EcfSupplierEmployee"].ToString();
                        objModel.ecfStatus = dr["EcfStatus"].ToString();
                        objModel.ecfAmount = dr["ecf_amount"].ToString();
                        objModel.ecfDate = dr["ecf_date"].ToString();
                        objModel.raiserID = Convert.ToInt64(dr["Raiser_Gid"].ToString());
                        objModel.batchID = Convert.ToInt64(dr["ecf_batch_gid"].ToString());
                        lst.Add(objModel);
                    }
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return lst;
        }


        public List<PhysicalDespatch_Model> Get_DespatchMode()
        {
            List<PhysicalDespatch_Model> lst = new List<PhysicalDespatch_Model>();
            try
            {
                Int64 Id = 0;
                ds = DataObject.ReadPhysicalDespatch(Id);
                dt = ds.Tables[1];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        PhysicalDespatch_Model objModel = new PhysicalDespatch_Model();
                        objModel.ecfDespatchModeID = Convert.ToInt64(dr["courier_gid"].ToString());
                        objModel.ecfDespatchModeName = dr["courier_code"].ToString();

                        lst.Add(objModel);
                    }
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return lst;
        }

        public DataTable Save_EcfDespatchedList(PhysicalDespatch_Model modelObj)
        {
            try
            {
                return DataObject.Save_EcfDespatchedList(modelObj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<PhysicalDespatch_Model> GetPhysicalDespatched(Int64 userId)
        {
            List<PhysicalDespatch_Model> lst = new List<PhysicalDespatch_Model>();
            try
            {
                ds = DataObject.ReadPhysicalDespatch(userId);
                dt = ds.Tables[2];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        PhysicalDespatch_Model objModel = new PhysicalDespatch_Model();

                        objModel.ecfID = Convert.ToInt64(dr["ecf_gid"].ToString());
                        objModel.ecfNumber = dr["ecf_no"].ToString();
                        objModel.ecfSubType = dr["ClaimType"].ToString();
                        objModel.raiserName = dr["Raiser"].ToString();
                        objModel.supplierName = dr["EcfSupplierEmployee"].ToString();
                        objModel.ecfStatus = dr["EcfStatus"].ToString();
                        objModel.ecfAmount = dr["ecf_amount"].ToString();
                        objModel.ecfDate = dr["ecf_date"].ToString();
                        objModel.raiserID = Convert.ToInt64(dr["Raiser_Gid"].ToString());
                        objModel.ecfDespatchDate = dr["despatched_date"].ToString();
                        objModel.ecfDespatchModeID = Convert.ToInt64(dr["despatch_modeId"].ToString());
                        objModel.ecfDespatchModeName = dr["despatch_mode"].ToString();
                        objModel.despatchEWBNumber = dr["awb"].ToString();
                        objModel.batchID = Convert.ToInt64(dr["ecf_batch_gid"].ToString());
                        lst.Add(objModel);
                    }
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return lst;
        }


    }
}
