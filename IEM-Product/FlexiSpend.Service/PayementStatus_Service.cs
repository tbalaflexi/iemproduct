﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccessHandler.Models.FlexiSpend;
using FlexiSpend.Data;
using System.Reflection;

namespace FlexiSpend.Service
{
    public class PayementStatus_Service
    {
        List<PaymentStatus_Model> ModelList = new List<PaymentStatus_Model>();
        PaymentStatus_Data DataObject = new PaymentStatus_Data();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        // Transaction read 
        public List<PaymentStatus_Model> ReadPaymentStatus()
        {
            List<PaymentStatus_Model> lst = new List<PaymentStatus_Model>();
            try
            {
                ds = DataObject.ReadPaymentStatus();
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        PaymentStatus_Model objModel = new PaymentStatus_Model();
                        objModel.chqid = Convert.ToInt64(dr["chq_gid"].ToString());
                        objModel.paymentPayrunVoucherID = Convert.ToInt64(dr["payrunvoucher_gid"].ToString());
                        objModel.paymentBankID =  Convert.ToInt64(dr["paybank_gid"].ToString());
                        objModel.paymentBank =     dr["Payment_Bank"].ToString() ;
                        objModel.paymentGLValDate = dr["GL_Value_Date"].ToString();
                        objModel.paymentAmount = dr["PVAmount"].ToString();
                        objModel.paymentPVNumber =  dr["PV_Number"].ToString();
                        objModel.paymentBeneficiary = dr["Ben_Name"].ToString();
                        objModel.paymentMode = dr["Payment_Mode"].ToString();
                        objModel.chqstatus_code = Convert.ToInt64(dr["payrunvoucher_memostatus"].ToString());
                       // objModel.chqstatus_code = dr["payrunvoucher_memostatus"].ToString();
                      objModel.chqstatus_name = dr["Paymentstatus"].ToString();
                       objModel.paymentDate= Convert.ToDateTime(dr["paymentDate"].ToString());
                      //  objModel.paymentDate =  (dr["paymentDate"].ToString());
                        objModel.paymentReaseon = dr["paymentReaseon"].ToString();
                        objModel.paymentRefNo = Convert.ToInt32(dr["paymentRefNo"].ToString());

                        //objModel.paymentStatusName = dr["PaymentStatus"].ToString();
                        lst.Add(objModel);
                    }
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return lst;
        }


        public List<PaymentStatus_Model> GetPaymentStatus()
        {
            List<PaymentStatus_Model> lst = new List<PaymentStatus_Model>();
            try
            {

                ds = DataObject.ReadPaymentStatus();
                dt = ds.Tables[1];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        PaymentStatus_Model objModel = new PaymentStatus_Model();
                        objModel.chqstatus_code = Convert.ToInt64(dr["chqstatus_code"].ToString());
                       // objModel.chqstatus_code = dr["chqstatus_code"].ToString();
                        objModel.chqstatus_name = dr["chqstatus_name"].ToString();

                        lst.Add(objModel);
                    }
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return lst;
        }


        //Update Payment Status Service
        public DataTable UpdatePaymentstatus(PaymentStatus_Model Obj_Model)
        {
            try
            {
                dt = DataObject.UpdatePaymentstatus(Obj_Model);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        //public List<PaymentStatus_Model> Getdropdown(string parentcode)
        //{
        //    try
        //    {
        //        List<PaymentStatus_Model> dropdown = new List<PaymentStatus_Model>();
        //        DataTable dt1 = new DataTable();
        //        dt1 = DataObject.Getdropdown(parentcode);
        //        dropdown = ConvertDataTable<PaymentStatus_Model>(dt1);
        //        return dropdown;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //#region Convert Datatable to List
        //public static List<T> ConvertDataTable<T>(DataTable dt)
        //{
        //    List<T> data = new List<T>();
        //    foreach (DataRow row in dt.Rows)
        //    {
        //        T item = GetItem<T>(row);
        //        data.Add(item);
        //    }
        //    return data;
        //}
        //public static T GetItem<T>(DataRow dr)
        //{
        //    Type temp = typeof(T);
        //    T obj = Activator.CreateInstance<T>();

        //    foreach (DataColumn column in dr.Table.Columns)
        //    {
        //        foreach (PropertyInfo pro in temp.GetProperties())
        //        {
        //            if (pro.Name == column.ColumnName)
        //                pro.SetValue(obj, dr[column.ColumnName], null);
        //            else
        //                continue;
        //        }
        //    }
        //    return obj;
        //}
        //#endregion

        public List<PaymentStatus_Model> GetPaymodeDropDownList(PaymentStatus_Model Obj_Model)
        {
            try
            {
                Obj_Model.Action = "QCD_CheqStatus";
                ds = DataObject.Getdropdown(Obj_Model);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        PaymentStatus_Model Obj = new PaymentStatus_Model();
                        Obj.chqstatus_code = Convert.ToInt32(dt.Rows[i]["chqstatus_code"].ToString());
                        Obj.chqstatus_name = dt.Rows[i]["chqstatus_name"].ToString();
                        ModelList.Add(Obj);
                    }
                }

                return ModelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
