﻿using DataAccessHandler.Models.FlexiSpend;
using FlexiSpend.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FlexiSpend.Service
{
   public class RejectDespatch_Service
    {
        RejectDespatch_Model ModelObject = new RejectDespatch_Model();                                   // RejectDespatch model object
        List<RejectDespatch_Model> ModelList = new List<RejectDespatch_Model>();                       // RejectDespatch list model object
        RejectDespatch_Data DataObject = new RejectDespatch_Data();                                   // RejectDespatch data object


        // RejectDespatch Read For Review
        public List<RejectDespatch_Model> ReadRecord()
        {
            try
            {
                return DataObject.ReadRecord();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


 


        //RejectDespatch Update for review
        public DataTable UpdateRecord(RejectDespatch_Model ModelObject)
        {
            try
            {
                return DataObject.UpdateRecord(ModelObject);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

       
        // GetTranType for Review(Type) and Despatch(despach mode)

        public List<RejectDespatch_Model> Getdropdown(string parentcode)
        {
            try
            {
                List<RejectDespatch_Model> dropdown = new List<RejectDespatch_Model>();
                DataTable dt1 = new DataTable();
                dt1 = DataObject.Getdropdown(parentcode);
                dropdown = ConvertDataTable<RejectDespatch_Model>(dt1);
                return dropdown;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



       //non qc dropdown values for Review
        public List<RejectDespatch_Model> dropdownvalues(string flag)
        {
            try
            {
                List<RejectDespatch_Model> dropdown = new List<RejectDespatch_Model>();
                DataTable dt1 = new DataTable();

                dt1 = DataObject.dropdownvalues(flag);
                dropdown = ConvertDataTable<RejectDespatch_Model>(dt1);
                return dropdown;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




       // Despatch Mode Dropdown binding qc For Despatch
        //public List<ChequePayment_Model> Getdropdown(string parentcode)
        //{
        //    try
        //    {
        //        List<ChequePayment_Model> dropdown = new List<ChequePayment_Model>();
        //        DataTable dt1 = new DataTable();
        //        dt1 = DataObject.Getdropdown(parentcode);
        //        dropdown = ConvertDataTable<ChequePayment_Model>(dt1);
        //        return dropdown;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}






        // RejectDespatch Read For Despatch
        public List<RejectDespatch_Model> Read_Record()
        {
            try
            {
                return DataObject.Read_Record();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        //RejectDespatch Update For Despatch
        public DataTable Update_Record(RejectDespatch_Model ModelObject)
        {
            try
            {
                return DataObject.Update_Record(ModelObject);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

       //
        public DataTable GetValues(string TranId, string parentcode)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = DataObject.GetValues(TranId, parentcode);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        #region Convert Datatable to List
        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
        #endregion
    }
}
