﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessHandler.Models.FlexiSpend;
using FlexiSpend.Data;
using System.Data;
namespace FlexiSpend.Service
{
    public class PaymentRun_Service
    {
        PaymentRun_Data ObjData = new PaymentRun_Data();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        public List<PaymentRun_Model> Read_FundPosition(Int64 User_Gid)
        {
            List<PaymentRun_Model> lst = new List<PaymentRun_Model>();
            try
            {
                dt = ObjData.Read_FundPosition(User_Gid);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PaymentRun_Model objModel = new PaymentRun_Model();
                        objModel.Fund_PayBank_Name = row["PayBank"].ToString();
                        objModel.Fund_PayBank_Date = row["fundupdate_date"].ToString();
                        objModel.Fund_Available = Convert.ToInt64(row["FundAvailable"].ToString());
                        objModel.Fund_Utilised = Convert.ToInt64(row["Utilised_Amount"].ToString());
                        objModel.Fund_Balance = Convert.ToInt64(row["Balance"].ToString());
                        lst.Add(objModel);
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return lst;
        }

        public List<PaymentRun_Model> Read_PaymentRun_BeforeGL(Int64 User_Gid)
        {
            List<PaymentRun_Model> lst = new List<PaymentRun_Model>();
            try
            {
                dt = ObjData.Read_PaymentRun_BeforeGL(User_Gid);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PaymentRun_Model objModel = new PaymentRun_Model();
                        objModel.PaymentRunGL_ECF_Gid =Convert.ToInt64(row["ecf_gid"]);
                        objModel.PaymentRunGL_Batch_Gid = Convert.ToInt64(row["ecf_batch_gid"]);
                        objModel.PaymentRunGL_ECF_No = row["ecf_no"].ToString();
                        objModel.PaymentRunGL_ECF_ClaimType = row["DocType"].ToString();
                        objModel.PaymentRunGL_ECF_Raiser_GID = Convert.ToInt64(row["ecf_raiser"]);
                        objModel.PaymentRunGL_ECF_Date = row["ecf_date"].ToString();
                        objModel.PaymentRunGL_ECF_DocType_SubType = row["DocType_SubType"].ToString();
                        objModel.PaymentRunGL_ECF_Status = row["ecfstatus_name"].ToString();
                        objModel.PaymentRunGL_ECF_Amount =Convert.ToDouble(row["ecf_amount"].ToString());
                        objModel.PaymentRunGL_ECF_Raiser = row["EmployeeCode_Name"].ToString();
                        objModel.PaymentRunGL_ECF_Supplier = row["SupplierCode_Name"].ToString();
                        objModel.PaymentRunGL_ECF_Netting = row["Netting"].ToString();
                        objModel.PaymentRunGL_ECF_Creditline_Amount = Convert.ToDouble(row["creditline_amount"]);
                        lst.Add(objModel);
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return lst;
        }
        public List<PaymentRun_Model> Read_PayBank()
        {
            List<PaymentRun_Model> lst = new List<PaymentRun_Model>();
            try
            {
                //dt = dataObj.Get_DropDownLists(Master, Depend_Code, TypeCode, SubTypeCode, AttributeParent, AttributeChild); 
                dt = ObjData.Read_PayBank();
                PaymentRun_Model objModel = new PaymentRun_Model();
                objModel.PaymentRunGL_PayBank_Gid = 0;
                objModel.PaymentRunGL_PayBank_Name = "--Select--";
                lst.Add(objModel);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        objModel = new PaymentRun_Model();
                        objModel.PaymentRunGL_PayBank_Gid = Convert.ToInt64(dt.Rows[i]["Depend_Code"].ToString());
                        objModel.PaymentRunGL_PayBank_Name = dt.Rows[i]["paybank_bank_name"].ToString();
                        lst.Add(objModel);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }
        public List<PaymentRun_Model> Read_PaymentRun_AfterGL(Int64 User_Gid)
        {
            List<PaymentRun_Model> lst = new List<PaymentRun_Model>();
            try
            {
                dt = ObjData.Read_PaymentRun_AfterGL(User_Gid);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PaymentRun_Model objModel = new PaymentRun_Model();
                        objModel.PaymentRunGL_ECF_Gid = Convert.ToInt64(row["ecf_gid"]);
                        objModel.PaymentRunGL_ECF_No = row["ecf_no"].ToString();
                        objModel.PaymentRunGL_ECF_Date = row["ecf_date"].ToString();
                        objModel.PaymentRunGL_ECF_DocType_SubType = row["DocType_SubType"].ToString();
                        objModel.PaymentRunGL_ECF_Status = row["ecfstatus_name"].ToString();
                        objModel.PaymentRunGL_ECF_Amount = Convert.ToDouble(row["ecf_amount"].ToString());
                        objModel.PaymentRunGL_ECF_Raiser = row["EmployeeCode_Name"].ToString();
                        objModel.PaymentRunGL_ECF_Supplier = row["SupplierCode_Name"].ToString();
                        objModel.PaymentRunGL_GLDate = row["paymentrungl_date"].ToString();
                        objModel.PaymentRunGL_PayBank_Name = row["PayBank_Name"].ToString();
                        objModel.PaymentRunGL_ECF_Creditline_Amount = Convert.ToDouble(row["creditline_amount"]);
                        lst.Add(objModel);
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return lst;
        }
        public DataTable Reverse_Authorization(String ECF_GID = "", Int64 Login_User_Gid = 0)
        {
            try
            {
                dt = ObjData.Reverse_Authorization(ECF_GID, Login_User_Gid);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dt;
        }
        public DataTable Update_GLValue_Date(PaymentRun_Model ObjModel, Int64 Login_User_Gid = 0)
        {
            try
            {
                dt = ObjData.Update_GLValue_Date(ObjModel, Login_User_Gid);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dt;
        }

        public DataTable Undo_PaymentRunGLDate(Int64 ECF_GID = 0, Int64 Login_User_Gid = 0)
        {
            try
            {
                dt = ObjData.Undo_PaymentRunGLDate(ECF_GID, Login_User_Gid);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dt;
        }
        public DataSet Set_PaymentRun(string ECF_GIDs = "", Int64 Login_User_Gid = 0)
        {
            try
            {
                ds = ObjData.Set_PaymentRun(ECF_GIDs, Login_User_Gid);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return ds;
        }
    }
}
