﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessHandler.Models.FlexiSpend;
using FlexiSpend.Data;
using System.Data;
namespace FlexiSpend.Service
{
   public class EFTFileGeneration_Service
    {
       PaymentRun_Data ObjPayRun_Data = new PaymentRun_Data();
       EFTFileDownload_Data ObjData = new EFTFileDownload_Data();
       DataTable dt = new DataTable();
       DataSet ds = new DataSet();

       public List<EFTFileGeneration_Model> Read_PayBank()
       {
           List<EFTFileGeneration_Model> lst = new List<EFTFileGeneration_Model>();
           try
           {
               //dt = dataObj.Get_DropDownLists(Master, Depend_Code, TypeCode, SubTypeCode, AttributeParent, AttributeChild); 
               dt = ObjPayRun_Data.Read_PayBank();
               EFTFileGeneration_Model objModel = new EFTFileGeneration_Model();
               objModel.PayBank_Gid = 0;
               objModel.PayBank = "--Select--";
               lst.Add(objModel);
               if (dt.Rows.Count > 0)
               {
                   for (int i = 0; i < dt.Rows.Count; i++)
                   {
                       objModel = new EFTFileGeneration_Model();
                       objModel.PayBank_Gid = Convert.ToInt64(dt.Rows[i]["Depend_Code"].ToString());
                       objModel.PayBank = dt.Rows[i]["paybank_bank_name"].ToString();
                       lst.Add(objModel);
                   }
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return lst;
       }

       public List<EFTFileGeneration_Model> Get_ClaimType()
       {
           List<EFTFileGeneration_Model> lst = new List<EFTFileGeneration_Model>();
           try
           {
               EFTFileGeneration_Model modelObj = new EFTFileGeneration_Model();               
               modelObj.ClaimType = "Employee";
               modelObj.ClaimType_Gid = "Employee";
               lst.Add(modelObj);

               modelObj = new EFTFileGeneration_Model();
               modelObj.ClaimType = "Supplier";
               modelObj.ClaimType_Gid = "Supplier";
               lst.Add(modelObj);
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return lst;
       }
       public List<EFTFileGeneration_Model> Read_EFTGrid(EFTFileGeneration_Model ObjModel, Int64 User_Gid)
       {
           List<EFTFileGeneration_Model> lst = new List<EFTFileGeneration_Model>();
           try
           {
               dt = ObjData.Read_EFTGrid(ObjModel,User_Gid);
               if (dt.Rows.Count > 0)
               {
                   foreach (DataRow row in dt.Rows)
                   {
                       EFTFileGeneration_Model objModel = new EFTFileGeneration_Model();
                       objModel.SNo = row["SlNo"].ToString();
                       objModel.PV_Gid = Convert.ToInt64(row["PvId"]);
                       objModel.Benificary_Bank_Name = Convert.ToString(row["Bankname"]);
                       objModel.PV_Date = row["PVDate"].ToString();
                       objModel.PV_No = row["PVNo"].ToString();
                       objModel.Pay_paymodemode = Convert.ToString(row["PayMode"]);
                       objModel.PV_From_Amount = Convert.ToDouble(row["PVAmount"]);
                       objModel.ClaimType = row["ClaimType"].ToString();
                       objModel.Supplier_Code = row["EmployeeSupplierCode"].ToString();
                       objModel.Supplier_Name = row["EmployeeSupplierName"].ToString();
                       lst.Add(objModel);
                   }
               }
           }
           catch (Exception ex)
           {

               throw ex;
           }
           return lst;
       }
       public DataTable SetEFTMemoDetails(EFTFileGeneration_Model ObjModel, Int64 Login_User_Gid)
       {
           try
           {
               dt = ObjData.SetEFTMemoDetails(ObjModel, Login_User_Gid);
           }
           catch (Exception ex)
           {

               throw ex;
           }
           return dt;
       }
       public DataSet PrintEFTMemoDetails(EFTFileGeneration_Model ObjModel, Int64 Login_User_Gid)
       {
           try
           {
               ds = ObjData.PrintEFTMemoDetails(ObjModel, Login_User_Gid);
           }
           catch (Exception ex)
           {

               throw ex;
           }
           return ds;
       }
       
    }
    
}
