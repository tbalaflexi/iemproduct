﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessHandler.Models.FlexiSpend;
using FlexiSpend.Data;
using System.Data;


namespace FlexiSpend.Service
{
    public class Pullout_Service
    {
        #region Declarations
        Pullout_Data objData = new Pullout_Data();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        List<Pullout_Model> lst = new List<Pullout_Model>();
        #endregion

        public List<Pullout_Model> Get_PulloutDetailList(Pullout_Model Obj_Model)
        {

            try
            {
                Obj_Model.Action = "GetPullout";
                ds = objData.Get_PulloutDetailList(Obj_Model);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Pullout_Model _obj = new Pullout_Model();
                        _obj.EcfGid = Convert.ToInt64(dt.Rows[i]["ecf_gid"].ToString());
                        _obj.EcfDate = Convert.ToDateTime(dt.Rows[i]["ecf_date"].ToString());
                        _obj.RefNo = dt.Rows[i]["RefNo"].ToString();

                        _obj.Type_SubType = dt.Rows[i]["Type / SubType"].ToString();
                        _obj.EcfStatus = dt.Rows[i]["ECF Status"].ToString();
                        _obj.EcfAmount = Convert.ToDecimal(dt.Rows[i]["Amount"].ToString());
                        _obj.RaiserId_Name = dt.Rows[i]["RaiserId / Name"].ToString();
                        _obj.SupplierId_Name = dt.Rows[i]["Suppliercode / Name"].ToString();
                        _obj.Attributes = dt.Rows[i]["Attributes"].ToString();
                        _obj.Physical = dt.Rows[i]["Physical"].ToString();
                        _obj.RecivedDate = Convert.ToDateTime(dt.Rows[i]["Received Date"].ToString());
                        _obj.BatchNo = dt.Rows[i]["Batch No"].ToString();
                        _obj.PulloutDate = Convert.ToDateTime(dt.Rows[i]["Pullout Date"].ToString());
                        _obj.HandedOverTo = dt.Rows[i]["Handed Over to"].ToString();
                        lst.Add(_obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }

        public List<Pullout_Model> Get_PulloutInterFilingList(Pullout_Model Obj_Model)
        {

            try
            {
                Obj_Model.Action = "Interfiling";
                ds = objData.Get_PulloutDetailList(Obj_Model);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Pullout_Model _obj = new Pullout_Model();
                        _obj.EcfGid = Convert.ToInt64(dt.Rows[i]["ecf_gid"].ToString());
                        _obj.EcfDate = Convert.ToDateTime(dt.Rows[i]["ecf_date"].ToString());
                        _obj.RefNo = dt.Rows[i]["RefNo"].ToString();

                        _obj.Type_SubType = dt.Rows[i]["Type / SubType"].ToString();
                        _obj.EcfStatus = dt.Rows[i]["ECF Status"].ToString();
                        _obj.EcfAmount = Convert.ToDecimal(dt.Rows[i]["Amount"].ToString());
                        _obj.RaiserId_Name = dt.Rows[i]["RaiserId / Name"].ToString();
                        _obj.SupplierId_Name = dt.Rows[i]["Suppliercode / Name"].ToString();
                        _obj.Attributes = dt.Rows[i]["Attributes"].ToString();
                        _obj.Physical = dt.Rows[i]["Physical"].ToString();
                        _obj.RecivedDate = Convert.ToDateTime(dt.Rows[i]["Received Date"].ToString());
                        _obj.BatchNo = dt.Rows[i]["Batch No"].ToString();
                        _obj.PulloutDate = Convert.ToDateTime(dt.Rows[i]["Pullout Date"].ToString());
                        _obj.HandedOverTo = dt.Rows[i]["Handed Over to"].ToString();
                        _obj.InterfilingDate = Convert.ToDateTime(dt.Rows[i]["Interfiling Date"].ToString());
                        lst.Add(_obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }
        public List<Pullout_Model> GetEcf(Pullout_Model Obj_Model)
        {
            try
            {
                Obj_Model.Action = "GetEcf";
                ds = objData.Get_PulloutDetailList(Obj_Model);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Pullout_Model Obj = new Pullout_Model();
                        Obj.Refid = Convert.ToInt64(dt.Rows[i]["RefId"]);
                        Obj.RefNo = Convert.ToString(dt.Rows[i]["RefNo"]);
                        lst.Add(Obj);
                    }
                }

                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Pullout_Model> GetEmp(Pullout_Model Obj_Model)
        {
            try
            {
                Obj_Model.Action = "GetEmp";
                ds = objData.Get_PulloutDetailList(Obj_Model);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Pullout_Model Obj = new Pullout_Model();
                        Obj.EmpGid = Convert.ToInt64(dt.Rows[i]["EmpGid"]);
                        Obj.EmpName = Convert.ToString(dt.Rows[i]["EmpName"]);
                        lst.Add(Obj);
                    }
                }
                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Pullout_Model> GetHandeddetails(Pullout_Model Obj_Model)
        {
            try
            {
                Obj_Model.Action = "GetEmp";
                ds = objData.Get_PulloutDetailList(Obj_Model);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Pullout_Model Obj = new Pullout_Model();
                        Obj.HandedEmpId = Convert.ToInt64(dt.Rows[i]["EmpGid"]);
                        Obj.HandedEmpName = Convert.ToString(dt.Rows[i]["EmpName"]);
                        lst.Add(Obj);
                    }
                }
                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable SETPulloutDetails(Pullout_Model Obj_Model)
        {
            try
            {
                dt = objData.Set_PulloutDetails(Obj_Model);

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
