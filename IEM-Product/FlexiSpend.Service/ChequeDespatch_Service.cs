﻿using DataAccessHandler.Models.FlexiSpend;
using FlexiSpend.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FlexiSpend.Service
{
   public class ChequeDespatch_Service
    {
        ChequePayment_Model ModelObject = new ChequePayment_Model();                                   // ChequeDespatch model object
        List<ChequePayment_Model> ModelList = new List<ChequePayment_Model>();                       // ChequeDespatch list model object
        ChequeDespatch_Data DataObject = new ChequeDespatch_Data();                                   // ChequeDespatch data object


        // ChequeDespatch Read
        public List<ChequePayment_Model> ReadRecord()
        {
            try
            {
                return DataObject.ReadRecord();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }









        //ChequeDespatch update
       public DataTable UpdateRecord(ChequePayment_Model ModelObject)
       {
           try
           {
               return DataObject.UpdateRecord(ModelObject);
           }

           catch (Exception ex)
           {
               throw ex;
           }
       }

       //
       public List<ChequePayment_Model> Getdropdown(string parentcode)
       {
           try
           {
               List<ChequePayment_Model> dropdown = new List<ChequePayment_Model>();
               DataTable dt1 = new DataTable();
               dt1 = DataObject.Getdropdown(parentcode);
               dropdown = ConvertDataTable<ChequePayment_Model>(dt1);
               return dropdown;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
       //
       public DataTable GetValues(string TranId, string parentcode)
       {
           try
           {
               DataTable dt = new DataTable();
               dt = DataObject.GetValues(TranId, parentcode);
               return dt;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }








       #region Convert Datatable to List
       public static List<T> ConvertDataTable<T>(DataTable dt)
       {
           List<T> data = new List<T>();
           foreach (DataRow row in dt.Rows)
           {
               T item = GetItem<T>(row);
               data.Add(item);
           }
           return data;
       }
       public static T GetItem<T>(DataRow dr)
       {
           Type temp = typeof(T);
           T obj = Activator.CreateInstance<T>();

           foreach (DataColumn column in dr.Table.Columns)
           {
               foreach (PropertyInfo pro in temp.GetProperties())
               {
                   if (pro.Name == column.ColumnName)
                       pro.SetValue(obj, dr[column.ColumnName], null);
                   else
                       continue;
               }
           }
           return obj;
       }
       #endregion
    }
}
