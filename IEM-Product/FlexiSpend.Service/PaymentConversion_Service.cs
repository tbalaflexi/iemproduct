﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessHandler.Models.FlexiSpend;
using FlexiSpend.Data;
using System.Data;

namespace FlexiSpend.Service
{
   public  class PaymentConversion_Service
    {
        #region Declarations
       PaymentConversion_Data objData = new PaymentConversion_Data();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        List<PaymentConversion_Model> lst = new List<PaymentConversion_Model>();
        #endregion

        public List<PaymentConversion_Model> Get_PaymentConversionList(PaymentConversion_Model Obj_Model)
       {

           try
           {
               
               ds = objData.Get_PaymentConDetailList(Obj_Model);
               if (ds.Tables.Count > 0)
               {
                   dt = ds.Tables[0];
                   for (int i = 0; i < dt.Rows.Count; i++)
                   {
                       PaymentConversion_Model _obj = new PaymentConversion_Model();
                       _obj.PV_Gid = Convert.ToInt64(dt.Rows[i]["payrunvoucher_gid"].ToString());
                       _obj.Payment_Bank = dt.Rows[i]["Payment Bank"].ToString();
                       _obj.PaymentReissue_Id = Convert.ToInt64(dt.Rows[i]["Paymentreissue_Id"].ToString());
                       _obj.Ecf_Gid = Convert.ToInt64(dt.Rows[i]["ecf_gid"].ToString());
                       _obj.PVGL_Gid = Convert.ToInt64(dt.Rows[i]["PaymentRunGL_Gid"].ToString());
                       _obj.GLValue_Date = Convert.ToDateTime(dt.Rows[i]["GL Value Date"].ToString());
                       _obj.PV_No = dt.Rows[i]["PV No"].ToString();
                       _obj.PV_Amount = Convert.ToDecimal(dt.Rows[i]["PV Amount"].ToString());
                       _obj.PV_Beneficiary = dt.Rows[i]["Beneficiary"].ToString();
                       _obj.PV_Paymode = dt.Rows[i]["Paymode"].ToString();
                       _obj.PV_Status = dt.Rows[i]["Status"].ToString();
                       _obj.PV_Reason = dt.Rows[i]["Reason"].ToString();
                       _obj.PV_Change_Paymode = dt.Rows[i]["Change Paymode"].ToString();
                       _obj.PV_Change_Beneficiary = dt.Rows[i]["Change Beneficiary"].ToString();
                       _obj.Change_GLValue_Date =Convert.ToDateTime(dt.Rows[i]["New GL Value Date"].ToString());
                       _obj.ECF_Supplier_Employee_Type = dt.Rows[i]["ecf_supplier_employee"].ToString();
                       _obj.ECF_Supplier_Employee_Gid = Convert.ToInt64(dt.Rows[i]["Supplier/Employee_Gid"].ToString());
                       lst.Add(_obj);
                   }
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return lst;
       }
        public List<PaymentConversion_Model> GetPaymodeList(PaymentConversion_Model Obj_Model)
        {
            try
            {
                Obj_Model.Action = "GetPaymode_All";
                ds = objData.Get_PaymentConDetailList(Obj_Model);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        PaymentConversion_Model Obj = new PaymentConversion_Model();
                        Obj.PV_Change_Paymode = dt.Rows[i]["Change Paymode"].ToString();                      
                        lst.Add(Obj);
                    }
                }

                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<PaymentConversion_Model> GetPaymodeDropDownList(PaymentConversion_Model Obj_Model)
        {
            try
            {
                Obj_Model.Action = "GetPaymode";
                ds = objData.Get_PaymentConDetailList(Obj_Model);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        PaymentConversion_Model Obj = new PaymentConversion_Model();
                        Obj.PV_Change_Paymode = dt.Rows[i]["Change Paymode"].ToString();
                        lst.Add(Obj);
                    }
                }

                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PaymentConversion_Model> GetBeneficiaryList(PaymentConversion_Model Obj_Model)
        {
            try
            {
                Obj_Model.Action = "GetBeneficiary";
                ds = objData.Get_PaymentConDetailList(Obj_Model);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        PaymentConversion_Model Obj = new PaymentConversion_Model();
                        Obj.PV_Change_Beneficiary = dt.Rows[i]["Change Beneficiary"].ToString();
                        lst.Add(Obj);
                    }
                }

                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable SETPaymentConvMaker(PaymentConversion_Model Obj_Model)
        {
            try
            {
                dt = objData.Set_PaymentConversionMaker(Obj_Model);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable SETPaymentConvChecker(PaymentConversion_Model Obj_Model)
        {
            try
            {
                dt = objData.Set_PaymentConversionChecker(Obj_Model);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
