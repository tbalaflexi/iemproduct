﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessHandler.Models.FlexiSpend;
using FlexiSpend.Data;
using System.Data;

namespace FlexiSpend.Service
{
    public class InwardService
    {
        #region Declarations
        InwardData dataObj = new InwardData();
        List<Inward_Model> objLst = new List<Inward_Model>();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        #endregion

        #region Inward Details // Get Inward Details
        public List<Inward_Model> GetInwardDetails(Inward_Model _data)
        {
            try
            {
                ds = dataObj.GetInwardDetails(_data);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (_data.Gridtype == "Inward" || _data.Gridtype == "InwardWithoutReceipt")
                        {
                            Inward_Model Obj = new Inward_Model();
                            Obj.Refid = Convert.ToInt64(dt.Rows[i]["RefId"]);
                            Obj.Refno = Convert.ToString(dt.Rows[i]["RefNo"]);
                            Obj.RefDate = Convert.ToDateTime(dt.Rows[i]["RefDate"]);
                            Obj.Type_Gid = Convert.ToInt64(dt.Rows[i]["Typeid"]);
                            Obj.Type_Name = Convert.ToString(dt.Rows[i]["TypeName"]);
                            Obj.SubType_Gid = Convert.ToInt64(dt.Rows[i]["Subtypeid"]);
                            Obj.SubType_Name = Convert.ToString(dt.Rows[i]["SubtypeName"]);
                            Obj.StatusId = Convert.ToInt64(dt.Rows[i]["Statusid"]);
                            Obj.Status = Convert.ToString(dt.Rows[i]["StatusName"]);
                            Obj.EcfAmt = Convert.ToDecimal(dt.Rows[i]["EcfAmt"]);
                            Obj.Raiserid = Convert.ToInt64(dt.Rows[i]["Raiserid"]);
                            Obj.RaiserName = Convert.ToString(dt.Rows[i]["RaiserName"]);
                            
                            Obj.ReciverDate = Convert.ToDateTime(dt.Rows[i]["ReceivedDate"]);
                            Obj.CourierId = Convert.ToInt64(dt.Rows[i]["CourierId"]);
                            Obj.CourierName = Convert.ToString(dt.Rows[i]["CourierName"]);
                            Obj.AwbNo = Convert.ToString(dt.Rows[i]["AWBNo"]);
                            Obj.BatchId = Convert.ToInt64(dt.Rows[i]["Batchid"]);
                            Obj.BatchNo = Convert.ToString(dt.Rows[i]["BatchNo"]);
                            Obj.SupEmpId = Convert.ToInt64(dt.Rows[i]["Emp/Supplier id"]);
                            Obj.SupEmpName = Convert.ToString(dt.Rows[i]["Emp/Supplier"]);
                            Obj.Pouchids = Convert.ToString(dt.Rows[i]["Pouchid"]);
                            Obj.AttributeDet = Convert.ToString(dt.Rows[i]["Attributes"]);
                            Obj.PhysicalReceipt = Convert.ToString(dt.Rows[i]["Physicalreceipt"]);
                            objLst.Add(Obj);
                        }
                        else if (_data.Gridtype == "Batch")
                        {
                            Inward_Model Obj = new Inward_Model();
                            Obj.ReciverDate = Convert.ToDateTime(dt.Rows[i]["ReceviedDate"]);
                            Obj.BatchCount = Convert.ToInt64(dt.Rows[i]["DocCount"]);
                            Obj.BatchNo = Convert.ToString(dt.Rows[i]["BatchNo"]);
                            Obj.BatchId = Convert.ToInt64(dt.Rows[i]["BatchId"]);
                            objLst.Add(Obj);
                        }

                        else if (_data.Gridtype == "InwardBatch")
                        {
                            Inward_Model Obj = new Inward_Model();
                            Obj.Refid = Convert.ToInt64(dt.Rows[i]["RefId"]);
                            Obj.Refno = Convert.ToString(dt.Rows[i]["RefNo"]);
                            Obj.BatchId = Convert.ToInt64(dt.Rows[i]["batchid"]);
                            Obj.BatchNo = Convert.ToString(dt.Rows[i]["batchno"]);
                            Obj.RefDate = Convert.ToDateTime(dt.Rows[i]["RefDate"]);
                            Obj.Type_Gid = Convert.ToInt64(dt.Rows[i]["Typeid"]);
                            Obj.Type_Name = Convert.ToString(dt.Rows[i]["TypeName"]);
                            Obj.SubType_Gid = Convert.ToInt64(dt.Rows[i]["Subtypeid"]);
                            Obj.SubType_Name = Convert.ToString(dt.Rows[i]["SubtypeName"]);
                            Obj.StatusId = Convert.ToInt64(dt.Rows[i]["Statusid"]);
                            Obj.Status = Convert.ToString(dt.Rows[i]["StatusName"]);
                            Obj.EcfAmt = Convert.ToDecimal(dt.Rows[i]["EcfAmt"]);
                            Obj.Raiserid = Convert.ToInt64(dt.Rows[i]["Raiserid"]);
                            Obj.RaiserName = Convert.ToString(dt.Rows[i]["RaiserName"]);

                            Obj.ReciverDate = Convert.ToDateTime(dt.Rows[i]["ReceivedDate"]);
                            Obj.CourierId = Convert.ToInt64(dt.Rows[i]["CourierId"]);
                            Obj.CourierName = Convert.ToString(dt.Rows[i]["CourierName"]);
                            Obj.AwbNo = Convert.ToString(dt.Rows[i]["AWBNo"]);
                            Obj.BatchId = Convert.ToInt64(dt.Rows[i]["Batchid"]);
                            Obj.BatchNo = Convert.ToString(dt.Rows[i]["BatchNo"]);
                            Obj.SupEmpId = Convert.ToInt64(dt.Rows[i]["Emp/Supplier id"]);
                            Obj.SupEmpName = Convert.ToString(dt.Rows[i]["Emp/Supplier"]);
                            Obj.Pouchids = Convert.ToString(dt.Rows[i]["Pouchid"]);
                            Obj.AttributeDet = Convert.ToString(dt.Rows[i]["Attributes"]);
                            Obj.PhysicalReceipt = Convert.ToString(dt.Rows[i]["Physicalreceipt"]);
                            objLst.Add(Obj);

                        }
                    }
                }
                return objLst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //InwardBatch Details
        public List<Inward_Model> GetPouch(Inward_Model _data)
        {
            try
            {
               
                ds = dataObj.GetInwardDetails(_data);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Inward_Model Obj = new Inward_Model();
                        Obj.ReciverDate = Convert.ToDateTime(dt.Rows[i]["ReceviedDate"]);
                        Obj.Refid = Convert.ToInt64(dt.Rows[i]["RefId"]);
                    }
                }
                return objLst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Inward_Model> GetEcf(Inward_Model _data)
        {
            try
            {
                
                ds = dataObj.GetInwardDetails(_data);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Inward_Model Obj = new Inward_Model();
                        Obj.Refid = Convert.ToInt64(dt.Rows[i]["RefId"]);
                        Obj.Refno = Convert.ToString(dt.Rows[i]["RefNo"]);
                        objLst.Add(Obj);
                    }
                }
                return objLst;
            }
             catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Inward_Model> GetCourier(Inward_Model _data)
        {
            try{

                ds = dataObj.GetInwardDetails(_data);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Inward_Model Obj = new Inward_Model();
                        Obj.CourierId = Convert.ToInt64(dt.Rows[i]["courierid"]);
                        Obj.CourierName = Convert.ToString(dt.Rows[i]["courierName"]);
                        objLst.Add(Obj);
                    }
                    return objLst;
                }
                return objLst;
            }
            catch (Exception ex){
                throw ex;
            }
        }

        public DataTable SetInwardDetails(Inward_Model _data)
        {
            try
            {
                ds = dataObj.SetInwardDetails(_data);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataTable SetBatchDetails(Inward_Model _data)
        {
            try
            {
                ds = dataObj.SetInwardDetails(_data);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
