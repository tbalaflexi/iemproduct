﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessHandler.Models.FlexiSpend;
using FlexiSpend.Data;
using System.Data;

namespace FlexiSpend.Service
{
    public class Urgent_Service
    {
        #region Declarations
        Urgent_Data objData = new Urgent_Data();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        List<Urgent_Model> lst = new List<Urgent_Model>();
        #endregion

        public List<Urgent_Model> Get_UrgentDetailsList(Urgent_Model Obj_Model)
        {

            try
            {
                Obj_Model.Action = "GetUrgent";
                ds = objData.Get_UrgentDetailList(Obj_Model);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Urgent_Model _obj = new Urgent_Model();
                        _obj.EcfGid = Convert.ToInt64(dt.Rows [i]["ecf_gid"].ToString());
                        _obj.EcfDate = Convert.ToDateTime(dt.Rows[i]["ecf_date"].ToString());
                        _obj.RefNo = dt.Rows[i]["RefNo"].ToString();

                        _obj.Type_SubType = dt.Rows[i]["Type / SubType"].ToString();
                        _obj.EcfStatus = dt.Rows[i]["ECF Status"].ToString();
                        _obj.EcfAmount = Convert.ToDecimal(dt.Rows[i]["Amount"].ToString());
                        _obj.RaiserId_Name = dt.Rows[i]["RaiserId / Name"].ToString();
                        _obj.SupplierId_Name = dt.Rows[i]["Suppliercode / Name"].ToString();
                        _obj.Attributes = dt.Rows[i]["Attributes"].ToString();
                        _obj.Physical = dt.Rows[i]["Physical"].ToString();
                        _obj.RecivedDate = Convert.ToDateTime(dt.Rows[i]["Received Date"].ToString());
                        _obj.BatchNo = dt.Rows[i]["Batch No"].ToString();
                        lst.Add(_obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }

        public List<Urgent_Model> GetEcf(Urgent_Model Obj_Model)
        {
            try
            {
                Obj_Model.Action = "GetEcf";
                ds = objData.Get_UrgentDetailList(Obj_Model);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Urgent_Model Obj = new Urgent_Model();
                        Obj.Refid = Convert.ToInt64(dt.Rows[i]["RefId"]);
                        Obj.RefNo = Convert.ToString(dt.Rows[i]["RefNo"]);
                        lst.Add(Obj);
                    }
                }

                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Urgent_Model> GetEmp(Urgent_Model Obj_Model)
        {
            try
            {
                Obj_Model.Action = "GetEmp";
                ds = objData.Get_UrgentDetailList(Obj_Model);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Urgent_Model Obj = new Urgent_Model();
                        Obj.EmpGid = Convert.ToInt64(dt.Rows[i]["EmpGid"]);
                        Obj.EmpName = Convert.ToString(dt.Rows[i]["EmpName"]);
                        lst.Add(Obj);
                    }
                }
                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable DMLUrgentDetails(Urgent_Model Obj_Model)
        {
            try
            {
                dt = objData.DML_UrgentDetails(Obj_Model);
               
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
