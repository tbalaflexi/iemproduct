﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccessHandler.Models.FlexiSpend;
using FlexiSpend.Data;

namespace FlexiSpend.Service
{
    public class APChecker_Service
    {
        APChecker_Data objData = new APChecker_Data();
        DataTable dt = new DataTable();
        public List<APCheker_Model> Get_APCheckerList(string GridAction)
        {
            List<APCheker_Model> lst = new List<APCheker_Model>();
            try
            {
                dt = objData.Get_APCheckerList(GridAction);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        APCheker_Model _obj = new APCheker_Model();
                        _obj.Ref_Gid = Convert.ToInt64(row["RefGid"].ToString());
                        _obj.Ref_No = row["RefNo"].ToString();
                        _obj.Ref_Date = Convert.ToDateTime(row["RefDate"].ToString());
                        _obj.Type_SubType = row["TypeSubType"].ToString();
                        _obj.Status = row["Status"].ToString();
                        _obj.Amount = Convert.ToDecimal(row["Amount"].ToString());
                        _obj.Raiser_Id_Name = row["Raiser"].ToString();
                        _obj.Supplier_Code_Name = row["Supplier"].ToString();
                        _obj.Type = row["Type"].ToString();
                        _obj.SubType = row["SubType"].ToString();
                        _obj.Type_Gid = Convert.ToInt64(row["TypeGid"].ToString());
                        _obj.SubType_Gid = Convert.ToInt64(row["SubTypeGid"].ToString());
                        _obj.Raiser = Convert.ToInt64(row["ecf_raiser"].ToString());
                        _obj.Batch_Gid = Convert.ToInt64(row["ecf_batch_gid"].ToString());
                        lst.Add(_obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }
    }
}
