﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Web;
using System.IO;
using System.Data;
using DataAccessHandler.Models.FlexiSpend;
using FlexiSpend.Data;
 


namespace FlexiSpend.Service
{
   public class ChequePayment_Service
    {

        ChequePayment_Model ModelObject = new ChequePayment_Model();                                   // ChequePayment model object
        List<ChequePayment_Model> ModelList = new List<ChequePayment_Model>();                       // ChequePayment list model object
        ChequePayment_Data DataObject = new ChequePayment_Data();                                   // ChequePayment data object


        // ChequePayment Read
       public List<ChequePayment_Model> ReadRecord()
       {
           try
           {
               return DataObject.ReadRecord();
           }
           catch (Exception ex)
           {

               throw ex;
           }
       }



       //ChequePayment insert
       public DataTable SaveRecord(ChequePayment_Model ModelObject)
       {
           try
           {
               return DataObject.SaveRecord(ModelObject);
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
       //ChequePayment Update
       public DataTable UpdateRecord(ChequePayment_Model ModelObject)
       {
           try
           {
               return DataObject.UpdateRecord(ModelObject);
           }

           catch (Exception ex)
           {
               throw ex;
           }
       }

    }
}
